/***********************************************************************************************************************
* File Name    : RangeKm_Conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 29/12/2020
***********************************************************************************************************************/

#ifndef RANGE_KM_CONF_H
#define RANGE_KM_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern  const SignalsValue_St_t			Range_SignalsValue_aSt[RANGE_SIG_LEN];

extern  const SignalConfig_St_t			RangeSig1Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t			RangeSig2Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t			RangeSig3Conf_ast[ELEVEN_E];

extern  const SegConfig_St_t			Range_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S1_Off_SegConf_aSt[TWO_SEG_E];

extern  const SegConfig_St_t			Range_S2_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S2_Off_SegConf_aSt[TWO_SEG_E];

extern  const SegConfig_St_t			Range_S3_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			Range_S3_Off_SegConf_aSt[TWO_SEG_E];


#endif /* RANGE_KM_CONF_H */


