/***********************************************************************************************************************
* File Name    : PowerConsum_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 29/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "PowerConsum_Conf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t	   PwrConsum_SignalsValue_aSt[POWER_CONSUMP_SIG_LEN] = 
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E, 	THREE_E, 		PwrConsum_B1_SigConf_ast  },
	{ ONE_E, 	TWO_E, 			PwrConsum_B2_SigConf_ast  },
	{ TWO_E,	TWO_E,			PwrConsum_B3_SigConf_ast  },
	{ THREE_E,	TWO_E,			PwrConsum_B4_SigConf_ast  },
	{ FOUR_E,	TWO_E,			PwrConsum_B5_SigConf_ast  },
	{ FIVE_E,	TWO_E,			PwrConsum_B6_SigConf_ast  },
	{ SIX_E,	TWO_E,			PwrConsum_B7_SigConf_ast  },
	{ SEVEN_E,	TWO_E,			PwrConsum_B8_SigConf_ast  },
	{ EIGHT_E,	TWO_E,			PwrConsum_B9_SigConf_ast  },
	{ NINE_E,	TWO_E,			PwrConsum_B10_SigConf_ast }
};

 const SignalConfig_St_t	       PwrConsum_B1_SigConf_ast[THREE_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ON_E,		ONE_SEG_E,	PwrConsum_B1_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B1_Off_SegConf_aSt },
	{ OFF_E,	ONE_SEG_E,	PwrConsum_B1_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_B2_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B2_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B2_Off_SegConf_aSt }
};


 const SignalConfig_St_t		PwrConsum_B3_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B3_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B3_Off_SegConf_aSt }
};


 const SignalConfig_St_t		PwrConsum_B4_SigConf_ast[TWO_E] = 
{
	{ ON_E, 	ONE_SEG_E,	PwrConsum_B4_On_SegConf_aSt  },
	{ OFF_BAR, 	ONE_SEG_E,	PwrConsum_B4_Off_SegConf_aSt }
};


 const SignalConfig_St_t		PwrConsum_B5_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B5_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B5_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_B6_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B6_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B6_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_B7_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B7_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B7_Off_SegConf_aSt }
};


 const SignalConfig_St_t		PwrConsum_B8_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_B8_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_B8_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_B9_SigConf_ast[TWO_E] = 
{
	{ON_E,		ONE_SEG_E,	PwrConsum_B9_On_SegConf_aSt  },
	{OFF_BAR,	ONE_SEG_E,	PwrConsum_B9_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_B10_SigConf_ast[TWO_E] = 
{
	{ON_E,		ONE_SEG_E,	PwrConsum_B10_On_SegConf_aSt  },
	{OFF_BAR,	ONE_SEG_E,	PwrConsum_B10_Off_SegConf_aSt }
};

 const SegConfig_St_t	PwrConsum_B1_On_SegConf_aSt[ONE_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{ &SEG4,		0x0DU,		0x02U }
};

 const SegConfig_St_t	PwrConsum_B2_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG4,0x0BU,0x04U}
};

 const SegConfig_St_t	PwrConsum_B3_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG4,0x07U,0x08U}
};

 const SegConfig_St_t	PwrConsum_B4_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x07U,0x08U}
};

 const SegConfig_St_t	PwrConsum_B5_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x0BU,0x04U}
};

 const SegConfig_St_t	PwrConsum_B6_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x0DU,0x02U}
};

 const SegConfig_St_t	PwrConsum_B7_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0EU,0x01U}
};

 const SegConfig_St_t	PwrConsum_B8_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0DU,0x02U}
};

 const SegConfig_St_t	PwrConsum_B9_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0BU,0x04U}
};

 const SegConfig_St_t	PwrConsum_B10_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x07U,0x08U}
};

 const SegConfig_St_t	PwrConsum_B1_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG4,0x0DU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B2_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG4,0x0BU,0x00U}
};


 const SegConfig_St_t	PwrConsum_B3_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG4,0x07U,0x00U}
};

 const SegConfig_St_t	PwrConsum_B4_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x07U,0x00U}
};

 const SegConfig_St_t	PwrConsum_B5_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x0BU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B6_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG5,0x0DU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B7_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0EU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B8_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0DU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B9_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x0BU,0x00U}
};

 const SegConfig_St_t	PwrConsum_B10_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,0x07U,0x00U}
};


/********************************************************EOF***********************************************************/