/***********************************************************************************************************************
* File Name    : TelltaleConf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/12/2020
***********************************************************************************************************************/

#ifndef TELLTALE_CONF_H
#define TELLTALE_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
			Reverse mode  Configration 
*/ 

extern	const SignalsValue_St_t			Reverse_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN];
extern	const SignalConfig_St_t			Reverse_Mode_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			Reverse_Mode_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Reverse_Mode_On_SegConf_aSt[ONE_SEG_E];

/*
			Neutral mode  Configration 
*/

extern	const SignalsValue_St_t			Neutral_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN];
extern	const SignalConfig_St_t			Neutral_Mode_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			Neutral_Mode_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Neutral_Mode_On_SegConf_aSt[ONE_SEG_E];

/*
			ECO mode  Configration 
*/

extern	const SignalsValue_St_t			Eco_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN];
extern	const SignalConfig_St_t			Eco_Mode_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			Eco_Mode_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Eco_Mode_On_SegConf_aSt[ONE_SEG_E];

/*
			Sports mode  Configration 
*/

extern	const SignalsValue_St_t			Sports_Mode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN];
extern	const SignalConfig_St_t			Sports_Mode_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			Sports_Mode_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Sports_Mode_On_SegConf_aSt[ONE_SEG_E];

/*
		Battery Indicator Configration
*/

extern	const SignalsValue_St_t			Batt_Ind_SignalsValue_aSt[BATTERY_IND_SIG_LEN];
extern	const SignalConfig_St_t			Batt_Ind_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			Batt_Ind_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Batt_Ind_On_SegConf_aSt[ONE_SEG_E];

/*
		WH/KM Text Configration
*/

extern	const SignalsValue_St_t			WHKM_Text_SignalsValue_aSt[WHKM_TEXT_SIG_LEN];
extern	const SignalConfig_St_t			WHKM_Text_Ind_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			WHKM_Text_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			WHKM_Text_On_SegConf_aSt[ONE_SEG_E];

/*
		Power Indicator configration
*/

extern	const SignalsValue_St_t			Power_Ind_SignalsValue_aSt[POWER_IND_SIG_LEN];
extern	const SignalConfig_St_t			Power_Ind_SigConf_ast[TWO_E];
extern	const SegConfig_St_t			PWR_Ind_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			PWR_Ind_On_SegConf_aSt[ONE_SEG_E];

/*
		Logo Telltale configration
*/

extern	const SignalsValue_St_t			Logo_SignalsValue_aSt[LOGO_SIG_LEN];
extern	const SignalConfig_St_t			LogoSigConf_ast[TWO_E];
extern	const SegConfig_St_t			Logo_Off_SegConf_aSt[ONE_SEG_E];
extern	const SegConfig_St_t			Logo_On_SegConf_aSt[ONE_SEG_E];


/*
   Left-turn indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			LeftInd_SignalsValue_aSt[LEFT_IND_SIG_LEN];
extern  const SignalConfig_St_t			LeftIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			LeftInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			LeftInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   Right-turn indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			RightInd_SignalsValue_aSt[RIGHT_IND_SIG_LEN];
extern  const SignalConfig_St_t			RightIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			RightInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			RightInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   Low-Beam indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			LowBeamInd_SignalsValue_aSt[LOW_BEAM_SIG_LEN];
extern  const SignalConfig_St_t			LowBeamIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			LowBeamInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			LowBeamInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   High-Beam indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			HighBeamInd_SignalsValue_aSt[HIGH_BEAM_SIG_LEN];
extern  const SignalConfig_St_t			HighBeamIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			HighBeamInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			HighBeamInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   BLE indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			BLE_Ind_SignalsValue_aSt[BLE_SIG_LEN];
extern  const SignalConfig_St_t			BLE_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			BLE_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			BLE_Ind_Off_SegConf_aSt[ONE_SEG_E];


/*
   RegenBraking  telltale Configuration 
*/
extern  const SignalsValue_St_t			RegenBrake_SignalsValue_aSt[REGEN_BRAKING_SIG_LEN];
extern  const SignalConfig_St_t			RegenBrake_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			RegenBrake_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			RegenBrake_Ind_Off_SegConf_aSt[ONE_SEG_E];


/*
   Warning indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			Warning_SignalsValue_aSt[WARN_IND_SIG_LEN];
extern  const SignalConfig_St_t			Warning_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			Warning_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Warning_Ind_Off_SegConf_aSt[ONE_SEG_E];


/*
   Service Reminder  telltale Configuration 
*/
extern  const SignalsValue_St_t			ServRem_SignalsValue_aSt[SERV_REM_SIG_LEN];
extern  const SignalConfig_St_t			ServRem_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			ServRem_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			ServRem_Off_SegConf_aSt[ONE_SEG_E];

/*
   Neutral Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			NeutralMode_SignalsValue_aSt[NEUTRAL_MODE_SIG_LEN];
extern  const SignalConfig_St_t			NeutralMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			NeutralMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			NeutralMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Economy Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			EcoMode_SignalsValue_aSt[ECO_MODE_SIG_LEN];
extern  const SignalConfig_St_t			EcoMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			EcoMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			EcoMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Sports Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			SportsMode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN];
extern  const SignalConfig_St_t			SportsMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			SportsMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SportsMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Reverse Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			ReverseMode_SignalsValue_aSt[REVERSE_MODE_SIG_LEN];
extern  const SignalConfig_St_t			ReverseMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			ReverseMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			ReverseMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Side-Stand indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			SideStand_SignalsValue_aSt[SIDE_STAND_SIG_LEN];
extern  const SignalConfig_St_t			SideStand_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			SideStand_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SideStand_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Safe-Mode indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			SafeMode_SignalsValue_aSt[SAFE_MODE_SIG_LEN];
extern  const SignalConfig_St_t			SafeMode_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			SafeMode_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SafeMode_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Battery-Fault indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			BattFlt_SignalsValue_aSt[BATT_FLT_SIG_LEN];
extern  const SignalConfig_St_t			BattFlt_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			BattFlt_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			BattFlt_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Motor-Fault indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			MotorFlt_SignalsValue_aSt[MOTOR_FLT_SIG_LEN];
extern  const SignalConfig_St_t			MotorFlt_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			MotorFlt_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			MotorFlt_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Kill-Switch telltale Configuration 
*/
extern  const SignalsValue_St_t			KillSwitch_SignalsValue_aSt[KILL_SWITCH_SIG_LEN];
extern  const SignalConfig_St_t			KillSwitch_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			KillSwitch_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			KillSwitch_Off_SegConf_aSt[ONE_SEG_E];

/*
   Common constant segments Configuration 
*/
extern const  SignalsValue_St_t			Common_SignalsValue_aSt[COMMON_SEG_SIG_LEN];
extern const  SignalConfig_St_t			Common_SigConf_ast[TWO_E];
extern const  SegConfig_St_t			Common_On_SegConf_aSt[ONE_SEG_E];
extern const  SegConfig_St_t			Common_Off_SegConf_aSt[ONE_SEG_E];

#endif /* TELLTALE_CONF_H */


