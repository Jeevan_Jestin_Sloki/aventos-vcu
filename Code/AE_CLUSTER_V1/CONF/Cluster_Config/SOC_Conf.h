/***********************************************************************************************************************
* File Name    : SOC_Conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 29/12/2020
***********************************************************************************************************************/

#ifndef SOC_CONF_H
#define SOC_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern  const SignalsValue_St_t			SOC_SignalsValue_aSt[BATT_SOC_SIG_LEN];
 
extern  const SignalConfig_St_t			SOC_B1_SigConf_ast[THREE_E];
extern  const SignalConfig_St_t			SOC_B2_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B3_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B4_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B5_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B6_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B7_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B8_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B9_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			SOC_B10_SigConf_ast[TWO_E];
 
extern  const SegConfig_St_t			SOC_B1_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B2_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B3_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B4_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B5_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B6_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B7_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B8_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B9_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B10_On_SegConf_aSt[ONE_SEG_E];
 
extern  const SegConfig_St_t			SOC_B1_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B2_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B3_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B4_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B5_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B6_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B7_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B8_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B9_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B10_Off_SegConf_aSt[ONE_SEG_E];

#endif /* SOC_CONF_H */


