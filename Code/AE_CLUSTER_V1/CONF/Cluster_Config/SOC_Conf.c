/***********************************************************************************************************************
* File Name    : SOC_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 29/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SOC_Conf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t		SOC_SignalsValue_aSt[BATT_SOC_SIG_LEN] = 
{
	{ ZERO_E,	THREE_E,	SOC_B1_SigConf_ast },
	{ ONE_E,	TWO_E,		SOC_B2_SigConf_ast },
	{ TWO_E,	TWO_E,		SOC_B3_SigConf_ast },
	{ THREE_E,	TWO_E,		SOC_B4_SigConf_ast },
	{ FOUR_E,	TWO_E,		SOC_B5_SigConf_ast },
	{ FIVE_E,	TWO_E,		SOC_B6_SigConf_ast },
	{ SIX_E,	TWO_E,		SOC_B7_SigConf_ast },
	{ SEVEN_E,	TWO_E,		SOC_B8_SigConf_ast },
	{ EIGHT_E,	TWO_E,		SOC_B9_SigConf_ast },
	{ NINE_E,	TWO_E,		SOC_B10_SigConf_ast},
};

 const SignalConfig_St_t		SOC_B1_SigConf_ast[THREE_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B1_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B1_Off_SegConf_aSt},
	{ OFF_E,	ONE_SEG_E,	SOC_B1_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B2_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B2_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B2_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B3_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B3_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B3_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B4_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B4_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B4_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B5_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B5_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B5_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B6_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B6_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B6_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B7_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B7_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B7_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B8_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B8_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B8_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B9_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B9_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B9_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B10_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B10_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B10_Off_SegConf_aSt}
};

 const SegConfig_St_t	SOC_B1_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG40,0x0BU,0x04U}
};

 const SegConfig_St_t	SOC_B2_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG40,0x07U,0x08U}
};

 const SegConfig_St_t	SOC_B3_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x07U,0x08U}
};

 const SegConfig_St_t	SOC_B4_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x0BU,0x04U}
};

 const SegConfig_St_t	SOC_B5_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x0DU,0x02U}
};

 const SegConfig_St_t	SOC_B6_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x0DU,0x02U}
};

 const SegConfig_St_t	SOC_B7_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x0BU,0x04U}
};

 const SegConfig_St_t	SOC_B8_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x07U,0x08U}
};

 const SegConfig_St_t	SOC_B9_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,0x0EU,0x01U}
};

 const SegConfig_St_t	SOC_B10_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG34,0x0EU,0x01U}
};

 const SegConfig_St_t	SOC_B1_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG40,0x0BU,0x00U}
};

 const SegConfig_St_t	SOC_B2_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG40,0x07U,0x00U}
};


 const SegConfig_St_t	SOC_B3_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x07U,0x00U}
};

 const SegConfig_St_t	SOC_B4_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x0BU,0x00U}
};

 const SegConfig_St_t	SOC_B5_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG39,0x0DU,0x00U}
};

 const SegConfig_St_t	SOC_B6_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x0DU,0x00U}
};

 const SegConfig_St_t	SOC_B7_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x0BU,0x00U}
};

 const SegConfig_St_t	SOC_B8_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG38,0x07U,0x00U}
};

 const SegConfig_St_t	SOC_B9_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,0x0EU,0x00U}
};

 const SegConfig_St_t	SOC_B10_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG34,0x0EU,0x00U}
};


/********************************************************EOF***********************************************************/