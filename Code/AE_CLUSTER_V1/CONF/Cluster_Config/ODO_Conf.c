/***********************************************************************************************************************
* File Name    : ODO_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 28/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODO_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t			ODO_SignalsValue_aSt[ODO_SIG_LEN] =
{
	{
		ZERO_E, 
		ELEVEN_E,
		ODOSig1Conf_ast,
	},
	{
		ONE_E, 
		ELEVEN_E,
		ODOSig2Conf_ast,
	},
	{
		TWO_E, 
		ELEVEN_E,
		ODOSig3Conf_ast,
	},
	{
		THREE_E, 
		ELEVEN_E,
		ODOSig4Conf_ast,
	},
	{
		FOUR_E, 
		ELEVEN_E,
		ODOSig5Conf_ast,
	},
	{
		FIVE_E, 
		ELEVEN_E,
		ODOSig6Conf_ast,
	},
	
};


 const SignalConfig_St_t			ODOSig1Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S1_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S1_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S1_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S1_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S1_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S1_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S1_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S1_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S1_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S1_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S1_off_SegConf_aSt,
	},
};


 const SignalConfig_St_t			ODOSig2Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S2_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S2_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S2_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S2_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S2_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S2_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S2_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S2_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S2_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S2_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S2_off_SegConf_aSt,
	},
};


 const SignalConfig_St_t			ODOSig3Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S3_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S3_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S3_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S3_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S3_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S3_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S3_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S3_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S3_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S3_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S3_off_SegConf_aSt,
	},
};


 const SignalConfig_St_t			ODOSig4Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S4_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S4_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S4_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S4_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S4_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S4_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S4_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S4_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S4_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S4_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S4_off_SegConf_aSt,
	},
};


 const SignalConfig_St_t			ODOSig5Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S5_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S5_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S5_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S5_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S5_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S5_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S5_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S5_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S5_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S5_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S5_off_SegConf_aSt,
	},
};


 const SignalConfig_St_t			ODOSig6Conf_ast[ELEVEN_E] = 
{
	{
		ZERO_E,
		TWO_SEG_E,
		ODO_S6_0_SegConf_aSt,
	},
	{
		ONE_E,
		TWO_SEG_E,
		ODO_S6_1_SegConf_aSt,
	},
	{
		TWO_E,
		TWO_SEG_E,
		ODO_S6_2_SegConf_aSt,
	},
	{
		THREE_E,
		TWO_SEG_E,
		ODO_S6_3_SegConf_aSt,
	},
	{
		FOUR_E,
		TWO_SEG_E,
		ODO_S6_4_SegConf_aSt,
	},
	{
		FIVE_E,
		TWO_SEG_E,
		ODO_S6_5_SegConf_aSt,
	},
	{
		SIX_E,
		TWO_SEG_E,
		ODO_S6_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		TWO_SEG_E,
		ODO_S6_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		TWO_SEG_E,
		ODO_S6_8_SegConf_aSt,
	},
	{
		NINE_E,
		TWO_SEG_E,
		ODO_S6_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		TWO_SEG_E,
		ODO_S6_off_SegConf_aSt,
	},
};


 const SegConfig_St_t			ODO_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0BU,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x00U,
	},
	{
		&SEG26,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x07U,
	},
	{
		&SEG26,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x05U,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0CU,
	},
	{
		&SEG26,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0DU,
	},
	{
		&SEG26,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0FU,
	},
	{
		&SEG26,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x00U,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0FU,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0DU,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S1_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG27,
		0x00U,
		0x0BU,
	},
	{
		&SEG26,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0BU,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x00U,
	},
	{
		&SEG28,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S2_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x07U,
	},
	{
		&SEG28,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S2_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x05U,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0CU,
	},
	{
		&SEG28,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S2_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0DU,
	},
	{
		&SEG28,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S2_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0FU,
	},
	{
		&SEG28,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S2_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x00U,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0FU,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0DU,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S2_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG29,
		0x00U,
		0x0BU,
	},
	{
		&SEG28,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0BU,
	},
	{
		&SEG30,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x00U,
	},
	{
		&SEG30,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S3_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x07U,
	},
	{
		&SEG30,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S3_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x05U,
	},
	{
		&SEG30,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0CU,
	},
	{
		&SEG30,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S3_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0DU,
	},
	{
		&SEG30,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S3_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0FU,
	},
	{
		&SEG30,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S3_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x00U,
	},
	{
		&SEG30,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0FU,
	},
	{
		&SEG30,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x0DU,
	},
	{
		&SEG30,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S3_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG31,
		0x00U,
		0x00U,
	},
	{
		&SEG30,
		0x01U,
		0x00U,
	},
};

 const SegConfig_St_t			ODO_S4_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0BU,
	},
	{
		&SEG32,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S4_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x00U,
	},
	{
		&SEG32,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S4_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x07U,
	},
	{
		&SEG32,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S4_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x05U,
	},
	{
		&SEG32,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S4_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0CU,
	},
	{
		&SEG32,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S4_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0DU,
	},
	{
		&SEG32,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S4_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0FU,
	},
	{
		&SEG32,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S4_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x00U,
	},
	{
		&SEG32,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S4_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0FU,
	},
	{
		&SEG32,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S4_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x0DU,
	},
	{
		&SEG32,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S4_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG33,
		0x00U,
		0x00U,
	},
	{
		&SEG32,
		0x01U,
		0x00U,
	},
};

 const SegConfig_St_t			ODO_S5_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0BU,
	},
	{
		&SEG34,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S5_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x00U,
	},
	{
		&SEG34,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S5_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x07U,
	},
	{
		&SEG34,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S5_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x05U,
	},
	{
		&SEG34,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S5_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0CU,
	},
	{
		&SEG34,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S5_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0DU,
	},
	{
		&SEG34,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S5_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0FU,
	},
	{
		&SEG34,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S5_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x00U,
	},
	{
		&SEG34,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S5_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0FU,
	},
	{
		&SEG34,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S5_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x0DU,
	},
	{
		&SEG34,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S5_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG35,
		0x00U,
		0x00U,
	},
	{
		&SEG34,
		0x01U,
		0x00U,
	},
};


 const SegConfig_St_t			ODO_S6_0_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0BU,
	},
	{
		&SEG36,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S6_1_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x00U,
	},
	{
		&SEG36,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S6_2_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x07U,
	},
	{
		&SEG36,
		0x01U,
		0x0CU,
	},
};

 const SegConfig_St_t			ODO_S6_3_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x05U,
	},
	{
		&SEG36,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S6_4_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0CU,
	},
	{
		&SEG36,
		0x01U,
		0x06U,
	},
};

 const SegConfig_St_t			ODO_S6_5_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0DU,
	},
	{
		&SEG36,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S6_6_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0FU,
	},
	{
		&SEG36,
		0x01U,
		0x0AU,
	},
};

 const SegConfig_St_t			ODO_S6_7_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x00U,
	},
	{
		&SEG36,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S6_8_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0FU,
	},
	{
		&SEG36,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S6_9_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x0DU,
	},
	{
		&SEG36,
		0x01U,
		0x0EU,
	},
};

 const SegConfig_St_t			ODO_S6_off_SegConf_aSt[TWO_SEG_E] = 
{
	{
		&SEG37,
		0x00U,
		0x00U,
	},
	{
		&SEG36,
		0x01U,
		0x00U,
	},
};

/********************************************************EOF***********************************************************/