/***********************************************************************************************************************
* File Name    : version.c
* Version      : 01
* Description  : This file contains the functions and data related to Software and Hardware Version Store and
				 Re-Store from the Data Flash.
* Created By   : Dileepa B S 
* Creation Date: 05/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "version.h"
#include "pfdl_user.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint8_t SWVersionNum_au8[ASW_SW_VERSION_LEN] 	= ASW_SW_VERSION;
uint8_t DFB1_Bytes_au8[DFB1_TOTAL_BYTES] 		= {0x00U}; 

/***********************************************************************************************************************
* Function Name: StoreSWVersion
* Description  : This function stores the Software Version Number to the Data Flash.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void StoreSWVersion(void)
{
	uint16_t BytesCount_u16 		= 0;
	uint16_t EqualBytesCounter_u16 	= 0;
	bool 	 StoreSWVersion_b 		= false;
	
	FDL_Read(FLASH_BLOCK_1, FLASH_BLOCK1_START_POS, DFB1_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB1_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB1_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	for(BytesCount_u16 = 0; BytesCount_u16 < ASW_SW_VERSION_LEN; BytesCount_u16++)
	{
		if(DFB1_Bytes_au8[BytesCount_u16] == SWVersionNum_au8[BytesCount_u16])
		{
			EqualBytesCounter_u16++;
		}
		DFB1_Bytes_au8[BytesCount_u16] = SWVersionNum_au8[BytesCount_u16];
	}
	
	if(EqualBytesCounter_u16 == ASW_SW_VERSION_LEN)
	{
		StoreSWVersion_b = false; 
	}
	else
	{
		StoreSWVersion_b = true;
	}
	
	if(true == StoreSWVersion_b)
	{
		//Store the Updated Software Version Number
		for(BytesCount_u16 = 0; BytesCount_u16 < ASW_SW_VERSION_LEN; BytesCount_u16++)
		{
			dubWriteBuffer[BytesCount_u16] = DFB1_Bytes_au8[BytesCount_u16];
		}
		
		FDL_Erase(FLASH_BLOCK_1, 1U); //Erase Flash Block - 1
	
		FDL_Write(FLASH_BLOCK_1, FLASH_BLOCK1_START_POS, DFB1_TOTAL_BYTES);
												//Write SW Version Bytes to the Flash.
	}
	else
	{
		; //Software Version Number is Not Updated
	}
	return;
}


/***********************************************************************************************************************
* Function Name: RestoreSWVersion
* Description  : This function Restores the Software Version Number from the Data Flash.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void RestoreSWVersion(void)
{
	NOP(); /*todo:dileepabs*/ //Implement Restore Software Version Number from the Flash API.
	return;
}


/********************************************************EOF***********************************************************/