/***********************************************************************************************************************
* File Name    : DataBank.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/09/2021
***********************************************************************************************************************/

#ifndef DATA_BANK_H
#define DATA_BANK_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define RESET_DATA_BANK								FALSE	//todo:configurable	/* TRUE OR FALSE*/


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	uint32_t 	ODOmeterSig_u32;
	uint16_t	PowerConsumBarsSig_u16;
	uint16_t 	RangeKmSig_u16;
	uint16_t 	HoursTimeSig_u16;
	uint16_t 	MinutesTimeSig_u16;
	uint8_t 	VehicleSpeedSig_u8;
	uint8_t 	SOCSig_u8;
	uint8_t 	MileageSig_u8;
	uint8_t 	SideStandSig_u8;
	uint8_t 	SafeModeSig_u8;
	uint8_t 	BatteryTextSig_u8;
	uint8_t 	MotorTextSig_u8;
	uint8_t 	KillSwitchSig_u8;
	uint8_t 	LeftIndicatorSig_u8;
	uint8_t 	RightIndicatorSig_u8;
	uint8_t 	LowBeamSig_u8;
	uint8_t 	HighBeamSig_u8;
	uint8_t 	ServiceReminderSig_u8;
	uint8_t 	WarningSig_u8;
	uint8_t 	RegenSig_u8;
	uint8_t 	BLE_Icon_Sig_u8;
	uint8_t 	EcoModeSig_u8;
	uint8_t 	SportsModeSig_u8;
	uint8_t 	ReverseModeSig_u8;
	uint8_t 	NeutralModeSig_u8;
	uint8_t 	IgnitionSig_u8;
	uint8_t 	ChargingStatus_u8;
	
}ClusterSignals_St_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern ClusterSignals_St_t	ClusterSignals_St;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
#if(RESET_DATA_BANK == TRUE)
	extern void ResetDataBankSignals(void);
#endif


#endif /* DATA_BANK_H */


