/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/09/2021
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include "UserButtuonIn.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TELLTALE_SOLID_ON					0x03U
#define TELLTALE_BLINK						0x01U
#define TELLTALE_OFF						0x00U

#define IGNITION_ON							0x01U
#define IGNITION_OFF						0x00U

#define GPIO_ACTIVEHIGH						0x00U
#define GPIO_ACTIVELOW						0x01U


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	USER_INPUT_SIG_START_E,
	INPUT_1_E = USER_INPUT_SIG_START_E,
	INPUT_2_E,
	INPUT_3_E,
	#if(READ_UIB_INPUT == FALSE)
	INPUT_4_E,
	INPUT_5_E,
	#endif
	INPUT_6_E,
	INPUT_7_E,
	INPUT_8_E,
	INPUT_9_E,
	INPUT_10_E,
	INPUT_11_E,
	#if(HMI_CAN_ONLY == FALSE)
	INPUT_12_E,
	#endif
	TOTAL_USER_INPUT_SIG_E,
	USER_INPUT_SIG_END_E = TOTAL_USER_INPUT_SIG_E,
}UserInputSig_En_t;


typedef enum
{
	ACTIVE_NONE_E,
	ACTIVE_HIGH_E,
	ACTIVE_LOW_E,
}InputActiveState_En_t;


typedef struct
{
	UserInputSig_En_t		UserInputSig_En;
	InputActiveState_En_t	InputActiveState_En;
	uint8_t 				UserInputVal_u8;
	uint8_t 				SigTurnOnVal_u8;
	uint8_t 				SigTurnOffVal_u8;
	uint8_t 				SignalState_u8;
}UserInputSigConf_St_t;


typedef enum
{
	IGNITION_NONE_E,
	IGNITION_CAN_E,
	IGNITION_GPIO_E,
}IgnitionInput_En_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadClusterInput(void);
extern void UpdateSigToDataBank(void);
extern void ResetTheUserInput(void);

#endif /* DATA_AQUIRE_H
*/