/***********************************************************************************************************************
* File Name    : DataAquire.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/9/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "DataBank.h"
#include "hmi_can_data.h"
#include "Time.h"
#include "Communicator.h"
#include "hmi_config_can.h"
#include "Cal_PowerConsum.h"
#include "board_conf.h"



/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E] = 
{
	{INPUT_1_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{INPUT_2_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{INPUT_3_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	#if(READ_UIB_INPUT == FALSE)
	{INPUT_4_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{INPUT_5_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	#endif
	{INPUT_6_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_BLINK, 	 TELLTALE_OFF, 0x00U },
	{INPUT_7_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_BLINK, 	 TELLTALE_OFF, 0x00U },
	{INPUT_8_E,	 	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{INPUT_9_E,	 	ACTIVE_HIGH_E, 0x00U, IGNITION_ON, 	  	 IGNITION_OFF, 0x00U },
	{INPUT_10_E,	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{INPUT_11_E,	ACTIVE_HIGH_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	#if(HMI_CAN_ONLY == FALSE)
	{INPUT_12_E,	ACTIVE_HIGH_E, 0x00U, TELLTALE_BLINK, 	 TELLTALE_OFF, 0x00U }
	#endif
};

IgnitionInput_En_t	IgnitionInput_En = IGNITION_NONE_E; //todo:configurable

/***********************************************************************************************************************
* Function Name: ReadClusterInput
* Description  : This function reads the user inputs.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadClusterInput(void)
{
	#if(HMI_CAN_WITH_IO == TRUE)
	uint16_t InSigCount_u16 = 0;
	
	UserInputSigConf_St[INPUT_1_E].UserInputVal_u8 	= P2_bit.no5; 		/*ANI5*/
	UserInputSigConf_St[INPUT_2_E].UserInputVal_u8 	= P2_bit.no1; 		/*ANI1*/
	UserInputSigConf_St[INPUT_3_E].UserInputVal_u8 	= P12_bit.no3; 		/*P123*/
	#if(READ_UIB_INPUT == FALSE)
	UserInputSigConf_St[INPUT_4_E].UserInputVal_u8 	= P2_bit.no0; 		/*ANI0*/			/*MENU*/
	UserInputSigConf_St[INPUT_5_E].UserInputVal_u8 	= P13_bit.no7; 		/*INTP5/P137*/		/*SET*/
	#endif
	UserInputSigConf_St[INPUT_6_E].UserInputVal_u8 	= P2_bit.no2; 		/*ANI2*/			/*Right Indicator*/		
	UserInputSigConf_St[INPUT_7_E].UserInputVal_u8 	= P2_bit.no3; 		/*ANI3*/			/*Left-Indicator*/	
	UserInputSigConf_St[INPUT_8_E].UserInputVal_u8 	= P2_bit.no4; 		/*ANI4*/			/*High-Beam*/	
	UserInputSigConf_St[INPUT_9_E].UserInputVal_u8 	= P2_bit.no6; 		/*ANI6*/			/*Ignitiion*/	
	UserInputSigConf_St[INPUT_10_E].UserInputVal_u8 = P2_bit.no7; 		/*ANI7*/			/*Low Beam*/	
	UserInputSigConf_St[INPUT_11_E].UserInputVal_u8 = P12_bit.no4;		/*P124*/			/*Kill Switch*/	
	#if(HMI_CAN_ONLY == FALSE)
	UserInputSigConf_St[INPUT_12_E].UserInputVal_u8 = P9_bit.no4; 		/*PWM-IN/P94*/ 		/*Side Stand*/
	#endif
	
	for(InSigCount_u16 = 0; InSigCount_u16 < TOTAL_USER_INPUT_SIG_E; InSigCount_u16++)
	{
		if(true == UserInputSigConf_St[InSigCount_u16].UserInputVal_u8)
		{
			if(UserInputSigConf_St[InSigCount_u16].InputActiveState_En == ACTIVE_HIGH_E)
			{
				UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOnVal_u8;
			}
			else if(UserInputSigConf_St[InSigCount_u16].InputActiveState_En == ACTIVE_LOW_E)
			{
				UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOffVal_u8;
			}
			else
			{
				;	
			}
		}
		else if(false == UserInputSigConf_St[InSigCount_u16].UserInputVal_u8)
		{
			if(UserInputSigConf_St[InSigCount_u16].InputActiveState_En == ACTIVE_HIGH_E)
			{
				UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOffVal_u8;
			}
			else if(UserInputSigConf_St[InSigCount_u16].InputActiveState_En == ACTIVE_LOW_E)
			{
				UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOnVal_u8;
			}
			else
			{
				;	
			}
		}
		else
		{
			;	
		}
	}
	#endif

	if(IgnitionInput_En == IGNITION_GPIO_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = UserInputSigConf_St[INPUT_9_E].SignalState_u8;
	}
	else if(IgnitionInput_En == IGNITION_CAN_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = HMI_Rx_Eve_1.Ignition_u8;
	}
	else if(IgnitionInput_En == IGNITION_NONE_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = 0x01U; //Set Ignition to ON
	}
	else
	{
		;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: UpdateSigToDataBank
* Description  : This function updates the user input signals and signals received over CAN to the Data Bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateSigToDataBank(void)
{
	ClusterSignals_St.ODOmeterSig_u32 		= HMI_Rx_Per_1.ODO_u32;
	
	ClusterSignals_St.PowerConsumBarsSig_u16 = Get_PowerConsum_Bars();	
	
	ClusterSignals_St.RangeKmSig_u16 		= HMI_Rx_Per_2.Range_Km_u16;
	
	ClusterSignals_St.VehicleSpeedSig_u8 	= HMI_Rx_Per_1.Vehicle_Speed_u8;
	
	ClusterSignals_St.SOCSig_u8 			= HMI_Rx_Per_2.Battery_SOC_u8;
	
	ClusterSignals_St.MileageSig_u8 		= HMI_Rx_Per_1.Mileage_u8;
	
	ClusterSignals_St.SafeModeSig_u8 		= HMI_Rx_Eve_1.Safe_Mode_u8;
	
	ClusterSignals_St.BatteryTextSig_u8 	= HMI_Rx_Eve_1.Battery_Fault_u8;
	
	ClusterSignals_St.MotorTextSig_u8 		= HMI_Rx_Eve_1.Motor_Fault_u8;
	
	ClusterSignals_St.ServiceReminderSig_u8 = HMI_Rx_Eve_1.Service_Indicator_u8;
	
	ClusterSignals_St.WarningSig_u8 		= HMI_Rx_Eve_1.Warning_Symbol_u8;
	
	ClusterSignals_St.RegenSig_u8 			= HMI_Rx_Per_2.Regen_Braking_u8;
	
	ClusterSignals_St.BLE_Icon_Sig_u8 		= HMI_Rx_Per_2.BLE_Icon_u8;
	
	ClusterSignals_St.EcoModeSig_u8 		= HMI_Rx_Eve_1.Eco_Mode_u8;
	
	ClusterSignals_St.SportsModeSig_u8 		= HMI_Rx_Eve_1.Sports_Mode_u8;
	
	ClusterSignals_St.ReverseModeSig_u8 	= HMI_Rx_Eve_1.Reverse_Mode_u8;
	
	ClusterSignals_St.NeutralModeSig_u8 	= HMI_Rx_Eve_1.Neutral_Mode_u8;
	
	ClusterSignals_St.ChargingStatus_u8 	= HMI_Rx_Eve_1.Battery_Status_u8;              
	
	
	SET_PRESENT_ODOMETER(ClusterSignals_St.ODOmeterSig_u32);
	SET_PRESENT_WHKM((uint16_t)ClusterSignals_St.MileageSig_u8);
	SET_PRESENT_SOC(ClusterSignals_St.SOCSig_u8);
	
	
	/*Update Present time to the data-bank*/
	if(TimeUpdateChnnl_En == TIME_UPDATE_TIMER_E)
	{
		ClusterSignals_St.HoursTimeSig_u16 		= HoursTime_u16;
		ClusterSignals_St.MinutesTimeSig_u16 	= MinutesTime_u16;
	}
	else if(TimeUpdateChnnl_En == TIME_UPDATE_CAN_E)
	{
		/*update the time received over CAN*/
		ClusterSignals_St.HoursTimeSig_u16 		= (uint16_t)HMI_Rx_Eve_1.Hours_Time_u8;
	    ClusterSignals_St.MinutesTimeSig_u16	= (uint16_t)HMI_Rx_Eve_1.Minutes_Time_u8;
		/*Note : If the Time Signals are not available over CAN, assign zero here*/
	}
	else
	{
		;
	}
	
	
	/*Input Channel to the Indicators is CAN*/
	ClusterSignals_St.KillSwitchSig_u8 		= HMI_Rx_Eve_1.Kill_Switch_u8;
	ClusterSignals_St.LeftIndicatorSig_u8 	= HMI_Rx_Eve_1.Left_Indicator_u8;
	ClusterSignals_St.RightIndicatorSig_u8 	= HMI_Rx_Eve_1.Right_Indicator_u8;
	ClusterSignals_St.HighBeamSig_u8 		= HMI_Rx_Eve_1.High_Beam_u8;
	ClusterSignals_St.LowBeamSig_u8 		= HMI_Rx_Eve_1.Low_Beam_u8;
	ClusterSignals_St.SideStandSig_u8 		= HMI_Rx_Eve_1.Side_Stand_u8;
	
	/*Input Channel to the Indicators is GPIO*/
	// ClusterSignals_St.KillSwitchSig_u8 		= UserInputSigConf_St[INPUT_11_E].SignalState_u8;
	// ClusterSignals_St.LeftIndicatorSig_u8 	= UserInputSigConf_St[INPUT_6_E].SignalState_u8;
	// ClusterSignals_St.RightIndicatorSig_u8 	= UserInputSigConf_St[INPUT_7_E].SignalState_u8;
	// ClusterSignals_St.HighBeamSig_u8 		= UserInputSigConf_St[INPUT_8_E].SignalState_u8;
	// ClusterSignals_St.LowBeamSig_u8 		= UserInputSigConf_St[INPUT_10_E].SignalState_u8;
	// ClusterSignals_St.SideStandSig_u8 		= UserInputSigConf_St[INPUT_12_E].SignalState_u8;
	
	SET_PRESENT_BRIGHTNESS((uint16_t)HMI_CONF_MSG_1.Brightness_u8);
	
	return;
}


/***********************************************************************************************************************
* Function Name: ResetTheUserInput
* Description  : This function resets the user input signals state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheUserInput(void)
{
	uint16_t InputCount_u16 = 0;
	
	for(InputCount_u16 = 0; InputCount_u16 < TOTAL_USER_INPUT_SIG_E; InputCount_u16++)
	{
		UserInputSigConf_St[InputCount_u16].SignalState_u8 = 0x00U;
	}
	
	return;
}


/********************************************************EOF***********************************************************/