/***********************************************************************************************************************
* File Name    : r_cg_cgc.h
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for CGC module.
* Creation Date: 03/12/2020
***********************************************************************************************************************/

#ifndef CGC_H
#define CGC_H

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/


/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void R_CGC_Create(void);
void R_CGC_Get_ResetSource(void);

#endif /* CGC_H */
