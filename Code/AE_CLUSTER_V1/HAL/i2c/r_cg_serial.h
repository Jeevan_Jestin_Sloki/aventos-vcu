/***********************************************************************************************************************
* File Name    : r_cg_serial.h
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for Serial module.
* Creation Date: 28/04/2021
***********************************************************************************************************************/

#ifndef SERIAL_H
#define SERIAL_H

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define IIC11_WAITTIME                           (160U)

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void R_SAU1_Create(void);
void R_IIC11_Create(void);
void R_IIC11_Master_Send(uint8_t adr, uint8_t * const tx_buf, uint16_t tx_num);
void R_IIC11_Master_Receive(uint8_t adr, uint8_t * const rx_buf, uint16_t rx_num);
void R_IIC11_Stop(void);
void R_IIC11_StartCondition(void);
void R_IIC11_StopCondition(void);

void  r_iic11_interrupt(void);
static void r_iic11_callback_master_error(MD_STATUS flag);
static void r_iic11_callback_master_receiveend(void);
static void r_iic11_callback_master_sendend(void);

/* Start user code for function. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
#endif
