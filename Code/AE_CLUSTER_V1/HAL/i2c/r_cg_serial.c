/***********************************************************************************************************************
* File Name    : r_cg_serial.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for Serial module.
* Creation Date: 28/04/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "r_cg_serial.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
volatile uint8_t    g_iic11_master_status_flag; /* iic11 start flag for send address check by master mode */
volatile uint8_t  * gp_iic11_tx_address;        /* iic11 send data pointer by master mode */
volatile uint16_t   g_iic11_tx_count;           /* iic11 send data size by master mode */
volatile uint8_t  * gp_iic11_rx_address;        /* iic11 receive data pointer by master mode */
volatile uint16_t   g_iic11_rx_count;           /* iic11 receive data size by master mode */
volatile uint16_t   g_iic11_rx_length;          /* iic11 receive data length by master mode */


/***********************************************************************************************************************
* Function Name: R_SAU1_Create
* Description  : This function initializes SAU1.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_SAU1_Create(void)
{
    SAU1EN = 1U;    /* supply SAU1 clock */
    NOP();
    NOP();
    NOP();
    NOP();
    SPS1 = 0x0001U;
    
    R_IIC11_Create();
}

/***********************************************************************************************************************
* Function Name: R_IIC11_Create
* Description  : This function initializes IIC11.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_Create(void)
{
    ST1 |= 0x0002U;  /* disable IIC11 */
    
    IICMK11 = 1U;    /* disable INTIIC11 interrupt */
    
    IICIF11 = 0U;    /* clear INTIIC11 interrupt flag */
    
    /* Set INTIIC11 level2 priority */
    IICPR111 = 1U;
    IICPR011 = 0U;
    
    SIR11 = 0x0003U;    /* error flag clear */
    
    SMR11 = 0x0024U; 
    
    SCR11 = 0x0017U;
    
    SDR11 = 0x9E00U; 
    
    SO1 |= 0x0202U; 
    
    /* use P60, P61 as SCL11, SDA11 */
    STSEL1 |= 0x00U;
    
    /* Set SCL11, SDA11 pin */
    P6 |= 0x03U;
    PM6 &= 0xFCU;
}

/***********************************************************************************************************************
* Function Name: R_IIC11_Master_Send
* Description  : This function starts transferring data for IIC11 in master mode.
* Arguments    : adr -
*                    set address for select slave
*                tx_buf -
*                    transfer buffer pointer
*                tx_num -
*                    buffer size
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_Master_Send(uint8_t adr, uint8_t * const tx_buf, uint16_t tx_num)
{    
    g_iic11_master_status_flag = 0x00U;    /* clear IIC11 master status flag */
    
    adr &= 0xFEU;    /* send mode */
    
    g_iic11_master_status_flag = 0x01U;    /* set master status flag */
    
    SCR11 &= ~0xC000U; 
    
    SCR11 |= 0x8000U; 
    
    /* Set paramater */
    g_iic11_tx_count = tx_num;
    gp_iic11_tx_address = tx_buf; 

    /* Start condition */
    R_IIC11_StartCondition();
    IICIF11 = 0U;    /* clear INTIIC11 interrupt flag */
    IICMK11 = 0U;    /* enable INTIIC11 */
    SDR11L = adr;
}

/***********************************************************************************************************************
* Function Name: R_IIC11_Master_Receive
* Description  : This function starts receiving data for IIC11 in master mode.
* Arguments    : adr -
*                    set address for select slave
*                rx_buf -
*                    receive buffer pointer
*                rx_num -
*                    buffer size
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_Master_Receive(uint8_t adr, uint8_t * const rx_buf, uint16_t rx_num)
{
    g_iic11_master_status_flag = 0x00U;    /* clear master status flag */
    
    adr |= 0x01U;    /* receive mode */
    
    g_iic11_master_status_flag = 0x02U;   /* set master status flag */
    
    SCR11 &= ~0xC000U; 
    
    SCR11 |= 0x8000U; 
    
    /* Set parameter */
    g_iic11_rx_length = rx_num;
    g_iic11_rx_count = 0U;
    gp_iic11_rx_address = rx_buf;
    
    /* Start condition */
    R_IIC11_StartCondition();
    
    IICIF11 = 0U;    /* clear INTIIC11 interrupt flag */
    IICMK11 = 0U;    /* enable INTIIC11 */
    SDR11L = adr;
}

/***********************************************************************************************************************
* Function Name: R_IIC11_Stop
* Description  : This function stops the IIC11 operation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_Stop(void)
{
    /* Stop transfer */
    IICMK11 = 1U;    /* disable INTIIC11 */
    ST1 |= 0x0002U;  /* disable IIC11 */
    IICIF11 = 0U;    /* clear INTIIC11 interrupt flag */
}

/***********************************************************************************************************************
* Function Name: R_IIC11_StartCondition
* Description  : This function starts IIC11 condition.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_StartCondition(void)
{
    uint8_t w_count;
    SO1 &= ~0x0002U;    /* clear IIC11 SDA */
    
    /* Change the waiting time according to the system */
    for (w_count = 0U; w_count <= IIC11_WAITTIME; w_count++)
    {
        NOP();
    }
    
    SOE1 |= 0x0002U;    /* enable IIC11 out */
    SO1 &= ~0x0200U;   /* clear IIC11 SCL */
    SS1 |= 0x0002U;      /* enable IIC11 */
}

/***********************************************************************************************************************
* Function Name: R_IIC11_StopCondition
* Description  : This function stops IIC11 condition.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_IIC11_StopCondition(void)
{
    uint8_t w_count;
    ST1 |= 0x0002U;       /* disable IIC11 */
    SOE1 &= ~0x0002U;   /* disable IIC11 out */
    SO1 &= ~0x0002U;    /* clear IIC11 SDA */
    SO1 |= 0x0200U;    /* set IIC11 SCL */
    
    /* Change the waiting time according to the system */
    for (w_count = 0U; w_count <= IIC11_WAITTIME; w_count++)
    {
        NOP();
    }
    
    SO1 |= 0x0002U;     /* set IIC11 SDA */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
