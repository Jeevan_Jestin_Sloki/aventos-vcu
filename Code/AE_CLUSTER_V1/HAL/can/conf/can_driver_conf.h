/***********************************************************************************************************************
* File Name    : can_driver_conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 04/01/2022
***********************************************************************************************************************/

#ifndef CAN_DRIVER_CONF_H
#define CAN_DRIVER_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define ENABLE_CAN_SLEEP_MODE				TRUE //todo:configurable	/* TRUE OR FALSE*/

/*
	CAN TX-MSG ID's
*/
#define TX_ID_7F1	                      	0x07F1 /*UDS-Response*/

#define TX_ID_7EB	                      	0x07EB /*TX Test Frame*/
	
/*	
	CAN RX-MSG ID's	
*/	
#define RX_ID7F0	                      	0x07F0 /*UDS-Request*/
	
#define RX_ID700							0x700
#define RX_ID701							0x701
#define RX_ID702							0X702
#define RX_ID703							0X703
#define RX_ID704							0X704
#define RX_ID705							0X705

#define RX_ID7EA	                      	0x07EA

/*
	DLC
*/
#define MSG_LENGTH                              0x08

#define TOTAL_CAN_TX_MSG						2
#define TOTAL_CAN_RX_MSG						8
#define TOTAL_CAN_MSGS							(TOTAL_CAN_TX_MSG + TOTAL_CAN_RX_MSG)


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	CAN_BAUD_NONE_E,
	CAN_BAUD_125KBPS_E,
	CAN_BAUD_250KBPS_E,
	CAN_BAUD_500KBPS_E,
	CAN_BAUD_1MBPS_E,
}CAN_Baud_Sel_En_t;


typedef enum
{
	CAN_TX_E,
	CAN_RX_E,
}CAN_MsgType_En_t;


typedef struct
{
	CAN_MsgType_En_t	CAN_MsgType_En;
	uint32_t 			ID_u32;
	uint8_t  			DLC_u8;
	uint8_t  			BufferNum_u8;
}CAN_Msg_Conf_St_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern 			CAN_Baud_Sel_En_t	CAN_Baud_Sel_En;
extern const 	CAN_Msg_Conf_St_t	CAN_Msg_Conf_St[TOTAL_CAN_MSGS];


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* CAN_DRIVER_CONF_H */


