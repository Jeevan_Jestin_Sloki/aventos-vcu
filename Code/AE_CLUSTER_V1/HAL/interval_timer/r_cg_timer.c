/***********************************************************************************************************************
* File Name    : r_cg_timer.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for TAU module.
* Creation Date: 05/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/ 
#include "r_cg_timer.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: R_TAU0_Create
* Description  : This function initializes the TAU0 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Create(void)
{
	#if(HMI_CAN_ONLY == TRUE)
	TAU0EN = 1U;        /* supply input clock */
    TPS0 = 0x0000U;
    /* Stop all channels */
    TT0 = 0x00FFU;
    /* Mask channel 0 interrupt */
    TMMK00 = 1U;        /* disable INTTM00 interrupt */
    TMIF00 = 0U;        /* clear INTTM00 interrupt flag */
    /* Mask channel 1 interrupt */
    TMMK01 = 1U;        /* disable INTTM01 interrupt */
    TMIF01 = 0U;        /* clear INTTM01 interrupt flag */
    /* Mask channel 2 interrupt */
    TMMK02 = 1U;        /* disable INTTM02 interrupt */
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
    /* Set INTTM00 level 2 priority */
    TMPR100 = 1U;
    TMPR000 = 0U;
    /* Set INTTM01 level 2 priority */
    TMPR101 = 1U;
    TMPR001 = 0U;
    /* Set INTTM02 level 1 priority */
    TMPR102 = 0U;
    TMPR002 = 1U;
    /* Channel 0 is used as master channel for PWM output function */
    TMR00 = 0x0801U;
    TDR00 = 0x7CFFU; /*Master channel cycle value : 1-ms*/ 
    TOM0 &= ~0x0001U; 
    TOL0 &= ~0x0001U; 
    TOE0 &= ~0x0001U; 
    /* Channel 1 is used as slave channel for PWM output function */
    TMR01 = 0x0409U; 
    TDR01 = 0x0000U;	//Set PWM-1 Duty Cycle to 0%
    TOM0 |= 0x0002U; 
    TOL0 &= ~0x0002U;	
    TO0 &= ~0x0002U; 	
    TOE0 |= 0x0002U; 	
    /* Channel 2 used as interval timer */
    TMR02 = 0x0000U; 
    TDR02 = 0x7CFFU; 
    TOM0 &= ~0x0004U;
    TOL0 &= ~0x0004U; 
    TOE0 &= ~0x0004U; 
    /* Set noise filter sampling clock divisor and channels selected*/
    TNFSMP0 = 0x00U; 
    TNFCS0 = 0x00U; 
    /* Set TO01 pin */
    TOS00 |= 0x08U;
    P9 &= 0xEFU;
    PM9 &= 0xEFU;
	
	#elif(HMI_CAN_WITH_IO == TRUE)
    TAU0EN = 1U;        /* supply input clock */
    //TPS0 = 0x0020U; 
    TPS0 = 0x0000U; 
    /* Stop all channels */
    TT0 = 0x00FFU;
    /* Mask channel 2 interrupt */
    TMMK02 = 1U;        /* disable INTTM02 interrupt */
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
    /* Set INTTM02 level 1 priority */
    TMPR102 = 0U;
    TMPR002 = 1U;
    /* Channel 2 used as interval timer */
    //TMR02 = 0x4000U;
    TMR02 = 0x0000U;
    //TDR02 = 0x9C3FU;
    TDR02 = 0x7CFFU;
    TOM0 &= ~0x0004U;
    TOL0 &= ~0x0004U;
    TOE0 &= ~0x0004U;
    /* Set noise filter sampling clock divisor and channels selected*/
    TNFSMP0 = 0x00U;
    TNFCS0 = 0x00U;
	#endif
}


#if(HMI_CAN_ONLY == TRUE)
/***********************************************************************************************************************
* Function Name: R_TAU0_Channel0_Start
* Description  : This function starts TAU0 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel0_Start(void)
{
    TMIF00 = 0U;        /* clear INTTM00 interrupt flag */
    TMMK00 = 0U;        /* enable INTTM00 interrupt */
    TMIF01 = 0U;        /* clear INTTM01 interrupt flag */
    TMMK01 = 0U;        /* enable INTTM01 interrupt */
    TOE0 |= 0x0002U; 
    TS0 |= 0x0003U; 
	
	TDR01 = 0x0000U;    //Set PWM-1 Duty Cycle to 0%
}

/***********************************************************************************************************************
* Function Name: R_TAU0_Channel0_Stop
* Description  : This function stops TAU0 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel0_Stop(void)
{
	while(TO0 != 0U); //Wait Untill the PWM-1 O/P at Pin TO0 becomes 0
	
    TT0 |= 0x0003U;
    TOE0 &= ~0x0002U; 
    TMMK00 = 1U;        /* disable INTTM00 interrupt */
    TMIF00 = 0U;        /* clear INTTM00 interrupt flag */
    TMMK01 = 1U;        /* disable INTTM01 interrupt */
    TMIF01 = 0U;        /* clear INTTM01 interrupt flag */
}	
#endif


/***********************************************************************************************************************
* Function Name: R_TAU0_Channel2_Start
* Description  : This function starts TAU0 channel 2 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel2_Start(void)
{
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
    TMMK02 = 0U;        /* enable INTTM02 interrupt */
    TS0 |= 0x0004U;
}

/***********************************************************************************************************************
* Function Name: R_TAU0_Channel2_Stop
* Description  : This function stops TAU0 channel 2 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel2_Stop(void)
{
    TT0 |= 0x0004U;
    TMMK02 = 1U;        /* disable INTTM02 interrupt */
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Create
* Description  : This function initializes the TAU2 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Create(void)
{
    TAU2EN = 1U;        /* supply input clock */
    TPS2 = 0x0000U;
    
    /* Stop all channels */
    TT2 = 0x00FFU;
	  
    /* Mask channel 0 interrupt */
    TMMK20 = 1U;        /* disable INTTM20 interrupt */
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    
    /* Mask channel 4 interrupt */
    TMMK24 = 1U;        /* disable INTTM24 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
    
    /* Set INTTM20 level 2 priority */
    TMPR120 = 1U;
    TMPR020 = 0U;
    
    /* Set INTTM24 level 2 priority */
    TMPR124 = 1U;
    TMPR024 = 0U;
    
    /* Channel 0 is used as master channel for PWM output function */
    TMR20 = 0x0801U;
    TDR20 = 0x7CFFU; /*Master channel cycle value : 1-ms*/
    TOM2 &= ~0x0001U;
    TOL2 &= ~0x0001U;
    TOE2 &= ~0x0001U;
    
    /* Channel 4 is used as slave channel for PWM output function */
    TMR24 = 0x0409U;
    TDR24 = 0x0000U; /*Set PWM duty cycle value to 0%*/
    TOM2 |= 0x0010U;
    TOL2 &= ~0x0010U;
    TO2 &= ~0x0010U;	/*Set Initial Output to 0V*/
    //TO2 |= 0x0010U;	/*Set Initial Output to 5V*/
    TOE2 |= 0x0010U;
    
    /* Set noise filter sampling clock divisor and channels selected*/
    TNFSMP2 = 0x00U;
    TNFCS2 = 0x00U;
    /* Set TO24 pin */
    TOS21 &= 0xFCU;  
    P6 &= 0xBFU;
    PM6 &= 0xBFU;
    return;
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Channel0_Start
* Description  : This function starts TAU2 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Channel0_Start(void)
{
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    TMMK20 = 0U;        /* enable INTTM20 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
    TMMK24 = 0U;        /* enable INTTM24 interrupt */
    TOE2 |= 0x0010U;
    TS2 |= 0x0011U;
    
    TDR24 = 0x0000U; /*todo:dileepa*/
    		/*Set Duty Cycle to 0%*/
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Channel0_Stop
* Description  : This function stops TAU2 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Channel0_Stop(void)
{
	while(TO2 != 0U); /*todo:dileepa*/
		/*Wait Untill the PWM-2 O/P at Pin TO2 becomes 0*/
    TT2 |= 0x0011U;
    TOE2 &= ~0x0010U;
    TMMK20 = 1U;        /* disable INTTM20 interrupt */
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    TMMK24 = 1U;        /* disable INTTM24 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
