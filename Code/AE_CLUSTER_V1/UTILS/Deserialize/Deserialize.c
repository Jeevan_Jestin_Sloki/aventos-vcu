/***********************************************************************************************************************
* File Name    : Deserialize.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/03/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Deserialize.h"
#include "hmi_can_data.h"
#include "Time.h"
#include "hmi_config_can.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: CANSched_HMI_RxMsgCallback
* Description  : This function works as a call back function to the upper layer segemented reception and Calls the
                 deserialize functions to update the HMI Signals.
* Arguments    : uint16_t CIL_SigName_En,CAN_MessageFrame1_St_t* Can_Applidata_St
* Return Value : None
***********************************************************************************************************************/
void CANSched_HMI_RxMsgCallback(uint16_t CIL_SigName_En,CAN_MessageFrame_St_t* Can_Applidata_St)
{
	switch(CIL_SigName_En)
	{
		case CIL_HMI_EVE_MSG_E:
		{
			
			Deserialize_HMI_Rx_Eve_1(&HMI_Rx_Eve_1, Can_Applidata_St->DataBytes_au8);
			if((TimeSetChnnl_En == TIME_SET_CAN_E) && (TimeUpdateChnnl_En != TIME_UPDATE_CAN_E))
			{
				Set_Real_Time((uint16_t)HMI_Rx_Eve_1.Hours_Time_u8, 
								(uint16_t)HMI_Rx_Eve_1.Minutes_Time_u8, 0U);
							//Set the Real Time
			}
			
			break;
		}
		case CIL_HMI_PER_MSG1_E:
		{
			Deserialize_HMI_Rx_Per_1(&HMI_Rx_Per_1, Can_Applidata_St->DataBytes_au8);
			break;
		}
		case CIL_HMI_PER_MSG2_E:
		{
			Deserialize_HMI_Rx_Per_2(&HMI_Rx_Per_2, Can_Applidata_St->DataBytes_au8);
			break;
		}
		// case CIL_HMI_INV_EVE_MSG_E:
		// {
		// 	Deserialize__i_HMI_Rx_Eve_1(&_i_HMI_Rx_Eve_1, Can_Applidata_St->DataBytes_au8);
		// 	break;
		// }
		// case CIL_HMI_INV_PER_MSG1_E:
		// {
		// 	Deserialize__i_HMI_Rx_Per_1(&_i_HMI_Rx_Per_1, Can_Applidata_St->DataBytes_au8);
		// 	break;
		// }
		// case CIL_HMI_INV_PER_MSG2_E:
		// {
		// 	Deserialize__i_HMI_Rx_Per_2(&_i_HMI_Rx_Per_2, Can_Applidata_St->DataBytes_au8);
		// 	break;
		// }
		case CIL_HMI_CONF_MSG_E:
		{
			Deserialize_HMI_CONF_MSG_1(&HMI_CONF_MSG_1, Can_Applidata_St->DataBytes_au8);
			break;
		}
		default:
		{
			;
		}
	}
  	return;
}


/********************************************************EOF***********************************************************/