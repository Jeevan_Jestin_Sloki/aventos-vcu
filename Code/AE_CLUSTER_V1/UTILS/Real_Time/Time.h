/***********************************************************************************************************************
* File Name    : Time.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 06/02/2021
***********************************************************************************************************************/

#ifndef REAL_TIME_H
#define REAL_TIME_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
extern uint16_t 		Hours_Counter_u16; 
extern uint16_t 		Minute_Counter_u16; 
extern uint16_t 		Second_Counter_u16;


#define GET_HOURS_TIME_COUNTER()				(Hours_Counter_u16)
#define GET_MINUTES_TIME_COUNTER()				(Minute_Counter_u16)
#define GET_SECONDS_TIME_COUNTER()				(Second_Counter_u16)


#define 	SEC_COUNT_LIMIT						60
#define 	MIN_COUNT_LIMIT						60
#define 	HOUR_COUNT_LIMIT					24


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	TIME_SET_NONE_E,
	TIME_SET_UIB_E,
	TIME_SET_CAN_E,
}TimeSetChnnl_En_t;

typedef enum
{
	TIME_UPDATE_NONE_E,
	TIME_UPDATE_TIMER_E,
	TIME_UPDATE_CAN_E,
}TimeUpdateChnnl_En_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern TimeSetChnnl_En_t		TimeSetChnnl_En;
extern TimeUpdateChnnl_En_t		TimeUpdateChnnl_En;
extern uint16_t					MinutesTime_u16;
extern uint16_t					HoursTime_u16;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Set_Real_Time(uint16_t, uint16_t, uint16_t);
extern void Update_Time(void);
extern void ResetRealTimeCounters(void);

#endif /* REAL_TIME_H */


