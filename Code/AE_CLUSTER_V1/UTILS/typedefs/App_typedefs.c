/*
 * App_typedefs.c
 *
 *  Created on: Oct 30, 2020
 *      Author: sandeepky
 */

/*
 *-----------------------------------------------------------------------------
 *
 *  File Name       : App_typedefs.c
 *
 *  @brief          : 
 *-----------------------------------------------------------------------------
 *
 *  Description :
 *-----------------------------------------------------------------------------
 *
 *  Revision History:
 *
 *  Version  Author   Date        Description
 *                  (DD/MM/YY)
 *
 *  1.0      Sloki  11/09/08     Original
 *
 *-----------------------------------------------------------------------------
 */

#include "App_typedefs.h"


volatile UINT32 TS_time_ms_u32 = 0;
