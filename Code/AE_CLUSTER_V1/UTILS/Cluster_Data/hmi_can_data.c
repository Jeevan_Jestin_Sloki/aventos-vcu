﻿/***********************************************************************************************************************
* File Name    : hmi_can_data.c
* Version      : 01
* Description  : This is the generated file from the DBC2CH Tool.
* Created By   : SLOKI SOFTWARE TECHNOLOGIES LLP
* Creation Date: 01/01/2021
***********************************************************************************************************************/


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
/* Start user code for include. Do not edit comment generated here */
#include "hmi_can_data.h"
#include "Communicator.h"
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for include. Do not edit comment added here */
HMI_Rx_Eve_1_t			HMI_Rx_Eve_1;
_i_HMI_Rx_Eve_1_t		_i_HMI_Rx_Eve_1;
HMI_Rx_Per_1_t			HMI_Rx_Per_1;
_i_HMI_Rx_Per_1_t		_i_HMI_Rx_Per_1;
HMI_Rx_Per_2_t			HMI_Rx_Per_2;
_i_HMI_Rx_Per_2_t		_i_HMI_Rx_Per_2;
/* End user code. Do not edit comment added here */

/*----------------------------------------------------------------------------*/

 uint32_t Deserialize_HMI_Rx_Eve_1(HMI_Rx_Eve_1_t* message, const uint8_t* data)
{
	message->Reverse_Mode_u8 = ((data[0] & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_REVERSE_MODE_U8_OFFSET;
	message->Sports_Mode_u8 = (((data[0] >> HMI_RX_EVE_1_SPORTS_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_SPORTS_MODE_U8_OFFSET;
	message->Eco_Mode_u8 = (((data[0] >> HMI_RX_EVE_1_ECO_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_ECO_MODE_U8_OFFSET;
	message->Neutral_Mode_u8 = (((data[0] >> HMI_RX_EVE_1_NEUTRAL_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_NEUTRAL_MODE_U8_OFFSET;
	message->Kill_Switch_u8 = ((data[1] & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_KILL_SWITCH_U8_OFFSET;
	message->Right_Indicator_u8 = (((data[1] >> HMI_RX_EVE_1_RIGHT_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_RIGHT_INDICATOR_U8_OFFSET;
	message->Left_Indicator_u8 = (((data[1] >> HMI_RX_EVE_1_LEFT_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_LEFT_INDICATOR_U8_OFFSET;
	message->Safe_Mode_u8 = (((data[1] >> HMI_RX_EVE_1_SAFE_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_SAFE_MODE_U8_OFFSET;
	message->Motor_Fault_u8 = ((data[2] & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_MOTOR_FAULT_U8_OFFSET;
	message->Battery_Fault_u8 = (((data[2] >> HMI_RX_EVE_1_BATTERY_FAULT_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_BATTERY_FAULT_U8_OFFSET;
	message->High_Beam_u8 = (((data[2] >> HMI_RX_EVE_1_HIGH_BEAM_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_HIGH_BEAM_U8_OFFSET;
	message->Low_Beam_u8 = (((data[2] >> HMI_RX_EVE_1_LOW_BEAM_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_LOW_BEAM_U8_OFFSET;
	message->Ignition_u8 = ((data[3] & (SIGNAL_READ_Mask1))) + HMI_RX_EVE_1_CANID_IGNITION_U8_OFFSET;
	message->Battery_Status_u8 = (((data[3] >> HMI_RX_EVE_1_BATTERY_STATUS_U8_MASK0) & (SIGNAL_READ_Mask1))) + HMI_RX_EVE_1_CANID_BATTERY_STATUS_U8_OFFSET;
	message->Warning_Symbol_u8 = (((data[3] >> HMI_RX_EVE_1_WARNING_SYMBOL_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_WARNING_SYMBOL_U8_OFFSET;
	message->Service_Indicator_u8 = (((data[3] >> HMI_RX_EVE_1_SERVICE_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_SERVICE_INDICATOR_U8_OFFSET;
	message->Side_Stand_u8 = (((data[3] >> HMI_RX_EVE_1_SIDE_STAND_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_EVE_1_CANID_SIDE_STAND_U8_OFFSET;
	message->Hours_Time_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + HMI_RX_EVE_1_CANID_HOURS_TIME_U8_OFFSET;
	message->Minutes_Time_u8 = ((data[5] & (SIGNAL_READ_Mask8))) + HMI_RX_EVE_1_CANID_MINUTES_TIME_U8_OFFSET;
	message->Cluster_Use_Place_u8 = ((data[6] & 0x80U) >> 7);
	message->Eve_1_Counter_u8 = (((data[7] >> HMI_RX_EVE_1_EVE_1_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + HMI_RX_EVE_1_CANID_EVE_1_COUNTER_U8_OFFSET;

	return HMI_RX_EVE_1_ID; 
}


/*----------------------------------------------------------------------------*/

 uint32_t Deserialize__i_HMI_Rx_Eve_1(_i_HMI_Rx_Eve_1_t* message, const uint8_t* data)
{
	message->_i_Reverse_Mode_u8 = ((data[0] & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_REVERSE_MODE_U8_OFFSET;
	message->_i_Sports_Mode_u8 = (((data[0] >> _I_HMI_RX_EVE_1__I_SPORTS_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_SPORTS_MODE_U8_OFFSET;
	message->_i_Eco_Mode_u8 = (((data[0] >> _I_HMI_RX_EVE_1__I_ECO_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_ECO_MODE_U8_OFFSET;
	message->_i_Neutral_Mode_u8 = (((data[0] >> _I_HMI_RX_EVE_1__I_NEUTRAL_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_NEUTRAL_MODE_U8_OFFSET;
	message->_i_Kill_Switch_u8 = ((data[1] & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_KILL_SWITCH_U8_OFFSET;
	message->_i_Right_Indicator_u8 = (((data[1] >> _I_HMI_RX_EVE_1__I_RIGHT_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_OFFSET;
	message->_i_Left_Indicator_u8 = (((data[1] >> _I_HMI_RX_EVE_1__I_LEFT_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_LEFT_INDICATOR_U8_OFFSET;
	message->_i_Safe_Mode_u8 = (((data[1] >> _I_HMI_RX_EVE_1__I_SAFE_MODE_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_SAFE_MODE_U8_OFFSET;
	message->_i_Motor_Fault_u8 = ((data[2] & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_MOTOR_FAULT_U8_OFFSET;
	message->_i_Battery_Fault_u8 = (((data[2] >> _I_HMI_RX_EVE_1__I_BATTERY_FAULT_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_BATTERY_FAULT_U8_OFFSET;
	message->_i_High_Beam_u8 = (((data[2] >> _I_HMI_RX_EVE_1__I_HIGH_BEAM_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_HIGH_BEAM_U8_OFFSET;
	message->_i_Low_Beam_u8 = (((data[2] >> _I_HMI_RX_EVE_1__I_LOW_BEAM_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_LOW_BEAM_U8_OFFSET;
	message->_i_Ignition_u8 = ((data[3] & (SIGNAL_READ_Mask1))) + _I_HMI_RX_EVE_1_CANID__I_IGNITION_U8_OFFSET;
	message->_i_Battery_Status_u8 = (((data[3] >> _I_HMI_RX_EVE_1__I_BATTERY_STATUS_U8_MASK0) & (SIGNAL_READ_Mask1))) + _I_HMI_RX_EVE_1_CANID__I_BATTERY_STATUS_U8_OFFSET;
	message->_i_Warning_Symbol_u8 = (((data[3] >> _I_HMI_RX_EVE_1__I_WARNING_SYMBOL_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_WARNING_SYMBOL_U8_OFFSET;
	message->_i_Service_Indicator_u8 = (((data[3] >> _I_HMI_RX_EVE_1__I_SERVICE_INDICATOR_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_OFFSET;
	message->_i_Side_Stand_u8 = (((data[3] >> _I_HMI_RX_EVE_1__I_SIDE_STAND_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_EVE_1_CANID__I_SIDE_STAND_U8_OFFSET;
	message->_i_Hours_Time_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_EVE_1_CANID__I_HOURS_TIME_U8_OFFSET;
	message->_i_Minutes_Time_u8 = ((data[5] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_EVE_1_CANID__I_MINUTES_TIME_U8_OFFSET;	
	message->_i_Cluster_Use_Place_u8 = ((data[6] & 0x80U) >> 7);  /*TODO : Dileepa*/	
	message->_i_Eve_1_Counter_u8 = (((data[7] >> _I_HMI_RX_EVE_1__I_EVE_1_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + _I_HMI_RX_EVE_1_CANID__I_EVE_1_COUNTER_U8_OFFSET;

	return _I_HMI_RX_EVE_1_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_HMI_Rx_Per_1(HMI_Rx_Per_1_t* message, const uint8_t* data)
{
	/* start user code here */
	/* ODO SIgnal data bytes recevied over can are typecasted to uint32_t*/
	message->ODO_u32 = (((((uint32_t)data[0]) & (SIGNAL_READ_Mask8)) << HMI_RX_PER_1_ODO_U32_MASK0) | \
  			    ((((uint32_t)data[1]) & (SIGNAL_READ_Mask8)) << HMI_RX_PER_1_ODO_U32_MASK1) | \
			    ((((uint32_t)data[2]) & (SIGNAL_READ_Mask8)) << HMI_RX_PER_1_ODO_U32_MASK2) | \
			    (((uint32_t)data[3]) & (SIGNAL_READ_Mask8))) + HMI_RX_PER_1_CANID_ODO_U32_OFFSET;
	/* end user code here */
  	message->Vehicle_Speed_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + HMI_RX_PER_1_CANID_VEHICLE_SPEED_U8_OFFSET;
  	message->Mileage_u8 = ((data[5] & (SIGNAL_READ_Mask8))) + HMI_RX_PER_1_CANID_MILEAGE_U8_OFFSET;
  	message->Per_1_Counter_u8 = (((data[7] >> HMI_RX_PER_1_PER_1_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + HMI_RX_PER_1_CANID_PER_1_COUNTER_U8_OFFSET;
	  
	return HMI_RX_PER_1_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize__i_HMI_Rx_Per_1(_i_HMI_Rx_Per_1_t* message, const uint8_t* data)
{
	/* start user code here */
	/* ODO SIgnal data bytes recevied over can are typecasted to uint32_t*/

	message->_i_ODO_u32 = (((((uint32_t)data[0]) & (SIGNAL_READ_Mask8)) << _I_HMI_RX_PER_1__I_ODO_U32_MASK0)  | \
	  			 ((((uint32_t)data[1]) & (SIGNAL_READ_Mask8)) << _I_HMI_RX_PER_1__I_ODO_U32_MASK1) | \
				 ((((uint32_t)data[2]) & (SIGNAL_READ_Mask8)) << _I_HMI_RX_PER_1__I_ODO_U32_MASK2) | \
				 (((uint32_t)data[3]) & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_1_CANID__I_ODO_U32_OFFSET;

	/* end user code here */

  	message->_i_Vehicle_Speed_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_1_CANID__I_VEHICLE_SPEED_U8_OFFSET;
  	message->_i_Mileage_u8 = ((data[5] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_1_CANID__I_MILEAGE_U8_OFFSET;
  	message->_i_Per_1_Counter_u8 = (((data[7] >> _I_HMI_RX_PER_1__I_PER_1_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + _I_HMI_RX_PER_1_CANID__I_PER_1_COUNTER_U8_OFFSET;
	return _I_HMI_RX_PER_1_ID; 
}


/*----------------------------------------------------------------------------*/

 uint32_t Deserialize_HMI_Rx_Per_2(HMI_Rx_Per_2_t* message, const uint8_t* data)
{
	message->Power_Consumption_u16 = (((data[0] & (SIGNAL_READ_Mask8)) << HMI_RX_PER_2_POWER_CONSUMPTION_U16_MASK0) | (data[1] & (SIGNAL_READ_Mask8))) + HMI_RX_PER_2_CANID_POWER_CONSUMPTION_U16_OFFSET;
	message->Range_Km_u16 = (((data[2] & (SIGNAL_READ_Mask8)) << HMI_RX_PER_2_RANGE_KM_U16_MASK0) | (data[3] & (SIGNAL_READ_Mask8))) + HMI_RX_PER_2_CANID_RANGE_KM_U16_OFFSET;
	message->Battery_SOC_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + HMI_RX_PER_2_CANID_BATTERY_SOC_U8_OFFSET;
	message->BLE_Icon_u8 = (((data[5] >> HMI_RX_PER_2_BLE_ICON_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_PER_2_CANID_BLE_ICON_U8_OFFSET;
	message->Regen_Braking_u8 = (((data[5] >> HMI_RX_PER_2_REGEN_BRAKING_U8_MASK0) & (SIGNAL_READ_Mask2))) + HMI_RX_PER_2_CANID_REGEN_BRAKING_U8_OFFSET;
	message->Per_2_Counter_u8 = (((data[7] >> HMI_RX_PER_2_PER_2_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + HMI_RX_PER_2_CANID_PER_2_COUNTER_U8_OFFSET;

	SET_PWRCONSUM_WATTS(message->Power_Consumption_u16);

	return HMI_RX_PER_2_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize__i_HMI_Rx_Per_2(_i_HMI_Rx_Per_2_t* message, const uint8_t* data)
{
	message->_i_Power_Consumption_u16 = (((data[0] & (SIGNAL_READ_Mask8)) << _I_HMI_RX_PER_2__I_POWER_CONSUMPTION_U16_MASK0) | (data[1] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_2_CANID__I_POWER_CONSUMPTION_U16_OFFSET;
	message->_i_Range_Km_u16 = (((data[2] & (SIGNAL_READ_Mask8)) << _I_HMI_RX_PER_2__I_RANGE_KM_U16_MASK0) | (data[3] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_2_CANID__I_RANGE_KM_U16_OFFSET;
	message->_i_Battery_SOC_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + _I_HMI_RX_PER_2_CANID__I_BATTERY_SOC_U8_OFFSET;
	message->_i_BLE_Icon_u8 = (((data[5] >> _I_HMI_RX_PER_2__I_BLE_ICON_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_PER_2_CANID__I_BLE_ICON_U8_OFFSET;
	message->_i_Regen_Braking_u8 = (((data[5] >> _I_HMI_RX_PER_2__I_REGEN_BRAKING_U8_MASK0) & (SIGNAL_READ_Mask2))) + _I_HMI_RX_PER_2_CANID__I_REGEN_BRAKING_U8_OFFSET;
	message->_i_Per_2_Counter_u8 = (((data[7] >> _I_HMI_RX_PER_2__I_PER_2_COUNTER_U8_MASK0) & (SIGNAL_READ_Mask4))) + _I_HMI_RX_PER_2_CANID__I_PER_2_COUNTER_U8_OFFSET;
	return _I_HMI_RX_PER_2_ID; 
}


/*----------------------------------------------------------------------------*/

 /* Start user code for include. Do not edit comment generated here */
 
/***********************************************************************************************************************
* Function Name: Clear_HMI_CAN_Signals
* Description  : This function clears the all the HMI CAN Signals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Clear_HMI_CAN_Signals(void)
{
	/*Clear Event frame signals*/
	HMI_Rx_Eve_1.Reverse_Mode_u8 		= CLEAR;
	HMI_Rx_Eve_1.Sports_Mode_u8 		= CLEAR;
	HMI_Rx_Eve_1.Eco_Mode_u8 			= CLEAR;
	HMI_Rx_Eve_1.Neutral_Mode_u8		= CLEAR;
	HMI_Rx_Eve_1.Kill_Switch_u8 		= CLEAR;
	HMI_Rx_Eve_1.Right_Indicator_u8 	= CLEAR;
	HMI_Rx_Eve_1.Left_Indicator_u8 		= CLEAR;
	HMI_Rx_Eve_1.Safe_Mode_u8 			= CLEAR;
	HMI_Rx_Eve_1.Motor_Fault_u8 		= CLEAR;
	HMI_Rx_Eve_1.Battery_Fault_u8 		= CLEAR;
	HMI_Rx_Eve_1.High_Beam_u8 			= CLEAR;
	HMI_Rx_Eve_1.Low_Beam_u8 			= CLEAR;
	HMI_Rx_Eve_1.Ignition_u8 			= CLEAR;
	HMI_Rx_Eve_1.Battery_Status_u8 		= CLEAR;
	HMI_Rx_Eve_1.Warning_Symbol_u8 		= CLEAR;
	HMI_Rx_Eve_1.Service_Indicator_u8 	= CLEAR;
	HMI_Rx_Eve_1.Side_Stand_u8 			= CLEAR;
	HMI_Rx_Eve_1.Hours_Time_u8 			= CLEAR;
	HMI_Rx_Eve_1.Minutes_Time_u8 		= CLEAR;
	HMI_Rx_Eve_1.Cluster_Use_Place_u8 	= CLEAR;
	HMI_Rx_Eve_1.Eve_1_Counter_u8 		= RESET;
	
	/*Clear Periodic frame-1 signals*/
	HMI_Rx_Per_1.ODO_u32 				= RESET;
	HMI_Rx_Per_1.Vehicle_Speed_u8 		= CLEAR;
	HMI_Rx_Per_1.Mileage_u8 			= CLEAR;
	HMI_Rx_Per_1.Per_1_Counter_u8 		= CLEAR;
	
	/*Clear Periodic frame-1 signals*/
	HMI_Rx_Per_2.Power_Consumption_u16 	= RESET;
	HMI_Rx_Per_2.Range_Km_u16 			= RESET;
	HMI_Rx_Per_2.Battery_SOC_u8 		= CLEAR;
	HMI_Rx_Per_2.BLE_Icon_u8 			= CLEAR;
	HMI_Rx_Per_2.Regen_Braking_u8 		= CLEAR;
	HMI_Rx_Per_2.Per_2_Counter_u8 		= CLEAR;
	return;  
}
/* End user code. Do not edit comment generated here */