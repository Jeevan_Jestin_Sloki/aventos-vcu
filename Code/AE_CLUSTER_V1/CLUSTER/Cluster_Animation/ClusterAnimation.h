/***********************************************************************************************************************
* File Name    : ClusterAnimation.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef ANIMATION_H
#define ANIMATION_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define INCREASE_IN_SPEED								5
#define DECREASE_IN_SPEED								10
#define MAXIMUM_SPEED									100
#define MINIMUM_SPEED									0

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	SPEED_NO_CHANGE_E,
	SPEED_ANIMATE_START_E,
	INCREASE_SPEED_E,
	INTERMEDIATE_DELAY_E,
	DECREASE_SPEED_E,
	SPEED_ANIMATE_END_E,
}SpeedAnimation_En_t;

typedef enum
{
	SIG_NO_CHANGE_E,
	TURN_ON_SIG_E,
	SET_SIG_ACTUAL_E,
}SignalState_En_t;

typedef enum
{
	CENTRAL_TELLTALE_NONE_E,
	CENTER_TELLTALE_1_E,
	CENTER_TELLTALE_2_E,
	CENTER_TELLTALE_3_E,
}CentralTellTale_En_t;

typedef struct
{
	uint32_t BarsCountValue_u32;
	uint16_t BarsCount_u16;
	int16_t  VehicleSpeed_s16;
	uint16_t SpeedDelayCounter_u16;
}AnimationData_St_t;



/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern bool	AnimationDone_b;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void AnimateOnStart(void);
extern void ResetAnimationData(void);
void AnimateIncreaseVehicleSpeed(void);
void AnimateDecreaseVehicleSpeed(void);
void SetTopRowTelltales(SignalState_En_t);
void Set_ODO_and_Time(SignalState_En_t);
void SetBottomRowTelltales(SignalState_En_t);
void SetTopRowTelltales(SignalState_En_t);
void Set_Range_and_Mileage(SignalState_En_t);
void Set_CentreTelltales(SignalState_En_t);
void SetBarsActualValue(void);
void SetDrivingModeRings(SignalState_En_t);
void ControlBrightness(uint16_t);

#endif /* ANIMATION_H */


