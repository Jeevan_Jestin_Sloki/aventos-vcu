/***********************************************************************************************************************
* File Name    : UserButtuonIn.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/12/2021
***********************************************************************************************************************/

#ifndef _USER_BUTTON_IN_H_
#define _USER_BUTTON_IN_H_


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h" 


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
extern volatile uint32_t TimeTick_ms_u32;

#define INC_TIME_MS_2()							(TimeTick_ms_u32++)
#define GET_CURRENT_TIME_MS()					(TimeTick_ms_u32)

#define MENU_BTN_PRESSED						 0x01U
#define MENU_BTN_RELEASED						 0x00U
												 
#define MENU_BTN_PRESS_DURATION					 5000	/*m-sec*/
#define NOP_FREE_TIME_LIMIT						 10000	/*m-sec*/

#define READ_UIB_INPUT							 FALSE //todo:configurable	/* TRUE OR FALSE*/										 

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	USER_SETTING_NOP_E,
	USER_SETTING_START_E,
	USER_SETTING_STOP_E,
}UserSettingSession_En_t;

typedef enum
{
	SET_TIME_START_E,
	SET_HOURS_TIME_E,
	SET_MINUTES_TIME_E,
	SET_TIME_END_E,
}TimeSetting_En_t;


/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern TimeSetting_En_t		TimeSetting_En;
extern uint16_t 			UIB_HoursTime_u16;
extern uint16_t 			UIB_MinutesTime_u16;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Read_UserInterfaceButton(void);
void UserSettingSession(UserSettingSession_En_t);
void Set_Hours_Time(void);
void Set_Minutes_Time(void);


#endif /* _USER_BUTTON_IN_H_ */


