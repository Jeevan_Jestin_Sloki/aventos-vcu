/***********************************************************************************************************************
* File Name    : CommonSigDisp.c
* Version      : 01
* Description  : This file contains the functions to turn-On the Common Signals on the HMI.
* Created By   : Dileepa B S 
* Creation Date: 04/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "CommonSigDisp.h"
#include "SegDispWrite.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: ControlCommonSig
* Description  : This function Turns-ON or Turns-OFF All the Common Signals on the HMI.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ControlCommonSig(bool SignalState_b)
{ 
	Write_SEG(COMMON_SIG_E, SignalState_b);        
	return;
}


/***********************************************************************************************************************
* Function Name: Control_LOGO
* Description  : This function Turns-ON or Turns-OFF the LOGO on the HMI.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Control_LOGO(bool SignalState_b)
{
	Write_SEG(LOGO_E, ON ); 
	return;
}

/***********************************************************************************************************************
* Function Name: Control_DrvMode_Texts
* Description  : This function Turns-ON or Turn-OFF All the Common Signals on the HMI.
* Arguments    : (DrvMode_En_t DrvMode_En, bool SignalState_b
* Return Value : None
***********************************************************************************************************************/
void Control_DrvMode_Texts(DrvMode_En_t DrvMode_En, bool SignalState_b)
{
	switch(DrvMode_En)
	{
		case DRV_NEUTRAL_MODE_E:
		{
			Write_SEG(NEUTRAL_TEXT_E, SignalState_b);
			break;
		}
		case DRV_ECO_MODE_E:
		{
			Write_SEG(ECO_TEXT_E, SignalState_b);
			break;
		}
		case DRV_SPORTS_MODE_E:
		{
			Write_SEG(SPORTS_TEXT_E, SignalState_b);
			break;
		}
		case DRV_REVERSE_MODE_E:
		{
			Write_SEG(REVERSE_TEXT_E, SignalState_b);
			break;
		}
		default:
		{
			;
		}
	}
	return;
}

/********************************************************EOF***********************************************************/