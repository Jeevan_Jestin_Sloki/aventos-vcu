/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_INTC.c
* Version      : 1.2.0
* Device(s)    : R7F701688
* Description  : This file implements device driver for Config_INTC.
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_INTC.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
extern volatile uint32_t g_cg_sync_read;
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_INTC_Create
* Description  : This function initializes the INTC module
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_Create(void)
{
    /* Disable INTP0 operation and clear request */
    INTC2.ICP0.BIT.MKP0 = _INT_PROCESSING_DISABLED;
    INTC2.ICP0.BIT.RFP0 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP1 operation and clear request */
    INTC2.ICP1.BIT.MKP1 = _INT_PROCESSING_DISABLED;
    INTC2.ICP1.BIT.RFP1 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP2 operation and clear request */
    INTC2.ICP2.BIT.MKP2 = _INT_PROCESSING_DISABLED;
    INTC2.ICP2.BIT.RFP2 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP3 operation and clear request */
    INTC2.ICP3.BIT.MKP3 = _INT_PROCESSING_DISABLED;
    INTC2.ICP3.BIT.RFP3 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP4 operation and clear request */
    INTC2.ICP4.BIT.MKP4 = _INT_PROCESSING_DISABLED;
    INTC2.ICP4.BIT.RFP4 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP5 operation and clear request */
    INTC2.ICP5.BIT.MKP5 = _INT_PROCESSING_DISABLED;
    INTC2.ICP5.BIT.RFP5 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP7 operation and clear request */
    INTC2.ICP7.BIT.MKP7 = _INT_PROCESSING_DISABLED;
    INTC2.ICP7.BIT.RFP7 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP8 operation and clear request */
    INTC2.ICP8.BIT.MKP8 = _INT_PROCESSING_DISABLED;
    INTC2.ICP8.BIT.RFP8 = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTP12 operation and clear request */
    INTC2.ICP12.BIT.MKP12 = _INT_PROCESSING_DISABLED;
    INTC2.ICP12.BIT.RFP12 = _INT_REQUEST_NOT_OCCUR;
    /* Set INTP0 setting */
    INTC2.ICP0.BIT.TBP0 = _INT_TABLE_VECTOR;
    INTC2.ICP0.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL0_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP1 setting */
    INTC2.ICP1.BIT.TBP1 = _INT_TABLE_VECTOR;
    INTC2.ICP1.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL1_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP2 setting */
    INTC2.ICP2.BIT.TBP2 = _INT_TABLE_VECTOR;
    INTC2.ICP2.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL2_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP3 setting */
    INTC2.ICP3.BIT.TBP3 = _INT_TABLE_VECTOR;
    INTC2.ICP3.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL3_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP4 setting */
    INTC2.ICP4.BIT.TBP4 = _INT_TABLE_VECTOR;
    INTC2.ICP4.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL4_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP5 setting */
    INTC2.ICP5.BIT.TBP5 = _INT_TABLE_VECTOR;
    INTC2.ICP5.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL5_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP7 setting */
    INTC2.ICP7.BIT.TBP7 = _INT_TABLE_VECTOR;
    INTC2.ICP7.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL7_INTPL = _INTC_EDGE_FALLING;
    /* Set INTP8 setting */
    INTC2.ICP8.BIT.TBP8 = _INT_TABLE_VECTOR;
    INTC2.ICP8.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL0_INTPH = _INTC_EDGE_FALLING;
    /* Set INTP12 setting */
    INTC2.ICP12.BIT.TBP12 = _INT_TABLE_VECTOR;
    INTC2.ICP12.UINT16 &= _INT_PRIORITY_LOWEST;
    FCLA0.CTL4_INTPH = _INTC_EDGE_FALLING;
    /* Synchronization processing */
    g_cg_sync_read = FCLA0.CTL4_INTPH;
    __syncp();
    /* Set INTP0 pin */
    JTAG.JPIBC0 &= _JPORT_CLEAR_BIT0;
    JTAG.JPBDC0 &= _JPORT_CLEAR_BIT0;
    JTAG.JPM0 |= _JPORT_SET_BIT0;
    JTAG.JPMC0 &= _JPORT_CLEAR_BIT0;
    JTAG.JPFC0 &= _JPORT_CLEAR_BIT0;
    JTAG.JPFCE0 &= _JPORT_CLEAR_BIT0;
    JTAG.JPMC0 |= _JPORT_SET_BIT0;
    /* Set INTP1 pin */
    JTAG.JPIBC0 &= _JPORT_CLEAR_BIT1;
    JTAG.JPBDC0 &= _JPORT_CLEAR_BIT1;
    JTAG.JPM0 |= _JPORT_SET_BIT1;
    JTAG.JPMC0 &= _JPORT_CLEAR_BIT1;
    JTAG.JPFC0 &= _JPORT_CLEAR_BIT1;
    JTAG.JPFCE0 &= _JPORT_CLEAR_BIT1;
    JTAG.JPMC0 |= _JPORT_SET_BIT1;
    /* Set INTP2 pin */
    PORT.PIBC0 &= _PORT_CLEAR_BIT6;
    PORT.PBDC0 &= _PORT_CLEAR_BIT6;
    PORT.PM0 |= _PORT_SET_BIT6;
    PORT.PMC0 &= _PORT_CLEAR_BIT6;
    PORT.PIPC0 &= _PORT_CLEAR_BIT6;
    PORT.PFC0 &= _PORT_CLEAR_BIT6;
    PORT.PFCE0 &= _PORT_CLEAR_BIT6;
    PORT.PMC0 |= _PORT_SET_BIT6;
    /* Set INTP3 pin */
    JTAG.JPIBC0 &= _JPORT_CLEAR_BIT3;
    JTAG.JPBDC0 &= _JPORT_CLEAR_BIT3;
    JTAG.JPM0 |= _JPORT_SET_BIT3;
    JTAG.JPMC0 &= _JPORT_CLEAR_BIT3;
    JTAG.JPFC0 &= _JPORT_CLEAR_BIT3;
    JTAG.JPMC0 |= _JPORT_SET_BIT3;
    /* Set INTP4 pin */
    PORT.PIBC8 &= _PORT_CLEAR_BIT0;
    PORT.PBDC8 &= _PORT_CLEAR_BIT0;
    PORT.PM8 |= _PORT_SET_BIT0;
    PORT.PMC8 &= _PORT_CLEAR_BIT0;
    PORT.PFC8 &= _PORT_CLEAR_BIT0;
    PORT.PFCE8 |= _PORT_SET_BIT0;
    PORT.PFCAE8 &= _PORT_CLEAR_BIT0;
    PORT.PMC8 |= _PORT_SET_BIT0;
    /* Set INTP5 pin */
    PORT.PIBC8 &= _PORT_CLEAR_BIT1;
    PORT.PBDC8 &= _PORT_CLEAR_BIT1;
    PORT.PM8 |= _PORT_SET_BIT1;
    PORT.PMC8 &= _PORT_CLEAR_BIT1;
    PORT.PFC8 &= _PORT_CLEAR_BIT1;
    PORT.PFCE8 |= _PORT_SET_BIT1;
    PORT.PFCAE8 &= _PORT_CLEAR_BIT1;
    PORT.PMC8 |= _PORT_SET_BIT1;
    /* Set INTP7 pin */
    PORT.PIBC8 &= _PORT_CLEAR_BIT3;
    PORT.PBDC8 &= _PORT_CLEAR_BIT3;
    PORT.PM8 |= _PORT_SET_BIT3;
    PORT.PMC8 &= _PORT_CLEAR_BIT3;
    PORT.PFC8 &= _PORT_CLEAR_BIT3;
    PORT.PFCE8 |= _PORT_SET_BIT3;
    PORT.PMC8 |= _PORT_SET_BIT3;
    /* Set INTP8 pin */
    PORT.PIBC8 &= _PORT_CLEAR_BIT4;
    PORT.PBDC8 &= _PORT_CLEAR_BIT4;
    PORT.PM8 |= _PORT_SET_BIT4;
    PORT.PMC8 &= _PORT_CLEAR_BIT4;
    PORT.PFC8 &= _PORT_CLEAR_BIT4;
    PORT.PFCE8 |= _PORT_SET_BIT4;
    PORT.PMC8 |= _PORT_SET_BIT4;
    /* Set INTP12 pin */
    PORT.PIBC0 &= _PORT_CLEAR_BIT9;
    PORT.PBDC0 &= _PORT_CLEAR_BIT9;
    PORT.PM0 |= _PORT_SET_BIT9;
    PORT.PMC0 &= _PORT_CLEAR_BIT9;
    PORT.PFC0 &= _PORT_CLEAR_BIT9;
    PORT.PFCE0 &= _PORT_CLEAR_BIT9;
    PORT.PMC0 |= _PORT_SET_BIT9;

    R_Config_INTC_Create_UserInit();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP0_Start
* Description  : This function enables INTP0 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP0_Start(void)
{
    /* Clear INTP0 request and enable operation */
    INTC2.ICP0.BIT.RFP0 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP0.BIT.MKP0 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP0_Stop
* Description  : This function disables INTP0 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP0_Stop(void)
{
    /* Disable INTP0 operation and clear request */
    INTC2.ICP0.BIT.MKP0 = _INT_PROCESSING_DISABLED;
    INTC2.ICP0.BIT.RFP0 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP0.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP1_Start
* Description  : This function enables INTP1 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP1_Start(void)
{
    /* Clear INTP1 request and enable operation */
    INTC2.ICP1.BIT.RFP1 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP1.BIT.MKP1 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP1_Stop
* Description  : This function disables INTP1 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP1_Stop(void)
{
    /* Disable INTP1 operation and clear request */
    INTC2.ICP1.BIT.MKP1 = _INT_PROCESSING_DISABLED;
    INTC2.ICP1.BIT.RFP1 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP1.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP2_Start
* Description  : This function enables INTP2 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP2_Start(void)
{
    /* Clear INTP2 request and enable operation */
    INTC2.ICP2.BIT.RFP2 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP2.BIT.MKP2 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP2_Stop
* Description  : This function disables INTP2 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP2_Stop(void)
{
    /* Disable INTP2 operation and clear request */
    INTC2.ICP2.BIT.MKP2 = _INT_PROCESSING_DISABLED;
    INTC2.ICP2.BIT.RFP2 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP2.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP3_Start
* Description  : This function enables INTP3 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP3_Start(void)
{
    /* Clear INTP3 request and enable operation */
    INTC2.ICP3.BIT.RFP3 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP3.BIT.MKP3 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP3_Stop
* Description  : This function disables INTP3 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP3_Stop(void)
{
    /* Disable INTP3 operation and clear request */
    INTC2.ICP3.BIT.MKP3 = _INT_PROCESSING_DISABLED;
    INTC2.ICP3.BIT.RFP3 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP3.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP4_Start
* Description  : This function enables INTP4 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP4_Start(void)
{
    /* Clear INTP4 request and enable operation */
    INTC2.ICP4.BIT.RFP4 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP4.BIT.MKP4 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP4_Stop
* Description  : This function disables INTP4 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP4_Stop(void)
{
    /* Disable INTP4 operation and clear request */
    INTC2.ICP4.BIT.MKP4 = _INT_PROCESSING_DISABLED;
    INTC2.ICP4.BIT.RFP4 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP4.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP5_Start
* Description  : This function enables INTP5 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP5_Start(void)
{
    /* Clear INTP5 request and enable operation */
    INTC2.ICP5.BIT.RFP5 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP5.BIT.MKP5 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP5_Stop
* Description  : This function disables INTP5 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP5_Stop(void)
{
    /* Disable INTP5 operation and clear request */
    INTC2.ICP5.BIT.MKP5 = _INT_PROCESSING_DISABLED;
    INTC2.ICP5.BIT.RFP5 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP5.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP7_Start
* Description  : This function enables INTP7 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP7_Start(void)
{
    /* Clear INTP7 request and enable operation */
    INTC2.ICP7.BIT.RFP7 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP7.BIT.MKP7 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP7_Stop
* Description  : This function disables INTP7 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP7_Stop(void)
{
    /* Disable INTP7 operation and clear request */
    INTC2.ICP7.BIT.MKP7 = _INT_PROCESSING_DISABLED;
    INTC2.ICP7.BIT.RFP7 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP7.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP8_Start
* Description  : This function enables INTP8 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP8_Start(void)
{
    /* Clear INTP8 request and enable operation */
    INTC2.ICP8.BIT.RFP8 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP8.BIT.MKP8 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP8_Stop
* Description  : This function disables INTP8 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP8_Stop(void)
{
    /* Disable INTP8 operation and clear request */
    INTC2.ICP8.BIT.MKP8 = _INT_PROCESSING_DISABLED;
    INTC2.ICP8.BIT.RFP8 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP8.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP12_Start
* Description  : This function enables INTP12 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP12_Start(void)
{
    /* Clear INTP12 request and enable operation */
    INTC2.ICP12.BIT.RFP12 = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICP12.BIT.MKP12 = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_INTC_INTP12_Stop
* Description  : This function disables INTP12 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_INTC_INTP12_Stop(void)
{
    /* Disable INTP12 operation and clear request */
    INTC2.ICP12.BIT.MKP12 = _INT_PROCESSING_DISABLED;
    INTC2.ICP12.BIT.RFP12 = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICP12.UINT16;
    __syncp();
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
