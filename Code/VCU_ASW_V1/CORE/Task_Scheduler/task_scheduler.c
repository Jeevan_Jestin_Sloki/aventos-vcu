/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : task_scheduler.c
|    Project	    : VCU
|    Module         : Task Scheduler
|    Description    : This file implements the Task_Scheduler.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 09/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef TASK_SCHEDULER_C
#define TASK_SCHEDULER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "task_scheduler.h"
#include "r_cg_userdefine.h"
#include "ostm_user.h"
#include "Config_WDT0.h"
#include "vcu.h"
#include "com_tasksched.h"
#include "Ext_WDT.h"
#include "r_cg_riic.h"
#include "Config_STBC.h"
#include "Config_CSIH0.h"
#include "Config_PORT.h"
//#include    <assert.h> 

/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 
static   bool TS_ExitSched_b   = false;
volatile bool TS_trigger_b     = false;
uint8_t data[5] = {0};
bool IIC_Tx_flag = true;
uint8_t I2CCHIPID = 0;
bool I2C_Receive_b = false;
bool I2C_Sent_b = false;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 MD_STATUS Status_Tx = 0;
 MD_STATUS Status_Rx = 0;
uint16_t Data_Tx[10] = {0x4002};
uint16_t Data_Rx[10] = {0};
bool Rx_Flag = false;
bool Tx_Flag = false;
uint32_t counter = 0;
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TaskScheduler_Start
*   Description   : This function schedules the tasks in multiple of 5ms.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void TaskScheduler_Start(void)
{
	uint8_t i = 0;
	bool flag = false;
	
//	Data_Tx[0] = 0x8C80;//0x4002,0x8004
//	PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
//	Status_Tx = R_Config_CSIH0_Send(&Data_Tx[0],1,_CSIH_SELECT_CHIP_7);
//	Status_Rx = R_Config_CSIH0_Receive(&Data_Rx[0],1,_CSIH_SELECT_CHIP_7);
//	while(!Tx_Flag);
//	Tx_Flag = false;
//	while(!Rx_Flag);
//	Rx_Flag = false;
//	PORT.P9 = ((PORT.P9) | (_PORT_Pn6_OUTPUT_HIGH));
//	PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
	
	//assert(i == 0);
	HandShakeWithWDT();
	//R_Config_STBC_Prepare_Stop_Mode();
	//R_Config_STBC_Start_Stop_Mode();
	while (!TS_ExitSched_b)
	{
		//EI();
		while(TaskScheduler_5ms_b)
		{
			TaskScheduler_5ms_b = false;
			counter++;
//			if((counter%200) == 0)
//			{
//				if(flag == false)
//				{
//					flag = true;
//					Data_Tx[0] = 0x8004;//0x4002,0x8004
//					PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
//					Status_Tx = R_Config_CSIH0_Send(&Data_Tx[0],1,_CSIH_SELECT_CHIP_7);
//					Status_Rx = R_Config_CSIH0_Receive(&Data_Rx[0],1,_CSIH_SELECT_CHIP_7);
//					while(!Tx_Flag);
//					Tx_Flag = false;
//					while(!Rx_Flag);
//					Rx_Flag = false;
//					PORT.P9 = ((PORT.P9) | (_PORT_Pn6_OUTPUT_HIGH));
//					PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
//					i++;
//				}
//				else
//				{
//					flag = false;
//					Data_Tx[0] = 0x4002;//0x4002,0x8004
//					//PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
//					Status_Tx = R_Config_CSIH0_Send(&Data_Tx[0],1,_CSIH_SELECT_CHIP_7);
//					Status_Rx = R_Config_CSIH0_Receive(&Data_Rx[1],1,_CSIH_SELECT_CHIP_7);
//					while(!Tx_Flag);
//					Tx_Flag = false;
//					while(!Rx_Flag);
//					Rx_Flag = false;
//					//PORT.P9 = ((PORT.P9) | (_PORT_Pn6_OUTPUT_HIGH));
//					//PORT.P9 = ((PORT.P9) & ~(_PORT_Pn6_OUTPUT_HIGH));
//					i++;
//				}
//			}
			//R_Config_WDT0_Restart(); /*Refresh the WDT every-time within 34.13ms*/
			
			Diag_TS_Proc_5ms();
			
			ReadUserInput();
			
			VCU_Proc();
			
//			if(true == IIC_Tx_flag)
//			{
//				IIC_Tx_flag = false;
//				R_Config_RIIC1_Master_Send(0x18,&data[0],0x01);
//				while(I2C_Sent_b == false);
//				I2C_Sent_b = false;
//				R_Config_RIIC1_Master_Receive(0x18, I2CCHIPID, 1U);
//				while(I2C_Receive_b == false);
//				I2C_Receive_b = false;
//			}
			
		}
	}

	return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TS_PowerOff
*   Description   : This function stops the tasks and try to shutdown the ECU
*					or put it into sleep.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void TS_PowerOff(void)
{
	/*EEPROM*/
	RESCTL.SWRESA = 0x01U;
	return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TasksScheduler_Exit
*   Description   : This function exit the task scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void TasksScheduler_Exit(void)
{
	TS_ExitSched_b = true;
	return;
}

#endif /* TASK_SCHEDULER_C */
/*---------------------- End of File -----------------------------------------*/