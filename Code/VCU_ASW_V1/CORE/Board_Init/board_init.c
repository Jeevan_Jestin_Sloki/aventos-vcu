/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : board_init.c
|    Project	    : VCU
|    Module         : Board Initialisation
|    Description    : This file contains the variables and functions to
|                     initialize the Hardware and Software Components.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 09/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BOARD_INIT_C
#define BOARD_INIT_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "board_init.h"
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_OSTM0.h"
#include "Config_ADCA0.h"
#include "Config_INTC.h"
#include "Config_WDT0.h"
#include "Config_PORT.h"
#include "Config_RIIC1.h"
#include "Config_UART1.h"
#include "Config_RTCA0.h"
#include "Config_UART0.h"
#include "r_cg_cgc.h"
#include "can_driver.h"
#include "Config_STBC.h"
#include "Config_CSIH0.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 

 
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 
 
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Hardware_Init
*   Description   : This function initializes the Hardware peripherals.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void Hardware_Init(void)
{
	DI();
	
	R_Config_PORT_Create();
    	R_CGC_Create();
    	R_Config_OSTM0_Create();
    	R_Config_ADCA0_Create();
	R_Config_WDT0_Create();
    	R_Config_INTC_Create();
    	
    	//R_Config_RIIC1_Create();
    	R_Config_UART1_Create();
    	R_Config_RTCA0_Create();
	R_Config_UART0_Create();
	R_Pins_Create();
	CAN_Init();
	R_Config_CSIH0_Create();
//	PORT.P11 = ((PORT.P11) | (_PORT_Pn1_OUTPUT_HIGH | _PORT_Pn2_OUTPUT_HIGH | _PORT_Pn3_OUTPUT_HIGH));
//	PORT.P9 = ((PORT.P9) | (_PORT_Pn5_OUTPUT_HIGH));
	/*Initialize CAN driver here*/
	
	return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Software_Init
*   Description   : This function initailizes the Software components and 
*		            starts the hardware peripherals
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void Software_Init(void)
{
	EI();
	
	R_Config_OSTM0_Start();
	R_Config_ADCA0_ScanGroup1_Start();
	R_Config_INTC_INTP2_Start();
	R_Config_INTC_INTP4_Start();
	R_Config_INTC_INTP12_Start();
	//R_Config_RIIC1_Start();
	R_Config_UART1_Start();
	R_Config_RTCA0_Start();
	R_Config_UART0_Start();
	R_Config_CSIH0_Start();
	Diag_TS_Init(); 
	VCU_Init(); 
	return;
}



#endif /* BOARD_INIT_C */
/*---------------------- End of File -----------------------------------------*/