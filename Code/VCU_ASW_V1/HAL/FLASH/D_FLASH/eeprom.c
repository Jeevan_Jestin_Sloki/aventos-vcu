
#include "r_typedefs.h"
#include "r_fdl.h"
#include "fdl_descriptor.h"
#include "target.h"
#include "eeprom.h"



EEPROM_AddrConfig_St_t EEPROM_AddrConfig_St[EEPROM_SIGNAL_END_E] = 
{
    {EEPROM_SIGNAL_DUMMY1_E,0xFF200000,64},
    {EEPROM_SIGNAL_DUMMY2_E,0xFF200040,64},
    {EEPROM_SIGNAL_DUMMY3_E,0xFF200080,64},
    {EEPROM_SIGNAL_DUMMY4_E,0xFF2000C0,64}
};
/**
* Function name    : DFlash_Init()
* @brief           : Initialize EEPROM
* @param           : none
* @return          : bool
*/
bool DFlash_Init(void)
{
	r_fdl_status_t            fdlRet;
	r_fdl_request_t           req;
	
	DCIB.EEPRDCYCL = 0x0F;	
	
    	/* 1st initialize the FDL */
    	fdlRet = R_FDL_Init( &sampleApp_fdlConfig_enu );
    	if( R_FDL_OK != fdlRet )
    	{   
        	/* Error handler */
        	return false;
    	}	
	#ifndef R_FDL_LIB_V1_COMPATIBILITY
        /* Prepare the environment */
        req.command_enu     = R_FDL_CMD_PREPARE_ENV;
        req.idx_u32         = 0;
        req.cnt_u16         = 0;
        req.accessType_enu  = R_FDL_ACCESS_NONE;
        R_FDL_Execute( &req );
        
        while( R_FDL_BUSY == req.status_enu )
        {
            R_FDL_Handler();
        }
        if( R_FDL_OK != req.status_enu )
        {   
            /* Error handler */
	    NOP();
            return false;
        }
    #endif
    // for()
    // {

    // }
// EEPROM_AddrConfig_St
	
	return true;
}

/**
* Function name    : DFlash_Write()
* @brief           : Write data to eeprom.
* @param           : EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8.
* @return          : bool
*/
bool DFlash_Write(EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8)
{
    uint32_t Address_u32 = 0;
    uint32_t Erase_StartIndex_u32 = 0;
    uint32_t Erase_EndIndex_u32 = 0;
    uint16_t Size_u16 = 0;
    uint8_t  Data_Backup[500] = {0};
    r_fdl_request_t req;


    Size_u16 = EEPROM_AddrConfig_St[EEPROM_EntryName_En].Size_u16;
    Address_u32 = EEPROM_AddrConfig_St[EEPROM_EntryName_En].StartAddress_u32 - (uint32_t)0xFF200000;
    DFlash_Read(EEPROM_EntryName_En, &Data_Backup[0]);
    
    if ((Address_u32 % 0x04) != 0x00U)
    {
        Address_u32 = (Address_u32 - (Address_u32 % 0x04));
        //Read and write
    }
    else
    {
        /* code */
    }
    Erase_StartIndex_u32 = (Address_u32/64);
    Erase_EndIndex_u32 = (Size_u16/64);

    req.command_enu     = R_FDL_CMD_ERASE;
    req.idx_u32         = Erase_StartIndex_u32;
    req.cnt_u16         = Erase_EndIndex_u32;
    req.accessType_enu  = R_FDL_ACCESS_USER;
    R_FDL_Execute( &req );
    
    while( R_FDL_BUSY == req.status_enu )
    {
        R_FDL_Handler();
    }
    if( R_FDL_OK != req.status_enu )
    {   
        /* Error handler */
        while( 1 )
            ;
    }
    
    
    req.command_enu     = R_FDL_CMD_WRITE;
    req.idx_u32         = Address_u32;//Address_u32;
    req.cnt_u16         = Size_u16 / 4;
    req.bufAddr_u32     = (uint32_t)(&Data_u8[0]);
    req.accessType_enu  = R_FDL_ACCESS_USER;
    R_FDL_Execute( &req );
    while( R_FDL_BUSY == req.status_enu )
    {
        R_FDL_Handler();
    }
    if( R_FDL_OK != req.status_enu )
    {   
        /* Error handler */
	NOP();
        return false;
    }
    NOP();
    return true;
    
}
/**
* Function name    : DFlash_Read()
* @brief           : Read data From eeprom.
* @param           : EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8.
* @return          : bool
*/
bool DFlash_Read(EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8)
{
    uint32_t Address_u32 = 0;
    uint16_t Size_u16 = 0;
    r_fdl_request_t req;

    Size_u16 = EEPROM_AddrConfig_St[EEPROM_EntryName_En].Size_u16;
    Address_u32 = EEPROM_AddrConfig_St[EEPROM_EntryName_En].StartAddress_u32 - (uint32_t)0xFF200000;

    if ((Address_u32 % 0x04) != 0x00U)
    {
        Address_u32 = (Address_u32 - (Address_u32 % 0x04));
        //Read and write
    }
    else
    {
        /* code */
    }

    req.command_enu     = R_FDL_CMD_READ;
    req.idx_u32         = Address_u32;
    req.cnt_u16         = Size_u16 / 4;
    req.bufAddr_u32     = (uint32_t)(&Data_u8[0]);
    req.accessType_enu  = R_FDL_ACCESS_USER;
    R_FDL_Execute( &req );
    while( R_FDL_BUSY == req.status_enu )
    {
        R_FDL_Handler();
    }
    if( R_FDL_OK != req.status_enu )
    {   
        /* Error handler */
        return false;
    }
    return true;
    
}
/**
* Function name    : Write()
* @brief           : Write data to eeprom.
* @param           : uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8.
* @return          : bool
*/
bool Write(uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8)
{
    uint32_t Local_Address_u32 = 0;
    uint32_t Erase_StartIndex_u32 = 0;
    uint32_t Erase_EndIndex_u32 = 0;
    uint8_t Differ_Index_u8 = 0;
    uint16_t Local_Size_u16 = 0;
    uint16_t Local_index_u16 = 0;
    uint8_t Local_Data_buff_u8[0x100] = {0};
    r_fdl_request_t req;

    if (0x00U != (Address_u32 % 0x04))
    {
        Local_Address_u32 = (Address_u32 - (Address_u32 % 0x04));
        Local_Size_u16 = Size_u16 + (Address_u32 % 0x04);
        Differ_Index_u8 = (Address_u32 % 0x04);
        //Read and write
    }
    else
    {
        Local_Address_u32 = Address_u32;
        Local_Size_u16 = Size_u16;
        Differ_Index_u8 = 0;
    }

    Read(Local_Address_u32, (Local_Size_u16 + (Local_Size_u16 % 64)), &Local_Data_buff_u8[0]);

    if ((0x00U != (Local_Size_u16 % 0x04)) || (0x04 > Local_Size_u16))
    {
        Local_Size_u16 = (Local_Size_u16 / 0x04) + 1;
    }
    else
    {
        Local_Size_u16 = (Local_Size_u16 / 0x04);
    }

    
    for (Local_index_u16 = 0; Local_index_u16 < Size_u16; Local_index_u16++)
    {
        Local_Data_buff_u8[Local_index_u16 + Differ_Index_u8] = Data_u8[Local_index_u16];
    }

    


    if ((Address_u32 % 0x04) != 0x00U)
    {
        Address_u32 = (Address_u32 - (Address_u32 % 0x04));
        //Read and write
    }
    else
    {
        /* code */
    }
    Local_Address_u32 = Local_Address_u32 - 0xFF200000;
    Erase_StartIndex_u32 = (Local_Address_u32/64);
    if (0 == ((Local_Address_u32 + (Local_Size_u16 * 0x04)) / 64))
    {
        Erase_EndIndex_u32 = ((Local_Address_u32 + (Local_Size_u16 * 0x04)) / 64);
    }
    else
    {
        Erase_EndIndex_u32 = ((Local_Address_u32 + (Local_Size_u16 * 0x04)) / 64) + 1;
    }

    req.command_enu     = R_FDL_CMD_ERASE;
    req.idx_u32         = Erase_StartIndex_u32;
    req.cnt_u16         = Erase_EndIndex_u32;
    req.accessType_enu  = R_FDL_ACCESS_USER;
    R_FDL_Execute( &req );
    
    while( R_FDL_BUSY == req.status_enu )
    {
        R_FDL_Handler();
    }
    if( R_FDL_OK != req.status_enu )
    {   
        /* Error handler */
        return false;
    }
    
    
    req.command_enu     = R_FDL_CMD_WRITE;
    req.idx_u32         = Local_Address_u32;//Address_u32;
    req.cnt_u16         = Local_Size_u16;
    req.bufAddr_u32     = (uint32_t)(&Local_Data_buff_u8[0]);
    req.accessType_enu  = R_FDL_ACCESS_USER;
    R_FDL_Execute( &req );
    while( R_FDL_BUSY == req.status_enu )
    {
        R_FDL_Handler();
    }
    if( R_FDL_OK != req.status_enu )
    {   
        /* Error handler */
	    NOP();
        return false;
    }
    NOP();
    return true;
    
}
uint32_t DUMMY = 0;
/**
* Function name    : Read()
* @brief           : Read data From eeprom.
* @param           : uint32_t Address_u32, uint16_t Size, uint8_t *Data_u8.
* @return          : bool
*/
bool Read(uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8)
{
    uint32_t Local_Address_u32 = 0;
    uint8_t Differ_Index_u8 = 0;
    uint16_t Local_Size_u16 = 0;
    uint16_t Local_index_u16 = 0;
    uint16_t Local_Buff_index_u16 = 0;
    uint8_t Local_Data_buff_u8[0x100] = {0};
    r_fdl_request_t req;

    
    if (0x00U != (Address_u32 % 0x04))
    {
        Local_Address_u32 = (Address_u32 - (Address_u32 % 0x04));
        Local_Size_u16 = Size_u16 + (Address_u32 % 0x04);
        Differ_Index_u8 = (Address_u32 % 0x04);
        //Read and write
    }
    else
    {
        Local_Address_u32 = Address_u32;
        Local_Size_u16 = Size_u16;
        Differ_Index_u8 = 0;
    }

    if ((0x00U != (Local_Size_u16 % 0x04)) || (0x04 > Local_Size_u16))
    {
        Local_Size_u16 = (Local_Size_u16 / 0x04) + 1;
    }
    else
    {
        Local_Size_u16 = (Local_Size_u16 / 0x04);

    }

    Local_Address_u32 = Local_Address_u32 - 0xFF200000;

    for (Local_index_u16 = 0; Local_index_u16 < Local_Size_u16; Local_index_u16++)
    {

        req.command_enu = R_FDL_CMD_BLANKCHECK;
        req.idx_u32 = Local_Address_u32;
        req.cnt_u16 = 1;
        req.accessType_enu = R_FDL_ACCESS_USER;
        R_FDL_Execute(&req);

        while (R_FDL_BUSY == req.status_enu)
        {
            R_FDL_Handler();
        }

        if (R_FDL_OK == req.status_enu)
        {
            /* The half word is blank... we may not read */
        }
        else if (R_FDL_ERR_BLANKCHECK == req.status_enu)
        {
            /* Read the data */
            req.command_enu = R_FDL_CMD_READ;
            req.idx_u32 = Local_Address_u32;
            req.cnt_u16 = 1;
            req.bufAddr_u32 = (uint32_t)(&Data_u8[Local_Buff_index_u16]);
            req.accessType_enu = R_FDL_ACCESS_USER;
            R_FDL_Execute(&req);
            while (R_FDL_BUSY == req.status_enu)
            {
                R_FDL_Handler();
            }
            if (R_FDL_OK != req.status_enu)
            {
                /* Error handler */
		NOP();
		DUMMY = 50;
                return false;
            }
        }
        else
        {
	    NOP();
	    DUMMY = 50;
            return false;
            /* Error handler.. on an internal error */
        }
        Local_Size_u16 -= 1;
        Local_Address_u32 += 4;
        Local_Buff_index_u16 += 4;
    }

    return true;
    
}