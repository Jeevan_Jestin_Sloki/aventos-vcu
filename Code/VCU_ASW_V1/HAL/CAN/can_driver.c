 /*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : can_driver.c
|    Project	    : VCU
|    Module         : CAN Driver
|    Description    : 
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 09/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CAN_DRIVER_C
#define CAN_DRIVER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "can_driver.h"
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Communicator.h"
#include "diag_typedefs.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/
#define TRUE 1
#define FALSE 0

#define DEBUG_FIFO_RX_GLOBAL  FALSE
#define DEBUG_FIFO_RX_TX      TRUE
#define DEBUG_RX_BUFFER       FALSE


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 
unsigned int i = 0;
unsigned char RX_Fifo_empty,RX_fifo_full,RX_Fifo_msg_lost = 0; 

// const Rx_SetRules_St_t Rx_SetRules_St[11U] = 
// {
//    {CAN0,0x07F0,0x1FFFFFFF,0x8U,0x80U,1U},
//    {CAN0,0x07DF,0x1FFFFFFF,0x8U,0x81U,2U},   
//    {CAN1,0x0150,0x1FFFFFFF,0x8U,0x80U,1U},
//    {CAN1,0x0200,0x1FFFFFFF,0x8U,0x81U,2U},
//    {CAN1,0x0250,0x1FFFFFFF,0x8U,0x80U,1U},
//    {CAN1,0x0650,0x1FFFFFFF,0x8U,0x81U,2U},
//    {CAN1,0x0750,0x1FFFFFFF,0x8U,0x80U,1U},
//    {CAN2,0x02AA,0x1FFFFFFF,0x8U,0x81U,2U},//CAN2
//    {CAN2,0x04AA,0x1FFFFFFF,0x8U,0x80U,1U},
//    {CAN2,0x04AB,0x1FFFFFFF,0x8U,0x81U,2U},
//    {CAN2,0x03AA,0x1FFFFFFF,0x8U,0x80U,1U},
// };
Rx_SetRules_St_t Rx_SetRules_St[] = 
{
   {CAN0,0x07F0,0x1FFFFFFF,0x8U,0x81U,1U},
   {CAN0,0x07DF,0x1FFFFFFF,0x8U,0x81U,2U},   
   {CAN1,0x0150,0x1FFFFFFF,0x8U,0x81U,8U},
   {CAN1,0x0200,0x1FFFFFFF,0x8U,0x81U,16U},
   {CAN1,0x0250,0x1FFFFFFF,0x8U,0x81U,16U},
   {CAN1,0x0650,0x1FFFFFFF,0x8U,0x81U,32U},
   {CAN1,0x0750,0x1FFFFFFF,0x8U,0x81U,32U},
   {CAN2,0x02AA,0x1FFFFFFF,0x8U,0x81U,64U},//CAN2
   {CAN2,0x04AA,0x1FFFFFFF,0x8U,0x81U,64U},
   {CAN2,0x04AB,0x1FFFFFFF,0x8U,0x81U,128U},
   {CAN2,0x03AA,0x1FFFFFFF,0x8U,0x81U,128U},
};
unsigned char 	rx_msg_DLC 		= 0x00U;
unsigned int 	rx_msg_ID 		= 0;

// RX_fifo_data Recd_RX_fifo_data_arr[8];
 
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 
 
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_Init
*   Description   : 
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void CAN_Init(void)
{
   uint8_t k = 0;
   volatile uint32_t *ptr;


   //Global Status Register
   while((RCFDC0.CFDGSTS.UINT8[LL] && 0x04)==0x04); //wait until CAN RAM is
                                                    //initialized
                                                    //global stop status flag
   //Global Control Register 
   RCFDC0.CFDGCTR.UINT8[LL]      &= 0xFB;   //Transition to global reset mode 
                                            //from global stop mode
               //GSLPR-->0 other than stop mode
               //GSLPR-->1 stop mode
   //Channel Control Register
   RCFDC0.CFDC0CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
   //todo: sandeep k y
   RCFDC0.CFDC1CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
               //CSLPR-->0 other than stop mode
               //CSLPR-->1 stop mode
   RCFDC0.CFDC2CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
               //CSLPR-->0 other than stop mode
               //CSLPR-->1 stop mode
     
//-------- Baud settings for 40MHz clock --------------------------------------
#if 0
   
   RSCAN0.GCFG.UINT32 = 0x00;      // timestamp, clock selection(clkc-->40MHz), Priority on ID

   //RSCAN0.C0CFG.UINT32 = 0x00230027;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140027;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230013;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140013;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230009;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140009;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230004;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   RSCAN0.C0CFG.UINT32 = 0x00140004;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
#endif

//---------- Baud settings for 16MHz clock -------------------------------------
#if 0   
        //Global Configuration Register
   RCFDC0.CFDGCFG.UINT32 = 0x10;   //xin clock selected
   
   //RSCAN0.C0CFG.UINT32 = 0x00230007;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140007;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //Channel CAN FD Configuration Register
   RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
   
   
   //Channel Data Bit Rate Configuration Register
   //RCFDC0.CFDC0DCFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RCFDC0.CFDC0NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   RCFDC0.CFDC0NCFG.UINT32 = 0x04090801;     // 500kbps, 16Tq, 1,4,9, 68.75% sampling
                              
   
   //RCFDC0.CFDC0FDCFG.UINT32 = 0x00230003;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140003;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140001;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230000;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140000;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
   
#endif   

//---------- Baud settings for 8MHz clock -------------------------------------
#if 1   
        //Global Configuration Register
   RCFDC0.CFDGCFG.UINT32 = 0x10;   //xin clock selected
   
   //RSCAN0.C0CFG.UINT32 = 0x00230007;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140007;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //Channel CAN FD Configuration Register
   RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
   //todo: sandeep k y
   RCFDC0.CFDC1FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
   //todo: sandeep k y
   RCFDC0.CFDC2FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
      

   
   //Channel Data Bit Rate Configuration Register
   RCFDC0.CFDC0NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //todo: sandeep k y
   RCFDC0.CFDC1NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //todo: sandeep k y
   RCFDC0.CFDC2NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
                                          
   
   //RCFDC0.CFDC0FDCFG.UINT32 = 0x00230003;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140003;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140001;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230000;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140000;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
   
#endif   

//--------- Receive Rule settings for RX FIFO -------------------------------------

        //Receive Rule Configuration Register
   RCFDC0.CFDGAFLCFG0.UINT32 = 0x00U;    // No. of rules for channel 0 //4

   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      if(CAN0 == Rx_SetRules_St[k].CAN_Module)
      {
         RCFDC0.CFDGAFLCFG0.UINT8[HH]++;// No. of rules for channel 0 //4
      }
      else if (CAN1 == Rx_SetRules_St[k].CAN_Module)
      {
         RCFDC0.CFDGAFLCFG0.UINT8[HL]++;// No. of rules for channel 1 //4
      }
      else if (CAN2 == Rx_SetRules_St[k].CAN_Module)
      {
         RCFDC0.CFDGAFLCFG0.UINT8[LH]++;// No. of rules for channel 2 //4  
      }
      else
      {
         /* code */
      }
      
   }   
      //Receive rule entry control register
   //RSCAN0.GAFLECTR.UINT8[1] = 0x01;   // Enable write to receive rule table
   RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x01;   // Enable write to receive rule table   
   //RSCAN0.GAFLECTR.UINT8[0] = 0x00;   // receive rule page no.configuration
   RCFDC0.CFDGAFLECTR.UINT8[LL] = 0x00;   // receive rule page no.configuration

   ptr = (volatile uint32_t*)&(RCFDC0.CFDGAFLID0.UINT32);
   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      *ptr = Rx_SetRules_St[k].CAN_ID;
      ptr++;
      *ptr = Rx_SetRules_St[k].CAN_ID_MASK;
      ptr++;
      *ptr = ((Rx_SetRules_St[k].BUFFER_NUMBER << 0x08U) | Rx_SetRules_St[0].CAN_DLC);
      ptr++;
      #if (TRUE == DEBUG_FIFO_RX_GLOBAL)
      *ptr = Rx_SetRules_St[k].Recive_Fifo;
      #elif(TRUE == DEBUG_FIFO_RX_TX)
      *ptr = Rx_SetRules_St[k].Recive_Fifo << 0x08U;
      #endif
      ptr++;
   }

        //Receive rule entry control register
   RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x00;   // Disable write to receive rule table
#if (TRUE == DEBUG_FIFO_RX_GLOBAL)   
   RCFDC0.CFDRFCC0.UINT16[L] = 0x1102;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
                                          // RFDC =0x010 Receive FIFO Buffer Depth Configuration 
                      // RFPLS = 0x111 Receive FIFO Buffer Payload Storage Size Select
                      // RFIE =1 Receive FIFO Interrupt Enable
   RCFDC0.CFDRFCC1.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC2.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC3.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC4.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC5.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC6.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
   RCFDC0.CFDRFCC7.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)                   
#endif
//-----------Transmit/receive FIFO settings in Receive mode -----------------
#if (TRUE == DEBUG_FIFO_RX_TX)
   RCFDC0.CFDCFCC0.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC1.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC2.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC3.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC4.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC5.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC6.UINT32 = 0x00601002;      // Receive mode selected
   RCFDC0.CFDCFCC7.UINT32 = 0x00601002;      // Receive mode selected
#endif

//--------- Receive Buffer settings -------------------------------------
// #if (TRUE == DEBUG_RX_BUFFER)
   RCFDC0.CFDRMNB.UINT8[0] = 0x18;  // no. of receive buffers
   RCFDC0.CFDRMNB.UINT8[1] = 0x00U; // payload size is 8 bytes
// #endif
   
//----------- Global Interrupt -------------------------------------
#if(TRUE == DEBUG_FIFO_RX_GLOBAL)
   RCFDC0.CFDGCTR.UINT8[LH] = 0x07;      // Transmit history interrupt, FIFO msg lost interrupt & DLC error interrupt enabled
#endif

//------------ Interrupt setting -----------------------------------

//NDG commented
   INTC1.ICRCAN0ERR.BIT.TBRCAN0ERR = 1;      // 0-Direct jump method, 1-vector method
   INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = 0;
   INTC1.ICRCAN0ERR.BIT.RFRCAN0ERR = 0;
    
   INTC1.ICRCAN0TRX.BIT.TBRCAN0TRX = 1;
   INTC1.ICRCAN0TRX.BIT.MKRCAN0TRX = 0;
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
 
   INTC1.ICRCAN0REC.BIT.TBRCAN0REC = 1;
   INTC1.ICRCAN0REC.BIT.MKRCAN0REC = 0;
   INTC1.ICRCAN0REC.BIT.RFRCAN0REC = 0;
   
   
   
   //todo: sandeep k y
   INTC2.ICRCAN1ERR.BIT.TBRCAN1ERR = 1;      // 0-Direct jump method, 1-vector method
   INTC2.ICRCAN1ERR.BIT.MKRCAN1ERR = 0;
   INTC2.ICRCAN1ERR.BIT.RFRCAN1ERR = 0;
    
   INTC2.ICRCAN1TRX.BIT.TBRCAN1TRX = 1;
   INTC2.ICRCAN1TRX.BIT.MKRCAN1TRX = 0;
   INTC2.ICRCAN1TRX.BIT.RFRCAN1TRX = 0;
 
   INTC2.ICRCAN1REC.BIT.TBRCAN1REC = 1;
   INTC2.ICRCAN1REC.BIT.MKRCAN1REC = 0;
   INTC2.ICRCAN1REC.BIT.RFRCAN1REC = 0;
   
   INTC2.ICRCAN2ERR.BIT.TBRCAN2ERR = 1;      // 0-Direct jump method, 1-vector method
   INTC2.ICRCAN2ERR.BIT.MKRCAN2ERR = 0;
   INTC2.ICRCAN2ERR.BIT.RFRCAN2ERR = 0;
    
   INTC2.ICRCAN2TRX.BIT.TBRCAN2TRX = 1;
   INTC2.ICRCAN2TRX.BIT.MKRCAN2TRX = 0;
   INTC2.ICRCAN2TRX.BIT.RFRCAN2TRX = 0;
 
   INTC2.ICRCAN2REC.BIT.TBRCAN2REC = 1;
   INTC2.ICRCAN2REC.BIT.MKRCAN2REC = 0;
   INTC2.ICRCAN2REC.BIT.RFRCAN2REC = 0;
   
   
   
   INTC1.ICRCANGERR0.BIT.TBRCANGERR0 = 1;
   INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = 0;
   INTC1.ICRCANGERR0.BIT.RFRCANGERR0 = 0;

   INTC1.ICRCANGRECC0.BIT.TBRCANGRECC0 = 1;
   INTC1.ICRCANGRECC0.BIT.MKRCANGRECC0 = 0;
   INTC1.ICRCANGRECC0.BIT.RFRCANGRECC0 = 0;
      
   
//------------ Operating mode --------------------------------------
   RCFDC0.CFDGCTR.UINT8[0] = 0x00;      //Other than global stop mode
                                           //Global operating mode
   for(i=0;i<0xfff;i++);   //wait for transistion
   
 #if(TRUE == DEBUG_FIFO_RX_GLOBAL)    
        
   //Set RFE bit in global operating mode 
   RCFDC0.CFDRFCC0.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC1.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC2.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC3.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC4.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC5.UINT8[0] |= 0x01;      // receive FIFO is used   
   RCFDC0.CFDRFCC6.UINT8[0] |= 0x01;      // receive FIFO is used
   RCFDC0.CFDRFCC7.UINT8[0] |= 0x01;      // receive FIFO is used      

   for(i=0;i<0xfff;i++);   //wait for transistion
 
#endif
   //commented because currently not using FIFO
#if(TRUE == DEBUG_FIFO_RX_TX)
   RCFDC0.CFDCFCC0.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC1.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC2.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC3.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC4.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC5.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC6.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   RCFDC0.CFDCFCC7.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
   for(i=0;i<0xfff;i++);   //wait for transistion
#endif
   RCFDC0.CFDC0CTR.UINT8[0] = 0x00;   //other than channel stop mode
                                           //channel communication mode
   //todo: sandeep k y
   RCFDC0.CFDC1CTR.UINT8[0] = 0x00;   //other than channel stop mode
                                           //channel communication mode
   //todo: sandeep k y
   RCFDC0.CFDC2CTR.UINT8[0] = 0x00;   //other than channel stop mode
                                           //channel communication mode
   for(i=0;i<0xfff;i++);   //wait for transistion

}


/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN0_Transmit
*   Description   : 
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void CAN0_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data)
{
   static bool can_flag = false;
   uint8_t i = 0;
   //----------- trnasmit using transmit buffer ---------------------------------------------
   if (can_flag == false)
   {
      if (!(RCFDC0.CFDTMSTS0 & 0x01))
      {
         /* Clear Tx buffer status */
         //do
         //{
         //   RCFDC0.CFDTMSTS1 = 0U;  //CAN_CLR =0
         //}  while(RCFDC0.CFDTMSTS1 != 0U);

         //Transmit buffer interrupt enable configuration register
         //RCFDC0.CFDTMIEC0.UINT8[0] = 0x02;      // Transmit Buffer 0 interrupt enabled
         RCFDC0.CFDTMIEC0.UINT32 = 0xFFFFFFFF;
         //Transmit buffer ID register
         RCFDC0.CFDTMID0.UINT32 = CanId; // Transmit message, standard data frame,
         //Transmit buffer pointer CFDTMSTS0
         //RCFDC0.CFDTMPTR0.UINT16[1] = 0x8012;   // 8 data bytes, label value as 0x12
         RCFDC0.CFDTMPTR0.UINT8[2] = 0x12;
         RCFDC0.CFDTMPTR0.UINT8[3] = data_len << 4U;
         //Transmit buffer data field register
         //RCFDC0.CFDTMDF0_0.UINT32 =  0x01000000;   // data bytes 0 to 3
         //RCFDC0.CFDTMDF1_0.UINT32 = 0x00000000;   // data bytes 4 to 7
         for (i = 0; i < data_len; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  RCFDC0.CFDTMDF0_0.UINT8[i] = data[i];
               }
               else
               {
                  RCFDC0.CFDTMDF1_0.UINT8[i - 4] = data[i];
               }
            }
         }
         //Transmit buffer status register
         if ((RCFDC0.CFDTMSTS0 & 0x01) == 0x00) //check if no other transmit request is present or transmission is in progress
            RCFDC0.CFDTMC0 = 0x01;              // Transmission requested - TMTR bit
         //tx_done = 0;
      }
      can_flag = true;
   }
   //----------- trnasmit using transmit buffer ---------------------------------------------
   else
   {
      if (!(RCFDC0.CFDTMSTS1 & 0x01))
      {
         /* Clear Tx buffer status */
         //do
         //{
         //   RCFDC0.CFDTMSTS1 = 0U;  //CAN_CLR =0
         //}  while(RCFDC0.CFDTMSTS1 != 0U);

         //Transmit buffer interrupt enable configuration register
         //RCFDC0.CFDTMIEC0.UINT8[0] = 0x02;      // Transmit Buffer 0 interrupt enabled
         RCFDC0.CFDTMIEC0.UINT32 = 0xFFFFFFFF;
         //Transmit buffer ID register
         RCFDC0.CFDTMID1.UINT32 = CanId; // Transmit message, standard data frame,
         //Transmit buffer pointer CFDTMSTS0
         //RCFDC0.CFDTMPTR0.UINT16[1] = 0x8012;   // 8 data bytes, label value as 0x12
         RCFDC0.CFDTMPTR1.UINT8[2] = 0x12;
         RCFDC0.CFDTMPTR1.UINT8[3] = data_len << 4U;
         //Transmit buffer data field register
         //RCFDC0.CFDTMDF0_0.UINT32 =  0x01000000;   // data bytes 0 to 3
         //RCFDC0.CFDTMDF1_0.UINT32 = 0x00000000;   // data bytes 4 to 7
         for (i = 0; i < data_len; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  RCFDC0.CFDTMDF0_1.UINT8[i] = data[i];
               }
               else
               {
                  RCFDC0.CFDTMDF1_1.UINT8[i - 4] = data[i];
               }
            }
         }
         //Transmit buffer status register
         if ((RCFDC0.CFDTMSTS1 & 0x01) == 0x00) //check if no other transmit request is present or transmission is in progress
         {
		    RCFDC0.CFDTMC1 = 0x01;              // Transmission requested - TMTR bit
		 }
         //tx_done = 0;
      }
      can_flag = false;
   }
}


/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN1_Transmit
*   Description   : 
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void CAN1_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data)
{
   uint8_t i = 0;
//----------- trnasmit using transmit buffer ---------------------------------------------   
   if(!(RCFDC0.CFDTMSTS33 & 0x01))
   {       
//      Transmit buffer interrupt enable configuration register
      RCFDC0.CFDTMIEC1.UINT32 = 0xFFFFFFFF;      // Transmit Buffer 0 interrupt enabled
      
 //     Transmit buffer ID register
      RCFDC0.CFDTMID33.UINT32 = CanId;   // Transmit message, standard data frame, 
 //     Transmit buffer pointer CFDTMSTS0
      RCFDC0.CFDTMPTR33.UINT16[1] = 0x8012;   // 8 data bytes, label value as 0x12
      RCFDC0.CFDTMPTR33.UINT8[2]  = 0x12;
      RCFDC0.CFDTMPTR33.UINT8[3]  = data_len<<4U;
 //     Transmit buffer data field register
      for(i = 0; i < data_len; i++)
      {
         if(i < 8)
         {
            if(i < 4)
            {
               RCFDC0.CFDTMDF0_33.UINT8[i] = data[i];
               
            }
            else
            {
               RCFDC0.CFDTMDF1_33.UINT8[i-4] = data[i];
            }
         }

      }
 //     Transmit buffer status register
      if((RCFDC0.CFDTMSTS33 &0x01) == 0x00)   //check if no other transmit request is present or transmission is in progress
      RCFDC0.CFDTMC33 = 0x01;         // Transmission requested - TMTR bit
 //     tx_done = 0;
   }
}




/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN2_Transmit
*   Description   : 
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void CAN2_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data)
{
   uint8_t i = 0;
//----------- trnasmit using transmit buffer ---------------------------------------------   
   if(!(RCFDC0.CFDTMSTS64 & 0x01))
   {       
//      Transmit buffer interrupt enable configuration register
      RCFDC0.CFDTMIEC2.UINT32 = 0xFFFFFFFF;      // Transmit Buffer 0 interrupt enabled
      
 //     Transmit buffer ID register
      RCFDC0.CFDTMID64.UINT32 = CanId;   // Transmit message, standard data frame, 
 //     Transmit buffer pointer CFDTMSTS0
      RCFDC0.CFDTMPTR64.UINT16[1] = 0x8012;   // 8 data bytes, label value as 0x12
      RCFDC0.CFDTMPTR64.UINT8[2]  = 0x12;
      RCFDC0.CFDTMPTR64.UINT8[3]  = data_len<<4U;
 //     Transmit buffer data field register
      for(i = 0; i < data_len; i++)
      {
         if(i < 8)
         {
            if(i < 4)
            {
               RCFDC0.CFDTMDF0_64.UINT8[i] = data[i];
               
            }
            else
            {
               RCFDC0.CFDTMDF1_64.UINT8[i-4] = data[i];
            }
         }

      }
 //     Transmit buffer status register
      if((RCFDC0.CFDTMSTS64 &0x01) == 0x00)   //check if no other transmit request is present or transmission is in progress
      RCFDC0.CFDTMC64 = 0x01;         // Transmission requested - TMTR bit
 //     tx_done = 0;
   }
}



/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_Transmit
*   Description   : 
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void CAN_Transmit( uint8_t Can_modude, uint32_t CanId, uint8_t data_len, uint8_t *data)
{
	
      if(CAN0 == Can_modude)
      {
         CAN0_Transmit(CanId,data_len,data);
      }
      else if (CAN1 == Can_modude)
      {
         CAN1_Transmit(CanId,data_len,data);
      }
      else if (CAN2 == Can_modude)
      {
         CAN2_Transmit(CanId,data_len,data); 
      }
      else
      {
         ;/* code */
      }
	
}



void my_can0_rx(void)
{
   uint8_t data[8] = {0};
   uint8_t k = 0;
//-------------------- Rx by reading Message buffer-------------------------------------------------------   
// #if 1
//    if((RCFDC0.CFDRMND0.UINT8[0] & 0x10) == 0x10)   // check if RMNS bit of RMND register is set to '1'
//    {
//       RCFDC0.CFDRMND0.UINT8[0] &= 0xEF;      // clear RMNS bit
      
//       rx_msg_ID = RCFDC0.CFDRMID4.UINT16[0];      // read received message ID
//       rx_msg_DLC = (RCFDC0.CFDRMPTR4.UINT8[3] & 0xF0) >> 4;   // read received message data length
      
//       rx_msg_data0 = RCFDC0.CFDRMDF0_4.UINT32;
//       rx_msg_data1 = RCFDC0.CFDRMDF1_4.UINT32;
      
//    }
//    if((RCFDC0.CFDRMND0.UINT8[0] & 0x20) == 0x20)   // check if RMNS bit of RMND register is set to '1'
//    {
//       RCFDC0.CFDRMND0.UINT8[0] &= 0xDF;      // clear RMNS bit
      
//       if(RCFDC0.CFDRMID5.UINT8[3] & 0x80 == 0x80 )    // check if extended or standard ID
//       {
//       rx_msg_ID = RCFDC0.CFDRMID5.UINT16[0];      // read received message ID
//       }
//       else
//       {
//          //return error as the expected ID is extended   
//          __nop();
//       }
//       rx_msg_DLC = (RCFDC0.CFDRMPTR5.UINT8[3] & 0xF0) >> 4;   // read received message data length
      
//       rx_msg_data0 = RCFDC0.CFDRMDF0_5.UINT32;
//       rx_msg_data1 = RCFDC0.CFDRMDF1_5.UINT32;
      
//    }
// #endif
//-------------------- Rx by reading Rx FIFO buffer-------------------------------------------------------   
#if 1
   //RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7; // CLEAR RFIF flag
   if ((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7; // CLEAR RFIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDRFID0.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  
                  data[i] = RCFDC0.CFDRFDF0_0.UINT8[i];

               }
               else
               {
                  data[i] = RCFDC0.CFDRFDF1_0.UINT8[i - 4];

               }
            }
         }

         RCFDC0.CFDRFPCTR0.UINT8[0] = 0xFF;
      } while ((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01); // check if FIFO empty
   }
   else if ((RCFDC0.CFDRFSTS1.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDRFSTS1.UINT8[0] &= 0xF7; // CLEAR RFIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDRFID1.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDRFPTR1.UINT8[3] & 0xf0) >> 4;

         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDRFDF0_1.UINT8[i];

               }
               else
               {
                  data[i] = RCFDC0.CFDRFDF1_1.UINT8[i - 4];

               }
            }
         }

         RCFDC0.CFDRFPCTR1.UINT8[0] = 0xFF;
      } while ((RCFDC0.CFDRFSTS1.UINT8[0] & 0x01) != 0x01); // check if FIFO empty
   }
   
#endif

//-------------------- Rx by reading Tx/Rx FIFO buffer-------------------------------------------------------   
#if 0
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xF7;   // CLEAR CFRXIF flag
   if((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      do
      {
         rx_msg_ID = RCFDC0.CFDCFID0.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR0.UINT8[3] & 0xf0) >> 4;
         
         rx_msg_data0 = RCFDC0.CFDCFDF0_0.UINT32;
         rx_msg_data1 = RCFDC0.CFDCFDF1_0.UINT32;
         
         RCFDC0.CFDCFPCTR0.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01);
   }
#endif

   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      if (rx_msg_ID == Rx_SetRules_St[k].CAN_ID)
      {
         DRV_HAL2CIL_CallBack(0, rx_msg_DLC, Rx_SetRules_St[k].CAN_Module, rx_msg_ID, &data[0]);
         break;
      }
   }

//--------------------------------------------------------------------------------------------------------
}

//**************************************************************************************************************************

#pragma interrupt CAN_Global_Error_ISR(enable=false, channel=22, fpu=true, callt=false)
 void CAN_Global_Error_ISR(void)
{
     //__nop();
     if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x04)  // check if FIFO message is lost
     {
        RX_Fifo_msg_lost = 1;
        //return;
     }
     
}

//**************************************************************************************************************************

#pragma interrupt CAN_Rx_FIFO_ISR(enable=false, channel=23, fpu=true, callt=false)
 void CAN_Rx_FIFO_ISR(void)
{
     __nop();

//     rx_flag = 1;
   //   RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7;   // CLEAR RFIF flag
   //   RCFDC0.CFDRFSTS1.UINT8[0] &= 0xF7;   // CLEAR RFIF flag
   //   RCFDC0.CFDRFSTS2.UINT8[0] &= 0xF7;   // CLEAR RFIF flag          

     if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x01)  // check if FIFO empty
     {
             RX_Fifo_empty = 1;   
      //   return;
     }
     else if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x02) // check if FIFO full
     {
        RX_fifo_full = 1;     
     }
//     else if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x04)  // check if FIFO message is lost
//     {
//        RX_Fifo_msg_lost = 1;
//        return;
//     }   
  
     my_can0_rx();
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Error_ISR(enable=false, channel=24, fpu=true, callt=false)
 void CAN0_Error_ISR(void)
{
     __nop();
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Tx_Rx_FIFO_Receive_ISR(enable=false, channel=25, fpu=true, callt=false)
void CAN0_Tx_Rx_FIFO_Receive_ISR(void)
{
   uint8_t data[0x08U] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
   uint8_t k = 0;
   __nop();

//-------------------- Rx by reading Tx/Rx FIFO buffer-------------------------------------------------------   
   if((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS0.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID0.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR0.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_0.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_0.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR0.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS1.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS1.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID1.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR1.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_1.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_1.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR1.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS1.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS2.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS2.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID2.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR2.UINT8[3] & 0xf0) >> 4;
         
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_2.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_2.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR2.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS2.UINT8[0] & 0x01) != 0x01);
   }
   else
   {
      /* code */
   }
   
   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      if (rx_msg_ID == Rx_SetRules_St[k].CAN_ID)
      {
         DRV_HAL2CIL_CallBack(0, rx_msg_DLC, Rx_SetRules_St[k].CAN_Module, rx_msg_ID, &data[0]);
         break;
      }
   }
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Tx_ISR(enable=false, channel=26, fpu=true, callt=false)
 void CAN0_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   //RCFDC0.CFDTMSTS0 = 0x00;
   RCFDC0.CFDTMSTS0 = 0x00;
   RCFDC0.CFDTMSTS1 = 0x00;
   __nop();
#else                  // TX_RX_FIFO   
   //RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
   __nop();
   SET_HMI_COMM_STATUS();	
//   tx_done = 1;
     
}

//**************************************************************************************************************************

#pragma interrupt CAN1_Error_ISR(enable=false, channel=113, fpu=true, callt=false)
 void CAN1_Error_ISR(void)
{
     __nop();
}

//**************************************************************************************************************************

#pragma interrupt CAN1_Tx_Rx_FIFO_Receive_ISR(enable=false, channel=114, fpu=true, callt=false)
 void CAN1_Tx_Rx_FIFO_Receive_ISR(void)
{
   uint8_t data[0x08U] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
   uint8_t k = 0;
   __nop();

//-------------------- Rx by reading Tx/Rx FIFO buffer-------------------------------------------------------   
   if((RCFDC0.CFDCFSTS3.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS3.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID3.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR3.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_3.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_3.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR3.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS3.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS4.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS4.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID4.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR4.UINT8[3] & 0xf0) >> 4;
         
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_4.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_4.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR4.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS4.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS5.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS5.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID5.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR5.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_5.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_5.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR5.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS5.UINT8[0] & 0x01) != 0x01);
   }
   else
   {
      /* code */
   }
   
   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      if (rx_msg_ID == Rx_SetRules_St[k].CAN_ID)
      {
         DRV_HAL2CIL_CallBack(0, rx_msg_DLC, Rx_SetRules_St[k].CAN_Module, rx_msg_ID, &data[0]);
         break;
      }
   }
}

//**************************************************************************************************************************

#pragma interrupt CAN1_Tx_ISR(enable=false, channel=115, fpu=true, callt=false)
 void CAN1_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   //RCFDC0.CFDTMSTS0 = 0x00;
   RCFDC0.CFDTMSTS33 = 0x00;
   __nop();
#else                  // TX_RX_FIFO   
   //RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC2.ICRCAN1TRX.BIT.RFRCAN1TRX = 0;
   __nop();
//   tx_done = 1;
     
}

//**************************************************************************************************************************


#pragma interrupt CAN2_Error_ISR(enable=false, channel=217, fpu=true, callt=false)
 void CAN2_Error_ISR(void)
{
     __nop();
}

//**************************************************************************************************************************

#pragma interrupt CAN2_Tx_Rx_FIFO_Receive_ISR(enable=false, channel=218, fpu=true, callt=false)
 void CAN2_Tx_Rx_FIFO_Receive_ISR(void)
{
   uint8_t data[0x08U] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
   uint8_t k = 0;
   __nop();

//-------------------- Rx by reading Tx/Rx FIFO buffer-------------------------------------------------------   
   if((RCFDC0.CFDCFSTS6.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS6.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID6.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR6.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_6.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_6.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR6.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS6.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS7.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS7.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID7.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR7.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_7.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_7.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR7.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS7.UINT8[0] & 0x01) != 0x01);
   }
   else if((RCFDC0.CFDCFSTS8.UINT8[0] & 0x01) != 0x01)
   {
      RCFDC0.CFDCFSTS8.UINT8[0] &= 0xF7; // CLEAR CFRXIF flag

      do
      {
         rx_msg_ID = RCFDC0.CFDCFID8.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR8.UINT8[3] & 0xf0) >> 4;
         for (i = 0; i < rx_msg_DLC; i++)
         {
            if (i < 8)
            {
               if (i < 4)
               {
                  data[i] = RCFDC0.CFDCFDF0_8.UINT8[i];
               }
               else
               {
                  data[i] = RCFDC0.CFDCFDF1_8.UINT8[i - 4];
               }
            }
         }
         
         RCFDC0.CFDCFPCTR8.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS8.UINT8[0] & 0x01) != 0x01);
   }
   else
   {
      /* code */
   }
   
   for(k = 0; k< (sizeof(Rx_SetRules_St) / sizeof(Rx_SetRules_St[0])); k++)
   {
      if (rx_msg_ID == Rx_SetRules_St[k].CAN_ID)
      {
         DRV_HAL2CIL_CallBack(0, rx_msg_DLC, Rx_SetRules_St[k].CAN_Module, rx_msg_ID, &data[0]);
         break;
      }
   } 
}

//**************************************************************************************************************************

#pragma interrupt CAN2_Tx_ISR(enable=false, channel=219, fpu=true, callt=false)
 void CAN2_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   //RCFDC0.CFDTMSTS0 = 0x00;
   RCFDC0.CFDTMSTS33 = 0x00;
   __nop();
#else                  // TX_RX_FIFO   
   //RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC2.ICRCAN2TRX.BIT.RFRCAN2TRX = 0;
   __nop(); 
//   tx_done = 1;
     
}


#endif /* CAN_DRIVER_C */
/*---------------------- End of File -----------------------------------------*/