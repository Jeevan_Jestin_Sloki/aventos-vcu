/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_CSIH2.c
* Version      : 1.2.0
* Device(s)    : R7F701689
* Description  : This file implements device driver for Config_CSIH2.
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_CSIH2.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
extern volatile uint32_t g_cg_sync_read;
volatile uint16_t  g_csih2_tx_num;                         /* csih2 transmit data number */
volatile uint16_t * gp_csih2_tx_address;                   /* csih2 transmit buffer address */
volatile uint16_t  g_csih2_rx_num;                         /* csih2 receive data number */
volatile uint16_t * gp_csih2_rx_address;                   /* csih2 receive buffer address */
volatile uint16_t  g_csih2_total_num;                      /* csih2 transmit/receive data total times */
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_CSIH2_Create
* Description  : This function initializes the Config_CSIH2 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_CSIH2_Create(void)
{
    uint32_t tmp_port;

    CSIH2.CTL0 = _CSIH_OPERATION_CLOCK_STOP;
    /* Disable INTCSIH2IC operation and clear request */
    INTC2.ICCSIH2IC.BIT.MKCSIH2IC = _INT_PROCESSING_DISABLED;
    INTC2.ICCSIH2IC.BIT.RFCSIH2IC = _INT_REQUEST_NOT_OCCUR;
    /* Disable INTCSIH2IRE operation and clear request */
    INTC2.ICCSIH2IRE.BIT.MKCSIH2IRE = _INT_PROCESSING_DISABLED;
    INTC2.ICCSIH2IRE.BIT.RFCSIH2IRE = _INT_REQUEST_NOT_OCCUR;
    /* Set CSIH2 interrupt(INTCSIH2IC) setting */
    INTC2.ICCSIH2IC.BIT.TBCSIH2IC = _INT_TABLE_VECTOR;
    INTC2.ICCSIH2IC.UINT16 &= _INT_PRIORITY_LOWEST;
    /* Set CSIH2 interrupt(INTCSIH2IRE) setting */
    INTC2.ICCSIH2IRE.BIT.TBCSIH2IRE = _INT_TABLE_VECTOR;
    INTC2.ICCSIH2IRE.UINT16 &= _INT_PRIORITY_LOWEST;
    /* Set CSIH2 control setting */
    CSIH2.CTL1 = _CSIH_CLOCK_INVERTING_HIGH | _CSIH_INTERRUPT_TIMING_NORMAL | _CSIH_DATA_CONSISTENCY_CHECK_DISABLE | 
                 _CSIH_NO_DELAY | _CSIH_CHIPSELECT0_ACTIVE_LOW | _CSIH_HANDSHAKE_DISABLE | 
                 _CSIH_CHIPSELECT_SIGNAL_HOLD_ACTIVE | _CSIH_SLAVE_SELECT_DISABLE;
    CSIH2.CTL2 = _CSIH2_SELECT_BASIC_CLOCK;
    CSIH2.BRS0 = _CSIH2_BAUD_RATE_0;
    CSIH2.BRS1 = _CSIH2_BAUD_RATE_1;
    CSIH2.BRS2 = _CSIH2_BAUD_RATE_2;
    CSIH2.BRS3 = _CSIH2_BAUD_RATE_3;
    /* Set CSIH2 configuration setting */
    CSIH2.CFG0 = _CSIH_USED_BAUDRATE_0 | _CSIH_PARITY_NO | _CSIH_DATA_DIRECTION_MSB | _CSIH_PHASE_SELECTION_TYPE1 | 
                 _CSIH_IDLE_INSERTED_NOT_ALWAYS | _CSIH_IDLE_TIME_0 | _CSIH_HOLD_TIME_0 | 
                 _CSIH_INTER_DATA_DELAY_TIME_0 | _CSIH_SETUP_TIME_0;
    /* Synchronization processing */
    g_cg_sync_read = CSIH2.CTL1;
    __syncp();
    /* Set CSIH2SC pin */
    PORT.PIBC11 &= _PORT_CLEAR_BIT3;
    PORT.PBDC11 &= _PORT_CLEAR_BIT3;
    PORT.PM11 |= _PORT_SET_BIT3;
    PORT.PMC11 &= _PORT_CLEAR_BIT3;
    PORT.PIPC11 &= _PORT_CLEAR_BIT3;
    tmp_port = PORT.PDSC11;
    PORT.PPCMD11 = _WRITE_PROTECT_COMMAND;
    PORT.PDSC11 = (tmp_port | _PORT_SET_BIT3);
    PORT.PDSC11 = (uint32_t) ~(tmp_port | _PORT_SET_BIT3);
    PORT.PDSC11 = (tmp_port | _PORT_SET_BIT3);
    PORT.PFC11 &= _PORT_CLEAR_BIT3;
    PORT.PFCE11 &= _PORT_CLEAR_BIT3;
    PORT.PFCAE11 &= _PORT_CLEAR_BIT3;
    PORT.PIPC11 |= _PORT_SET_BIT3;
    PORT.PMC11 |= _PORT_SET_BIT3;
    /* Set CSIH2SO pin */
    PORT.PIBC11 &= _PORT_CLEAR_BIT2;
    PORT.PBDC11 &= _PORT_CLEAR_BIT2;
    PORT.PM11 |= _PORT_SET_BIT2;
    PORT.PMC11 &= _PORT_CLEAR_BIT2;
    PORT.PIPC11 &= _PORT_CLEAR_BIT2;
    tmp_port = PORT.PDSC11;
    PORT.PPCMD11 = _WRITE_PROTECT_COMMAND;
    PORT.PDSC11 = (tmp_port | _PORT_SET_BIT2);
    PORT.PDSC11 = (uint32_t) ~(tmp_port | _PORT_SET_BIT2);
    PORT.PDSC11 = (tmp_port | _PORT_SET_BIT2);
    PORT.PFC11 &= _PORT_CLEAR_BIT2;
    PORT.PFCE11 &= _PORT_CLEAR_BIT2;
    PORT.PFCAE11 &= _PORT_CLEAR_BIT2;
    PORT.PIPC11 |= _PORT_SET_BIT2;
    PORT.PMC11 |= _PORT_SET_BIT2;
    /* Set CSIH2SI pin */
    PORT.PIBC11 &= _PORT_CLEAR_BIT4;
    PORT.PBDC11 &= _PORT_CLEAR_BIT4;
    PORT.PM11 |= _PORT_SET_BIT4;
    PORT.PMC11 &= _PORT_CLEAR_BIT4;
    PORT.PMC11 |= _PORT_SET_BIT4;
    /* Set CSIH2CSS0 pin */
    PORT.PIBC9 &= _PORT_CLEAR_BIT0;
    PORT.PBDC9 &= _PORT_CLEAR_BIT0;
    PORT.PM9 |= _PORT_SET_BIT0;
    PORT.PMC9 &= _PORT_CLEAR_BIT0;
    PORT.PFC9 &= _PORT_CLEAR_BIT0;
    PORT.PFCE9 |= _PORT_SET_BIT0;
    PORT.PFCAE9 &= _PORT_CLEAR_BIT0;
    PORT.PMC9 |= _PORT_SET_BIT0;
    PORT.PM9 &= _PORT_CLEAR_BIT0;

    R_Config_CSIH2_Create_UserInit();
}

/***********************************************************************************************************************
* Function Name: R_Config_CSIH2_Start
* Description  : This function starts the Config_CSIH2 module operation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_CSIH2_Start(void)
{
    /* Enable CSIH2 operation */
	CSIH2.CTL0 = _CSIH_OPERATION_CLOCK_PROVIDE | _CSIH_TRANSMISSION_PERMIT | _CSIH_RECEPTION_PERMIT | _CSIH_DIRECTACCESS;
	/* Clear CSIH2 interrupt request and enable operation */
    INTC2.ICCSIH2IC.BIT.RFCSIH2IC = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICCSIH2IRE.BIT.RFCSIH2IRE = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICCSIH2IC.BIT.MKCSIH2IC = _INT_PROCESSING_ENABLED;
    INTC2.ICCSIH2IRE.BIT.MKCSIH2IRE = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_CSIH2_Stop
* Description  : This function stops the CSIH2 module operation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_CSIH2_Stop(void)
{
    /* Disable CSIH2 interrupt operation */
    INTC2.ICCSIH2IC.BIT.MKCSIH2IC = _INT_PROCESSING_DISABLED;
    INTC2.ICCSIH2IRE.BIT.MKCSIH2IRE = _INT_PROCESSING_DISABLED;
    /* Disable CSIH2 operation */
    CSIH2.CTL0 &= (uint8_t) ~_CSIH_RECEPTION_PERMIT;
    CSIH2.CTL0 &= (uint8_t) ~_CSIH_TRANSMISSION_PERMIT;
    CSIH2.CTL0 &= (uint8_t) ~_CSIH_OPERATION_CLOCK_PROVIDE;
    /* Synchronization processing */
    g_cg_sync_read = CSIH2.CTL0;
    __syncp();
    /* Clear CSIH2 interrupt operation */
    INTC2.ICCSIH2IC.BIT.RFCSIH2IC = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICCSIH2IRE.BIT.RFCSIH2IRE = _INT_REQUEST_NOT_OCCUR;
    /* Synchronization processing */
    g_cg_sync_read = INTC2.ICCSIH2IC.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_CSIH2_Send_Receive
* Description  : This function sends and receives CSIH2 data.
* Arguments    : tx_buf -
*                    send buffer pointer
*                tx_num -
*                    buffer size
*                rx_buf -
*                    receive buffer pointer
*                chip_id -
*                    set chip select id
* Return Value : status -
*                    MD_OK or MD_ARGERROR
***********************************************************************************************************************/
MD_STATUS R_Config_CSIH2_Send_Receive(uint16_t* const tx_buf, uint16_t tx_num, uint16_t* const rx_buf, uint32_t chip_id)
{
    MD_STATUS status = MD_OK;
    uint32_t regValue = _CSIH2_SETTING_INIT;

    if ((tx_num < 1U) || (chip_id < _CSIH_SELECT_CHIP_0) || (chip_id > _CSIH_SELECT_CHIP_3))
    {
        status = MD_ARGERROR;
    }
    else
    {
        /* Set transmit setting */
        gp_csih2_tx_address = tx_buf;
        g_csih2_total_num = tx_num;
        g_csih2_tx_num = 0U;
        /* Set receive setting */
        gp_csih2_rx_address = rx_buf;
        /* Set select chip id */
        regValue &= (~chip_id);
        if (0U != gp_csih2_tx_address)
        {
            regValue |= (*gp_csih2_tx_address);
            gp_csih2_tx_address++;
        }
        else
        {
            regValue |= 0xFFFFU;
        }
        /* Disable CSIH2 interrupt operation */
        INTC2.ICCSIH2IC.BIT.MKCSIH2IC = _INT_PROCESSING_DISABLED;
        /* Synchronization processing */
        g_cg_sync_read = INTC2.ICCSIH2IC.UINT16;
        __syncp();
        /* Set transmit data */
        CSIH2.TX0W = regValue;
        g_csih2_tx_num++;
        /* Enable CSIH2 interrupt operation */
        INTC2.ICCSIH2IC.BIT.MKCSIH2IC = _INT_PROCESSING_ENABLED;
        /* Synchronization processing */
        g_cg_sync_read = CSIH2.CTL1;
        __syncp();
    }

    return (status);
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
