/***********************************************************************************************************************
* File Name    : RangeMileage.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef RANGE_MILEAGE_H
#define RANGE_MILEAGE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define M_DATA_POINTS					240
#define  RANGE_MILEAGE_CAL_DISTANCE								500 //TODO:Configurable


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	real_t Present_AvailableEnergy_Wh;
	real_t Previous_AvailableEnergy_Wh;
	real_t Wh_Consumed;
	real_t Wh_Km_M;
	real_t Wh_Km_M_Values[M_DATA_POINTS];
	real_t Historic_Wh_Km;
	real_t Instant_Wh_Km;
	real_t Range_in_Km;
}Mileage_Range_St_t;

typedef struct
{
	uint8_t Esti_Mileage_Wh_Km_u8;
	uint16_t Esti_Range_Km_u16;	
}EstimatedRangeMileage_St_t;



/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern real_t ODOforRM;
extern EstimatedRangeMileage_St_t	EstimatedRangeMileage_St;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Compute_Wh_On_VehicleStart(real_t AvailableCapacity_Ah, real_t BatteryVoltage_V);
extern void Estimate_Mileage_Range(real_t, real_t);
void Store_M_To_CircularBuffer(real_t);
/***********************************************************************************************************************
* Function Name: Restore_Wh_Km_from_FEE
* Description  : This function reads the Wh/Km M0 Data point from the flash memory and stores it to Local circular 
					buffer [On Init].
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
extern void Restore_Wh_Km_from_FEE(void);

/***********************************************************************************************************************
* Function Name: Store_Wh_Km_To_FEE
* Description  : This function stores the Wh/Km M0 Data Point to the Flash memory [On STOP/SHUTDOWN]
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
extern void Store_Wh_Km_To_FEE(void);

#endif /* RANGE_MILEAGE_H */


