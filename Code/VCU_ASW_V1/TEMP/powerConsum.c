#include "powerConsum.h"
	

uint16_t CalculatePowerConsum(float BatteryVoltage, float BatteryCurrent, DrivingMode_En_t DrivingMode_En)
{
	float 		PowerConsumptionW 		= 0; //Watts
	float 		ValueDifference 		= 0;
	float 		PwrConsumBar 			= 0;
	uint16_t 	PwrConsum_in_W_u16 		= 0;
	uint16_t 	PwrConsumBar_u16 		= 0;

	DrivingMode_En = ECO_MODE_RUN_STATE_E;

	if(BatteryCurrent < 0.0f)
	{
		BatteryCurrent *= (-1); 
	}
	else
	{
		/* code */
	}
	
	
	PowerConsumptionW = BatteryVoltage*BatteryCurrent;
						/*BatteryVoltage in volts, BatteryCurrent in Amps*/
	
	PwrConsum_in_W_u16 = (uint16_t)PowerConsumptionW;
	
	ValueDifference = PowerConsumptionW - PwrConsum_in_W_u16;
	
	if(ValueDifference >= 0.5f)
	{
		PwrConsum_in_W_u16 = PwrConsum_in_W_u16 + 1;
	}
	else
	{
		;
	}
	switch(DrivingMode_En)
	{
		case NEUTRAL_MODE_RUN_STATE_E:
		{
			PwrConsumBar = 0;
			break;
		}
		case ECO_MODE_RUN_STATE_E:
		{
			PwrConsumBar = ((float)PwrConsum_in_W_u16 / (MOTOR_POWER_LIMIT_ECO_MODE/MAX_PWR_CONSUM_BARS));
			break;
		}
		case SPORT_MODE_RUN_STATE_E:
		{
			PwrConsumBar = ((float)PwrConsum_in_W_u16 / (MOTOR_POWER_LIMIT_SPORTS_MODE/MAX_PWR_CONSUM_BARS));
			break;
		}
		case SAFE_MODE_RUN_STATE_E:
		{
			PwrConsumBar = ((float)PwrConsum_in_W_u16 / (MOTOR_POWER_LIMIT_SAFE_MODE/MAX_PWR_CONSUM_BARS));
			break;
		}
		case REVERSE_MODE_RUN_STATE_E:
		{
			PwrConsumBar = ((float)PwrConsum_in_W_u16 / (MOTOR_POWER_LIMIT_REVERSE_MODE/MAX_PWR_CONSUM_BARS));
			break;
		}
		default:
		{

		}
	}
	
	PwrConsumBar_u16 = (uint16_t)PwrConsumBar;
	
	ValueDifference = PwrConsumBar - PwrConsumBar_u16;
	
	if(ValueDifference > 0.00f)
	{
		PwrConsumBar_u16 = PwrConsumBar_u16 + 1;
	}
	else
	{
		;
	}
	
	return PwrConsumBar_u16;
}