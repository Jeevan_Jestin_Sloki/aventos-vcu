/***********************************************************************************************************************
* File Name    : temp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "temp.h"

#include "RangeMileage.h"
#include "bms.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define _100_METER_ODO    100

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
SpeedFilter_St_t		SpeedFilter_St =
{
	0, 	/*Present Calculated Speed*/
	0, 	/*Previous Calculated Speed*/
	0, 	/*From_Speed*/
	0, 	/*To_Speed*/
	false,	/*Increase Speed Flag*/
	false   /*Decrease Speed Flag*/
};

ODO_Estimate_St_t       ODO_Estimate_St = {0,0,0,0,true};
real_t ODO_m = 0;

/***********************************************************************************************************************
* Function Name: CalculateVehicleSpeed
* Description  : This function calculates the vehicle speed in Kmph corresponding to the received motor speed in RPM.
* Arguments    : float 	MotorSpeedRPM_f32
* Return Value : uint16_t    	VehicleSpeedKmph_u16
***********************************************************************************************************************/
uint16_t CalculateVehicleSpeed(float MotorSpeedRPM_f32)//Motor_Speed_s16//when running
{
    real_t      VehicleSpeedRPM         = 0;
    real_t      VehicleSpeedKmph        = 0;
    real_t      DecimalDifference       = 0;
    uint16_t    VehicleSpeedKmph_u16    = 0;
    
    
    if(MotorSpeedRPM_f32 < 0)
    {
        MotorSpeedRPM_f32 = (MotorSpeedRPM_f32 * (-1));
    }
    else
    {
        ;
    }
    
    VehicleSpeedRPM = (MotorSpeedRPM_f32 / GEAR_RATIO);
    
    VehicleSpeedKmph = ((VehicleSpeedRPM * PIE * WHEEL_DIAMETER * 60)/(1000));
    
    VehicleSpeedKmph_u16 = VehicleSpeedKmph;
    
    DecimalDifference = VehicleSpeedKmph - VehicleSpeedKmph_u16;
    
    if(DecimalDifference >= 0.5f)
    {
        VehicleSpeedKmph_u16 = VehicleSpeedKmph_u16 + 1;
    }
    else
    {
        ;
    }
    
    return VehicleSpeedKmph_u16;
}


/***********************************************************************************************************************
* Function Name: VehicleSpeedLPF
* Description  : Filter function to prepare for the Speed oscillations. Call this function @x ms rate periodically
		 [Ex: call @50-ms rate].
* Arguments    : uint16_t PresentSpeedKmph_u16
* Return Value : uint16_t SpeedToDisp_u16
***********************************************************************************************************************/
uint16_t VehicleSpeedFilter(uint16_t PresentSpeedKmph_u16)//VehicleSpeedKmph_u16//for hmi only
{	
	uint16_t SpeedToDisp_u16 = 0;
	
	SpeedFilter_St.PresentSpeed_u16 = PresentSpeedKmph_u16;
	
	if(SpeedFilter_St.PresentSpeed_u16 != SpeedFilter_St.PrevSpeed_u16)
	{
		int16_t SpeedRange_s16 = 0;
		
		SpeedFilter_St.ToSpeed_u16 = SpeedFilter_St.PresentSpeed_u16;
		
		SpeedRange_s16 = SpeedFilter_St.FromSpeed_u16 - SpeedFilter_St.ToSpeed_u16;
		
		if(SpeedRange_s16 < 0)
		{
			SpeedRange_s16 = ((SpeedRange_s16)*(-1));
			SpeedFilter_St.IncreaseSpeed_b = true;
			SpeedFilter_St.DecreaseSpeed_b = false;
		}
		else
		{
			SpeedFilter_St.DecreaseSpeed_b = true;
			SpeedFilter_St.IncreaseSpeed_b = false;
		}
		
		SpeedFilter_St.PrevSpeed_u16 = SpeedFilter_St.PresentSpeed_u16;
	}
	
	
	if(SpeedFilter_St.FromSpeed_u16 != SpeedFilter_St.ToSpeed_u16)
	{
		if(true == SpeedFilter_St.IncreaseSpeed_b)
		{
			SpeedFilter_St.FromSpeed_u16 += 1;
		}
		else if(true == SpeedFilter_St.DecreaseSpeed_b)
		{
			SpeedFilter_St.FromSpeed_u16 -= 1;
		}
		else
		{
			;
		}
	}
	
	SpeedToDisp_u16 = SpeedFilter_St.FromSpeed_u16;
	
	return SpeedToDisp_u16;
}

/***********************************************************************************************************************
* Function Name: ResetTheSpeedFilterData
* Description  : This function resets the data and flags related to Speed Filter algorithm.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheSpeedFilterData(void)
{
	SpeedFilter_St.PresentSpeed_u16 	= 0;
	SpeedFilter_St.PrevSpeed_u16 		= 0;
	SpeedFilter_St.FromSpeed_u16 		= 0;
	SpeedFilter_St.ToSpeed_u16 		    = 0;
	SpeedFilter_St.IncreaseSpeed_b 		= false;
	SpeedFilter_St.DecreaseSpeed_b 		= false;
	
	return;
}


/*---------------------------------------------------------------------*/



/***********************************************************************************************************************
* Function Name: Estimate_ODO
* Description  : This function estimates the ODO in meters [Distance covered by the vehicle] corresponding to the MRN 
		 received from the Motor Controller Unit.
* Arguments    : uint32_t MRN_u32
* Return Value : uint32_t ODO_Out_u32
***********************************************************************************************************************/
uint32_t Estimate_ODO(uint32_t MRN_u32) //Motor_Rotation_Number_u32 pass
{
    static real_t Prev_ODO_Meter = 0;
    real_t Distence_Covered_Meter = 0;
    uint16_t ODO_Meters_u16 = 0;
    uint32_t MRN_Over_u32 = 0;
    static uint8_t RM_Tigger_Counter_u8 = 0;

    ODO_Estimate_St.PresentMRN_u32 = MRN_u32;

    if (ODO_Estimate_St.PresentMRN_u32 != ODO_Estimate_St.PreviousMRN_u32)
    {


        MRN_Over_u32 = (ODO_Estimate_St.PresentMRN_u32 - ODO_Estimate_St.PreviousMRN_u32);

        if (MRN_Over_u32 >= 12)
        {

            ODO_Estimate_St.PreviousMRN_u32 = ODO_Estimate_St.PresentMRN_u32;
	    
            Distence_Covered_Meter = (((PIE * WHEEL_DIAMETER) / GEAR_RATIO) * (MRN_Over_u32));

            ODO_m = Prev_ODO_Meter + Distence_Covered_Meter;

            if (ODO_m > _100_METER_ODO)
            {
                ODO_Meters_u16 = (uint16_t)ODO_m;
                ODO_m = ODO_m - _100_METER_ODO;
                ODO_Estimate_St.Present_ODO_u32 += ODO_Meters_u16;
                RM_Tigger_Counter_u8++;
            }
            else
            {
                /* code */
            }
	    
	     Prev_ODO_Meter = ODO_m;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }

    if(RM_Tigger_Counter_u8 == (RANGE_MILEAGE_CAL_DISTANCE/_100_METER_ODO))
    {
        RM_Tigger_Counter_u8 = 0;
        CalculateRangeMileage_b = true; 
        /*Set to true for each distance travelled of 500m to estimate mileage and range km*/
    }
    else
    {
        /* code */
    }
    

    return ODO_Estimate_St.Present_ODO_u32; /*ODO in meters*/
}
/***********************************************************************************************************************
* Function Name: Restore_ODO_From_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_ODO_From_FEE(void)
{
    ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO from EEPROM on Start
}
/***********************************************************************************************************************
* Function Name: Store_ODO_To_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Store_ODO_To_FEE(void)
{
    ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO to EEPROM on SHUT DOWN
}
/***********************************************************************************************************************
* Function Name: ResetTheODOEstimationData
* Description  : This function resets the Data and the Flags related to ODO estimation function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOEstimationData(void)
{
	ODO_Estimate_St.PresentMRN_u32 		= 0;
	ODO_Estimate_St.PreviousMRN_u32 	= 0;
	ODO_Estimate_St.Present_ODO_u32 	= 0;
	ODO_Estimate_St.Previous_ODO_u32 	= 0;
	ODO_Estimate_St.UpdateODOFromFlash_b 	= true;
	return;
}

/*---------------------------------------------------------------------*/



/********************************************************EOF***********************************************************/