/***********************************************************************************************************************
* File Name    : temp.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef TEMP_H
#define TEMP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/*
	Vehicle-Speed Estimation Conf
*/
#define GEAR_RATIO				12	/* Gear Ratio */
#define PIE					3.142	/* Pie */
#define WHEEL_DIAMETER				0.594	/* Wheel Diameter in meters */

typedef struct
{
	uint16_t PresentSpeed_u16;
	uint16_t PrevSpeed_u16;
	uint16_t FromSpeed_u16;
	uint16_t ToSpeed_u16;
	bool	 IncreaseSpeed_b;
	bool 	 DecreaseSpeed_b;
}SpeedFilter_St_t;



/*
	ODO Estimation Conf
*/
//#define GEAR_RATIO				12	/* Gear Ratio */
//#define PIE					3.142	/* Pie */
//#define WHEEL_DIAMETER			0.594	/* Wheel Diameter in meters */
#define WHEEL_CIRCUMFERENCE			(PIE*WHEEL_DIAMETER)

typedef struct
{
    uint32_t PresentMRN_u32;
    uint32_t PreviousMRN_u32;
    uint32_t Present_ODO_u32;
    uint32_t Previous_ODO_u32;
    bool     UpdateODOFromFlash_b; 
}ODO_Estimate_St_t;


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern uint16_t CalculateVehicleSpeed(float);
extern uint16_t VehicleSpeedFilter(uint16_t);
extern void ResetTheSpeedFilterData(void);

extern uint32_t Estimate_ODO(uint32_t);

/***********************************************************************************************************************
* Function Name: Restore_ODO_From_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_ODO_From_FEE(void);
/***********************************************************************************************************************
* Function Name: Store_ODO_To_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Store_ODO_To_FEE(void);
extern void ResetTheODOEstimationData(void);



#endif /* TEMP */


