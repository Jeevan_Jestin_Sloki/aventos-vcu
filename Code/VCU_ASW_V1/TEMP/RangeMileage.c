/***********************************************************************************************************************
* File Name    : RangeMileage.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "RangeMileage.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
real_t ODOforRM = 0;

Mileage_Range_St_t	Mileage_Range_St = {0,0};

bool WhCompute_On_Start_b = false;

EstimatedRangeMileage_St_t	EstimatedRangeMileage_St;

/***********************************************************************************************************************
* Function Name: Compute_Wh_On_VehicleStart
* Description  : This function estimates the present Wh in the battery on Ignition-On.
			This function is called when ODO is @ 0Km
* Arguments    : real_t AvailableCapacity_Ah, real_t BatteryVoltage_V
* Return Value : None
***********************************************************************************************************************/
void Compute_Wh_On_VehicleStart(real_t AvailableCapacity_Ah, real_t BatteryVoltage_V)
{
	
	Mileage_Range_St.Previous_AvailableEnergy_Wh = (AvailableCapacity_Ah * BatteryVoltage_V);
	
	WhCompute_On_Start_b = true;
	
	return;
}



/***********************************************************************************************************************
* Function Name: Estimate_Mileage_Range
* Description  : This function estimates the Mileage [Wh/Km] and Range [Km].
			This function is called every time  when ODO reaches 0.5km.
* Arguments    : real_t AvailableCapacity_Ah, real_t BatteryVoltage_V
* Return Value : EstimatedRangeMileage_St_t 
***********************************************************************************************************************/
void Estimate_Mileage_Range(real_t AvailableCapacity_Ah, real_t BatteryVoltage_V)
{
	real_t SumOf_Wh_Km			= 0;
	uint16_t LoopCount_u16		= 0;
	
	Mileage_Range_St.Present_AvailableEnergy_Wh = (AvailableCapacity_Ah * BatteryVoltage_V);
	
	Mileage_Range_St.Wh_Consumed = (Mileage_Range_St.Previous_AvailableEnergy_Wh - Mileage_Range_St.Present_AvailableEnergy_Wh);
	
	if(Mileage_Range_St.Wh_Consumed < 0)
	{
		Mileage_Range_St.Wh_Consumed *= (-1);
	}
	else
	{
		;
	}
	
	Mileage_Range_St.Wh_Km_M = (Mileage_Range_St.Wh_Consumed / ((real_t)RANGE_MILEAGE_CAL_DISTANCE/1000)); 
	
	Store_M_To_CircularBuffer(Mileage_Range_St.Wh_Km_M);
	
	Mileage_Range_St.Previous_AvailableEnergy_Wh = Mileage_Range_St.Present_AvailableEnergy_Wh;
	
	for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
	{
		SumOf_Wh_Km = SumOf_Wh_Km + Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16];
	}
	
	Mileage_Range_St.Historic_Wh_Km = (SumOf_Wh_Km / M_DATA_POINTS);	
						/*This Historic Wh/Km is dispalyed as mileage on HMI*/
	
	SumOf_Wh_Km = 0;
	
	for(LoopCount_u16 = 0; LoopCount_u16 < 4; LoopCount_u16++)
	{
		SumOf_Wh_Km = SumOf_Wh_Km + Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16];
	}
	
	Mileage_Range_St.Instant_Wh_Km = (SumOf_Wh_Km / 4);
	
	Mileage_Range_St.Range_in_Km = ((Mileage_Range_St.Present_AvailableEnergy_Wh) / ((0.7*Mileage_Range_St.Instant_Wh_Km) + \
							(0.3*Mileage_Range_St.Historic_Wh_Km)));
						/*0.7 and 0.3 are the weightage values, they may change based on testing*/

	EstimatedRangeMileage_St.Esti_Mileage_Wh_Km_u8 = (uint8_t)Mileage_Range_St.Historic_Wh_Km;
	EstimatedRangeMileage_St.Esti_Range_Km_u16 = (uint16_t)Mileage_Range_St.Range_in_Km;
	
	return;
}


/***********************************************************************************************************************
* Function Name: Store_M_To_CircularBuffer
* Description  : This function stores the estimated Wh/Km into circular buffer.
* Arguments    : real_t Present_WhPerKm_M
* Return Value : None
***********************************************************************************************************************/
void Store_M_To_CircularBuffer(real_t Present_WhPerKm_M)
{
	uint16_t LoopCount_u16 = 0;
	
	if(WhCompute_On_Start_b == true)
	{
		WhCompute_On_Start_b = false;
		
		for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
		{
			Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Present_WhPerKm_M;
		}
	}
	else
	{
		for(LoopCount_u16 = (M_DATA_POINTS - 1); LoopCount_u16 >= 1 ;  LoopCount_u16--)
        	{
            		Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16 - 1];
        	}
        
        	Mileage_Range_St.Wh_Km_M_Values[0] = Present_WhPerKm_M;	
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: Restore_Wh_Km_from_FEE
* Description  : This function reads the Wh/Km M0 Data point from the flash memory and stores it to Local circular 
					buffer [On Init].
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_Wh_Km_from_FEE(void)
{
	real_t Wh_Km_M0 = 10.0 ; //Read from FEE and Update
	uint16_t LoopCount_u16 = 0;
	for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
	{
		Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Wh_Km_M0;
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Store_Wh_Km_To_FEE
* Description  : This function stores the Wh/Km M0 Data Point to the Flash memory [On STOP/SHUTDOWN]
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Store_Wh_Km_To_FEE(void)
{
	real_t Wh_Km_M0 = 0 ;
	Wh_Km_M0 = Mileage_Range_St.Wh_Km_M_Values[0];

	//Todo:Store Wh_Km_M0 value into the EEPROM

	return;

}

/********************************************************EOF***********************************************************/

/*
Note: To be discuss with Geeth
- What is the RangeKm and Wh/Km on start [first time]
- M values on first time in Buffer 
- What is the Ah and Voltage given from the BMS at 100% SOC
- How Ah and V varies for each 500m
*/







