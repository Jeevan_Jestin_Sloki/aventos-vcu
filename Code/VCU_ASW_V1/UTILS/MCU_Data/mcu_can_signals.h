﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : mcu_can_signals.h
|    Project        : VCU
|    Module         : motor signals data
|    Description    : This is the generated file from the DBC2CH Tool for motor.
|                     controller
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 04/05/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MCU_CAN_SIGNALS_H
#define MCU_CAN_SIGNALS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include <stdint.h>
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define MOTOR_0X150_RX_MOTOR_SPEED_S16_MASK0 8U
#define MOTOR_0X150_RX_BATTERY_CURRENT_S16_MASK0 8U
#define MOTOR_0X150_RX_RESERVED_FOR_LTVS_S16_MASK0 8U
#define MOTOR_0X150_RX_POSITION_SENSOR_ERROR_U8_MASK0 6U
#define MOTOR_0X150_RX_THROTTLE_DISABLE_STATUS_U8_MASK0 7U
#define MOTOR_0X150_RX_PERIPHERAL_SUPPLY_FAILURE_U8_MASK0 1U
#define MOTOR_0X150_RX_MOTOR_TEMPERATURE_WARNING_U8_MASK0 2U
#define MOTOR_0X150_RX_CONTROLLER_TEMP_WARNING_U8_MASK0 3U
#define MOTOR_0X150_RX_OVER_VOLTAGE_ERROR_U8_MASK0 4U
#define MOTOR_0X150_RX_UNDER_VOLTAGE_ERROR_U8_MASK0 5U
#define MOTOR_0X150_RX_OVER_CURRENT_ERROR_U8_MASK0 6U
#define MOTOR_0X150_RX_TIMEOUT_ERROR_U8_MASK0 7U
#define MOTOR_0X150_RX_DRIVING_DIRECTION_ACTUAL_U8_MASK0 1U
#define MOTOR_0X150_RX_RIDE_MODE_ACTUAL_U8_MASK0 2U
#define MOTOR_0X150_RX_THROTTLE_MAP_ACTUAL_U8_MASK0 5U

#define MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK0 32U
#define MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK1 24U
#define MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK2 16U
#define MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK3 8U

#define MOTOR_0X250_RX_MOTOR_TEMPERATURE_S16_MASK0 8U
#define MOTOR_0X250_RX_CONTROLLER_TEMPERATURE_S16_MASK0 8U
#define MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK0 24U
#define MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK1 16U
#define MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK2 8U

#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK0 56U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK1 48U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK2 40U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK3 32U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK4 24U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK5 16U
#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK6 8U

#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK0 56U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK1 48U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK2 40U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK3 32U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK4 24U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK5 16U
#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK6 8U

#define MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK0 32U
#define MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK1 24U
#define MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK2 16U
#define MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK3 8U

#define VEHICLE_0X100_TX_MOTOR_SPEED_LIMIT_S16_MASK0 8U
#define VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U32_MASK0 16U
#define VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U32_MASK1 8U
#define VEHICLE_0X100_TX_THROTTLE_MAP_SELECT_U8_MASK0 3U
#define VEHICLE_0X100_TX_MOTORSTOP_U8_MASK0 6U
#define VEHICLE_0X100_TX_DRIVINGDIRECTION_U8_MASK0 7U

#define SIGNAL_READ_Mask0 0U
#define SIGNAL_READ_Mask1 0x01U
#define SIGNAL_READ_Mask2 0x03U
#define SIGNAL_READ_Mask3 0x07U
#define SIGNAL_READ_Mask4 0x0FU
#define SIGNAL_READ_Mask5 0x1FU
#define SIGNAL_READ_Mask6 0x3FU
#define SIGNAL_READ_Mask7 0x7FU
#define SIGNAL_READ_Mask8 0xFFU

#define SIGNAL_WRITE_Mask0 0x80U
#define SIGNAL_WRITE_Mask1 0xC0U
#define SIGNAL_WRITE_Mask2 0xE0U
#define SIGNAL_WRITE_Mask3 0xF0U
#define SIGNAL_WRITE_Mask4 0xF8U
#define SIGNAL_WRITE_Mask5 0xFCU
#define SIGNAL_WRITE_Mask6 0xFEU
#define SIGNAL_WRITE_Mask7 0xFFU
/* def @MOTOR_0X150_RX CAN Message                                   (336) */
#define MOTOR_0X150_RX_ID (336U)
#define MOTOR_0X150_RX_IDE (0U)
#define MOTOR_0X150_RX_DLC (8U)

#define MOTOR_0X150_RX_MOTOR_SPEED_S16FACTOR (0.30518)
#define MOTOR_0X150_RX_CANID_MOTOR_SPEED_S16_STARTBIT (0)
#define MOTOR_0X150_RX_CANID_MOTOR_SPEED_S16_OFFSET (0)
#define MOTOR_0X150_RX_CANID_MOTOR_SPEED_S16_MIN (-10000)
#define MOTOR_0X150_RX_CANID_MOTOR_SPEED_S16_MAX (10000)
#define MOTOR_0X150_RX_BATTERY_CURRENT_S16FACTOR (0.0127)
#define MOTOR_0X150_RX_CANID_BATTERY_CURRENT_S16_STARTBIT (16)
#define MOTOR_0X150_RX_CANID_BATTERY_CURRENT_S16_OFFSET (0)
#define MOTOR_0X150_RX_CANID_BATTERY_CURRENT_S16_MIN (-415.98976)
#define MOTOR_0X150_RX_CANID_BATTERY_CURRENT_S16_MAX (415.97707)
#define MOTOR_0X150_RX_RESERVED_FOR_LTVS_S16FACTOR (1)
#define MOTOR_0X150_RX_CANID_RESERVED_FOR_LTVS_S16_STARTBIT (32)
#define MOTOR_0X150_RX_CANID_RESERVED_FOR_LTVS_S16_OFFSET (0)
#define MOTOR_0X150_RX_CANID_RESERVED_FOR_LTVS_S16_MIN (0)
#define MOTOR_0X150_RX_CANID_RESERVED_FOR_LTVS_S16_MAX (0)
#define MOTOR_0X150_RX_POSITION_SENSOR_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_POSITION_SENSOR_ERROR_U8_STARTBIT (46)
#define MOTOR_0X150_RX_CANID_POSITION_SENSOR_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_POSITION_SENSOR_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_POSITION_SENSOR_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_THROTTLE_DISABLE_STATUS_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_THROTTLE_DISABLE_STATUS_U8_STARTBIT (47)
#define MOTOR_0X150_RX_CANID_THROTTLE_DISABLE_STATUS_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_THROTTLE_DISABLE_STATUS_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_THROTTLE_DISABLE_STATUS_U8_MAX (1)
#define MOTOR_0X150_RX_MOTOR_ENABLE_STATUS_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_MOTOR_ENABLE_STATUS_U8_STARTBIT (48)
#define MOTOR_0X150_RX_CANID_MOTOR_ENABLE_STATUS_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_MOTOR_ENABLE_STATUS_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_MOTOR_ENABLE_STATUS_U8_MAX (1)
#define MOTOR_0X150_RX_PERIPHERAL_SUPPLY_FAILURE_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_PERIPHERAL_SUPPLY_FAILURE_U8_STARTBIT (49)
#define MOTOR_0X150_RX_CANID_PERIPHERAL_SUPPLY_FAILURE_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_PERIPHERAL_SUPPLY_FAILURE_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_PERIPHERAL_SUPPLY_FAILURE_U8_MAX (1)
#define MOTOR_0X150_RX_MOTOR_TEMPERATURE_WARNING_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_MOTOR_TEMPERATURE_WARNING_U8_STARTBIT (50)
#define MOTOR_0X150_RX_CANID_MOTOR_TEMPERATURE_WARNING_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_MOTOR_TEMPERATURE_WARNING_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_MOTOR_TEMPERATURE_WARNING_U8_MAX (1)
#define MOTOR_0X150_RX_CONTROLLER_TEMP_WARNING_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_CONTROLLER_TEMP_WARNING_U8_STARTBIT (51)
#define MOTOR_0X150_RX_CANID_CONTROLLER_TEMP_WARNING_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_CONTROLLER_TEMP_WARNING_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_CONTROLLER_TEMP_WARNING_U8_MAX (1)
#define MOTOR_0X150_RX_OVER_VOLTAGE_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_OVER_VOLTAGE_ERROR_U8_STARTBIT (52)
#define MOTOR_0X150_RX_CANID_OVER_VOLTAGE_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_OVER_VOLTAGE_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_OVER_VOLTAGE_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_UNDER_VOLTAGE_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_UNDER_VOLTAGE_ERROR_U8_STARTBIT (53)
#define MOTOR_0X150_RX_CANID_UNDER_VOLTAGE_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_UNDER_VOLTAGE_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_UNDER_VOLTAGE_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_OVER_CURRENT_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_OVER_CURRENT_ERROR_U8_STARTBIT (54)
#define MOTOR_0X150_RX_CANID_OVER_CURRENT_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_OVER_CURRENT_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_OVER_CURRENT_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_TIMEOUT_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_TIMEOUT_ERROR_U8_STARTBIT (55)
#define MOTOR_0X150_RX_CANID_TIMEOUT_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_TIMEOUT_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_TIMEOUT_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_ANALOG_THROTTLE_ERROR_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_ANALOG_THROTTLE_ERROR_U8_STARTBIT (56)
#define MOTOR_0X150_RX_CANID_ANALOG_THROTTLE_ERROR_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_ANALOG_THROTTLE_ERROR_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_ANALOG_THROTTLE_ERROR_U8_MAX (1)
#define MOTOR_0X150_RX_DRIVING_DIRECTION_ACTUAL_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_DRIVING_DIRECTION_ACTUAL_U8_STARTBIT (57)
#define MOTOR_0X150_RX_CANID_DRIVING_DIRECTION_ACTUAL_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_DRIVING_DIRECTION_ACTUAL_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_DRIVING_DIRECTION_ACTUAL_U8_MAX (1)
#define MOTOR_0X150_RX_RIDE_MODE_ACTUAL_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_RIDE_MODE_ACTUAL_U8_STARTBIT (58)
#define MOTOR_0X150_RX_CANID_RIDE_MODE_ACTUAL_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_RIDE_MODE_ACTUAL_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_RIDE_MODE_ACTUAL_U8_MAX (7)
#define MOTOR_0X150_RX_THROTTLE_MAP_ACTUAL_U8FACTOR (1)
#define MOTOR_0X150_RX_CANID_THROTTLE_MAP_ACTUAL_U8_STARTBIT (61)
#define MOTOR_0X150_RX_CANID_THROTTLE_MAP_ACTUAL_U8_OFFSET (0)
#define MOTOR_0X150_RX_CANID_THROTTLE_MAP_ACTUAL_U8_MIN (0)
#define MOTOR_0X150_RX_CANID_THROTTLE_MAP_ACTUAL_U8_MAX (7)

/* def @MOTOR_0X200_RX CAN Message                                   (512) */
#define MOTOR_0X200_RX_ID (512U)
#define MOTOR_0X200_RX_IDE (0U)
#define MOTOR_0X200_RX_DLC (8U)

#define MOTOR_0X200_RX_THROTTLE_OUT_U8FACTOR (0.39063)
#define MOTOR_0X200_RX_CANID_THROTTLE_OUT_U8_STARTBIT (0)
#define MOTOR_0X200_RX_CANID_THROTTLE_OUT_U8_OFFSET (0)
#define MOTOR_0X200_RX_CANID_THROTTLE_OUT_U8_MIN (0)
#define MOTOR_0X200_RX_CANID_THROTTLE_OUT_U8_MAX (100)
#define MOTOR_0X200_RX_THROTTLE_REFERENCE_U8FACTOR (0.01953)
#define MOTOR_0X200_RX_CANID_THROTTLE_REFERENCE_U8_STARTBIT (8)
#define MOTOR_0X200_RX_CANID_THROTTLE_REFERENCE_U8_OFFSET (0)
#define MOTOR_0X200_RX_CANID_THROTTLE_REFERENCE_U8_MIN (0)
#define MOTOR_0X200_RX_CANID_THROTTLE_REFERENCE_U8_MAX (4.980469)
#define MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64FACTOR (1)
#define MOTOR_0X200_RX_CANID_RESERVED_FOR_LTVS_U64_STARTBIT (16)
#define MOTOR_0X200_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET (0)
#define MOTOR_0X200_RX_CANID_RESERVED_FOR_LTVS_U64_MIN (0)
#define MOTOR_0X200_RX_CANID_RESERVED_FOR_LTVS_U64_MAX (0)
#define MOTOR_0X200_RX_BATTERY_VOLTAGE_U8FACTOR (0.25825)
#define MOTOR_0X200_RX_CANID_BATTERY_VOLTAGE_U8_STARTBIT (56)
#define MOTOR_0X200_RX_CANID_BATTERY_VOLTAGE_U8_OFFSET (0)
#define MOTOR_0X200_RX_CANID_BATTERY_VOLTAGE_U8_MIN (0)
#define MOTOR_0X200_RX_CANID_BATTERY_VOLTAGE_U8_MAX (65.852864)

/* def @MOTOR_0X250_RX CAN Message                                   (592) */
#define MOTOR_0X250_RX_ID (592U)
#define MOTOR_0X250_RX_IDE (0U)
#define MOTOR_0X250_RX_DLC (8U)

#define MOTOR_0X250_RX_MOTOR_TEMPERATURE_S16FACTOR (0.39063)
#define MOTOR_0X250_RX_CANID_MOTOR_TEMPERATURE_S16_STARTBIT (0)
#define MOTOR_0X250_RX_CANID_MOTOR_TEMPERATURE_S16_OFFSET (0)
#define MOTOR_0X250_RX_CANID_MOTOR_TEMPERATURE_S16_MIN (0)
#define MOTOR_0X250_RX_CANID_MOTOR_TEMPERATURE_S16_MAX (199.80469)
#define MOTOR_0X250_RX_CONTROLLER_TEMPERATURE_S16FACTOR (0.29297)
#define MOTOR_0X250_RX_CANID_CONTROLLER_TEMPERATURE_S16_STARTBIT (16)
#define MOTOR_0X250_RX_CANID_CONTROLLER_TEMPERATURE_S16_OFFSET (0)
#define MOTOR_0X250_RX_CANID_CONTROLLER_TEMPERATURE_S16_MIN (-40)
#define MOTOR_0X250_RX_CANID_CONTROLLER_TEMPERATURE_S16_MAX (150)
#define MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32FACTOR (1)
#define MOTOR_0X250_RX_CANID_MOTOR_ROTATION_NUMBER_U32_STARTBIT (32)
#define MOTOR_0X250_RX_CANID_MOTOR_ROTATION_NUMBER_U32_OFFSET (0)
#define MOTOR_0X250_RX_CANID_MOTOR_ROTATION_NUMBER_U32_MIN (0)
#define MOTOR_0X250_RX_CANID_MOTOR_ROTATION_NUMBER_U32_MAX (4295000000)

/* def @VEHICLE_0X300_TX CAN Message                                   (768) */
#define VEHICLE_0X300_TX_ID (768U)
#define VEHICLE_0X300_TX_IDE (0U)
#define VEHICLE_0X300_TX_DLC (8U)

#define VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64FACTOR (1)
#define VEHICLE_0X300_TX_CANID_RESERVED_FOR_LTVS_U64_STARTBIT (0)
#define VEHICLE_0X300_TX_CANID_RESERVED_FOR_LTVS_U64_OFFSET (0)
#define VEHICLE_0X300_TX_CANID_RESERVED_FOR_LTVS_U64_MIN (0)
#define VEHICLE_0X300_TX_CANID_RESERVED_FOR_LTVS_U64_MAX (0)

/* def @VEHICLE_0X400_TX CAN Message                                   (1024) */
#define VEHICLE_0X400_TX_ID (1024U)
#define VEHICLE_0X400_TX_IDE (0U)
#define VEHICLE_0X400_TX_DLC (8U)

#define VEHICLE_0X400_TX_BROADCAST_RATE_U8FACTOR (4)
#define VEHICLE_0X400_TX_CANID_BROADCAST_RATE_U8_STARTBIT (0)
#define VEHICLE_0X400_TX_CANID_BROADCAST_RATE_U8_OFFSET (0)
#define VEHICLE_0X400_TX_CANID_BROADCAST_RATE_U8_MIN (0)
#define VEHICLE_0X400_TX_CANID_BROADCAST_RATE_U8_MAX (1020)
#define VEHICLE_0X400_TX_BROADCAST_MODE_U8FACTOR (1)
#define VEHICLE_0X400_TX_CANID_BROADCAST_MODE_U8_STARTBIT (8)
#define VEHICLE_0X400_TX_CANID_BROADCAST_MODE_U8_OFFSET (0)
#define VEHICLE_0X400_TX_CANID_BROADCAST_MODE_U8_MIN (0)
#define VEHICLE_0X400_TX_CANID_BROADCAST_MODE_U8_MAX (1)

/* def @MOTOR_0X650_RX CAN Message                                   (1616) */
#define MOTOR_0X650_RX_ID (1616U)
#define MOTOR_0X650_RX_IDE (0U)
#define MOTOR_0X650_RX_DLC (8U)

#define MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64FACTOR (1)
#define MOTOR_0X650_RX_CANID_RESERVED_FOR_LTVS_U64_STARTBIT (0)
#define MOTOR_0X650_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET (0)
#define MOTOR_0X650_RX_CANID_RESERVED_FOR_LTVS_U64_MIN (0)
#define MOTOR_0X650_RX_CANID_RESERVED_FOR_LTVS_U64_MAX (0)

/* def @MOTOR_0X750_RX CAN Message                                   (1872) */
#define MOTOR_0X750_RX_ID (1872U)
#define MOTOR_0X750_RX_IDE (0U)
#define MOTOR_0X750_RX_DLC (8U)

#define MOTOR_0X750_RX_SOFTWARE_VERSION_U8FACTOR (1)
#define MOTOR_0X750_RX_CANID_SOFTWARE_VERSION_U8_STARTBIT (0)
#define MOTOR_0X750_RX_CANID_SOFTWARE_VERSION_U8_OFFSET (0)
#define MOTOR_0X750_RX_CANID_SOFTWARE_VERSION_U8_MIN (0)
#define MOTOR_0X750_RX_CANID_SOFTWARE_VERSION_U8_MAX (250)
#define MOTOR_0X750_RX_HARDWARE_VERSION_U8FACTOR (1)
#define MOTOR_0X750_RX_CANID_HARDWARE_VERSION_U8_STARTBIT (8)
#define MOTOR_0X750_RX_CANID_HARDWARE_VERSION_U8_OFFSET (0)
#define MOTOR_0X750_RX_CANID_HARDWARE_VERSION_U8_MIN (0)
#define MOTOR_0X750_RX_CANID_HARDWARE_VERSION_U8_MAX (250)
#define MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64FACTOR (1)
#define MOTOR_0X750_RX_CANID_RESERVED_FOR_LTVS_U64_STARTBIT (16)
#define MOTOR_0X750_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET (0)
#define MOTOR_0X750_RX_CANID_RESERVED_FOR_LTVS_U64_MIN (0)
#define MOTOR_0X750_RX_CANID_RESERVED_FOR_LTVS_U64_MAX (0)

/* def @VEHICLE_0X100_TX CAN Message                                   (256) */
#define VEHICLE_0X100_TX_ID (256U)
#define VEHICLE_0X100_TX_IDE (0U)
#define VEHICLE_0X100_TX_DLC (8U)

#define VEHICLE_0X100_TX_MOTOR_SPEED_LIMIT_S16FACTOR (0.305176)
#define VEHICLE_0X100_TX_CANID_MOTOR_SPEED_LIMIT_S16_STARTBIT (0)
#define VEHICLE_0X100_TX_CANID_MOTOR_SPEED_LIMIT_S16_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_MOTOR_SPEED_LIMIT_S16_MIN (-10000)
#define VEHICLE_0X100_TX_CANID_MOTOR_SPEED_LIMIT_S16_MAX (10000)
#define VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U8_STARTBIT (16)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U8_MAX (0)
#define VEHICLE_0X100_TX_MAX_REGENERATION_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_MAX_REGENERATION_U8_STARTBIT (24)
#define VEHICLE_0X100_TX_CANID_MAX_REGENERATION_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_MAX_REGENERATION_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_MAX_REGENERATION_U8_MAX (100)
#define VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U32FACTOR (1)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U32_STARTBIT (32)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U32_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U32_MIN (0)
#define VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U32_MAX (0)
#define VEHICLE_0X100_TX_RIDE_MODE_REQUEST_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_RIDE_MODE_REQUEST_U8_STARTBIT (56)
#define VEHICLE_0X100_TX_CANID_RIDE_MODE_REQUEST_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_RIDE_MODE_REQUEST_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_RIDE_MODE_REQUEST_U8_MAX (7)
#define VEHICLE_0X100_TX_THROTTLE_MAP_SELECT_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_THROTTLE_MAP_SELECT_U8_STARTBIT (59)
#define VEHICLE_0X100_TX_CANID_THROTTLE_MAP_SELECT_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_THROTTLE_MAP_SELECT_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_THROTTLE_MAP_SELECT_U8_MAX (7)
#define VEHICLE_0X100_TX_MOTORSTOP_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_MOTORSTOP_U8_STARTBIT (62)
#define VEHICLE_0X100_TX_CANID_MOTORSTOP_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_MOTORSTOP_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_MOTORSTOP_U8_MAX (1)
#define VEHICLE_0X100_TX_DRIVINGDIRECTION_U8FACTOR (1)
#define VEHICLE_0X100_TX_CANID_DRIVINGDIRECTION_U8_STARTBIT (63)
#define VEHICLE_0X100_TX_CANID_DRIVINGDIRECTION_U8_OFFSET (0)
#define VEHICLE_0X100_TX_CANID_DRIVINGDIRECTION_U8_MIN (0)
#define VEHICLE_0X100_TX_CANID_DRIVINGDIRECTION_U8_MAX (1)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef struct
{
  int16_t Motor_Speed_s16;
  int16_t Battery_current_s16;
  int16_t Reserved_for_LTVS_s16;
  uint8_t Position_Sensor_Error_u8;
  uint8_t Throttle_Disable_Status_u8;
  uint8_t Motor_Enable_Status_u8;
  uint8_t Peripheral_supply_failure_u8;
  uint8_t Motor_temperature_warning_u8;
  uint8_t Controller_temp_warning_u8;
  uint8_t Over_Voltage_error_u8;
  uint8_t Under_Voltage_Error_u8;
  uint8_t Over_current_error_u8;
  uint8_t Timeout_error_u8;
  uint8_t Analog_throttle_error_u8;
  uint8_t Driving_direction_actual_u8;
  uint8_t Ride_mode_Actual_u8;
  uint8_t Throttle_Map_Actual_u8;
} Motor_0x150_Rx_t;

typedef struct
{
  int64_t Reserved_for_LTVS_u64;
  uint8_t Throttle_out_u8;
  uint8_t Throttle_Reference_u8;
  uint8_t Battery_Voltage_u8;
} Motor_0x200_Rx_t;

typedef struct
{
  uint32_t Motor_Rotation_Number_u32;
  int16_t Motor_temperature_s16;
  int16_t Controller_temperature_s16; 
} Motor_0x250_Rx_t;

typedef struct
{
  int64_t Reserved_for_LTVS_u64;
} Motor_0x650_Rx_t;

typedef struct
{
  int64_t Reserved_for_LTVS_u64;
  uint8_t Software_version_u8;
  uint8_t Hardware_version_u8;
} Motor_0x750_Rx_t;

typedef struct
{
  int64_t Reserved_for_LTVS_u64;
} Vehicle_0x300_Tx_t;

typedef struct
{
  uint8_t Broadcast_rate_u8;
  uint8_t Broadcast_Mode_u8;
} Vehicle_0x400_Tx_t;

typedef struct
{
	 uint32_t Reserved_for_LTVS_u32;
  int16_t Motor_Speed_Limit_s16;
  uint8_t Reserved_for_LTVS_u8;
  uint8_t Max_Regeneration_u8;
  uint8_t Ride_mode_Request_u8;
  uint8_t Throttle_Map_select_u8;
  uint8_t MotorStop_u8;
  uint8_t DrivingDirection_u8;
} Vehicle_0x100_Tx_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x100_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x100_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Serialize_Vehicle_0x100_Tx(Vehicle_0x100_Tx_t *message,
                                           uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x300_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x300_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Serialize_Vehicle_0x300_Tx(Vehicle_0x300_Tx_t *message,
                                           uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x400_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x400_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Serialize_Vehicle_0x400_Tx(Vehicle_0x400_Tx_t *message,
                                           uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x150_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x150_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_Motor_0x150_Rx(Motor_0x150_Rx_t *message,
                                           const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x200_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x200_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_Motor_0x200_Rx(Motor_0x200_Rx_t *message,
                                           const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x250_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x250_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_Motor_0x250_Rx(Motor_0x250_Rx_t *message,
                                           const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x650_Rx
*   Description   : This function updates the signals data from the recived CAN  
*		    data.
*   Parameters    : Motor_0x650_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_Motor_0x650_Rx(Motor_0x650_Rx_t *message,
                                           const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x750_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x750_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_Motor_0x750_Rx(Motor_0x750_Rx_t *message,
                                           const uint8_t *data);

#endif /* MCU_CAN_SIGNALS_H */
/*---------------------- End of File -----------------------------------------*/
