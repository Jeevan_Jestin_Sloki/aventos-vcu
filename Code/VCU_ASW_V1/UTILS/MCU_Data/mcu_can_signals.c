﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : mcu_can_signals.c
|    Project        : VCU
|    Module         : motor signals data
|    Description    : This is the generated file from the DBC2CH Tool for motor.
|                     controller
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 04/05/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MCU_CAN_SIGNALS_C
#define MCU_CAN_SIGNALS_C

#include "mcu_can_signals.h"


/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 

 
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 
 
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x100_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x100_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Serialize_Vehicle_0x100_Tx(Vehicle_0x100_Tx_t* message, uint8_t* data)
{
  message->Motor_Speed_Limit_s16 = (message->Motor_Speed_Limit_s16  - VEHICLE_0X100_TX_CANID_MOTOR_SPEED_LIMIT_S16_OFFSET);
  message->Reserved_for_LTVS_u8 = (message->Reserved_for_LTVS_u8  - VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U8_OFFSET);
  message->Max_Regeneration_u8 = (message->Max_Regeneration_u8  - VEHICLE_0X100_TX_CANID_MAX_REGENERATION_U8_OFFSET);
  message->Reserved_for_LTVS_u32 = (message->Reserved_for_LTVS_u32  - VEHICLE_0X100_TX_CANID_RESERVED_FOR_LTVS_U32_OFFSET);
  message->Ride_mode_Request_u8 = (message->Ride_mode_Request_u8  - VEHICLE_0X100_TX_CANID_RIDE_MODE_REQUEST_U8_OFFSET);
  message->Throttle_Map_select_u8 = (message->Throttle_Map_select_u8  - VEHICLE_0X100_TX_CANID_THROTTLE_MAP_SELECT_U8_OFFSET);
  message->MotorStop_u8 = (message->MotorStop_u8  - VEHICLE_0X100_TX_CANID_MOTORSTOP_U8_OFFSET);
  message->DrivingDirection_u8 = (message->DrivingDirection_u8  - VEHICLE_0X100_TX_CANID_DRIVINGDIRECTION_U8_OFFSET);
  data[0] = (message->Motor_Speed_Limit_s16 & (SIGNAL_READ_Mask8)) ;
  data[1] = ((message->Motor_Speed_Limit_s16 >> VEHICLE_0X100_TX_MOTOR_SPEED_LIMIT_S16_MASK0) & (SIGNAL_READ_Mask8)) ;
  data[2] = (message->Reserved_for_LTVS_u8 & (SIGNAL_READ_Mask8)) ;
  data[3] = (message->Max_Regeneration_u8 & (SIGNAL_READ_Mask8)) ;
  data[4] = (message->Reserved_for_LTVS_u32 & (SIGNAL_READ_Mask8)) ;
  data[5] = ((message->Reserved_for_LTVS_u32 >> VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U32_MASK1) & (SIGNAL_READ_Mask8)) ;
  data[6] = ((message->Reserved_for_LTVS_u32 >> VEHICLE_0X100_TX_RESERVED_FOR_LTVS_U32_MASK0) & (SIGNAL_READ_Mask8)) ;
  data[7] = (message->Ride_mode_Request_u8 & (SIGNAL_READ_Mask3)) | ((message->Throttle_Map_select_u8 & (SIGNAL_READ_Mask3)) << VEHICLE_0X100_TX_THROTTLE_MAP_SELECT_U8_MASK0) | ((message->MotorStop_u8 & (SIGNAL_READ_Mask1)) << VEHICLE_0X100_TX_MOTORSTOP_U8_MASK0) | ((message->DrivingDirection_u8 & (SIGNAL_READ_Mask1)) << VEHICLE_0X100_TX_DRIVINGDIRECTION_U8_MASK0) ;
  return VEHICLE_0X100_TX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x300_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x300_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Serialize_Vehicle_0x300_Tx(Vehicle_0x300_Tx_t* message, uint8_t* data)
{
  message->Reserved_for_LTVS_u64 = (message->Reserved_for_LTVS_u64  - VEHICLE_0X300_TX_CANID_RESERVED_FOR_LTVS_U64_OFFSET);
  data[0] = (message->Reserved_for_LTVS_u64 & (SIGNAL_READ_Mask8)) ;
  data[1] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK6) & (SIGNAL_READ_Mask8)) ;
  data[2] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK5) & (SIGNAL_READ_Mask8)) ;
  data[3] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK4) & (SIGNAL_READ_Mask8)) ;
  data[4] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK3) & (SIGNAL_READ_Mask8)) ;
  data[5] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK2) & (SIGNAL_READ_Mask8)) ;
  data[6] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK1) & (SIGNAL_READ_Mask8)) ;
  data[7] = ((message->Reserved_for_LTVS_u64 >> VEHICLE_0X300_TX_RESERVED_FOR_LTVS_U64_MASK0) & (SIGNAL_READ_Mask8)) ;
  return VEHICLE_0X300_TX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Vehicle_0x400_Tx
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : Vehicle_0x400_Tx_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Serialize_Vehicle_0x400_Tx(Vehicle_0x400_Tx_t* message, uint8_t* data)
{
  message->Broadcast_rate_u8 = (message->Broadcast_rate_u8  - VEHICLE_0X400_TX_CANID_BROADCAST_RATE_U8_OFFSET);
  message->Broadcast_Mode_u8 = (message->Broadcast_Mode_u8  - VEHICLE_0X400_TX_CANID_BROADCAST_MODE_U8_OFFSET);
  data[0] = (message->Broadcast_rate_u8 & (SIGNAL_READ_Mask8)) ;
  data[1] = (message->Broadcast_Mode_u8 & (SIGNAL_READ_Mask1)) ;
  return VEHICLE_0X400_TX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x150_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x150_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Deserialize_Motor_0x150_Rx(Motor_0x150_Rx_t* message, const uint8_t* data)
{
  message->Motor_Speed_s16 = (((data[1] & (SIGNAL_READ_Mask8)) << MOTOR_0X150_RX_MOTOR_SPEED_S16_MASK0) | (data[0] & (SIGNAL_READ_Mask8))) + MOTOR_0X150_RX_CANID_MOTOR_SPEED_S16_OFFSET;
  message->Battery_current_s16 = (((data[3] & (SIGNAL_READ_Mask8)) << MOTOR_0X150_RX_BATTERY_CURRENT_S16_MASK0) | (data[2] & (SIGNAL_READ_Mask8))) + MOTOR_0X150_RX_CANID_BATTERY_CURRENT_S16_OFFSET;
  message->Reserved_for_LTVS_s16 = (((data[5] & (SIGNAL_READ_Mask6)) << MOTOR_0X150_RX_RESERVED_FOR_LTVS_S16_MASK0) | (data[4] & (SIGNAL_READ_Mask8))) + MOTOR_0X150_RX_CANID_RESERVED_FOR_LTVS_S16_OFFSET;
  message->Position_Sensor_Error_u8 = (((data[5] >> MOTOR_0X150_RX_POSITION_SENSOR_ERROR_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_POSITION_SENSOR_ERROR_U8_OFFSET;
  message->Throttle_Disable_Status_u8 = (((data[5] >> MOTOR_0X150_RX_THROTTLE_DISABLE_STATUS_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_THROTTLE_DISABLE_STATUS_U8_OFFSET;
  message->Motor_Enable_Status_u8 = ((data[6] & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_MOTOR_ENABLE_STATUS_U8_OFFSET;
  message->Peripheral_supply_failure_u8 = (((data[6] >> MOTOR_0X150_RX_PERIPHERAL_SUPPLY_FAILURE_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_PERIPHERAL_SUPPLY_FAILURE_U8_OFFSET;
  message->Motor_temperature_warning_u8 = (((data[6] >> MOTOR_0X150_RX_MOTOR_TEMPERATURE_WARNING_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_MOTOR_TEMPERATURE_WARNING_U8_OFFSET;
  message->Controller_temp_warning_u8 = (((data[6] >> MOTOR_0X150_RX_CONTROLLER_TEMP_WARNING_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_CONTROLLER_TEMP_WARNING_U8_OFFSET;
  message->Over_Voltage_error_u8 = (((data[6] >> MOTOR_0X150_RX_OVER_VOLTAGE_ERROR_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_OVER_VOLTAGE_ERROR_U8_OFFSET;
  message->Under_Voltage_Error_u8 = (((data[6] >> MOTOR_0X150_RX_UNDER_VOLTAGE_ERROR_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_UNDER_VOLTAGE_ERROR_U8_OFFSET;
  message->Over_current_error_u8 = (((data[6] >> MOTOR_0X150_RX_OVER_CURRENT_ERROR_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_OVER_CURRENT_ERROR_U8_OFFSET;
  message->Timeout_error_u8 = (((data[6] >> MOTOR_0X150_RX_TIMEOUT_ERROR_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_TIMEOUT_ERROR_U8_OFFSET;
  message->Analog_throttle_error_u8 = ((data[7] & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_ANALOG_THROTTLE_ERROR_U8_OFFSET;
  message->Driving_direction_actual_u8 = (((data[7] >> MOTOR_0X150_RX_DRIVING_DIRECTION_ACTUAL_U8_MASK0) & (SIGNAL_READ_Mask1))) + MOTOR_0X150_RX_CANID_DRIVING_DIRECTION_ACTUAL_U8_OFFSET;
  message->Ride_mode_Actual_u8 = (((data[7] >> MOTOR_0X150_RX_RIDE_MODE_ACTUAL_U8_MASK0) & (SIGNAL_READ_Mask3))) + MOTOR_0X150_RX_CANID_RIDE_MODE_ACTUAL_U8_OFFSET;
  message->Throttle_Map_Actual_u8 = (((data[7] >> MOTOR_0X150_RX_THROTTLE_MAP_ACTUAL_U8_MASK0) & (SIGNAL_READ_Mask3))) + MOTOR_0X150_RX_CANID_THROTTLE_MAP_ACTUAL_U8_OFFSET;
  return MOTOR_0X150_RX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x200_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x200_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Deserialize_Motor_0x200_Rx(Motor_0x200_Rx_t* message, const uint8_t* data)
{
  message->Throttle_out_u8 = ((data[0] & (SIGNAL_READ_Mask8))) + MOTOR_0X200_RX_CANID_THROTTLE_OUT_U8_OFFSET;
  message->Throttle_Reference_u8 = ((data[1] & (SIGNAL_READ_Mask8))) + MOTOR_0X200_RX_CANID_THROTTLE_REFERENCE_U8_OFFSET;
  message->Reserved_for_LTVS_u64 = (((uint64_t)(data[6] & (SIGNAL_READ_Mask8)) << MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK0) | ((data[5] & (SIGNAL_READ_Mask8)) << MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK1) | ((data[4] & (SIGNAL_READ_Mask8)) << MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK2) | ((data[3] & (SIGNAL_READ_Mask8)) << MOTOR_0X200_RX_RESERVED_FOR_LTVS_U64_MASK3) | (data[2] & (SIGNAL_READ_Mask8))) + MOTOR_0X200_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET;
  message->Battery_Voltage_u8 = ((data[7] & (SIGNAL_READ_Mask8))) + MOTOR_0X200_RX_CANID_BATTERY_VOLTAGE_U8_OFFSET;
  return MOTOR_0X200_RX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x250_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x250_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Deserialize_Motor_0x250_Rx(Motor_0x250_Rx_t* message, const uint8_t* data)
{
  message->Motor_temperature_s16 = (((data[1] & (SIGNAL_READ_Mask2)) << MOTOR_0X250_RX_MOTOR_TEMPERATURE_S16_MASK0) | (data[0] & (SIGNAL_READ_Mask8))) + MOTOR_0X250_RX_CANID_MOTOR_TEMPERATURE_S16_OFFSET;
  message->Controller_temperature_s16 = (((data[3] & (SIGNAL_READ_Mask2)) << MOTOR_0X250_RX_CONTROLLER_TEMPERATURE_S16_MASK0) | (data[2] & (SIGNAL_READ_Mask8))) + MOTOR_0X250_RX_CANID_CONTROLLER_TEMPERATURE_S16_OFFSET;
  message->Motor_Rotation_Number_u32 = (((data[7] & (SIGNAL_READ_Mask8)) << MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK0) | ((data[6] & (SIGNAL_READ_Mask8)) << MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK1) | ((data[5] & (SIGNAL_READ_Mask8)) << MOTOR_0X250_RX_MOTOR_ROTATION_NUMBER_U32_MASK2) | (data[4] & (SIGNAL_READ_Mask8))) + MOTOR_0X250_RX_CANID_MOTOR_ROTATION_NUMBER_U32_OFFSET;
  return MOTOR_0X250_RX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_Motor_0x650_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x650_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
uint32_t Deserialize_Motor_0x650_Rx(Motor_0x650_Rx_t* message, const uint8_t* data)
{
  message->Reserved_for_LTVS_u64 = (((uint64_t)(data[7] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK0) | ((uint64_t)(data[6] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK1) | ((uint64_t)(data[5] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK2) | ((uint64_t)(data[4] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK3) | ((data[3] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK4) | ((data[2] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK5) | ((data[1] & (SIGNAL_READ_Mask8)) << MOTOR_0X650_RX_RESERVED_FOR_LTVS_U64_MASK6) | (data[0] & (SIGNAL_READ_Mask8))) + MOTOR_0X650_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET;
  return MOTOR_0X650_RX_ID; 
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_Motor_0x750_Rx
*   Description   : This function updates the signals data from the recived CAN 
*		    data.
*   Parameters    : Motor_0x750_Rx_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/  
 uint32_t Deserialize_Motor_0x750_Rx(Motor_0x750_Rx_t* message, const uint8_t* data)
{
  message->Software_version_u8 = ((data[0] & (SIGNAL_READ_Mask8))) + MOTOR_0X750_RX_CANID_SOFTWARE_VERSION_U8_OFFSET;
  message->Hardware_version_u8 = ((data[1] & (SIGNAL_READ_Mask8))) + MOTOR_0X750_RX_CANID_HARDWARE_VERSION_U8_OFFSET;
  message->Reserved_for_LTVS_u64 = (((uint64_t)(data[6] & (SIGNAL_READ_Mask8)) << MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK0) | ((data[5] & (SIGNAL_READ_Mask8)) << MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK1) | ((data[4] & (SIGNAL_READ_Mask8)) << MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK2) | ((data[3] & (SIGNAL_READ_Mask8)) << MOTOR_0X750_RX_RESERVED_FOR_LTVS_U64_MASK3) | (data[2] & (SIGNAL_READ_Mask8))) + MOTOR_0X750_RX_CANID_RESERVED_FOR_LTVS_U64_OFFSET;
  return MOTOR_0X750_RX_ID; 
}

#endif /* MCU_CAN_SIGNALS_C */
/*---------------------- End of File -----------------------------------------*/
