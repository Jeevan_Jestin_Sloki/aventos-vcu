﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms_can_signals.c
|    Project        : VCU
|    Module         : BMS can signals data
|    Description    : This is the generated file from the DBC2CH Tool for BMS.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 08/07/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_CAN_SIGNALS_C
#define BMS_CAN_SIGNALS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "bms_can_signals.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x2AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x2AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Deserialize_BMS_Rx_0x2AA(BMS_Rx_0x2AA_t *message, const uint8_t *data)
{
  message->SoC_u8 = ((data[0] & (SIGNAL_READ_Mask8))) + BMS_RX_0X2AA_CANID_SOC_U8_OFFSET;
  return BMS_RX_0X2AA_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x4AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x4AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Deserialize_BMS_Rx_0x4AA(BMS_Rx_0x4AA_t *message, const uint8_t *data)
{
  message->HighModuleTemperature_u8 = ((data[0] & (SIGNAL_READ_Mask8))) + BMS_RX_0X4AA_CANID_HIGHMODULETEMPERATURE_U8_OFFSET;
  return BMS_RX_0X4AA_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x4AB
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x4AB_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Deserialize_BMS_Rx_0x4AB(BMS_Rx_0x4AB_t *message, const uint8_t *data)
{
  message->AvailableCapacity_u32 = (((data[3] & (SIGNAL_READ_Mask8)) << BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK0) | ((data[2] & (SIGNAL_READ_Mask8)) << BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK1) | ((data[1] & (SIGNAL_READ_Mask8)) << BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK2) | (data[0] & (SIGNAL_READ_Mask8))) + BMS_RX_0X4AB_CANID_AVAILABLECAPACITY_U32_OFFSET;
  message->PDU_Temperature_u8 = ((data[4] & (SIGNAL_READ_Mask8))) + BMS_RX_0X4AB_CANID_PDU_TEMPERATURE_U8_OFFSET;
  message->BMS_Temperature_u8 = ((data[5] & (SIGNAL_READ_Mask8))) + BMS_RX_0X4AB_CANID_BMS_TEMPERATURE_U8_OFFSET;
  return BMS_RX_0X4AB_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x3AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x3AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Deserialize_BMS_Rx_0x3AA(BMS_Rx_0x3AA_t *message, const uint8_t *data)
{
  message->BatteryState_u8 = ((data[0] & (SIGNAL_READ_Mask8))) + BMS_RX_0X3AA_CANID_BATTERYSTATE_U8_OFFSET;
  message->BatteryVoltage_u16 = (((data[2] & (SIGNAL_READ_Mask8)) << BMS_RX_0X3AA_BATTERYVOLTAGE_U16_MASK0) | (data[1] & (SIGNAL_READ_Mask8))) + BMS_RX_0X3AA_CANID_BATTERYVOLTAGE_U16_OFFSET;
  return BMS_RX_0X3AA_ID;
}

#endif /* BMS_CAN_SIGNALS_C */
/*---------------------- End of File -----------------------------------------*/