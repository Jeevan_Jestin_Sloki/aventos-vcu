﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms_can_signals.h
|    Project        : VCU
|    Module         : BMS can signals data
|    Description    : This is the generated file from the DBC2CH Tool for BMS.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 08/07/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_CAN_SIGNALS_H
#define BMS_CAN_SIGNALS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include <stdint.h>
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK0             24U
#define BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK1             16U
#define BMS_RX_0X4AB_AVAILABLECAPACITY_U32_MASK2             8U


#define BMS_RX_0X3AA_BATTERYVOLTAGE_U16_MASK0                8U


#define SIGNAL_READ_Mask0                                    0U    
#define SIGNAL_READ_Mask1                                    0x01U 
#define SIGNAL_READ_Mask2                                    0x03U 
#define SIGNAL_READ_Mask3                                    0x07U 
#define SIGNAL_READ_Mask4                                    0x0FU 
#define SIGNAL_READ_Mask5                                    0x1FU 
#define SIGNAL_READ_Mask6                                    0x3FU 
#define SIGNAL_READ_Mask7                                    0x7FU 
#define SIGNAL_READ_Mask8                                    0xFFU 


#define SIGNAL_WRITE_Mask0                                   0x80U 
#define SIGNAL_WRITE_Mask1                                   0xC0U 
#define SIGNAL_WRITE_Mask2                                   0xE0U 
#define SIGNAL_WRITE_Mask3                                   0xF0U 
#define SIGNAL_WRITE_Mask4                                   0xF8U 
#define SIGNAL_WRITE_Mask5                                   0xFCU 
#define SIGNAL_WRITE_Mask6                                   0xFEU 
#define SIGNAL_WRITE_Mask7                                   0xFFU
/* def @BMS_RX_0X2AA CAN Message                             (682) */
#define BMS_RX_0X2AA_ID                                      (682U)
#define BMS_RX_0X2AA_IDE                                     (0U)
#define BMS_RX_0X2AA_DLC                                     (8U)


#define BMS_RX_0X2AA_SOC_U8FACTOR                            (1)
#define BMS_RX_0X2AA_CANID_SOC_U8_STARTBIT                   (0)
#define BMS_RX_0X2AA_CANID_SOC_U8_OFFSET                     (0)
#define BMS_RX_0X2AA_CANID_SOC_U8_MIN                        (0)
#define BMS_RX_0X2AA_CANID_SOC_U8_MAX                        (100)



/* def @BMS_RX_0X4AA CAN Message                             (1194) */
#define BMS_RX_0X4AA_ID                                      (1194U)
#define BMS_RX_0X4AA_IDE                                     (0U)
#define BMS_RX_0X4AA_DLC                                     (8U)


#define BMS_RX_0X4AA_HIGHMODULETEMPERATURE_U8FACTOR          (1)
#define BMS_RX_0X4AA_CANID_HIGHMODULETEMPERATURE_U8_STARTBIT (0)
#define BMS_RX_0X4AA_CANID_HIGHMODULETEMPERATURE_U8_OFFSET   (0)
#define BMS_RX_0X4AA_CANID_HIGHMODULETEMPERATURE_U8_MIN      (-128)
#define BMS_RX_0X4AA_CANID_HIGHMODULETEMPERATURE_U8_MAX      (127)



/* def @BMS_RX_0X4AB CAN Message                             (1195) */
#define BMS_RX_0X4AB_ID                                      (1195U)
#define BMS_RX_0X4AB_IDE                                     (0U)
#define BMS_RX_0X4AB_DLC                                     (8U)


#define BMS_RX_0X4AB_AVAILABLECAPACITY_U32FACTOR             (0.001)
#define BMS_RX_0X4AB_CANID_AVAILABLECAPACITY_U32_STARTBIT    (0)
#define BMS_RX_0X4AB_CANID_AVAILABLECAPACITY_U32_OFFSET      (0)
#define BMS_RX_0X4AB_CANID_AVAILABLECAPACITY_U32_MIN         (0)
#define BMS_RX_0X4AB_CANID_AVAILABLECAPACITY_U32_MAX         (4294967.295)
#define BMS_RX_0X4AB_PDU_TEMPERATURE_U8FACTOR                (1)
#define BMS_RX_0X4AB_CANID_PDU_TEMPERATURE_U8_STARTBIT       (32)
#define BMS_RX_0X4AB_CANID_PDU_TEMPERATURE_U8_OFFSET         (0)
#define BMS_RX_0X4AB_CANID_PDU_TEMPERATURE_U8_MIN            (-128)
#define BMS_RX_0X4AB_CANID_PDU_TEMPERATURE_U8_MAX            (127)
#define BMS_RX_0X4AB_BMS_TEMPERATURE_U8FACTOR                (1)
#define BMS_RX_0X4AB_CANID_BMS_TEMPERATURE_U8_STARTBIT       (40)
#define BMS_RX_0X4AB_CANID_BMS_TEMPERATURE_U8_OFFSET         (0)
#define BMS_RX_0X4AB_CANID_BMS_TEMPERATURE_U8_MIN            (-128)
#define BMS_RX_0X4AB_CANID_BMS_TEMPERATURE_U8_MAX            (127)



/* def @BMS_RX_0X3AA CAN Message                             (938) */
#define BMS_RX_0X3AA_ID                                      (938U)
#define BMS_RX_0X3AA_IDE                                     (0U)
#define BMS_RX_0X3AA_DLC                                     (8U)


#define BMS_RX_0X3AA_BATTERYSTATE_U8FACTOR                   (1)
#define BMS_RX_0X3AA_CANID_BATTERYSTATE_U8_STARTBIT          (0)
#define BMS_RX_0X3AA_CANID_BATTERYSTATE_U8_OFFSET            (0)
#define BMS_RX_0X3AA_CANID_BATTERYSTATE_U8_MIN               (0)
#define BMS_RX_0X3AA_CANID_BATTERYSTATE_U8_MAX               (256)
#define BMS_RX_0X3AA_BATTERYVOLTAGE_U16FACTOR                (0.001)
#define BMS_RX_0X3AA_CANID_BATTERYVOLTAGE_U16_STARTBIT       (8)
#define BMS_RX_0X3AA_CANID_BATTERYVOLTAGE_U16_OFFSET         (0)
#define BMS_RX_0X3AA_CANID_BATTERYVOLTAGE_U16_MIN            (0)
#define BMS_RX_0X3AA_CANID_BATTERYVOLTAGE_U16_MAX            (65.535)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef struct
{
  uint8_t SoC_u8;
} BMS_Rx_0x2AA_t;

typedef struct
{
  int8_t HighModuleTemperature_u8;
} BMS_Rx_0x4AA_t;

typedef struct
{
  uint32_t AvailableCapacity_u32;
  int8_t PDU_Temperature_u8;
  int8_t BMS_Temperature_u8;
} BMS_Rx_0x4AB_t;

typedef struct
{
  uint16_t BatteryVoltage_u16;
  uint8_t BatteryState_u8;
 
} BMS_Rx_0x3AA_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x2AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x2AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_BMS_Rx_0x2AA(BMS_Rx_0x2AA_t *message,
                                         const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x4AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x4AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_BMS_Rx_0x4AA(BMS_Rx_0x4AA_t *message,
                                         const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x4AB
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x4AB_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_BMS_Rx_0x4AB(BMS_Rx_0x4AB_t *message,
                                         const uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Deserialize_BMS_Rx_0x3AA
*   Description   : This function updates the signals data to the Receiving CAN 
*		                data.
*   Parameters    : BMS_Rx_0x3AA_t* message, const uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Deserialize_BMS_Rx_0x3AA(BMS_Rx_0x3AA_t *message,
                                         const uint8_t *data);

#endif /* BMS_CAN_SIGNALS_H */
/*---------------------- End of File -----------------------------------------*/