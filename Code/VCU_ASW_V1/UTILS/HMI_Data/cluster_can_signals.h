﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : cluster_can_signals.h
|    Project        : VCU
|    Module         : cluster signals data
|    Description    : This is the generated file from the DBC2CH Tool for HMI.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 04/05/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CLUSTER_CAN_SIGNALS_H
#define CLUSTER_CAN_SIGNALS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include <stdint.h>
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define _I_HMI_TX_EVE_1__I_SPORTS_MODE_U8_MASK0  2U
#define _I_HMI_TX_EVE_1__I_ECO_MODE_U8_MASK0  4U
#define _I_HMI_TX_EVE_1__I_NEUTRAL_MODE_U8_MASK0  6U
#define _I_HMI_TX_EVE_1__I_RIGHT_INDICATOR_U8_MASK0  2U
#define _I_HMI_TX_EVE_1__I_LEFT_INDICATOR_U8_MASK0  4U
#define _I_HMI_TX_EVE_1__I_SAFE_MODE_U8_MASK0  6U
#define _I_HMI_TX_EVE_1__I_BATTERY_FAULT_U8_MASK0  2U
#define _I_HMI_TX_EVE_1__I_HIGH_BEAM_U8_MASK0  4U
#define _I_HMI_TX_EVE_1__I_LOW_BEAM_U8_MASK0  6U
#define _I_HMI_TX_EVE_1__I_BATTERY_STATUS_U8_MASK0  1U
#define _I_HMI_TX_EVE_1__I_WARNING_SYMBOL_U8_MASK0  2U
#define _I_HMI_TX_EVE_1__I_SERVICE_INDICATOR_U8_MASK0  4U
#define _I_HMI_TX_EVE_1__I_SIDE_STAND_U8_MASK0  6U
#define _I_HMI_TX_EVE_1__I_CLUSTER_USE_PLACE_U8_MASK0  7U
#define _I_HMI_TX_EVE_1__I_EVE_1_COUNTER_U8_MASK0  4U


#define HMI_TX_PER_1_ODO_U32_MASK0  24U
#define HMI_TX_PER_1_ODO_U32_MASK1  16U
#define HMI_TX_PER_1_ODO_U32_MASK2  8U
#define HMI_TX_PER_1_PER_1_COUNTER_U8_MASK0  4U


#define _I_HMI_TX_PER_1__I_ODO_U32_MASK0  24U
#define _I_HMI_TX_PER_1__I_ODO_U32_MASK1  16U
#define _I_HMI_TX_PER_1__I_ODO_U32_MASK2  8U
#define _I_HMI_TX_PER_1__I_PER_1_COUNTER_U8_MASK0  4U


#define HMI_TX_PER_2_POWER_CONSUMPTION_U16_MASK0  8U
#define HMI_TX_PER_2_RANGE_KM_U16_MASK0  8U
#define HMI_TX_PER_2_BLE_ICON_U8_MASK0  4U
#define HMI_TX_PER_2_REGEN_BRAKING_U8_MASK0  6U
#define HMI_TX_PER_2_PER_2_COUNTER_U8_MASK0  4U


#define _I_HMI_TX_PER_2__I_POWER_CONSUMPTION_U16_MASK0  8U
#define _I_HMI_TX_PER_2__I_RANGE_KM_U16_MASK0  8U
#define _I_HMI_TX_PER_2__I_BLE_ICON_U8_MASK0  4U
#define _I_HMI_TX_PER_2__I_REGEN_BRAKING_U8_MASK0  6U
#define _I_HMI_TX_PER_2__I_PER_2_COUNTER_U8_MASK0  4U


#define HMI_TX_EVE_1_SPORTS_MODE_U8_MASK0  2U
#define HMI_TX_EVE_1_ECO_MODE_U8_MASK0  4U
#define HMI_TX_EVE_1_NEUTRAL_MODE_U8_MASK0  6U
#define HMI_TX_EVE_1_RIGHT_INDICATOR_U8_MASK0  2U
#define HMI_TX_EVE_1_LEFT_INDICATOR_U8_MASK0  4U
#define HMI_TX_EVE_1_SAFE_MODE_U8_MASK0  6U
#define HMI_TX_EVE_1_BATTERY_FAULT_U8_MASK0  2U
#define HMI_TX_EVE_1_HIGH_BEAM_U8_MASK0  4U
#define HMI_TX_EVE_1_LOW_BEAM_U8_MASK0  6U
#define HMI_TX_EVE_1_BATTERY_STATUS_U8_MASK0  1U
#define HMI_TX_EVE_1_WARNING_SYMBOL_U8_MASK0  2U
#define HMI_TX_EVE_1_SERVICE_INDICATOR_U8_MASK0  4U
#define HMI_TX_EVE_1_SIDE_STAND_U8_MASK0  6U
#define HMI_TX_EVE_1_CLUSTER_USE_PLACE_U8_MASK0  7U
#define HMI_TX_EVE_1_EVE_1_COUNTER_U8_MASK0  4U


#define SIGNAL_READ_Mask0     0U    
#define SIGNAL_READ_Mask1     0x01U 
#define SIGNAL_READ_Mask2     0x03U 
#define SIGNAL_READ_Mask3     0x07U 
#define SIGNAL_READ_Mask4     0x0FU 
#define SIGNAL_READ_Mask5     0x1FU 
#define SIGNAL_READ_Mask6     0x3FU 
#define SIGNAL_READ_Mask7     0x7FU 
#define SIGNAL_READ_Mask8     0xFFU 


#define SIGNAL_WRITE_Mask0    0x80U 
#define SIGNAL_WRITE_Mask1    0xC0U 
#define SIGNAL_WRITE_Mask2    0xE0U 
#define SIGNAL_WRITE_Mask3    0xF0U 
#define SIGNAL_WRITE_Mask4    0xF8U 
#define SIGNAL_WRITE_Mask5    0xFCU 
#define SIGNAL_WRITE_Mask6    0xFEU 
#define SIGNAL_WRITE_Mask7    0xFFU 
/* def @_I_HMI_TX_EVE_1 CAN Message                                   (1793) */
#define _I_HMI_TX_EVE_1_ID                                            (1793U)
#define _I_HMI_TX_EVE_1_IDE                                           (0U)
#define _I_HMI_TX_EVE_1_DLC                                           (8U)


#define _I_HMI_TX_EVE_1__I_REVERSE_MODE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_REVERSE_MODE_U8_STARTBIT                           (1)
#define _I_HMI_TX_EVE_1_CANID__I_REVERSE_MODE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_REVERSE_MODE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_REVERSE_MODE_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_SPORTS_MODE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_SPORTS_MODE_U8_STARTBIT                           (3)
#define _I_HMI_TX_EVE_1_CANID__I_SPORTS_MODE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_SPORTS_MODE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_SPORTS_MODE_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_ECO_MODE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_ECO_MODE_U8_STARTBIT                           (5)
#define _I_HMI_TX_EVE_1_CANID__I_ECO_MODE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_ECO_MODE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_ECO_MODE_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_NEUTRAL_MODE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_NEUTRAL_MODE_U8_STARTBIT                           (7)
#define _I_HMI_TX_EVE_1_CANID__I_NEUTRAL_MODE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_NEUTRAL_MODE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_NEUTRAL_MODE_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_KILL_SWITCH_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_KILL_SWITCH_U8_STARTBIT                           (9)
#define _I_HMI_TX_EVE_1_CANID__I_KILL_SWITCH_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_KILL_SWITCH_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_KILL_SWITCH_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_RIGHT_INDICATOR_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_STARTBIT                           (11)
#define _I_HMI_TX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_LEFT_INDICATOR_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_LEFT_INDICATOR_U8_STARTBIT                           (13)
#define _I_HMI_TX_EVE_1_CANID__I_LEFT_INDICATOR_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_LEFT_INDICATOR_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_LEFT_INDICATOR_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_SAFE_MODE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_SAFE_MODE_U8_STARTBIT                           (15)
#define _I_HMI_TX_EVE_1_CANID__I_SAFE_MODE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_SAFE_MODE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_SAFE_MODE_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_MOTOR_FAULT_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_MOTOR_FAULT_U8_STARTBIT                           (17)
#define _I_HMI_TX_EVE_1_CANID__I_MOTOR_FAULT_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_MOTOR_FAULT_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_MOTOR_FAULT_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_BATTERY_FAULT_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_FAULT_U8_STARTBIT                           (19)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_FAULT_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_FAULT_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_FAULT_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_HIGH_BEAM_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_HIGH_BEAM_U8_STARTBIT                           (21)
#define _I_HMI_TX_EVE_1_CANID__I_HIGH_BEAM_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_HIGH_BEAM_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_HIGH_BEAM_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_LOW_BEAM_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_LOW_BEAM_U8_STARTBIT                           (23)
#define _I_HMI_TX_EVE_1_CANID__I_LOW_BEAM_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_LOW_BEAM_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_LOW_BEAM_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_IGNITION_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_IGNITION_U8_STARTBIT                           (24)
#define _I_HMI_TX_EVE_1_CANID__I_IGNITION_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_IGNITION_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_IGNITION_U8_MAX                                (1)
#define _I_HMI_TX_EVE_1__I_BATTERY_STATUS_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_STATUS_U8_STARTBIT                           (25)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_STATUS_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_STATUS_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_BATTERY_STATUS_U8_MAX                                (1)
#define _I_HMI_TX_EVE_1__I_WARNING_SYMBOL_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_WARNING_SYMBOL_U8_STARTBIT                           (27)
#define _I_HMI_TX_EVE_1_CANID__I_WARNING_SYMBOL_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_WARNING_SYMBOL_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_WARNING_SYMBOL_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_SERVICE_INDICATOR_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_STARTBIT                           (29)
#define _I_HMI_TX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_SIDE_STAND_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_SIDE_STAND_U8_STARTBIT                           (31)
#define _I_HMI_TX_EVE_1_CANID__I_SIDE_STAND_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_SIDE_STAND_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_SIDE_STAND_U8_MAX                                (3)
#define _I_HMI_TX_EVE_1__I_HOURS_TIME_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_HOURS_TIME_U8_STARTBIT                           (39)
#define _I_HMI_TX_EVE_1_CANID__I_HOURS_TIME_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_HOURS_TIME_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_HOURS_TIME_U8_MAX                                (255)
#define _I_HMI_TX_EVE_1__I_MINUTES_TIME_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_MINUTES_TIME_U8_STARTBIT                           (47)
#define _I_HMI_TX_EVE_1_CANID__I_MINUTES_TIME_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_MINUTES_TIME_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_MINUTES_TIME_U8_MAX                                (255)
#define _I_HMI_TX_EVE_1__I_CLUSTER_USE_PLACE_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_CLUSTER_USE_PLACE_U8_STARTBIT                           (55)
#define _I_HMI_TX_EVE_1_CANID__I_CLUSTER_USE_PLACE_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_CLUSTER_USE_PLACE_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_CLUSTER_USE_PLACE_U8_MAX                                (1)
#define _I_HMI_TX_EVE_1__I_EVE_1_COUNTER_U8FACTOR                                   (1)
#define _I_HMI_TX_EVE_1_CANID__I_EVE_1_COUNTER_U8_STARTBIT                           (63)
#define _I_HMI_TX_EVE_1_CANID__I_EVE_1_COUNTER_U8_OFFSET                             (0)
#define _I_HMI_TX_EVE_1_CANID__I_EVE_1_COUNTER_U8_MIN                                (0)
#define _I_HMI_TX_EVE_1_CANID__I_EVE_1_COUNTER_U8_MAX                                (15)


/* def @HMI_TX_PER_1 CAN Message                                   (1794) */
#define HMI_TX_PER_1_ID                                            (1794U)
#define HMI_TX_PER_1_IDE                                           (0U)
#define HMI_TX_PER_1_DLC                                           (8U)


#define HMI_TX_PER_1_ODO_U32FACTOR                                   (1)
#define HMI_TX_PER_1_CANID_ODO_U32_STARTBIT                           (7)
#define HMI_TX_PER_1_CANID_ODO_U32_OFFSET                             (0)
#define HMI_TX_PER_1_CANID_ODO_U32_MIN                                (0)
#define HMI_TX_PER_1_CANID_ODO_U32_MAX                                (99999900)
#define HMI_TX_PER_1_VEHICLE_SPEED_U8FACTOR                                   (1)
#define HMI_TX_PER_1_CANID_VEHICLE_SPEED_U8_STARTBIT                           (39)
#define HMI_TX_PER_1_CANID_VEHICLE_SPEED_U8_OFFSET                             (0)
#define HMI_TX_PER_1_CANID_VEHICLE_SPEED_U8_MIN                                (0)
#define HMI_TX_PER_1_CANID_VEHICLE_SPEED_U8_MAX                                (199)
#define HMI_TX_PER_1_MILEAGE_U8FACTOR                                   (1)
#define HMI_TX_PER_1_CANID_MILEAGE_U8_STARTBIT                           (47)
#define HMI_TX_PER_1_CANID_MILEAGE_U8_OFFSET                             (0)
#define HMI_TX_PER_1_CANID_MILEAGE_U8_MIN                                (0)
#define HMI_TX_PER_1_CANID_MILEAGE_U8_MAX                                (99)
#define HMI_TX_PER_1_PER_1_COUNTER_U8FACTOR                                   (1)
#define HMI_TX_PER_1_CANID_PER_1_COUNTER_U8_STARTBIT                           (63)
#define HMI_TX_PER_1_CANID_PER_1_COUNTER_U8_OFFSET                             (0)
#define HMI_TX_PER_1_CANID_PER_1_COUNTER_U8_MIN                                (0)
#define HMI_TX_PER_1_CANID_PER_1_COUNTER_U8_MAX                                (15)


/* def @_I_HMI_TX_PER_1 CAN Message                                   (1795) */
#define _I_HMI_TX_PER_1_ID                                            (1795U)
#define _I_HMI_TX_PER_1_IDE                                           (0U)
#define _I_HMI_TX_PER_1_DLC                                           (8U)


#define _I_HMI_TX_PER_1__I_ODO_U32FACTOR                                   (1)
#define _I_HMI_TX_PER_1_CANID__I_ODO_U32_STARTBIT                           (7)
#define _I_HMI_TX_PER_1_CANID__I_ODO_U32_OFFSET                             (0)
#define _I_HMI_TX_PER_1_CANID__I_ODO_U32_MIN                                (0)
#define _I_HMI_TX_PER_1_CANID__I_ODO_U32_MAX                                (4294967295)
#define _I_HMI_TX_PER_1__I_VEHICLE_SPEED_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_1_CANID__I_VEHICLE_SPEED_U8_STARTBIT                           (39)
#define _I_HMI_TX_PER_1_CANID__I_VEHICLE_SPEED_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_1_CANID__I_VEHICLE_SPEED_U8_MIN                                (0)
#define _I_HMI_TX_PER_1_CANID__I_VEHICLE_SPEED_U8_MAX                                (255)
#define _I_HMI_TX_PER_1__I_MILEAGE_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_1_CANID__I_MILEAGE_U8_STARTBIT                           (47)
#define _I_HMI_TX_PER_1_CANID__I_MILEAGE_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_1_CANID__I_MILEAGE_U8_MIN                                (0)
#define _I_HMI_TX_PER_1_CANID__I_MILEAGE_U8_MAX                                (255)
#define _I_HMI_TX_PER_1__I_PER_1_COUNTER_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_1_CANID__I_PER_1_COUNTER_U8_STARTBIT                           (63)
#define _I_HMI_TX_PER_1_CANID__I_PER_1_COUNTER_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_1_CANID__I_PER_1_COUNTER_U8_MIN                                (0)
#define _I_HMI_TX_PER_1_CANID__I_PER_1_COUNTER_U8_MAX                                (15)



/* def @HMI_TX_PER_2 CAN Message                                   (1796) */
#define HMI_TX_PER_2_ID                                            (1796U)
#define HMI_TX_PER_2_IDE                                           (0U)
#define HMI_TX_PER_2_DLC                                           (8U)


#define HMI_TX_PER_2_POWER_CONSUMPTION_U16FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_POWER_CONSUMPTION_U16_STARTBIT                           (7)
#define HMI_TX_PER_2_CANID_POWER_CONSUMPTION_U16_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_POWER_CONSUMPTION_U16_MIN                                (0)
#define HMI_TX_PER_2_CANID_POWER_CONSUMPTION_U16_MAX                                (5000)
#define HMI_TX_PER_2_RANGE_KM_U16FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_RANGE_KM_U16_STARTBIT                           (23)
#define HMI_TX_PER_2_CANID_RANGE_KM_U16_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_RANGE_KM_U16_MIN                                (0)
#define HMI_TX_PER_2_CANID_RANGE_KM_U16_MAX                                (999)
#define HMI_TX_PER_2_BATTERY_SOC_U8FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_BATTERY_SOC_U8_STARTBIT                           (39)
#define HMI_TX_PER_2_CANID_BATTERY_SOC_U8_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_BATTERY_SOC_U8_MIN                                (0)
#define HMI_TX_PER_2_CANID_BATTERY_SOC_U8_MAX                                (100)
#define HMI_TX_PER_2_BLE_ICON_U8FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_BLE_ICON_U8_STARTBIT                           (45)
#define HMI_TX_PER_2_CANID_BLE_ICON_U8_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_BLE_ICON_U8_MIN                                (0)
#define HMI_TX_PER_2_CANID_BLE_ICON_U8_MAX                                (3)
#define HMI_TX_PER_2_REGEN_BRAKING_U8FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_REGEN_BRAKING_U8_STARTBIT                           (47)
#define HMI_TX_PER_2_CANID_REGEN_BRAKING_U8_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_REGEN_BRAKING_U8_MIN                                (0)
#define HMI_TX_PER_2_CANID_REGEN_BRAKING_U8_MAX                                (3)
#define HMI_TX_PER_2_PER_2_COUNTER_U8FACTOR                                   (1)
#define HMI_TX_PER_2_CANID_PER_2_COUNTER_U8_STARTBIT                           (63)
#define HMI_TX_PER_2_CANID_PER_2_COUNTER_U8_OFFSET                             (0)
#define HMI_TX_PER_2_CANID_PER_2_COUNTER_U8_MIN                                (0)
#define HMI_TX_PER_2_CANID_PER_2_COUNTER_U8_MAX                                (15)


/* def @_I_HMI_TX_PER_2 CAN Message                                   (1797) */
#define _I_HMI_TX_PER_2_ID                                            (1797U)
#define _I_HMI_TX_PER_2_IDE                                           (0U)
#define _I_HMI_TX_PER_2_DLC                                           (8U)


#define _I_HMI_TX_PER_2__I_POWER_CONSUMPTION_U16FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_POWER_CONSUMPTION_U16_STARTBIT                           (7)
#define _I_HMI_TX_PER_2_CANID__I_POWER_CONSUMPTION_U16_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_POWER_CONSUMPTION_U16_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_POWER_CONSUMPTION_U16_MAX                                (65535)
#define _I_HMI_TX_PER_2__I_RANGE_KM_U16FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_RANGE_KM_U16_STARTBIT                           (23)
#define _I_HMI_TX_PER_2_CANID__I_RANGE_KM_U16_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_RANGE_KM_U16_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_RANGE_KM_U16_MAX                                (65535)
#define _I_HMI_TX_PER_2__I_BATTERY_SOC_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_BATTERY_SOC_U8_STARTBIT                           (39)
#define _I_HMI_TX_PER_2_CANID__I_BATTERY_SOC_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_BATTERY_SOC_U8_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_BATTERY_SOC_U8_MAX                                (255)
#define _I_HMI_TX_PER_2__I_BLE_ICON_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_BLE_ICON_U8_STARTBIT                           (45)
#define _I_HMI_TX_PER_2_CANID__I_BLE_ICON_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_BLE_ICON_U8_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_BLE_ICON_U8_MAX                                (3)
#define _I_HMI_TX_PER_2__I_REGEN_BRAKING_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_REGEN_BRAKING_U8_STARTBIT                           (47)
#define _I_HMI_TX_PER_2_CANID__I_REGEN_BRAKING_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_REGEN_BRAKING_U8_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_REGEN_BRAKING_U8_MAX                                (3)
#define _I_HMI_TX_PER_2__I_PER_2_COUNTER_U8FACTOR                                   (1)
#define _I_HMI_TX_PER_2_CANID__I_PER_2_COUNTER_U8_STARTBIT                           (63)
#define _I_HMI_TX_PER_2_CANID__I_PER_2_COUNTER_U8_OFFSET                             (0)
#define _I_HMI_TX_PER_2_CANID__I_PER_2_COUNTER_U8_MIN                                (0)
#define _I_HMI_TX_PER_2_CANID__I_PER_2_COUNTER_U8_MAX                                (15)



/* def @HMI_TX_EVE_1 CAN Message                                   (1792) */
#define HMI_TX_EVE_1_ID                                            (1792U)
#define HMI_TX_EVE_1_IDE                                           (0U)
#define HMI_TX_EVE_1_DLC                                           (8U)


#define HMI_TX_EVE_1_REVERSE_MODE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_REVERSE_MODE_U8_STARTBIT                           (1)
#define HMI_TX_EVE_1_CANID_REVERSE_MODE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_REVERSE_MODE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_REVERSE_MODE_U8_MAX                                (3)
#define HMI_TX_EVE_1_SPORTS_MODE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_SPORTS_MODE_U8_STARTBIT                           (3)
#define HMI_TX_EVE_1_CANID_SPORTS_MODE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_SPORTS_MODE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_SPORTS_MODE_U8_MAX                                (3)
#define HMI_TX_EVE_1_ECO_MODE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_ECO_MODE_U8_STARTBIT                           (5)
#define HMI_TX_EVE_1_CANID_ECO_MODE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_ECO_MODE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_ECO_MODE_U8_MAX                                (3)
#define HMI_TX_EVE_1_NEUTRAL_MODE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_NEUTRAL_MODE_U8_STARTBIT                           (7)
#define HMI_TX_EVE_1_CANID_NEUTRAL_MODE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_NEUTRAL_MODE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_NEUTRAL_MODE_U8_MAX                                (3)
#define HMI_TX_EVE_1_KILL_SWITCH_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_KILL_SWITCH_U8_STARTBIT                           (9)
#define HMI_TX_EVE_1_CANID_KILL_SWITCH_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_KILL_SWITCH_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_KILL_SWITCH_U8_MAX                                (3)
#define HMI_TX_EVE_1_RIGHT_INDICATOR_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_RIGHT_INDICATOR_U8_STARTBIT                           (11)
#define HMI_TX_EVE_1_CANID_RIGHT_INDICATOR_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_RIGHT_INDICATOR_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_RIGHT_INDICATOR_U8_MAX                                (3)
#define HMI_TX_EVE_1_LEFT_INDICATOR_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_LEFT_INDICATOR_U8_STARTBIT                           (13)
#define HMI_TX_EVE_1_CANID_LEFT_INDICATOR_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_LEFT_INDICATOR_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_LEFT_INDICATOR_U8_MAX                                (3)
#define HMI_TX_EVE_1_SAFE_MODE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_SAFE_MODE_U8_STARTBIT                           (15)
#define HMI_TX_EVE_1_CANID_SAFE_MODE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_SAFE_MODE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_SAFE_MODE_U8_MAX                                (3)
#define HMI_TX_EVE_1_MOTOR_FAULT_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_MOTOR_FAULT_U8_STARTBIT                           (17)
#define HMI_TX_EVE_1_CANID_MOTOR_FAULT_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_MOTOR_FAULT_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_MOTOR_FAULT_U8_MAX                                (3)
#define HMI_TX_EVE_1_BATTERY_FAULT_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_BATTERY_FAULT_U8_STARTBIT                           (19)
#define HMI_TX_EVE_1_CANID_BATTERY_FAULT_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_BATTERY_FAULT_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_BATTERY_FAULT_U8_MAX                                (3)
#define HMI_TX_EVE_1_HIGH_BEAM_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_HIGH_BEAM_U8_STARTBIT                           (21)
#define HMI_TX_EVE_1_CANID_HIGH_BEAM_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_HIGH_BEAM_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_HIGH_BEAM_U8_MAX                                (3)
#define HMI_TX_EVE_1_LOW_BEAM_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_LOW_BEAM_U8_STARTBIT                           (23)
#define HMI_TX_EVE_1_CANID_LOW_BEAM_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_LOW_BEAM_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_LOW_BEAM_U8_MAX                                (3)
#define HMI_TX_EVE_1_IGNITION_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_IGNITION_U8_STARTBIT                           (24)
#define HMI_TX_EVE_1_CANID_IGNITION_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_IGNITION_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_IGNITION_U8_MAX                                (1)
#define HMI_TX_EVE_1_BATTERY_STATUS_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_BATTERY_STATUS_U8_STARTBIT                           (25)
#define HMI_TX_EVE_1_CANID_BATTERY_STATUS_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_BATTERY_STATUS_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_BATTERY_STATUS_U8_MAX                                (1)
#define HMI_TX_EVE_1_WARNING_SYMBOL_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_WARNING_SYMBOL_U8_STARTBIT                           (27)
#define HMI_TX_EVE_1_CANID_WARNING_SYMBOL_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_WARNING_SYMBOL_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_WARNING_SYMBOL_U8_MAX                                (3)
#define HMI_TX_EVE_1_SERVICE_INDICATOR_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_SERVICE_INDICATOR_U8_STARTBIT                           (29)
#define HMI_TX_EVE_1_CANID_SERVICE_INDICATOR_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_SERVICE_INDICATOR_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_SERVICE_INDICATOR_U8_MAX                                (3)
#define HMI_TX_EVE_1_SIDE_STAND_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_SIDE_STAND_U8_STARTBIT                           (31)
#define HMI_TX_EVE_1_CANID_SIDE_STAND_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_SIDE_STAND_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_SIDE_STAND_U8_MAX                                (3)
#define HMI_TX_EVE_1_HOURS_TIME_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_HOURS_TIME_U8_STARTBIT                           (39)
#define HMI_TX_EVE_1_CANID_HOURS_TIME_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_HOURS_TIME_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_HOURS_TIME_U8_MAX                                (23)
#define HMI_TX_EVE_1_MINUTES_TIME_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_MINUTES_TIME_U8_STARTBIT                           (47)
#define HMI_TX_EVE_1_CANID_MINUTES_TIME_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_MINUTES_TIME_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_MINUTES_TIME_U8_MAX                                (59)
#define HMI_TX_EVE_1_CLUSTER_USE_PLACE_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_CLUSTER_USE_PLACE_U8_STARTBIT                           (55)
#define HMI_TX_EVE_1_CANID_CLUSTER_USE_PLACE_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_CLUSTER_USE_PLACE_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_CLUSTER_USE_PLACE_U8_MAX                                (1)
#define HMI_TX_EVE_1_EVE_1_COUNTER_U8FACTOR                                   (1)
#define HMI_TX_EVE_1_CANID_EVE_1_COUNTER_U8_STARTBIT                           (63)
#define HMI_TX_EVE_1_CANID_EVE_1_COUNTER_U8_OFFSET                             (0)
#define HMI_TX_EVE_1_CANID_EVE_1_COUNTER_U8_MIN                                (0)
#define HMI_TX_EVE_1_CANID_EVE_1_COUNTER_U8_MAX                                (15)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/

typedef struct
{
  uint8_t _i_Reverse_Mode_u8;
  uint8_t _i_Sports_Mode_u8;
  uint8_t _i_Eco_Mode_u8;
  uint8_t _i_Neutral_Mode_u8;
  uint8_t _i_Kill_Switch_u8;
  uint8_t _i_Right_Indicator_u8;
  uint8_t _i_Left_Indicator_u8;
  uint8_t _i_Safe_Mode_u8;
  uint8_t _i_Motor_Fault_u8;
  uint8_t _i_Battery_Fault_u8;
  uint8_t _i_High_Beam_u8;
  uint8_t _i_Low_Beam_u8;
  uint8_t _i_Ignition_u8;
  uint8_t _i_Battery_Status_u8;
  uint8_t _i_Warning_Symbol_u8;
  uint8_t _i_Service_Indicator_u8;
  uint8_t _i_Side_Stand_u8;
  uint8_t _i_Hours_Time_u8;
  uint8_t _i_Minutes_Time_u8;
  uint8_t _i_Cluster_Use_Place_u8;
  uint8_t _i_Eve_1_Counter_u8;
}_i_HMI_Tx_Eve_1_t;


typedef struct
{
  uint32_t ODO_u32;
  uint8_t Vehicle_Speed_u8;
  uint8_t Mileage_u8;
  uint8_t Per_1_Counter_u8;
}HMI_Tx_Per_1_t;


typedef struct
{
  uint32_t _i_ODO_u32;
  uint8_t _i_Vehicle_Speed_u8;
  uint8_t _i_Mileage_u8;
  uint8_t _i_Per_1_Counter_u8;
}_i_HMI_Tx_Per_1_t;

typedef struct
{
  uint16_t Power_Consumption_u16;
  uint16_t Range_Km_u16;
  uint8_t Battery_SOC_u8;
  uint8_t BLE_Icon_u8;
  uint8_t Regen_Braking_u8;
  uint8_t Per_2_Counter_u8;
}HMI_Tx_Per_2_t;


typedef struct
{
  uint16_t _i_Power_Consumption_u16;
  uint16_t _i_Range_Km_u16;
  uint8_t _i_Battery_SOC_u8;
  uint8_t _i_BLE_Icon_u8;
  uint8_t _i_Regen_Braking_u8;
  uint8_t _i_Per_2_Counter_u8;
}_i_HMI_Tx_Per_2_t;


typedef struct
{
  uint8_t Reverse_Mode_u8;
  uint8_t Sports_Mode_u8;
  uint8_t Eco_Mode_u8;
  uint8_t Neutral_Mode_u8;
  uint8_t Kill_Switch_u8;
  uint8_t Right_Indicator_u8;
  uint8_t Left_Indicator_u8;
  uint8_t Safe_Mode_u8;
  uint8_t Motor_Fault_u8;
  uint8_t Battery_Fault_u8;
  uint8_t High_Beam_u8;
  uint8_t Low_Beam_u8;
  uint8_t Ignition_u8;
  uint8_t Battery_Status_u8;
  uint8_t Warning_Symbol_u8;
  uint8_t Service_Indicator_u8;
  uint8_t Side_Stand_u8;
  uint8_t Hours_Time_u8;
  uint8_t Minutes_Time_u8;
  uint8_t Cluster_Use_Place_u8;
  uint8_t Eve_1_Counter_u8;
}HMI_Tx_Eve_1_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/


/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Eve_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Eve_1_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
extern uint32_t Serialize__i_HMI_Tx_Eve_1(_i_HMI_Tx_Eve_1_t* message, uint8_t* data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Per_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Per_1_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
extern uint32_t Serialize_HMI_Tx_Per_1(HMI_Tx_Per_1_t* message, uint8_t* data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Per_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Per_1_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
extern uint32_t Serialize__i_HMI_Tx_Per_1(_i_HMI_Tx_Per_1_t* message, uint8_t* data);
 /* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Per_2
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Per_2_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/ 
 extern uint32_t Serialize_HMI_Tx_Per_2(HMI_Tx_Per_2_t* message, uint8_t* data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Per_2
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Per_2_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/  
extern uint32_t Serialize__i_HMI_Tx_Per_2(_i_HMI_Tx_Per_2_t* message, uint8_t* data);
 /* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Eve_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Eve_1_t* message, uint8_t* data
*   Return Value  : uint32_t
*******************************************************************************/  
extern uint32_t Serialize_HMI_Tx_Eve_1(HMI_Tx_Eve_1_t* message, uint8_t* data);
 
#endif /* CLUSTER_CAN_SIGNALS_H */
/*---------------------- End of File -----------------------------------------*/
