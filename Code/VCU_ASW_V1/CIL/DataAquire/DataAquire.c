/***********************************************************************************************************************
* File Name    : DataAquire.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "DataBank.h"
#include "Config_ADCA0.h"
#include "adc_user.h"
#include "intc_user.h"
//#include "can_if.h"
#include "cil_can_conf.h"
#include "Config_RTCA0.h"
#include "Config_STBC.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
InputSigConf_St_t	InputSigConf_aSt[TOTAL_USER_INPUT_SIGNALS_E]= 
{ 
	{ HIGH_BEAM_SWITCH_E, 		 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	HIGH_BEAM_DEBOUNCE_COUNT, 	0x00 },
	{ LIGHTS_SWITCH_E, 		 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	LIGHT_SWITCH_DEBOUNCE_COUNT, 	0x00 },
	{ LEFT_TURN_SWITCH_E, 		 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	LEFT_TURN_DEBOUNCE_COUNT, 	0x00 },
	{ RIGHT_TURN_SWITCH_E, 		 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	RIGHT_TURN_DEBOUNCE_COUNT, 	0x00 },
	{ BRAKE_SWITCH_E, 		 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	BRAKE_DEBOUNCE_COUNT, 		0x00 },
	{ EXTRA_1_E, 		 	 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	EXTRA_1_DEBOUNCE_COUNT, 	0x00 },
	{ EXTRA_2_E, 		 	 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	BRAKE_DEBOUNCE_COUNT,    	0x00 },
	{ EXTRA_3_E, 		 	 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	EXTRA_1_DEBOUNCE_COUNT, 	0x00 },
	{ EXTRA_4_E, 		 	 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	EXTRA_1_DEBOUNCE_COUNT, 	0x00 },
	//{ EXTRA_5_E, 		 	 ADC_E, 	&ReadInputThroghADC, 	0, 	0, 	EXTRA_1_DEBOUNCE_COUNT, 	0x00 },
	
	{ KEY_SWITCH_E, 		 GPIO_E, 	&ReadInputThroghGPIO, 	0, 	0, 	KEY_SWITCH_DEBOUNCE_COUNT, 	0x00 },
	{ KILL_SWITCH_E, 		 GPIO_E, 	&ReadInputThroghGPIO, 	0, 	0, 	KILL_SWITCH_DEBOUNCE_COUNT, 	0x00 },
	{ KICK_STAND_SWITCH_E, 		 GPIO_E, 	&ReadInputThroghGPIO, 	0, 	0, 	KICK_STAND_DEBOUNCE_COUNT, 	0x00 },
	
	{ MODE_SELECT_SWITCH_UP_E, 	 EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODESELECT_UP_DEBOUNCE_COUNT, 	0x00 },
	{ MODE_SELECT_SWITCH_DOWN_E, 	 EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODESELECT_DOWN_DEBOUNCE_COUNT, 0x00 },
	{ MODE_SELECT_SWITCH_SET_E, 	 EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODESELECT_SET_DEBOUNCE_COUNT, 	0x00 }
};

DrivingModes_En_t	DrivingModes_En  = NEUTRAL_MODE_E;

uint16_t		ADC_Result[TOTAL_ADC_INPUT_E+1] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

uint16_t PORT8_Read_Val_u16  = 0;
uint16_t PORT10_Read_Val_u16 = 0;

/*static */int16_t Drive_Mode_Counter_s16 = 0;


/***********************************************************************************************************************
* Function Name: ReadUserInput
* Description  : This function reads the User Input Signals and Update the Signal State into the Data-Bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadUserInput(void)
{
	uint16_t SigCount_u16 = 0;
	 
	 for(SigCount_u16 = INPUT_SIG_START_E; SigCount_u16 < TOTAL_USER_INPUT_SIGNALS_E; SigCount_u16++)
	 {
		 InputSigConf_aSt[SigCount_u16].ReadInputs_Fptr((UserInputSig_En_t)SigCount_u16);
	 }
	 
	 UpdateSigToDataBank();
	 
	return;
}
uint8_t Read_Throttle_LS_u8 = 0;
uint8_t Read_Throttle_HS_u8 = 0;


/***********************************************************************************************************************
* Function Name: ReadInputThroghADC
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghADC(UserInputSig_En_t	Signal_En)
{
	static bool Read_ADC_Result_b = true;
	static bool Read_Throttle_b = false;
	//static uint8_t Read_Throttle_LS_u8 = 0;
	//static uint8_t Read_Throttle_HS_u8 = 0;
	
	
	if(Read_ADC_Result_b)
	{
		R_Config_ADCA0_ScanGroup1_OperationOn();
		while(Group1_Scan_Completed_b == false); /*Wait untill convertion completes*/
		Group1_Scan_Completed_b = false;
		R_Config_ADCA0_ScanGroup1_GetResult(ADC_Result);
		Read_ADC_Result_b = false;
	}
	else
	{
		;
	}
	
	switch(Signal_En)
	{
		case HIGH_BEAM_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[HIGHBEAM_ADC_E];
			break;
		}
		
		case LIGHTS_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[LIGHTS_ADC_E];
			break;
		}
		
		case LEFT_TURN_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[LEFT_TURN_ADC_E];
			break;
		}
		
		case RIGHT_TURN_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[RIGHT_TURN_ADC_E];
			break;
		}
		
		case BRAKE_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[BRAKE_ADC_E];
			break;
		}
		
		case EXTRA_1_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_1_ADC_E];
			break;
			//Read_ADC_Result_b = true;
		}
		case EXTRA_2_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_2_ADC_E];
			break;
			//Read_ADC_Result_b = true;
		}
		case EXTRA_3_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_3_ADC_E];
			break;
			//Read_ADC_Result_b = true;
		}
		case EXTRA_4_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_4_ADC_E];
			Read_ADC_Result_b = true;
			break;
		}
//		case EXTRA_5_E:
//		{
//			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_5_ADC_E];
//			Read_ADC_Result_b = true;
//		}
		default:
		{
			;
		}
	}
	
	if(Signal_En != EXTRA_1_E)
	{
		if(Signal_En == EXTRA_3_E)
		{
			Read_Throttle_HS_u8 = (uint8_t)((float)((InputSigConf_aSt[Signal_En].ReadInputVal_u16)*100) / 3610);
		}
		else if(Signal_En == EXTRA_4_E)
		{
			Read_Throttle_LS_u8 = (uint8_t)((float)((InputSigConf_aSt[Signal_En].ReadInputVal_u16)*100) / 3610);
			Read_Throttle_b = true;
			if(true == Read_Throttle_b)
			{
				Read_Throttle_b = false;
				if(((Read_Throttle_HS_u8+Read_Throttle_LS_u8) <= 102) 
					&& ((Read_Throttle_HS_u8+Read_Throttle_LS_u8) >= 98))
				{
					InputSigConf_aSt[Signal_En-1].SignalState_s8 = Read_Throttle_HS_u8;
					InputSigConf_aSt[Signal_En].SignalState_s8 = Read_Throttle_LS_u8;
				}
				else
				{
					NOP();
					InputSigConf_aSt[Signal_En-1].SignalState_s8 = Read_Throttle_HS_u8;
					InputSigConf_aSt[Signal_En].SignalState_s8 = Read_Throttle_LS_u8;
				}
			}
			else
			{
				;
			}
		}
		else
		{
			if(InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if(InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}
		}

	}
	else if(Signal_En == EXTRA_1_E)
	{

		Set_Drive_Mode_Counters(Signal_En); 
		/*Set the Drive mode counters - using user input through ADC*/
	}
	else
	{
		;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: ReadInputThroghGPIO
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghGPIO(UserInputSig_En_t	Signal_En)
{
	PORT8_Read_Val_u16 	= PORT.PPR8;  /*P8_3 and P8_4*/
	PORT10_Read_Val_u16 	= PORT.PPR10; /*P10_13*/
	
	switch(Signal_En)
	{
		case KEY_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = PORT8_Read_Val_u16;
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 & 0x0008U);
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >> 3);
			if(GPIO_PIN_SET == InputSigConf_aSt[Signal_En].ReadInputVal_u16)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			break;
		}
		
		case KILL_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = PORT8_Read_Val_u16;
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 & 0x0010U);
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >> 4);
			if(GPIO_PIN_SET == InputSigConf_aSt[Signal_En].ReadInputVal_u16)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
				
			}
			else
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			
			break;
		}
		
		case KICK_STAND_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = PORT10_Read_Val_u16;
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 & 0x2000U);
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >> 13);
			if(GPIO_PIN_SET == InputSigConf_aSt[Signal_En].ReadInputVal_u16)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			break;
		}
		
		default:
		{
			;
		}
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: ReadInputThroghINT
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghINT(UserInputSig_En_t	Signal_En)
{
	switch(Signal_En)
	{
		case MODE_SELECT_SWITCH_UP_E:
		{
			/*todo:dileepabs*/
			break;
		}
		
		case MODE_SELECT_SWITCH_DOWN_E:
		{
			/*todo:dileepabs*/
			break;
		}
		
		case MODE_SELECT_SWITCH_SET_E:
		{
			InputSigConf_aSt[Signal_En].SignalState_s8 = (int8_t)SetSwitch_b;
			break;
		}
		
		default:
		{
			;
		}
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: UpdateSigToDataBank
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateSigToDataBank(void)
{
	int8_t ModeSelPos_s8 		= 0;
	
	UserInputSig_St.HighBeam_SwitchState_u8  = InputSigConf_aSt[HIGH_BEAM_SWITCH_E].SignalState_s8;
	UserInputSig_St.Lights_SwitchState_u8 	 = InputSigConf_aSt[LIGHTS_SWITCH_E].SignalState_s8;
	UserInputSig_St.LeftTurn_SwitchState_u8  = InputSigConf_aSt[LEFT_TURN_SWITCH_E].SignalState_s8;
	UserInputSig_St.RightTurn_SwitchState_u8 = InputSigConf_aSt[RIGHT_TURN_SWITCH_E].SignalState_s8;
	UserInputSig_St.Brake_SwitchState_Front_u8 	 = InputSigConf_aSt[BRAKE_SWITCH_E].SignalState_s8;
	UserInputSig_St.Brake_SwitchState_Back_u8 	 = InputSigConf_aSt[EXTRA_2_E].SignalState_s8;
	UserInputSig_St.Key_SwitchState_u8 	 = InputSigConf_aSt[KEY_SWITCH_E].SignalState_s8;
	UserInputSig_St.Kill_SwitchState_u8 	 = InputSigConf_aSt[KILL_SWITCH_E].SignalState_s8;
	UserInputSig_St.KickStand_SwitchState_u8 = InputSigConf_aSt[KICK_STAND_SWITCH_E].SignalState_s8;
	UserInputSig_St.Throttle_u8 = InputSigConf_aSt[EXTRA_4_E].SignalState_s8;
	
	ModeSelPos_s8 = (int8_t)Drive_Mode_Counter_s16;

	if(true == InputSigConf_aSt[MODE_SELECT_SWITCH_SET_E].SignalState_s8)
	{
		/*Set the Driver Mode Selected*/
		if(ModeSelPos_s8 <= (int8_t)REVERSE_MODE_E)
		{
			ModeSelPos_s8 = (int8_t)REVERSE_MODE_E;
		}
		else if(ModeSelPos_s8 >= (int8_t)SPORTS_MODE_E)
		{
			ModeSelPos_s8 = (int8_t)SPORTS_MODE_E;
		}
		else
		{
			;
		}
		
		switch((DrivingModes_En_t)ModeSelPos_s8)
		{
			case NEUTRAL_MODE_E:
			{
				UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
				UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
				break;
			}
			
			case ECONOMY_MODE_E:
			{
				UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E;
				UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
				break;
			}

			case SPORTS_MODE_E:
			{
				UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_ON_E;
				UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
				break;
			}

			case REVERSE_MODE_E:
			{
				UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;

				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
				break;
			}
			
			default:
			{
				;
			}
		}
	}
	else
	{
		;
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: Set_Drive_Mode_Counters
* Description  : 
* Arguments    : UserInputSig_En_t Signal_En
* Return Value : None
***********************************************************************************************************************/
void Set_Drive_Mode_Counters(UserInputSig_En_t Signal_En)
{
	static bool JoyStick_ADC_Result_b = true;
	
	if((InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= JOYSTICK_UP_MAXI_THRESHOLD_VALUE) &&
			(JoyStick_ADC_Result_b == true))
	{
		/*Joystick-Up*/
		if(Drive_Mode_Counter_s16 < SPORTS_MODE_E)
		{
			Drive_Mode_Counter_s16++;
			if (SPORTS_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}
			else if (ECONOMY_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}			
			else if (NEUTRAL_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}
			else if (REVERSE_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_ON_E;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			;
		}
		JoyStick_ADC_Result_b = false;
		
		/*Note : On Power-Up, In first cycle ADC Read values are zero. So it is treated as up switch is pressed.
			 and Drive_Mode_Counter_s16 will increments by 1. Since the Neutral Mode = 1, It has 
			 no effect*/
	}
	else if((InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= JOYSTICK_DOWN_MIN_THRESHOLD_VALUE) &&
			(JoyStick_ADC_Result_b == true))
	{	
		/*Joystick-Down*/
		if(Drive_Mode_Counter_s16 > REVERSE_MODE_E)
		{
			Drive_Mode_Counter_s16--;
			if (SPORTS_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}
			else if (ECONOMY_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}
			else if (NEUTRAL_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_ON_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
			}
			else if (REVERSE_MODE_E == Drive_Mode_Counter_s16)
			{
				UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
				UserInputSig_St.Reverse_Mode_u8 = TURNED_ON_E;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			;
		}
		JoyStick_ADC_Result_b = false;
	}
	else if((InputSigConf_aSt[Signal_En].ReadInputVal_u16 < JOYSTICK_DOWN_MIN_THRESHOLD_VALUE) &&
			(InputSigConf_aSt[Signal_En].ReadInputVal_u16 > JOYSTICK_UP_MAXI_THRESHOLD_VALUE))
	{
		JoyStick_ADC_Result_b = true;
	}
	else
	{
		;
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Clear_Drive_Mode
* Description  : 
* Arguments    : DrivingModes_En_t DrivingModes_En
* Return Value : None
***********************************************************************************************************************/
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En)
{
	switch (DrivingModes_En)
	{
	case NEUTRAL_MODE_E:
	{
		Drive_Mode_Counter_s16 = NEUTRAL_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
		break;
	}
	case ECONOMY_MODE_E:
	{
		Drive_Mode_Counter_s16 = ECONOMY_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
		break;
	}
	case SPORTS_MODE_E:
	{
		Drive_Mode_Counter_s16 = SPORTS_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
		break;
	}
	case REVERSE_MODE_E:
	{
		Drive_Mode_Counter_s16 = REVERSE_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;

		UserInputSig_St.Neutral_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_u8 = TURNED_OFF_E;
		break;
	}

	default:
	{
		break;
	}
	}
}
//void CANSched_Debug_TxMsgCallback(void)
//{
//	uint8_t i = 0;
//	CAN_MessageFrame_St_t Can_Applidata_St;
//	for (i = 0; i < 8; i+=2)
//	{
//		Can_Applidata_St.DataBytes_au8[i] = (uint8_t)(ADC_Result[8]>>8U);
//		Can_Applidata_St.DataBytes_au8[i+1] = (uint8_t)ADC_Result[8];
//	}
//	Can_Applidata_St.DataBytes_au8[2] = (uint8_t)(ADC_Result[7]>>8U);
//	Can_Applidata_St.DataBytes_au8[3] = (uint8_t)ADC_Result[7];
//	Can_Applidata_St.DataBytes_au8[6] = InputSigConf_aSt[EXTRA_3_E].SignalState_s8;//UserInputSig_St.Throttle_u8;
//	Can_Applidata_St.DataBytes_au8[7] = InputSigConf_aSt[EXTRA_4_E].SignalState_s8;
//	Can_Applidata_St.DataLength_u8 = 8;
//	Can_Applidata_St.MessageType_u8 = STD_E;
//	CIL_CAN_Tx_AckMsg(CIL_TEST3_TX_E, Can_Applidata_St);

//}
rtc_counter_value_t rtc_time;
void CANSched_Debug_TxMsgCallback(void)
{
	static bool flag = false;
	CAN_MessageFrame_St_t Can_Applidata_St;
//	Can_Applidata_St.DataBytes_au8[0] = (uint8_t)(ADC_Result[8]>>8U);
//	Can_Applidata_St.DataBytes_au8[1] = (uint8_t)ADC_Result[8];
//	Can_Applidata_St.DataBytes_au8[2] = (uint8_t)(ADC_Result[7]>>8U);
//	Can_Applidata_St.DataBytes_au8[3] = (uint8_t)ADC_Result[7];
//	Can_Applidata_St.DataBytes_au8[4] = (uint8_t)0;
//	Can_Applidata_St.DataBytes_au8[5] = (uint8_t)UserInputSig_St.Throttle_u8;
//	Can_Applidata_St.DataBytes_au8[6] = InputSigConf_aSt[EXTRA_4_E].SignalState_s8;
//	Can_Applidata_St.DataBytes_au8[7] = InputSigConf_aSt[EXTRA_3_E].SignalState_s8;//UserInputSig_St.Throttle_u8;
	R_Config_RTCA0_Get_CounterDirectValue(&rtc_time);
	//BCD to Decimal conversion for RTC.
	Can_Applidata_St.DataBytes_au8[0] = (uint8_t)((((rtc_time.sec & 0x70) >> 4) * 10) + (rtc_time.sec % 16));
	Can_Applidata_St.DataBytes_au8[1] = (uint8_t)((((rtc_time.min & 0x70) >> 4) * 10) + (rtc_time.min % 16));
	Can_Applidata_St.DataBytes_au8[2] = (uint8_t)((((rtc_time.hour & 0x70) >> 4) * 10) + (rtc_time.hour % 16));
	Can_Applidata_St.DataBytes_au8[3] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[4] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[5] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[6] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[7] = (uint8_t)0;

	Can_Applidata_St.DataLength_u8 = 8;
	Can_Applidata_St.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_TEST3_TX_E, Can_Applidata_St);
	if(((Can_Applidata_St.DataBytes_au8[0]) == 30) && (flag == false))
	{
		flag = true;
		//R_Config_STBC_Prepare_Stop_Mode();
		//R_Config_STBC_Start_Stop_Mode();
		EI();
	}

}
/********************************************************EOF***********************************************************/