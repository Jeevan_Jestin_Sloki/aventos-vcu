/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define HIGH_BEAM_DEBOUNCE_COUNT					1
#define LIGHT_SWITCH_DEBOUNCE_COUNT					1
#define LEFT_TURN_DEBOUNCE_COUNT					1
#define RIGHT_TURN_DEBOUNCE_COUNT					1
#define BRAKE_DEBOUNCE_COUNT						1
#define EXTRA_1_DEBOUNCE_COUNT						1

#define KEY_SWITCH_DEBOUNCE_COUNT					1
#define KILL_SWITCH_DEBOUNCE_COUNT					1
#define KICK_STAND_DEBOUNCE_COUNT					1

#define MODESELECT_UP_DEBOUNCE_COUNT					1
#define MODESELECT_DOWN_DEBOUNCE_COUNT					1
#define MODESELECT_SET_DEBOUNCE_COUNT					1


#define MIN_ADC_VALUE_FOR_TURN_ON					4000
#define MIN_ADC_VALUE_FOR_TURN_OFF					100

#define GPIO_PIN_SET							1
#define GPIO_PIN_CLR							0

#define NEUTEAL_MODE_VALUE						0
#define ECONOMY_MODE_VALUE						1
#define SPORTS_MODE_VALUE						2
#define REVERSE_MODE_VALUE					       -1

#define JOYSTICK_UP_MAXI_THRESHOLD_VALUE				250 /*todo:dileepabs*/
#define JOYSTICK_DOWN_MIN_THRESHOLD_VALUE				4000

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/

typedef enum
{
	INPUT_SIG_START_E,
	/*ADC*/
	HIGH_BEAM_SWITCH_E = INPUT_SIG_START_E,
	LIGHTS_SWITCH_E,
	LEFT_TURN_SWITCH_E,
	RIGHT_TURN_SWITCH_E,
	BRAKE_SWITCH_E,
	EXTRA_1_E,
	EXTRA_2_E,
	EXTRA_3_E,
	EXTRA_4_E,
	//EXTRA_5_E,
	
	/*GPIO*/
	KEY_SWITCH_E,
	KILL_SWITCH_E,
	KICK_STAND_SWITCH_E,
	
	/*EXTERNAL INTERRUPT*/
	MODE_SELECT_SWITCH_UP_E,
	MODE_SELECT_SWITCH_DOWN_E,
	MODE_SELECT_SWITCH_SET_E,
	
	INPUT_SIG_END_E,
	TOTAL_USER_INPUT_SIGNALS_E = INPUT_SIG_END_E
	
}UserInputSig_En_t;


typedef enum
{
	HIGHBEAM_ADC_E,
	LIGHTS_ADC_E,
	LEFT_TURN_ADC_E,
	RIGHT_TURN_ADC_E,
	BRAKE_ADC_E,
	EXTRA_1_ADC_E, /*UP and DOWN*/
	EXTRA_2_ADC_E,
	EXTRA_3_ADC_E,
	EXTRA_4_ADC_E,
	//EXTRA_5_ADC_E,
	TOTAL_ADC_INPUT_E,
}ADC_Input_Sig_En_t;


typedef void (*ReadInputs_Fptr_t)(UserInputSig_En_t);


typedef enum
{
	ADC_E,
	GPIO_E,
	EXT_INT_E
}InputChannel_En_t;


typedef struct
{
	UserInputSig_En_t	UserInputSig_En;
	InputChannel_En_t	InputChannel_En;
	ReadInputs_Fptr_t  	ReadInputs_Fptr;
	uint16_t		ReadInputVal_u16;
	uint16_t	 	DebounceCounter_u16;
	uint16_t		DebounceCountLimit;
	int8_t 	   		SignalState_s8;
}InputSigConf_St_t;

typedef enum
{
	REVERSE_MODE_E = 0,
	NEUTRAL_MODE_E,
	ECONOMY_MODE_E,
	SPORTS_MODE_E,
}DrivingModes_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern InputSigConf_St_t	InputSigConf_aSt[TOTAL_USER_INPUT_SIGNALS_E];
//extern uint16_t		ADC_Result[TOTAL_ADC_INPUT_E+1];
void ReadInputThroghADC(UserInputSig_En_t);
void ReadInputThroghGPIO(UserInputSig_En_t);
void ReadInputThroghINT(UserInputSig_En_t);
void UpdateSigToDataBank(void);
void Set_Drive_Mode_Counters(UserInputSig_En_t);
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En);

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadUserInput(void);

#endif /* DATA_AQUIRE_H */


