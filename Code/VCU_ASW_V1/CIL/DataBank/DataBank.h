/***********************************************************************************************************************
* File Name    : DataBank.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef DATA_BANK_H
#define DATA_BANK_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	TURNED_OFF_E,
	TURNED_ON_E,
}SignalState_En_t;

typedef struct
{
	uint8_t 	HighBeam_SwitchState_u8;
	uint8_t 	Lights_SwitchState_u8;
	uint8_t 	LeftTurn_SwitchState_u8;
	uint8_t 	RightTurn_SwitchState_u8;
	uint8_t 	Brake_SwitchState_Front_u8;
	uint8_t 	Brake_SwitchState_Back_u8;
	uint8_t 	Key_SwitchState_u8;
	uint8_t 	Kill_SwitchState_u8;
	uint8_t 	KickStand_SwitchState_u8;
	uint8_t 	Throttle_u8;
	
	uint8_t 	Fall_SwitchState_u8;
	
	/*Driver Mode Selected*/
	uint8_t 	Neutral_Mode_Sel_u8;
	uint8_t 	Economy_Mode_Sel_u8;
	uint8_t 	Sports_Mode_Sel_u8;
	uint8_t 	Reverse_Mode_Sel_u8;
	/*Driver Mode Selected but not set*/
	uint8_t 	Neutral_Mode_u8;
	uint8_t 	Economy_Mode_u8;
	uint8_t 	Sports_Mode_u8;
	uint8_t 	Reverse_Mode_u8;
	
}UserInputSig_St_t;


/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern UserInputSig_St_t	UserInputSig_St;
extern void CANSched_Debug_TxMsgCallback(void);

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* DATA_BANK_H */


