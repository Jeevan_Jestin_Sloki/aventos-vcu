/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : Communicator.h
|    Project	    : VCU
|    Module         : Communicator module 
|    Description    : This file contains the variables and functions 			
|                     to Communicator with BMS, VCU, HMI, and other module.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 20/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H


/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "cluster_can_signals.h"
#include "mcu_can_signals.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
 #define GET_HMI_COMM_STATUS()  (HMI_Comm_Ok_b)
 #define SET_HMI_COMM_STATUS()  (HMI_Comm_Ok_b = true) 
 #define CLEAR_HMI_COMM_STATUS()  (HMI_Comm_Ok_b = false) 

 #define GET_BMS_COMM_STATUS()  (BMS_Comm_b)
 #define SET_BMS_COMM_STATUS()  (BMS_Comm_b = true)
 #define CLEAR_BMS_COMM_STATUS()  (BMS_Comm_b = false)

 #define GET_MC_COMM_STATUS()   (MC_Comm_Ok_b)
 #define SET_MC_COMM_STATUS()   (MC_Comm_Ok_b = true)
 #define CLEAR_MC_COMM_STATUS()   (MC_Comm_Ok_b = false)

 

 #define GET_VCU_TIME_MS() (Vehicle_Time_out_u32)
 #define SET_VCU_TIME_MS(x) (Vehicle_Time_out_u32 = x)
 #define INC_VCU_TIME_MS() (Vehicle_Time_out_u32++)



//  #define GET_VEHICLE_ON_STATE() (Vehicle_TurnOn_b)
//  #define GET_VEHICLE_IGNITION_STATE() (Vehicle_ignition_b)

 #define NEUTRAL 							(0U)
 #define ECO 								(1U)
 #define SPORT 								(2U)
 #define REVERSE                            (3U)
 #define SAFE_MODE                          (4U)

 #define ZERO                (0U)
 #define ONE                 (1U)
 #define TWO                 (2U)
 #define THREE               (3U)
 #define FOUR                (4U)
 #define FIVE                (5U)
 #define SIX                 (6U)
 #define SEVEN               (7U)
 #define EIGHT               (8U)
 #define NINE                (9U)
 #define TEN                 (10U)
 #define FOURTY              (40U) 

 //#define TRUE                (true)
 //#define FALSE               (false)



 #define Get_Analog_throttle_error()         (Motor_0x150_SetRx.Analog_throttle_error_u8)
 #define Get_Battery_current()               (Motor_0x150_SetRx.Battery_current_s16)
 #define Get_Controller_temp_warn()          (Motor_0x150_SetRx.Controller_temp_warning_u8)
 #define Get_Driving_direction_actual()      (Motor_0x150_SetRx.Driving_direction_actual_u8)
 #define Get_Motor_Enable_Status()           (Motor_0x150_SetRx.Motor_Enable_Status_u8)
 #define Get_Motor_Speed()                   (Motor_0x150_SetRx.Motor_Speed_s16)
 #define Get_Motor_temperature_warn()        (Motor_0x150_SetRx.Motor_temperature_warning_u8)
 #define Get_Over_current_error()            (Motor_0x150_SetRx.Over_current_error_u8)
 #define Get_Over_Voltage_error()            (Motor_0x150_SetRx.Over_Voltage_error_u8)
 #define Get_Peripheral_supply_failure()     (Motor_0x150_SetRx.Peripheral_supply_failure_u8)
 #define Get_Position_Sensor_Error()         (Motor_0x150_SetRx.Position_Sensor_Error_u8)
 #define Get_Reserved_for_0x150_LTVS_s16()   (Motor_0x150_SetRx.Reserved_for_LTVS_s16)
//  #define Get_Ride_mode_Actual()              (Motor_0x150_SetRx.Ride_mode_Actual_u8)
 #define Get_Throttle_Disable_Status()       (Motor_0x150_SetRx.Throttle_Disable_Status_u8)
 #define Get_Throttle_Map_Actual()           (Motor_0x150_SetRx.Throttle_Map_Actual_u8)
 #define Get_Timeout_error()                 (Motor_0x150_SetRx.Timeout_error_u8)
 #define Get_Under_Voltage_Error()           (Motor_0x150_SetRx.Under_Voltage_Error_u8)


 #define BREAK_STATUS()  (Breaks_Applied_b)
 #define SET_BREAK_STATUS(x)  (Breaks_Applied_b = x)

 #define FORWARD                (1U)
 #define BACKWARD               (0U)
 #define RUN                (0U)
 #define STOP               (1U)
//  #define Get_ODO()                   (Cluster_Rx_Per_1.ODO_u32)
//  #define Get_Vehicle_Speed()         (Cluster_Rx_Per_1.Vehicle_Speed_u8)
//  #define Get_Mileage()               (Cluster_Rx_Per_1.Mileage_u8)
//  #define Get_Power_Consumption()     (Cluster_Rx_Per_2.Power_Consumption_u16)
//  #define Get_Range_Km()              (Cluster_Rx_Per_2.Range_Km_u16)
//  #define Get_Battery_SOC()           (Cluster_Rx_Per_2.Battery_SOC_u8)
//  #define Get_BLE_Icon()              (Cluster_Rx_Per_2.BLE_Icon_u8)
//  #define Get_Regen_Breaking()        (Cluster_Rx_Per_2.Regen_Braking_u8)
 
 
//  #define Set_ODO(x)                  (Cluster_Rx_Per_1.ODO_u32 = x)
//  #define Set_Vehicle_Speed(x)        (Cluster_Rx_Per_1.Vehicle_Speed_u8 = x)
//  #define Set_Mileage(x)              (Cluster_Rx_Per_1.Mileage_u8 = x)
//  #define Set_Power_Consumption(x)    (Cluster_Rx_Per_2.Power_Consumption_u16 = x)
//  #define Set_Range_Km(x)             (Cluster_Rx_Per_2.Range_Km_u16 = x)
//  #define Set_Battery_SOC(x)          (Cluster_Rx_Per_2.Battery_SOC_u8 = x)
//  #define Set_BLE_Icon(x)             (Cluster_Rx_Per_2.BLE_Icon_u8 = x)
//  #define Set_Regen_Breaking(x)       (Cluster_Rx_Per_2.Regen_Braking_u8 = x)
/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/

typedef enum
{
    CLUSTER_EVENT_START_E = 0,
    CLUSTER_REVERSE_MODE_E = CLUSTER_EVENT_START_E,
    CLUSTER_SPORTS_MODE_E,
    CLUSTER_ECO_MODE_E,
    CLUSTER_NEUTRAL_MODE_E,
    CLUSTER_KILL_SWITCH_E,
    CLUSTER_RIGHT_INDICATOR_E,
    CLUSTER_LEFT_INDICATOR_E,
    CLUSTER_SAFE_MODE_E,
    CLUSTER_MOTOR_FAULT_E,
    CLUSTER_BATTERY_FAULT_E,
    CLUSTER_HIGH_BEAM_E,
    CLUSTER_LOW_BEAM_E,
    CLUSTER_IGNITION_E,
    CLUSTER_BATTERY_STATUS_E,
    CLUSTER_WARNING_SYMBOL_E,
    CLUSTER_SERVICE_INDICATOR_E,
    CLUSTER_SIDE_STAND_E,
    CLUSTER_HOURS_TIME_E,
    CLUSTER_MINUTES_TIME_E,
    CLUSTER_EVENT_END_E
}Cluster_Tx_Eve_En_t;
typedef enum
{
	VEHICLE_INIT_STATE_E,
	VEHICLE_START_STATE_E,
	VEHICLE_RUN_STATE_E,
	VEHICLE_SHUT_DOWN_STATE_E,
    VEHICLE_SAFE_MODE_STATE_E,
	VEHICLE_STANDBY_STATE_E,
} Vehicle_State_En_t;
typedef struct Communicator
{
    uint8_t data;
}Cluster_Tx_Eve_St_t;
typedef enum
{
	VEHICLE_BACKWARD_E = 0,
	VEHICLE_FORWARD_E,
} Vehicle_Direction_En_t;
typedef enum
{
    VEHICLE_SAFE_MODE_START_E = 0,
	VEHICLE_SAFE_MODE_MOTOR_TEM_E = VEHICLE_SAFE_MODE_START_E,
	VEHICLE_SAFE_MODE_CONTROLLER_TEM_E,
	VEHICLE_SAFE_MODE_MC_COMM_E,
    // VEHICLE_SAFE_MODE_HMI_COMM_E,
	VEHICLE_SAFE_MODE_BMS_COMM_E,
	VEHICLE_SAFE_MODE_MODULE_TEM_E,
	VEHICLE_SAFE_MODE_PDU_TEM_E,
	VEHICLE_SAFE_MODE_BMS_TEM_E,
	VEHICLE_SAFE_MODE_BMS_SOC_E,
    VEHICLE_SAFE_MODE_END_E
} Vehicle_SafeMode_Event_En_t;
typedef enum
{
	VEHICLE_SAFE_MODE_ON_E,
	VEHICLE_SAFE_MODE_OFF_E,
} Vehicle_SafeMode_Set_En_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
extern bool HMI_Comm_Ok_b;
extern bool BMS_Comm_b;
extern bool MC_Comm_Ok_b;

extern uint32_t Vehicle_Time_out_u32;

// extern bool Vehicle_TurnOn_b;
// extern bool Vehicle_ignition_b;

extern HMI_Tx_Per_2_t Cluster_Rx_Per_2;
extern HMI_Tx_Per_1_t Cluster_Rx_Per_1;

extern Motor_0x150_Rx_t Motor_0x150_SetRx;
extern int16_t Motor_Speed_s16;

extern bool Breaks_Applied_b;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SET_CLUSTER_DATA
*   Description   : This function Set the cluster data.
*   Parameters    : Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En, uint8_t data
*   Return Value  : None
*******************************************************************************/ 
extern void SET_CLUSTER_DATA(Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En, uint8_t data);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_DATA
*   Description   : This function get the cluster data.
*   Parameters    : Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En
*   Return Value  : uint8_t
*******************************************************************************/ 
extern uint8_t GET_CLUSTER_DATA(Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_PER_1_DATA
*   Description   : This function get the periodic 1 cluster data.
*   Parameters    : None
*   Return Value  : HMI_Rx_Per_1_t
*******************************************************************************/ 
extern HMI_Tx_Per_1_t GET_CLUSTER_PER_1_DATA(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_PER_2_DATA
*   Description   : This function get the periodic 2 cluster data.
*   Parameters    : Nones
*   Return Value  : HMI_Rx_Per_2_t
*******************************************************************************/ 
extern HMI_Tx_Per_2_t GET_CLUSTER_PER_2_DATA(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CLEAR_CLUSTER_DATA
*   Description   : This function clears the cluster data.
*   Parameters    : None.
*   Return Value  : None.
*******************************************************************************/ 
extern void CLEAR_CLUSTER_DATA(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_VCU_STATE
*   Description   : This function get the vcu state.
*   Parameters    : None
*   Return Value  : Vehicle_State_En_t
*******************************************************************************/ 
extern Vehicle_State_En_t GET_VCU_STATE(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SafeMode_SetEvent
*   Description   : This function implements Safe Mode Event setting.
*   Parameters    : Vehicle_SafeMode_Event_En_t Vehicle_SafeMode_Event_En, 
*					Vehicle_SafeMode_Set_En_t SafeMode_Set_En
*   Return Value  : None
*******************************************************************************/
extern void SafeMode_SetEvent(Vehicle_SafeMode_Event_En_t Vehicle_SafeMode_Event_En, 
                        Vehicle_SafeMode_Set_En_t SafeMode_Set_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SafeMode_ClearEvent
*   Description   : This function clears Safe Mode Event setting.
*   Parameters    : None.
*   Return Value  : None.
*******************************************************************************/
extern void SafeMode_ClearEvent(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_SafeMode_Data
*   Description   : This function Gets Safe Mode data.
*   Parameters    : None, 
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Get_SafeMode_Data(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_SafeMode_State
*   Description   : This function Gets Safe Mode State.
*   Parameters    : None, 
*   Return Value  : bool
*******************************************************************************/
extern bool Get_SafeMode_State(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Vehicle_Speed
*   Description   : This function Gets Current Vehicle Speed.
*   Parameters    : None, 
*   Return Value  : uint16_t
*******************************************************************************/
extern uint16_t Get_Vehicle_Speed(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Drive_Dir
*   Description   : This function Sets Vehicle direction.
*   Parameters    : Vehicle_Direction_En_t Vehicle_Direction_En 
*   Return Value  : None
*******************************************************************************/
extern void Set_Drive_Dir(Vehicle_Direction_En_t Vehicle_Direction_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Drive_Dir
*   Description   : This function Gets Vehicle direction.
*   Parameters    : None
*   Return Value  : Vehicle_Direction_En_t
*******************************************************************************/
extern Vehicle_Direction_En_t Get_Drive_Dir(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Ride_mode_Request
*   Description   : This function Sets Ride Mode.
*   Parameters    : uint8_t Ride_mode_u8
*   Return Value  : None
*******************************************************************************/
extern void Set_Ride_mode_Request(uint8_t Ride_mode_u8);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Ride_mode_Request
*   Description   : This function Gets Ride Mode.
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
extern uint8_t Get_Ride_mode_Request(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Motor_Stop_Request
*   Description   : This function Gets Motor Stop request.
*   Parameters    : uint8_t Motor_Request_u8
*   Return Value  : None
*******************************************************************************/
extern void Set_Motor_Stop_Request(uint8_t Motor_Request_u8);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Motor_Stop_Request
*   Description   : This function Gets Motor Stop request.
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
extern uint8_t Get_Motor_Stop_Request(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Motor_Speed_Limit
*   Description   : This function Sets Motor Speed limit.
*   Parameters    : int16_t Motor_Speed_s16
*   Return Value  : None
*******************************************************************************/
extern void Set_Motor_Speed_Limit(int16_t Motor_Speed_s16);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Motor_Speed_Limit
*   Description   : This function Gets Motor Speed limit.
*   Parameters    : None
*   Return Value  : int16_t
*******************************************************************************/
extern int16_t Get_Motor_Speed_Limit(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Actual_Ride_Mode
*   Description   : This function Gets Actaul Ride mode from Motor.
*   Parameters    : None, 
*   Return Value  : uint8_t
*******************************************************************************/
extern uint8_t Get_Actual_Ride_Mode(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BatteryCharging_State
*   Description   : This function Gets battery charging State.
*   Parameters    : None, 
*   Return Value  : bool
*******************************************************************************/
extern bool Get_BatteryCharging_State(void);

#endif /* COMMUNICATOR_H */
/*---------------------- End of File -----------------------------------------*/