/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : cluster.c
|    Project        : VCU
|    Module         : cluster
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the cluster functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CLUSTER_C
#define CLUSTER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "cluster.h"
#include "Communicator.h"
#include "cluster_can_signals.h"
#include "can_if.h"
#include "cil_can_conf.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

static HMI_Tx_Per_1_t HMI_Tx_Per_1;
static _i_HMI_Tx_Per_1_t _i_HMI_Tx_Per_1;
static HMI_Tx_Per_2_t HMI_Tx_Per_2;
static _i_HMI_Tx_Per_2_t _i_HMI_Tx_Per_2;
static HMI_Tx_Eve_1_t HMI_Tx_Eve_1;
static _i_HMI_Tx_Eve_1_t _i_HMI_Tx_Eve_1;

static HMI_Tx_Eve_1_St_t HMI_Tx_Eve_1_St[HMI_EVENT_END_E] =
	{
		{ZERO, &HMI_Tx_Eve_1.Reverse_Mode_u8},
		{ZERO, &HMI_Tx_Eve_1.Sports_Mode_u8},
		{ZERO, &HMI_Tx_Eve_1.Eco_Mode_u8},
		{ZERO, &HMI_Tx_Eve_1.Neutral_Mode_u8},
		{ZERO, &HMI_Tx_Eve_1.Kill_Switch_u8},
		{ZERO, &HMI_Tx_Eve_1.Right_Indicator_u8},
		{ZERO, &HMI_Tx_Eve_1.Left_Indicator_u8},
		{ZERO, &HMI_Tx_Eve_1.Safe_Mode_u8},
		{ZERO, &HMI_Tx_Eve_1.Motor_Fault_u8},
		{ZERO, &HMI_Tx_Eve_1.Battery_Fault_u8},
		{ZERO, &HMI_Tx_Eve_1.High_Beam_u8},
		{ZERO, &HMI_Tx_Eve_1.Low_Beam_u8},
		{ZERO, &HMI_Tx_Eve_1.Ignition_u8},
		{ZERO, &HMI_Tx_Eve_1.Battery_Status_u8},
		{ZERO, &HMI_Tx_Eve_1.Warning_Symbol_u8},
		{ZERO, &HMI_Tx_Eve_1.Service_Indicator_u8},
		{ZERO, &HMI_Tx_Eve_1.Side_Stand_u8},
		{ZERO, &HMI_Tx_Eve_1.Hours_Time_u8},
		{ZERO, &HMI_Tx_Eve_1.Minutes_Time_u8},
};
static Cluster_RunTime_St_t Cluster_RunTime_St;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_StateMachine_proc
*   Description   : This function implements Cluster State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Cluster_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_HMI_EVE_1_TxMsg
*   Description   : This function Send Cluster Event frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void CAN_HMI_EVE_1_TxMsg(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_I_HMI_EVE_1_TxMsg
*   Description   : This function Send Cluster Inverted Event frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void CAN_I_HMI_EVE_1_TxMsg(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Update_data
*   Description   : This function implements Cluster data update operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Cluster_Update_data(void);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Init
*   Description   : This function implements Cluster initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Cluster_Init(void)
{
	HMI_Tx_Eve_1_En_t HMI_Tx_Eve_1_En = HMI_EVENT_START_E;
	Cluster_RunTime_St.Cluster_Event_En = CLUSTER_STOP_EVENT_E;
	Cluster_RunTime_St.Cluster_State_En = CLUSTER_STANDBY_STATE_E;
	Cluster_RunTime_St.Cluster_ignition_b = false;
	Cluster_RunTime_St.Cluster_kill_b = true;
	Cluster_RunTime_St.Cluster_Start_b = false;
//	HMI_Tx_Eve_1.Ignition_u8 = ZERO;
//	HMI_Tx_Eve_1.Battery_Fault_u8 = ZERO;
//	HMI_Tx_Eve_1.Battery_Status_u8 = ZERO;
//	HMI_Tx_Eve_1.Eco_Mode_u8 = ZERO;
	HMI_Tx_Eve_1.Eve_1_Counter_u8 = ZERO;
//	HMI_Tx_Eve_1.High_Beam_u8 = ZERO;
//	HMI_Tx_Eve_1.Hours_Time_u8 = ZERO;
//	HMI_Tx_Eve_1.Kill_Switch_u8 = ZERO;
//	HMI_Tx_Eve_1.Left_Indicator_u8 = ZERO;
//	HMI_Tx_Eve_1.Low_Beam_u8 = ZERO;
//	HMI_Tx_Eve_1.Minutes_Time_u8 = ZERO;
//	HMI_Tx_Eve_1.Motor_Fault_u8 = ZERO;
//	HMI_Tx_Eve_1.Neutral_Mode_u8 = ZERO;
//	HMI_Tx_Eve_1.Reverse_Mode_u8 = ZERO;
//	HMI_Tx_Eve_1.Right_Indicator_u8 = ZERO;
//	HMI_Tx_Eve_1.Safe_Mode_u8 = ZERO;
//	HMI_Tx_Eve_1.Service_Indicator_u8 = ZERO;
//	HMI_Tx_Eve_1.Side_Stand_u8 = ZERO;
//	HMI_Tx_Eve_1.Sports_Mode_u8 = ZERO;
//	HMI_Tx_Eve_1.Warning_Symbol_u8 = ZERO;
	HMI_Tx_Per_2.Battery_SOC_u8 = ZERO;
	HMI_Tx_Per_2.BLE_Icon_u8 = ZERO;
	HMI_Tx_Per_2.Per_2_Counter_u8 = ZERO;
	HMI_Tx_Per_2.Power_Consumption_u16 = ZERO;
	HMI_Tx_Per_2.Range_Km_u16 = ZERO;
	HMI_Tx_Per_2.Regen_Braking_u8 = ZERO;
	HMI_Tx_Per_1.Mileage_u8 = ZERO;
	HMI_Tx_Per_1.ODO_u32 = ZERO;
	HMI_Tx_Per_1.Vehicle_Speed_u8 = ZERO;
	HMI_Tx_Per_1.Per_1_Counter_u8 = ZERO;
	for (HMI_Tx_Eve_1_En = HMI_EVENT_START_E; HMI_Tx_Eve_1_En < HMI_EVENT_END_E; HMI_Tx_Eve_1_En++)
	{
		*HMI_Tx_Eve_1_St[HMI_Tx_Eve_1_En].data_pu8 = ZERO;
		HMI_Tx_Eve_1_St[HMI_Tx_Eve_1_En].data_u8 = ZERO;
	}
	
	_i_HMI_Tx_Eve_1._i_Eve_1_Counter_u8 = ZERO;
	_i_HMI_Tx_Per_2._i_Per_2_Counter_u8 = ZERO;
	_i_HMI_Tx_Per_1._i_Per_1_Counter_u8 = ZERO;
	CLEAR_HMI_COMM_STATUS();
	CLEAR_CLUSTER_DATA();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Proc
*   Description   : This function implements Cluster Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Cluster_Proc(void)
{
	Cluster_StateMachine_proc();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_StateMachine_proc
*   Description   : This function implements Cluster State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Cluster_StateMachine_proc(void)
{
	HMI_Tx_Eve_1_En_t HMI_Tx_Eve_1_En = HMI_EVENT_START_E;
	Cluster_State_En_t Cluster_State_En = (Cluster_State_En_t)GET_VCU_STATE();

	if ((true == Cluster_RunTime_St.Cluster_ignition_b) || (CLUSTER_STOP_EVENT_E != Cluster_RunTime_St.Cluster_State_En))
	{
		for (HMI_Tx_Eve_1_En = HMI_EVENT_START_E; HMI_Tx_Eve_1_En < HMI_EVENT_END_E; HMI_Tx_Eve_1_En++)
		{
			if (HMI_Tx_Eve_1_St[HMI_Tx_Eve_1_En].data_u8 != GET_CLUSTER_DATA((Cluster_Tx_Eve_En_t)HMI_Tx_Eve_1_En))
			{
				Cluster_RunTime_St.Cluster_Send_Event_b = true;
				*HMI_Tx_Eve_1_St[HMI_Tx_Eve_1_En].data_pu8 = GET_CLUSTER_DATA((Cluster_Tx_Eve_En_t)HMI_Tx_Eve_1_En);
				HMI_Tx_Eve_1_St[HMI_Tx_Eve_1_En].data_u8 = GET_CLUSTER_DATA((Cluster_Tx_Eve_En_t)HMI_Tx_Eve_1_En);
			}
		}
	}
	Cluster_RunTime_St.Cluster_State_En = Cluster_State_En;
	switch (Cluster_RunTime_St.Cluster_State_En)
	{
	case CLUSTER_INIT_STATE_E:
	{
		Cluster_RunTime_St.Cluster_State_En = CLUSTER_START_STATE_E;
		HMI_Tx_Eve_1.Ignition_u8 = ON;
		Cluster_RunTime_St.Cluster_Start_b = true;
		CAN_HMI_EVE_1_TxMsg();
		CAN_I_HMI_EVE_1_TxMsg();
		break;
	}
	case CLUSTER_START_STATE_E:
	{

		if (true == Cluster_RunTime_St.Cluster_Send_Event_b)
		{
			if (true == GET_HMI_COMM_STATUS())
			{
				//SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				CAN_HMI_EVE_1_TxMsg();
				CAN_I_HMI_EVE_1_TxMsg();
			}
			else if (false == GET_HMI_COMM_STATUS())
			{
				// SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_ON_E);
			}
			else
			{
				/* code */
			}

			Cluster_RunTime_St.Cluster_Send_Event_b = false;
		}
		else
		{
			/* code */
		}

		Cluster_Update_data();
		break;
	}
	case CLUSTER_RUN_STATE_E:
	{
		if (ON == HMI_Tx_Eve_1.Kill_Switch_u8)
		{
			Cluster_RunTime_St.Cluster_Send_Event_b = true;
			HMI_Tx_Eve_1.Kill_Switch_u8 = OFF;
		}
		else
		{
			/* code */
		}

		if (true == Cluster_RunTime_St.Cluster_Send_Event_b)
		{
			if (true == GET_HMI_COMM_STATUS())
			{
				//SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				CAN_HMI_EVE_1_TxMsg();
				CAN_I_HMI_EVE_1_TxMsg();
			}
			else if (false == GET_HMI_COMM_STATUS())
			{
				// SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_ON_E);
			}
			else
			{
				/* code */
			}
			Cluster_RunTime_St.Cluster_Send_Event_b = false;
		}
		else
		{
			/* code */
		}
		
		Cluster_Update_data();
		break;
	}
	case CLUSTER_SHUT_DOWN_STATE_E:
	{
		if (ON == HMI_Tx_Eve_1.Ignition_u8)
		{
			HMI_Tx_Eve_1.Ignition_u8 = OFF;
			Cluster_RunTime_St.Cluster_Send_Event_b = true;
		}
		else
		{
			/* code */
		}
		if (true == Cluster_RunTime_St.Cluster_Send_Event_b)
		{
			if (true == GET_HMI_COMM_STATUS())
			{
				//SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				CAN_HMI_EVE_1_TxMsg();
				CAN_I_HMI_EVE_1_TxMsg();
			}
			else if (false == GET_HMI_COMM_STATUS())
			{
				// SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_ON_E);
			}
			else
			{
				/* code */
			}
			Cluster_RunTime_St.Cluster_Send_Event_b = false;
		}
		else
		{
			/* code */
		}

		Cluster_RunTime_St.Cluster_Start_b = false;
		break;
	}
	case CLUSTER_SAFE_MODE_STATE_E:
	{
		if (true == Cluster_RunTime_St.Cluster_Send_Event_b)
		{
			if (true == GET_HMI_COMM_STATUS())
			{
				//SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				CAN_HMI_EVE_1_TxMsg();
				CAN_I_HMI_EVE_1_TxMsg();
			}
			else if (false == GET_HMI_COMM_STATUS())
			{
				// SafeMode_SetEvent(VEHICLE_SAFE_MODE_HMI_COMM_E, VEHICLE_SAFE_MODE_ON_E);
			}
			else
			{
				/* code */
			}
			Cluster_RunTime_St.Cluster_Send_Event_b = false;
		}
		else
		{
			/* code */
		}
		
		Cluster_Update_data();
		break;
	}
	case CLUSTER_STANDBY_STATE_E:
	{
		Cluster_RunTime_St.Cluster_Send_Event_b = false;
		CLEAR_HMI_COMM_STATUS();
		// Cluster_Update_data();
		break;
	}

	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_HMI_PER_1_TxMsgCallback
*   Description   : This function Send Cluster Periodic frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CANSched_HMI_PER_1_TxMsgCallback(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		HMI_Tx_Per_1.Per_1_Counter_u8++;
		Serialize_HMI_Tx_Per_1(&HMI_Tx_Per_1, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_HMI_PER_1_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_HMI_PER_2_TxMsgCallback
*   Description   : This function Send Cluster Periodic frame 2 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CANSched_HMI_PER_2_TxMsgCallback(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		HMI_Tx_Per_2.Per_2_Counter_u8++;
		Serialize_HMI_Tx_Per_2(&HMI_Tx_Per_2, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_HMI_PER_2_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_I_HMI_PER_1_TxMsgCallback
*   Description   : This function Send Cluster Inverted Periodic frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CANSched_I_HMI_PER_1_TxMsgCallback(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		_i_HMI_Tx_Per_1._i_Mileage_u8 = ~HMI_Tx_Per_1.Mileage_u8;
		_i_HMI_Tx_Per_1._i_ODO_u32 = ~HMI_Tx_Per_1.ODO_u32;
		_i_HMI_Tx_Per_1._i_Vehicle_Speed_u8 = ~HMI_Tx_Per_1.Vehicle_Speed_u8;
		_i_HMI_Tx_Per_1._i_Per_1_Counter_u8++;

		Serialize__i_HMI_Tx_Per_1(&_i_HMI_Tx_Per_1, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_I_HMI_PER_1_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_HMI_PER_2_TxMsgCallback
*   Description   : This function Send Cluster Inverted Periodic frame 2 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CANSched_I_HMI_PER_2_TxMsgCallback(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		_i_HMI_Tx_Per_2._i_Battery_SOC_u8 = ~HMI_Tx_Per_2.Battery_SOC_u8;
		_i_HMI_Tx_Per_2._i_BLE_Icon_u8 = ~HMI_Tx_Per_2.BLE_Icon_u8;
		_i_HMI_Tx_Per_2._i_Power_Consumption_u16 = ~HMI_Tx_Per_2.Power_Consumption_u16;
		_i_HMI_Tx_Per_2._i_Range_Km_u16 = ~HMI_Tx_Per_2.Range_Km_u16;
		_i_HMI_Tx_Per_2._i_Regen_Braking_u8 = ~HMI_Tx_Per_2.Regen_Braking_u8;
		_i_HMI_Tx_Per_2._i_Per_2_Counter_u8++;

		Serialize__i_HMI_Tx_Per_2(&_i_HMI_Tx_Per_2, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_I_HMI_PER_2_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_HMI_EVE_1_TxMsg
*   Description   : This function Send Cluster Event frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CAN_HMI_EVE_1_TxMsg(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		HMI_Tx_Eve_1.Eve_1_Counter_u8++;
		Serialize_HMI_Tx_Eve_1(&HMI_Tx_Eve_1, &data[ZERO]);
		
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_HMI_EVE_1_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CAN_I_HMI_EVE_1_TxMsg
*   Description   : This function Send Cluster Inverted Event frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void CAN_I_HMI_EVE_1_TxMsg(void)
{
	if (true == Cluster_RunTime_St.Cluster_Start_b)
	{
		uint8_t data[EIGHT];
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		_i_HMI_Tx_Eve_1._i_High_Beam_u8 = ~HMI_Tx_Eve_1.High_Beam_u8;
		_i_HMI_Tx_Eve_1._i_Low_Beam_u8 = ~HMI_Tx_Eve_1.Low_Beam_u8;
		_i_HMI_Tx_Eve_1._i_Neutral_Mode_u8 = ~HMI_Tx_Eve_1.Neutral_Mode_u8;
		_i_HMI_Tx_Eve_1._i_Reverse_Mode_u8 = ~HMI_Tx_Eve_1.Reverse_Mode_u8;
		_i_HMI_Tx_Eve_1._i_Right_Indicator_u8 = ~HMI_Tx_Eve_1.Right_Indicator_u8;
		_i_HMI_Tx_Eve_1._i_Safe_Mode_u8 = ~HMI_Tx_Eve_1.Safe_Mode_u8;
		_i_HMI_Tx_Eve_1._i_Service_Indicator_u8 = ~HMI_Tx_Eve_1.Service_Indicator_u8;
		_i_HMI_Tx_Eve_1._i_Side_Stand_u8 = ~HMI_Tx_Eve_1.Side_Stand_u8;
		_i_HMI_Tx_Eve_1._i_Sports_Mode_u8 = ~HMI_Tx_Eve_1.Sports_Mode_u8;
		_i_HMI_Tx_Eve_1._i_Warning_Symbol_u8 = ~HMI_Tx_Eve_1.Warning_Symbol_u8;
		_i_HMI_Tx_Eve_1._i_Left_Indicator_u8 = ~HMI_Tx_Eve_1.Left_Indicator_u8;
		_i_HMI_Tx_Eve_1._i_Battery_Fault_u8 = ~HMI_Tx_Eve_1.Battery_Fault_u8;
		_i_HMI_Tx_Eve_1._i_Battery_Status_u8 = ~HMI_Tx_Eve_1.Battery_Status_u8;
		_i_HMI_Tx_Eve_1._i_Cluster_Use_Place_u8 = ~HMI_Tx_Eve_1.Cluster_Use_Place_u8;
		_i_HMI_Tx_Eve_1._i_Eco_Mode_u8 = ~HMI_Tx_Eve_1.Eco_Mode_u8;
		_i_HMI_Tx_Eve_1._i_Hours_Time_u8 = ~HMI_Tx_Eve_1.Hours_Time_u8;
		_i_HMI_Tx_Eve_1._i_Ignition_u8 = ~HMI_Tx_Eve_1.Ignition_u8;
		_i_HMI_Tx_Eve_1._i_Kill_Switch_u8 = ~HMI_Tx_Eve_1.Kill_Switch_u8;
		_i_HMI_Tx_Eve_1._i_Minutes_Time_u8 = ~HMI_Tx_Eve_1.Minutes_Time_u8;

		_i_HMI_Tx_Eve_1._i_Eve_1_Counter_u8++;
		Serialize__i_HMI_Tx_Eve_1(&_i_HMI_Tx_Eve_1, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_I_HMI_EVE_1_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Update_data
*   Description   : This function implements Cluster data update operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Cluster_Update_data(void)
{
	uint8_t counter = ZERO;
	counter = HMI_Tx_Per_1.Per_1_Counter_u8;
	HMI_Tx_Per_1 = GET_CLUSTER_PER_1_DATA();
	HMI_Tx_Per_1.Per_1_Counter_u8 = counter;

	counter = HMI_Tx_Per_2.Per_2_Counter_u8;
	HMI_Tx_Per_2 = GET_CLUSTER_PER_2_DATA();
	HMI_Tx_Per_2.Per_2_Counter_u8 = counter;
	// HMI_Tx_Per_2.Battery_SOC_u8 = 30;
}

#endif /* CLUSTER_C */
/*---------------------- End of File -----------------------------------------*/