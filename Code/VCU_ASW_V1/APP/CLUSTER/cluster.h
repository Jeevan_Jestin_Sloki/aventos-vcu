/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : cluster.h
|    Project        : VCU
|    Module         : cluster
|    Description    : This file contains the export variables and functions
|                     to initialize and Operate the cluster functanality.      
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 15/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CLUSTER_H
#define CLUSTER_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define ON 1
#define OFF 0
#define BLINK 3

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
    HMI_EVENT_START_E = 0,
    REVERSE_MODE_E = HMI_EVENT_START_E,
    SPORTS_MODE_E,
    ECO_MODE_E,
    NEUTRAL_MODE_E,
    KILL_SWITCH_E,
    RIGHT_INDICATOR_E,
    LEFT_INDICATOR_E,
    SAFE_MODE_E,
    MOTOR_FAULT_E,
    BATTERY_FAULT_E,
    HIGH_BEAM_E,
    LOW_BEAM_E,
    IGNITION_E,
    BATTERY_STATUS_E,
    WARNING_SYMBOL_E,
    SERVICE_INDICATOR_E,
    SIDE_STAND_E,
    HOURS_TIME_E,
    MINUTES_TIME_E,
    HMI_EVENT_END_E
    //EVE_1_COUNTER_E,
} HMI_Tx_Eve_1_En_t;

typedef enum
{
    CLUSTER_INIT_STATE_E, //added
    CLUSTER_START_STATE_E,
    CLUSTER_RUN_STATE_E,
    CLUSTER_SHUT_DOWN_STATE_E, // added
    CLUSTER_SAFE_MODE_STATE_E,
    CLUSTER_STANDBY_STATE_E,
} Cluster_State_En_t;
typedef enum
{
    CLUSTER_INIT_EVENT_E, //added
    CLUSTER_START_EVENT_E,
    CLUSTER_RUN_EVENT_E,
    CLUSTER_SHUT_DOWN_EVENT_E, // added
    CLUSTER_SAFE_MODE_EVENT_E,
    CLUSTER_STOP_EVENT_E,
} Cluster_Event_En_t;
typedef struct
{
    bool Cluster_ignition_b;
    bool Cluster_kill_b;
    bool Cluster_Start_b;
    bool Cluster_Send_Event_b;
    Cluster_Event_En_t Cluster_Event_En;
    Cluster_State_En_t Cluster_State_En;
} Cluster_RunTime_St_t;
typedef struct
{
    uint8_t data_u8;
    uint8_t *data_pu8;
} HMI_Tx_Eve_1_St_t;
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Init
*   Description   : This function implements Cluster initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void Cluster_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Proc
*   Description   : This function implements Cluster Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void Cluster_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_HMI_PER_1_TxMsgCallback
*   Description   : This function Send Cluster Periodic frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void CANSched_HMI_PER_1_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_HMI_PER_2_TxMsgCallback
*   Description   : This function Send Cluster Periodic frame 2 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void CANSched_HMI_PER_2_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_I_HMI_PER_1_TxMsgCallback
*   Description   : This function Send Cluster Inverted Periodic frame 1 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void CANSched_I_HMI_PER_1_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CANSched_I_HMI_PER_2_TxMsgCallback
*   Description   : This function Send Cluster Inverted Periodic frame 2 data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void CANSched_I_HMI_PER_2_TxMsgCallback(void);

#endif /* CLUSTER_H */
/*---------------------- End of File -----------------------------------------*/