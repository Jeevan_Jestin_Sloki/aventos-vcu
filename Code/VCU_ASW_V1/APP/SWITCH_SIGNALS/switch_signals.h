/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : switch_signals.h
|    Project        : VCU
|    Module         : Switch Signals
|    Description    : This file contains the export variables and functions                     
|                     to initialize and Operate the vehicle driver mode 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 15/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SWITCH_SIGNALS_H
#define SWITCH_SIGNALS_H


/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"


/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
	LOW_BEAM_ON_STATE_E,
	HIGH_BEAM_ON_STATE_E,
}HeadLight_State_En_t;
typedef enum
{
	LOW_BEAM_ON_EVENT_E,
	HIGH_BEAM_ON_EVENT_E,
}HeadLight_Event_En_t;
typedef enum
{
	LIGHT_SWITCH_OFF_STATE_E,
	LIGHT_SWITCH_ON_STATE_E,
}LightSwitch_State_En_t;
typedef enum
{
	LIGHT_SWITCH_OFF_EVENT_E,
	LIGHT_SWITCH_ON_EVENT_E,
}LightSwitch_Event_En_t;

typedef enum
{
	LEFT_TURN_SWITCH_OFF_STATE_E,
	LEFT_TURN_SWITCH_ON_STATE_E,
}LeftTurnSwitch_State_En_t;
typedef enum
{
	LEFT_TURN_SWITCH_OFF_EVENT_E,
	LEFT_TURN_SWITCH_ON_EVENT_E,
}LeftTurnSwitch_Event_En_t;
typedef enum
{
	RIGHT_TURN_SWITCH_OFF_STATE_E,
	RIGHT_TURN_SWITCH_ON_STATE_E,
}RightTurnSwitch_State_En_t;
typedef enum
{
	RIGHT_TURN_SWITCH_OFF_EVENT_E,
	RIGHT_TURN_SWITCH_ON_EVENT_E,
}RightTurnSwitch_Event_En_t;

typedef enum
{
	BREAK_SWITCH_LOW_STATE_E,
	BREAK_SWITCH_HIGH_STATE_E,
}BreakSwitch_State_En_t;
typedef enum
{
	BREAK_SWITCH_LOW_EVENT_E,
	BREAK_SWITCH_HIGH_EVENT_E,
}BreakSwitch_Event_En_t;

typedef struct
{
	LightSwitch_State_En_t  LightSwitch_State_En;
	LightSwitch_Event_En_t 	LightSwitch_Event_En;
	HeadLight_State_En_t    HeadLight_State_En;
	HeadLight_Event_En_t    HeadLight_Event_En;
	bool			HLB_FirstEntry_b;
}Light_RunTime_St_t;
typedef struct
{
	LeftTurnSwitch_State_En_t     LeftTurnSwitch_State_En;
	LeftTurnSwitch_Event_En_t     LeftTurnSwitch_Event_En;
	RightTurnSwitch_State_En_t    RightTurnSwitch_State_En;
	RightTurnSwitch_Event_En_t    RightTurnSwitch_Event_En;
}Turn_RunTime_St_t;

typedef struct
{
	BreakSwitch_State_En_t 	 BreakSwitch_State_En;
	BreakSwitch_Event_En_t   BreakSwitch_Event_En;
}Break_RunTime_St_t;

typedef enum
{
	KILL_SWITCH_OFF_STATE_E,
	KILL_SWITCH_ON_STATE_E,
}KillSwitch_State_En_t;
typedef enum
{
	KILL_SWITCH_OFF_EVENT_E,
	KILL_SWITCH_ON_EVENT_E,
}KillSwitch_Event_En_t;

typedef struct
{
	KillSwitch_State_En_t 	 KillSwitch_State_En;
	KillSwitch_Event_En_t    KillSwitch_Event_En;
	bool					 Kill_FirstEntry_b;
}Kill_RunTime_St_t;
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Init
*   Description   : This function implements Switch Signals initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void SwitchSignals_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Proc
*   Description   : This function implements Switch Signals Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void SwitchSignals_Proc(void);

#endif /* SWITCH_SIGNALS_H */
/*---------------------- End of File -----------------------------------------*/