/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : switch_signals.c
|    Project        : VCU
|    Module         : Switch Signals 
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the vehicle switch signals 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SWITCH_SIGNALS_C
#define SWITCH_SIGNALS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "switch_signals.h"
#include "Communicator.h"
#include "DataAquire.h"
#include "DataBank.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static Turn_RunTime_St_t Turn_RunTime_St;
static Break_RunTime_St_t Break_RunTime_St;
static Light_RunTime_St_t Light_RunTime_St;
static Kill_RunTime_St_t Kill_RunTime_St;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Break_StateMachine_proc
*   Description   : This function implements Vehicle Break Event operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Break_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Kill_StateMachine_proc
*   Description   : This function implements Vehicle Kill State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Kill_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleTurn_StateMachine_proc
*   Description   : This function implements Vehicle Turn State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VehicleTurn_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleLights_StateMachine_proc
*   Description   : This function implements Vehicle Lights State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VehicleLights_StateMachine_proc(void);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Init
*   Description   : This function implements Switch Signals initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void SwitchSignals_Init(void)
{
	Break_RunTime_St.BreakSwitch_Event_En = BREAK_SWITCH_LOW_EVENT_E;
	Break_RunTime_St.BreakSwitch_State_En = BREAK_SWITCH_LOW_STATE_E;
	SET_BREAK_STATUS(false);
	Turn_RunTime_St.LeftTurnSwitch_State_En = LEFT_TURN_SWITCH_OFF_STATE_E;
	Turn_RunTime_St.RightTurnSwitch_State_En = RIGHT_TURN_SWITCH_OFF_STATE_E;
	Turn_RunTime_St.RightTurnSwitch_Event_En = RIGHT_TURN_SWITCH_OFF_EVENT_E;
	Turn_RunTime_St.LeftTurnSwitch_Event_En = LEFT_TURN_SWITCH_OFF_EVENT_E;
	Light_RunTime_St.HeadLight_State_En = LOW_BEAM_ON_STATE_E;
	Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
	Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
	Light_RunTime_St.LightSwitch_State_En = LIGHT_SWITCH_OFF_STATE_E;
	Light_RunTime_St.HLB_FirstEntry_b = true;
	Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_ON_EVENT_E;
	Kill_RunTime_St.KillSwitch_State_En = KILL_SWITCH_ON_STATE_E;
	Kill_RunTime_St.Kill_FirstEntry_b = true;
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Proc
*   Description   : This function implements Switch Signals Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void SwitchSignals_Proc(void)
{
	Break_StateMachine_proc();
	VehicleTurn_StateMachine_proc();
	VehicleLights_StateMachine_proc();
	Kill_StateMachine_proc();
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Break_StateMachine_proc
*   Description   : This function implements Vehicle Break State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Break_StateMachine_proc(void)
{
	BreakSwitch_State_En_t BreakSwitch_State_En = (BreakSwitch_State_En_t)(UserInputSig_St.Brake_SwitchState_Front_u8 
							| UserInputSig_St.Brake_SwitchState_Back_u8);

	Break_RunTime_St.BreakSwitch_State_En = BreakSwitch_State_En;

	switch (Break_RunTime_St.BreakSwitch_State_En)
	{
	case BREAK_SWITCH_LOW_STATE_E:
	{
		if (true == BREAK_STATUS())
		{
 			SET_BREAK_STATUS(false);
			//Todo: Enable the torque output of the motor when breaks are Released
		}
		/* code */
		break;
	}
	case BREAK_SWITCH_HIGH_STATE_E:
	{
		if (false == BREAK_STATUS())
		{
			SET_BREAK_STATUS(true);
			//Todo: Disable the torque output of the motor when breaks are applied
		}
		/* code */
		break;
	}
	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Kill_StateMachine_proc
*   Description   : This function implements Vehicle Kill State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Kill_StateMachine_proc(void)
{
	KillSwitch_State_En_t KillSwitch_State_En = (KillSwitch_State_En_t)UserInputSig_St.Kill_SwitchState_u8;

	Kill_RunTime_St.KillSwitch_State_En = KillSwitch_State_En;

	switch (Kill_RunTime_St.KillSwitch_State_En)
	{
	case KILL_SWITCH_ON_STATE_E:
	{
		if ((KILL_SWITCH_OFF_EVENT_E == Kill_RunTime_St.KillSwitch_Event_En) ||
			(true == Kill_RunTime_St.Kill_FirstEntry_b))
		{
			Kill_RunTime_St.Kill_FirstEntry_b = false;
			Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_ON_EVENT_E;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, THREE);
		}
		else
		{
			/* code */
		}

		break;
	}
	case KILL_SWITCH_OFF_STATE_E:
	{
		if ((KILL_SWITCH_ON_EVENT_E == Kill_RunTime_St.KillSwitch_Event_En) ||
			(true == Kill_RunTime_St.Kill_FirstEntry_b))
		{
			Kill_RunTime_St.Kill_FirstEntry_b = false;
			Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_OFF_EVENT_E;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, ZERO);
		}
		else
		{
			/* code */
		}
		
		break;
	}
	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleTurn_StateMachine_proc
*   Description   : This function implements Vehicle Turn State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VehicleTurn_StateMachine_proc(void)
{

	RightTurnSwitch_State_En_t RightTurnSwitch_State_En =
					(RightTurnSwitch_State_En_t)UserInputSig_St.RightTurn_SwitchState_u8;
	LeftTurnSwitch_State_En_t LeftTurnSwitch_State_En = 
					(LeftTurnSwitch_State_En_t)UserInputSig_St.LeftTurn_SwitchState_u8;

	Turn_RunTime_St.RightTurnSwitch_State_En = RightTurnSwitch_State_En;
	Turn_RunTime_St.LeftTurnSwitch_State_En = LeftTurnSwitch_State_En;

	switch (Turn_RunTime_St.RightTurnSwitch_State_En)
	{
	case RIGHT_TURN_SWITCH_ON_STATE_E:
	{
		if (RIGHT_TURN_SWITCH_OFF_EVENT_E == Turn_RunTime_St.RightTurnSwitch_Event_En)
		{
			SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ONE);
			Turn_RunTime_St.RightTurnSwitch_Event_En = RIGHT_TURN_SWITCH_ON_EVENT_E;
			//Todo: VCU must transmit this information via CAN to the HMI
			//in order to illuminate the Left/right turn icon in the HMI
		}
		else
		{
			/* code */
		}

		break;
	}
	case RIGHT_TURN_SWITCH_OFF_STATE_E:
	{
		if (RIGHT_TURN_SWITCH_ON_EVENT_E == Turn_RunTime_St.RightTurnSwitch_Event_En)
		{
			SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
			Turn_RunTime_St.RightTurnSwitch_Event_En = RIGHT_TURN_SWITCH_OFF_EVENT_E;
			//Todo: VCU must transmit this information via CAN to the HMI
			//in order to illuminate the Left/right turn icon in the HMI
		}
		else
		{
			/* code */
		}

		break;
	}
	default:
	{
		break;
	}
	}

	switch (Turn_RunTime_St.LeftTurnSwitch_State_En)
	{
	case LEFT_TURN_SWITCH_ON_STATE_E:
	{
		if (LEFT_TURN_SWITCH_OFF_EVENT_E == Turn_RunTime_St.LeftTurnSwitch_Event_En)
		{
			SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ONE);
			Turn_RunTime_St.LeftTurnSwitch_Event_En = LEFT_TURN_SWITCH_ON_EVENT_E;
			//Todo: VCU must transmit this information via CAN to the HMI
			//in order to illuminate the Left/right turn icon in the HMI
		}
		/* code */
		break;
	}
	case LEFT_TURN_SWITCH_OFF_STATE_E:
	{
		if (LEFT_TURN_SWITCH_ON_EVENT_E == Turn_RunTime_St.LeftTurnSwitch_Event_En)
		{
			SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
			Turn_RunTime_St.LeftTurnSwitch_Event_En = LEFT_TURN_SWITCH_OFF_EVENT_E;
			//Todo: VCU must transmit this information via CAN to the HMI
			//in order to illuminate the Left/right turn icon in the HMI
		}
		/* code */
		break;
	}
	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleLights_StateMachine_proc
*   Description   : This function implements Vehicle Lights State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VehicleLights_StateMachine_proc(void)
{
	Vehicle_State_En_t Vehicle_State_En = GET_VCU_STATE();
	LightSwitch_State_En_t LightSwitch_State_En =
		(LightSwitch_State_En_t)UserInputSig_St.Lights_SwitchState_u8;
	HeadLight_State_En_t HeadLight_State_En =
		(HeadLight_State_En_t)UserInputSig_St.HighBeam_SwitchState_u8;

	Light_RunTime_St.HeadLight_State_En = HeadLight_State_En;
	Light_RunTime_St.LightSwitch_State_En = LightSwitch_State_En;
	if((Vehicle_State_En == VEHICLE_START_STATE_E) ||
			(Vehicle_State_En == VEHICLE_RUN_STATE_E) ||
						(Vehicle_State_En == VEHICLE_SAFE_MODE_STATE_E))
	{
		switch (Light_RunTime_St.HeadLight_State_En)
		{
		case LOW_BEAM_ON_STATE_E:
		{
			if ((LIGHT_SWITCH_ON_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
				((HIGH_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En) || (true == Light_RunTime_St.HLB_FirstEntry_b)))
			{
				Light_RunTime_St.HLB_FirstEntry_b = false;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, THREE);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_ON_EVENT_E;
				//Todo: VCU must transmit this information via CAN to the HMI in
				//order to illuminate the “Headlight” icon in the HMI.
			}
			else if ((LIGHT_SWITCH_OFF_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
					 (LOW_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En)&&
							(LIGHT_SWITCH_ON_EVENT_E == Light_RunTime_St.LightSwitch_Event_En))
			{
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
				Light_RunTime_St.HLB_FirstEntry_b = true;
			}
			else
			{
				;/* code */
			}

			/* code */
			break;
		}
		case HIGH_BEAM_ON_STATE_E:
		{
			if ((LIGHT_SWITCH_ON_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
				((LOW_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En)||((true == Light_RunTime_St.HLB_FirstEntry_b))))
			{
				Light_RunTime_St.HLB_FirstEntry_b = false;
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, THREE);
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = HIGH_BEAM_ON_EVENT_E;
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_ON_EVENT_E;
				//Todo: VCU must transmit this information via CAN to the HMI in
				//order to illuminate the “Headlight” icon in the HMI.
			}
			else if ((LIGHT_SWITCH_OFF_STATE_E == Light_RunTime_St.LightSwitch_State_En)&&
					 (HIGH_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En) &&
							(LIGHT_SWITCH_ON_EVENT_E == Light_RunTime_St.LightSwitch_Event_En))
			{
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = HIGH_BEAM_ON_EVENT_E;
				Light_RunTime_St.HLB_FirstEntry_b = true;
			}

			/* code */
			break;
		}
		default:
		{
			break;
		}
		}
	}
}

#endif /* SWITCH_SIGNALS_C */
	   /*---------------------- End of File -----------------------------------------*/