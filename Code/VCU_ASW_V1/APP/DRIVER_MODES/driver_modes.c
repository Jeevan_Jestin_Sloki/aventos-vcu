/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : driver_modes.c
|    Project        : VCU
|    Module         : driver modes 
|    Description    : This file contains the variables and functions to 			
|                     initialize and Operate the vehicle driver mode 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef DRIVER_MODES_C
#define DRIVER_MODES_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "driver_modes.h"
#include "Communicator.h"
#include "DataBank.h"
#include "DataAquire.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/
//  #define FALSE 0
//  #define TRUE  1
#define IDLE_CHECK_ENABLE false

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static DriverMode_RunTime_St_t DriverMode_RunTime_St;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_StateMachine_proc
*   Description   : This function implements Driver Mode State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void DriverMode_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Driver_Mode_Set
*   Description   : This function Sets Driver Mode in the HMI(cluster).
*   Parameters    : uint8_t Driver_Mode_Data_u8
*   Return Value  : None
*******************************************************************************/
static void Cluster_Driver_Mode_Set(uint8_t Driver_Mode_Data_u8);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Driver_Mode_Move
*   Description   : This function Gets Driver Mode movement (joystic)
*					in the HMI(cluster).
*   Parameters    : uint8_t Driver_Mode_Data_u8
*   Return Value  : None
*******************************************************************************/
static void Cluster_Driver_Mode_Move(uint8_t Driver_Mode_Data_u8);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Init
*   Description   : This function implements Driver Mode initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void DriverMode_Init(void)
{
	DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
	// DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
	DriverMode_RunTime_St.SafeMode_TurnOn_b = false;
	DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
	DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
	DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
	DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
	DriverMode_RunTime_St.SafeMode_data_u32 = 0;
	DriverMode_RunTime_St.Driver_Economy_Move_b = false;
	DriverMode_RunTime_St.Driver_Sports_Move_b = false;
	DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
	DriverMode_RunTime_St.Driver_Neutral_Move_b = false;

	SafeMode_ClearEvent();
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Proc
*   Description   : This function implements Driver Mode Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void DriverMode_Proc(void)
{
	DriverMode_StateMachine_proc();
	/*Add other module Initialisation*/
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_StateMachine_proc
*   Description   : This function implements Driver Mode State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void DriverMode_StateMachine_proc(void)
{
	Vehicle_State_En_t Vehicle_State_En = (Vehicle_State_En_t)GET_VCU_STATE();

	switch (DriverMode_RunTime_St.DriverMode_State_En)
	{
	case DRIVER_IDLE_MODE_STATE_E:
	{
		if (/*(VEHICLE_INIT_STATE_E == Vehicle_State_En) ||
			(VEHICLE_START_STATE_E == Vehicle_State_En) ||*/
			(VEHICLE_RUN_STATE_E == Vehicle_State_En))
		{
			// Clear_Drive_Mode(NEUTRAL_MODE_E);
			if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_IDLE_MODE_EVENT_E;
			}
			else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_ECO_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_ECO_MODE_EVENT_E;
			}
			else if (true == UserInputSig_St.Sports_Mode_Sel_u8)
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SPORT_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_SPORT_MODE_EVENT_E;
			}
			else if (true == UserInputSig_St.Reverse_Mode_Sel_u8)
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_REVERSE_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_REVERSE_MODE_EVENT_E;
			}
			else
			{
				/* code */
			}
		}
		else if (VEHICLE_START_STATE_E == Vehicle_State_En)
		{
			if (ZERO < Get_SafeMode_Data() && (false == DriverMode_RunTime_St.SafeMode_TurnOn_b))
			{
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SAFE_MODE_RUN_STATE_E;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}

		break;
	}
	case DRIVER_NEUTRAL_MODE_RUN_STATE_E:
	{
		if (ZERO >= Get_SafeMode_Data() && (false == DriverMode_RunTime_St.SafeMode_TurnOn_b))
		{
			if (VEHICLE_RUN_STATE_E == Vehicle_State_En)
			{
				if ((true == UserInputSig_St.Sports_Mode_Sel_u8) ||
					(true == UserInputSig_St.Economy_Mode_Sel_u8) ||
					(true == UserInputSig_St.Reverse_Mode_Sel_u8))
				{
					if ((DRIVER_NEUTRAL_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En) ||
						(DRIVER_MODE_IDLE_MODE_EVENT_E == DriverMode_RunTime_St.DriverMode_Event_En))
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if ((FIVE >= Get_Vehicle_Speed()) && (true == BREAK_STATUS()))
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (SPORT == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(SPORT);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_SPORT_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
							}
							else if (ECO == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(ECO);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_ECO_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else if (REVERSE == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
								Set_Drive_Dir(VEHICLE_BACKWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(REVERSE);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_REVERSE_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(NEUTRAL_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(STOP);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_NEUTRAL_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(STOP);
								if (true == UserInputSig_St.Sports_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
									Set_Ride_mode_Request(SPORT);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_SPORT_MODE_EVENT_E;
								}
								else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
									Set_Ride_mode_Request(ECO);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_ECO_MODE_EVENT_E;
								}
								else if (true == UserInputSig_St.Reverse_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
									Set_Ride_mode_Request(REVERSE);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_REVERSE_MODE_EVENT_E;
								}
								else
								{
									/* code */
								}
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							Clear_Drive_Mode(NEUTRAL_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_NEUTRAL_MODE_RUN_STATE_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							if (true == UserInputSig_St.Sports_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
							}
							else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else if (true == UserInputSig_St.Reverse_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
						else
						{
							/* code */
						}
					}
				}
				else if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
				{
					if (false == UserInputSig_St.Neutral_Mode_u8)
					{
						if ((UserInputSig_St.Economy_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Economy_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Economy_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_MOVE_SET | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Sports_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Sports_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Sports_Move_b = true;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_MOVE_SET | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Reverse_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Reverse_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_MOVE_SET);
							Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
						}
						else
						{
							if (DRIVER_MODE_TRANSITION_JOYSTICK_TIME <=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
								// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
								// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
								// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
								Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(NEUTRAL_MODE_E);
								Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							}
						}
					}
					else
					{
						// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
						// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
						// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
						if ((UserInputSig_St.Neutral_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Neutral_Move_b))
						{
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = true;
							Cluster_Driver_Mode_Move(NEUTRAL_MOVE_SET | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
						}
					}
					Set_Drive_Dir(VEHICLE_FORWARD_E);
					Set_Motor_Stop_Request(RUN);
					Set_Ride_mode_Request(NEUTRAL);
					DriverMode_RunTime_St.DriverMode_State_En =
						DRIVER_NEUTRAL_MODE_RUN_STATE_E;
					DriverMode_RunTime_St.DriverMode_Event_En =
						DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
				}
				else
				{
					/* code */
				}
			}
			else if (VEHICLE_START_STATE_E == Vehicle_State_En)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
			else
			{
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
		}
		else
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SAFE_MODE_RUN_STATE_E;
		}

		/* code */
		break;
	}
	case DRIVER_SPORT_MODE_RUN_STATE_E:
	{
		if (ZERO >= Get_SafeMode_Data() && (false == DriverMode_RunTime_St.SafeMode_TurnOn_b))
		{
			if (VEHICLE_RUN_STATE_E == Vehicle_State_En)
			{
				if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
				{
					if (DRIVER_MODE_SPORT_MODE_EVENT_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if (TEN >= Get_Vehicle_Speed())
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (NEUTRAL == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(STOP);
								Set_Ride_mode_Request(NEUTRAL);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_NEUTRAL_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(SPORTS_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_SPORT_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Set_Ride_mode_Request(NEUTRAL);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
							Clear_Drive_Mode(SPORTS_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_SPORT_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_SPORT_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (UserInputSig_St.Economy_Mode_Sel_u8)
				{
					if (DRIVER_SPORT_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if (FOURTY >= Get_Vehicle_Speed())
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (ECO == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(ECO);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_ECO_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(SPORTS_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_SPORT_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
								Set_Ride_mode_Request(ECO);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
							Clear_Drive_Mode(SPORTS_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_SPORT_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_SPORT_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_ECO_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (UserInputSig_St.Reverse_Mode_Sel_u8)
				{
					if (DRIVER_SPORT_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if ((FIVE >= Get_Vehicle_Speed()) && (true == BREAK_STATUS()))
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (REVERSE == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
								Set_Drive_Dir(VEHICLE_BACKWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(REVERSE);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_REVERSE_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(SPORTS_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_SPORT_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
								Set_Ride_mode_Request(REVERSE);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
							Clear_Drive_Mode(SPORTS_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_SPORT_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_SPORT_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_REVERSE_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (true == UserInputSig_St.Sports_Mode_Sel_u8)
				{
					if (false == UserInputSig_St.Sports_Mode_u8)
					{
						if ((UserInputSig_St.Economy_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Economy_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Economy_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_MOVE_SET | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Neutral_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Neutral_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_MOVE_SET | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Reverse_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Reverse_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_MOVE_SET);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
						}
						else
						{
							if (DRIVER_MODE_TRANSITION_JOYSTICK_TIME <=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
								// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
								// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
								// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
								Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(SPORTS_MODE_E);
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
							}
						}
					}
					else
					{
						// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
						// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
						// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
						if ((UserInputSig_St.Sports_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Sports_Move_b))
						{
							// DriverMode_RunTime_St.Driver_Sports_Move_b = true;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_MOVE_SET | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
						}
					}

					DriverMode_RunTime_St.DriverMode_State_En =
						DRIVER_SPORT_MODE_RUN_STATE_E;
					DriverMode_RunTime_St.DriverMode_Event_En =
						DRIVER_MODE_SPORT_MODE_EVENT_E;
				}
				else
				{
					/* code */
				}
			}
			else if (VEHICLE_START_STATE_E == Vehicle_State_En)
			{
				if (TEN >= Get_Vehicle_Speed())
				{
					Clear_Drive_Mode(NEUTRAL_MODE_E);
				}
				else
				{
					Clear_Drive_Mode(SPORTS_MODE_E);
				}
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
			}
			else
			{
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
		}
		else
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SAFE_MODE_RUN_STATE_E;
		}
#if (true == IDLE_CHECK_ENABLE)
		if (ONE >= Get_Vehicle_Speed())
		{
			if (false == DriverMode_RunTime_St.Vehicle_Idle_Check_b)
			{
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = true;
				DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else if ((true == DriverMode_RunTime_St.Vehicle_Idle_Check_b) &&
					 (VEHICLE_IDLE_TIME <=
					  (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32)))
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
#endif

		break;
	}
	case DRIVER_ECO_MODE_RUN_STATE_E:
	{
		if (ZERO >= Get_SafeMode_Data() && (false == DriverMode_RunTime_St.SafeMode_TurnOn_b))
		{
			if (VEHICLE_RUN_STATE_E == Vehicle_State_En)
			{
				if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
				{
					if (DRIVER_ECO_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if (TEN >= Get_Vehicle_Speed())
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (NEUTRAL == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(STOP);
								Set_Ride_mode_Request(NEUTRAL);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_NEUTRAL_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Clear_Drive_Mode(ECONOMY_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_ECO_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Set_Ride_mode_Request(NEUTRAL);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
							Clear_Drive_Mode(ECONOMY_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_ECO_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_ECO_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (UserInputSig_St.Sports_Mode_Sel_u8)
				{
					if (DRIVER_ECO_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}

					if (DRIVER_MODE_TRANSITION_TIME <=
						(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
					{
						if (SPORT == Get_Actual_Ride_Mode())
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
							Set_Drive_Dir(VEHICLE_FORWARD_E);
							Set_Motor_Stop_Request(RUN);
							Set_Ride_mode_Request(SPORT);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_SPORT_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_SPORT_MODE_EVENT_E;
						}
						else
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
							Clear_Drive_Mode(ECONOMY_MODE_E);
							Set_Drive_Dir(VEHICLE_FORWARD_E);
							Set_Motor_Stop_Request(RUN);
							Set_Ride_mode_Request(Get_Actual_Ride_Mode());
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_ECO_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_ECO_MODE_EVENT_E;
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
						}
					}
					else
					{
						if (FIVE >=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
							Set_Ride_mode_Request(SPORT);
							Set_Drive_Dir(VEHICLE_FORWARD_E);
							Set_Motor_Stop_Request(RUN);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_SPORT_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (UserInputSig_St.Reverse_Mode_Sel_u8)
				{
					if (DRIVER_ECO_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if ((FIVE >= Get_Vehicle_Speed()) && (true == BREAK_STATUS()))
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (REVERSE == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
								Set_Drive_Dir(VEHICLE_BACKWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(REVERSE);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_REVERSE_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Clear_Drive_Mode(ECONOMY_MODE_E);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_ECO_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{
							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_TOGGLE);
								Set_Ride_mode_Request(REVERSE);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
							Clear_Drive_Mode(ECONOMY_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_ECO_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_ECO_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_REVERSE_MODE_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
				}
				else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
				{
					if (false == UserInputSig_St.Economy_Mode_u8)
					{
						if ((UserInputSig_St.Sports_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Sports_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Sports_Move_b = true;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_MOVE_SET | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Neutral_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Neutral_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_MOVE_SET | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_ON | REVERSE_OFF);
						}
						else if ((UserInputSig_St.Reverse_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Reverse_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_MOVE_SET);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_TOGGLE);
						}
						else
						{
							if (DRIVER_MODE_TRANSITION_JOYSTICK_TIME <=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
								// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
								// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
								// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
								Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(ECONOMY_MODE_E);
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
							}
						}
					}
					else
					{
						// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
						// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
						// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
						if ((UserInputSig_St.Economy_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Economy_Move_b))
						{
							// DriverMode_RunTime_St.Driver_Economy_Move_b = true;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_MOVE_SET | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
						}
					}
					DriverMode_RunTime_St.DriverMode_State_En =
						DRIVER_ECO_MODE_RUN_STATE_E;
					DriverMode_RunTime_St.DriverMode_Event_En =
						DRIVER_MODE_ECO_MODE_EVENT_E;
				}
				else
				{
					/* code */
				}
			}
			else if (VEHICLE_START_STATE_E == Vehicle_State_En)
			{
				if (TEN >= Get_Vehicle_Speed())
				{
					Clear_Drive_Mode(NEUTRAL_MODE_E);
				}
				else
				{
					Clear_Drive_Mode(ECONOMY_MODE_E);
				}
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
			else
			{
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
		}
		else
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SAFE_MODE_RUN_STATE_E;
		}

#if (true == IDLE_CHECK_ENABLE)
		if (ONE >= Get_Vehicle_Speed())
		{
			if (false == DriverMode_RunTime_St.Vehicle_Idle_Check_b)
			{
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = true;
				DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else if ((true == DriverMode_RunTime_St.Vehicle_Idle_Check_b) &&
					 (VEHICLE_IDLE_TIME <=
					  (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32)))
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
#endif

		break;
	}
	case DRIVER_REVERSE_MODE_RUN_STATE_E:
	{
		if (ZERO >= Get_SafeMode_Data() && (false == DriverMode_RunTime_St.SafeMode_TurnOn_b))
		{
			if (VEHICLE_RUN_STATE_E == Vehicle_State_En)
			{
				if ((true == UserInputSig_St.Sports_Mode_Sel_u8) ||
					(true == UserInputSig_St.Economy_Mode_Sel_u8) ||
					(true == UserInputSig_St.Neutral_Mode_Sel_u8))
				{
					if (DRIVER_REVERSE_MODE_RUN_STATE_E == DriverMode_RunTime_St.DriverMode_Event_En)
					{
						DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
					}
					else
					{
						/* code */
					}
					if ((FIVE >= Get_Vehicle_Speed()) && (true == BREAK_STATUS()))
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							if (SPORT == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(SPORT);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_SPORT_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_SPORT_MODE_EVENT_E;
							}
							else if (ECO == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(ECO);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_ECO_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else if (NEUTRAL == Get_Actual_Ride_Mode())
							{
								Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Set_Drive_Dir(VEHICLE_FORWARD_E);
								Set_Motor_Stop_Request(STOP);
								Set_Ride_mode_Request(NEUTRAL);
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_NEUTRAL_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
								Clear_Drive_Mode(REVERSE_MODE_E);
								Set_Drive_Dir(VEHICLE_BACKWARD_E);
								Set_Motor_Stop_Request(RUN);
								Set_Ride_mode_Request(Get_Actual_Ride_Mode());
								DriverMode_RunTime_St.DriverMode_State_En =
									DRIVER_REVERSE_MODE_RUN_STATE_E;
								DriverMode_RunTime_St.DriverMode_Event_En =
									DRIVER_MODE_REVERSE_MODE_EVENT_E;
								DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							}
						}
						else
						{

							if (FIVE >=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								if (true == UserInputSig_St.Sports_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_ON);
									Set_Ride_mode_Request(SPORT);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_SPORT_MODE_EVENT_E;
								}
								else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_ON);
									Set_Ride_mode_Request(ECO);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_ECO_MODE_EVENT_E;
								}
								else if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
								{
									Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_OFF | REVERSE_ON);
									Set_Ride_mode_Request(NEUTRAL);
									DriverMode_RunTime_St.DriverMode_Event_En =
										DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
								}
								else
								{
									/* code */
								}
								Set_Drive_Dir(VEHICLE_BACKWARD_E);
								Set_Motor_Stop_Request(RUN);
							}
							else
							{
								/* code */
							}
						}
					}
					else
					{
						if (DRIVER_MODE_TRANSITION_TIME <=
							(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
							Clear_Drive_Mode(REVERSE_MODE_E);
							DriverMode_RunTime_St.DriverMode_State_En =
								DRIVER_REVERSE_MODE_RUN_STATE_E;
							DriverMode_RunTime_St.DriverMode_Event_En =
								DRIVER_MODE_REVERSE_MODE_EVENT_E;
						}
						else if (FIVE >=
								 (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
						{
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
							if (true == UserInputSig_St.Sports_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_SPORT_MODE_EVENT_E;
							}
							else if (true == UserInputSig_St.Economy_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_ECO_MODE_EVENT_E;
							}
							else if (true == UserInputSig_St.Neutral_Mode_Sel_u8)
							{
								DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
							}
							else
							{
								/* code */
							}
						}
						else
						{
							/* code */
						}
					}
				}
				else if (true == UserInputSig_St.Reverse_Mode_Sel_u8)
				{
					if (false == UserInputSig_St.Reverse_Mode_u8)
					{
						if ((UserInputSig_St.Sports_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Sports_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Sports_Move_b = true;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_MOVE_SET | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_ON);
						}
						else if ((UserInputSig_St.Neutral_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Neutral_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_MOVE_SET | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_OFF | REVERSE_ON);
						}
						else if ((UserInputSig_St.Economy_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Economy_Move_b))
						{
							DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
							// DriverMode_RunTime_St.Driver_Economy_Move_b = true;
							// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
							// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_MOVE_SET | REVERSE_OFF);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_ON);
						}
						else
						{
							if (DRIVER_MODE_TRANSITION_JOYSTICK_TIME <=
								(GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32))
							{
								// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
								// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
								// DriverMode_RunTime_St.Driver_Reverse_Move_b = false;
								// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
								Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
								Clear_Drive_Mode(REVERSE_MODE_E);
								Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
							}
						}
					}
					else
					{
						// DriverMode_RunTime_St.Driver_Sports_Move_b = false;
						// DriverMode_RunTime_St.Driver_Neutral_Move_b = false;
						// DriverMode_RunTime_St.Driver_Economy_Move_b = false;
						if ((UserInputSig_St.Reverse_Mode_u8 == true) && (false == DriverMode_RunTime_St.Driver_Reverse_Move_b))
						{
							// DriverMode_RunTime_St.Driver_Reverse_Move_b = true;
							Cluster_Driver_Mode_Move(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_MOVE_SET);
							Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
						}
					}
					DriverMode_RunTime_St.DriverMode_State_En =
						DRIVER_REVERSE_MODE_RUN_STATE_E;
					DriverMode_RunTime_St.DriverMode_Event_En =
						DRIVER_MODE_REVERSE_MODE_EVENT_E;
				}
				else
				{
					/* code */
				}
			}
			else if (VEHICLE_START_STATE_E == Vehicle_State_En)
			{
				if (TEN >= Get_Vehicle_Speed())
				{
					Clear_Drive_Mode(NEUTRAL_MODE_E);
				}
				else
				{
					Clear_Drive_Mode(REVERSE_MODE_E);
				}
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
			else
			{
				Clear_Drive_Mode(NEUTRAL_MODE_E);
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
			}
		}
		else
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			DriverMode_RunTime_St.DriverMode_State_En = DRIVER_SAFE_MODE_RUN_STATE_E;
		}

#if (true == IDLE_CHECK_ENABLE)
		if (ONE >= Get_Vehicle_Speed())
		{
			if (false == DriverMode_RunTime_St.Vehicle_Idle_Check_b)
			{
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = true;
				DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else if ((true == DriverMode_RunTime_St.Vehicle_Idle_Check_b) &&
					 (VEHICLE_IDLE_TIME <=
					  (GET_VCU_TIME_MS() - DriverMode_RunTime_St.Timer_u32)))
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
#endif

		break;
	}
	case DRIVER_SAFE_MODE_RUN_STATE_E:
	{
		if ((VEHICLE_START_STATE_E == Vehicle_State_En) ||
			(VEHICLE_RUN_STATE_E == Vehicle_State_En) ||
			(VEHICLE_SAFE_MODE_STATE_E == Vehicle_State_En))
		{
			if (ZERO < Get_SafeMode_Data())
			{
				if ((false == DriverMode_RunTime_St.SafeMode_TurnOn_b) ||
					(ZERO == DriverMode_RunTime_St.SafeMode_data_u32))
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, THREE);
					Set_Ride_mode_Request(SAFE_MODE);
					//if (SAFE_MODE == Get_Actual_Ride_Mode())
					//{
					DriverMode_RunTime_St.SafeMode_TurnOn_b = true;
					//}
					//todo: change the hmi signal to SAFE mode and send signal
					// to motor controller if needed
				}
				else
				{
					/* code */
				}
				DriverMode_RunTime_St.SafeMode_data_u32 = Get_SafeMode_Data();
				if ((false == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
					(ZERO < (Get_SafeMode_Data() &
							 (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
				{
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
					SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, THREE);
					DriverMode_RunTime_St.SafeMode_MC_Set_b = true;
				}
				else if ((true == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
						 (ZERO == (Get_SafeMode_Data() &
								   (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
				{
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ZERO);
					DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
				}
				else
				{
					/* code */
				}

				if ((false == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
					(ZERO < (Get_SafeMode_Data() &
							 (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
							  SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM))))
				{
					SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, THREE);
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
					DriverMode_RunTime_St.SafeMode_BMS_Set_b = true;
				}
				else if ((true == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
						 (ZERO == (Get_SafeMode_Data() &
								   (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
									SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM))))
				{
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
					DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
				}
				else
				{
					/* code */
				}

				if (ZERO != (Get_SafeMode_Data() &
							 (SAFE_MODE_MC_COMM | SAFE_MODE_BMS_COMM)))
				{
					//set vehicle speed to zero
					Set_Motor_Speed_Limit(ZERO);
				}
				else
				{
					/* code */
				}
			}
			else if (ZERO == Get_SafeMode_Data())
			{
				if (true == UserInputSig_St.Economy_Mode_Sel_u8)
				{
					Clear_Drive_Mode(NEUTRAL_MODE_E);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_ECO_MODE_E, ZERO);
					DriverMode_RunTime_St.DriverMode_State_En =
						DRIVER_IDLE_MODE_STATE_E;
					DriverMode_RunTime_St.SafeMode_TurnOn_b = false;
				}
				else
				{
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ONE);
					SET_CLUSTER_DATA(CLUSTER_ECO_MODE_E, ONE);
				}

				if ((true == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
					(ZERO == (Get_SafeMode_Data() &
							  (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
				{
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ZERO);
					DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
				}
				else
				{
					/* code */
				}

				if ((true == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
					(ZERO == (Get_SafeMode_Data() &
							  (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
							   SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM))))
				{
					SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
					DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
				}
				else
				{
					/* code */
				}
				DriverMode_RunTime_St.SafeMode_data_u32 = ZERO;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
		}

		break;
	}
	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Driver_Mode_Set
*   Description   : This function Sets Driver Mode in the HMI(cluster).
*   Parameters    : uint8_t Driver_Mode_Data_u8
*   Return Value  : None
*******************************************************************************/
static void Cluster_Driver_Mode_Set(uint8_t Driver_Mode_Data_u8)
{
	SET_CLUSTER_DATA(CLUSTER_NEUTRAL_MODE_E, ((Driver_Mode_Data_u8 >> 6) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_SPORTS_MODE_E, ((Driver_Mode_Data_u8 >> 4) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_ECO_MODE_E, ((Driver_Mode_Data_u8 >> 2) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_REVERSE_MODE_E, ((Driver_Mode_Data_u8 >> 0) & 0x03U));
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Cluster_Driver_Mode_Move
*   Description   : This function Gets Driver Mode movement (joystic)
*					in the HMI(cluster).
*   Parameters    : uint8_t Driver_Mode_Data_u8
*   Return Value  : None
*******************************************************************************/
static void Cluster_Driver_Mode_Move(uint8_t Driver_Mode_Data_u8)
{
	DriverMode_RunTime_St.Driver_Sports_Move_b = (bool)((Driver_Mode_Data_u8 >> 2) & 1);
	DriverMode_RunTime_St.Driver_Neutral_Move_b = (bool)((Driver_Mode_Data_u8 >> 3) & 1);
	DriverMode_RunTime_St.Driver_Reverse_Move_b = (bool)((Driver_Mode_Data_u8 >> 0) & 1);
	DriverMode_RunTime_St.Driver_Economy_Move_b = (bool)((Driver_Mode_Data_u8 >> 1) & 1);
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverSafe_Mode_State
*   Description   : This function Gets Driver Safe Mode State.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool DriverSafe_Mode_State(void)
{
	return DriverMode_RunTime_St.SafeMode_TurnOn_b;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_DriverMode
*   Description   : This function Gets Driver Mode State operation.
*   Parameters    : None
*   Return Value  : DriverMode_State_En_t
*******************************************************************************/
DriverMode_State_En_t Get_DriverMode(void)
{
	return DriverMode_RunTime_St.DriverMode_State_En;
}
#endif /* DRIVER_MODES_C */

/*---------------------- End of File -----------------------------------------*/