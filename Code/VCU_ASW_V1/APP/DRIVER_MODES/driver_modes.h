/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : driver_modes.h
|    Project        : VCU
|    Module         : driver modes
|    Description    : This file contains the export variables and functions                     
|                     to initialize and Operate the vehicle driver mode 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 15/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef DRIVER_MODES_H
#define DRIVER_MODES_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define SAFE_MODE_MOTOR_TEM             (1 << 0)
#define SAFE_MODE_CONTROLLER_TEM        (1 << 1)
#define SAFE_MODE_MC_COMM               (1 << 2)
//#define SAFE_MODE_HMI_COMM              (1 << 3)
#define SAFE_MODE_BMS_COMM              (1 << 3)
#define SAFE_MODE_MODULE_TEM            (1 << 4)
#define SAFE_MODE_PDU_TEM               (1 << 5)
#define SAFE_MODE_BMS_TEM               (1 << 6)
#define SAFE_MODE_BMS_SOC               (1 << 7)

#define DRIVER_MODE_TRANSITION_TIME     (2000U)
#define VEHICLE_IDLE_TIME               (6000U)
#define DRIVER_MODE_TRANSITION_JOYSTICK_TIME     (5000U) 

#define NEUTRAL_ON                      (3 << 6)
#define NEUTRAL_TOGGLE                  (1 << 6)
#define NEUTRAL_OFF                     (0)
#define SPORTS_ON                       (3 << 4)
#define SPORTS_TOGGLE                   (1 << 4)
#define SPORTS_OFF                      (0)
#define ECO_ON                          (3 << 2)
#define ECO_TOGGLE                      (1 << 2)
#define ECO_OFF                         (0)
#define REVERSE_ON                      (3 << 0)
#define REVERSE_TOGGLE                  (1 << 0)
#define REVERSE_OFF                     (0)


#define NEUTRAL_MOVE_SET                      (1 << 3)
#define SPORTS_MOVE_SET                       (1 << 2)
#define ECO_MOVE_SET                          (1 << 1)
#define REVERSE_MOVE_SET                      (1 << 0)

#define SAFE_MODE_BMS_SOC               (1 << 7)


/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
	DRIVER_IDLE_MODE_STATE_E,
	DRIVER_NEUTRAL_MODE_RUN_STATE_E,
	DRIVER_SPORT_MODE_RUN_STATE_E,
	DRIVER_ECO_MODE_RUN_STATE_E,
	DRIVER_REVERSE_MODE_RUN_STATE_E,
	DRIVER_SAFE_MODE_RUN_STATE_E,
} DriverMode_State_En_t;
typedef enum
{
	DRIVER_MODE_IDLE_MODE_EVENT_E,
	//DRIVER_MODE_EVENT_START_E,
	DRIVER_MODE_NEUTRAL_MODE_EVENT_E,// = DRIVER_MODE_EVENT_START_E,
	DRIVER_MODE_SPORT_MODE_EVENT_E,
	DRIVER_MODE_ECO_MODE_EVENT_E,
	DRIVER_MODE_REVERSE_MODE_EVENT_E,
	DRIVER_MODE_SAFE_MODE_EVENT_E,
	//DRIVER_MODE_EVENT_END_E,
} DriverMode_Event_En_t;

typedef struct
{
	bool SafeMode_TurnOn_b;
	bool SafeMode_BMS_Set_b;
	bool SafeMode_MC_Set_b;
	uint32_t Timer_u32;
	uint32_t SafeMode_data_u32;
	bool Vehicle_Idle_Check_b;
	bool Driver_Economy_Move_b;
	bool Driver_Sports_Move_b;
	bool Driver_Reverse_Move_b;
	bool Driver_Neutral_Move_b;
	DriverMode_State_En_t DriverMode_State_En;
	DriverMode_Event_En_t DriverMode_Event_En;
} DriverMode_RunTime_St_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Init
*   Description   : This function implements Driver Mode initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void DriverMode_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Proc
*   Description   : This function implements Driver Mode Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void DriverMode_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverSafe_Mode_State
*   Description   : This function Gets Driver Safe Mode State.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
extern bool DriverSafe_Mode_State(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_DriverMode
*   Description   : This function Gets Driver Mode State operation.
*   Parameters    : None
*   Return Value  : DriverMode_State_En_t
*******************************************************************************/
extern DriverMode_State_En_t Get_DriverMode(void);

#endif /* DRIVER_MODES_H */
/*---------------------- End of File -----------------------------------------*/