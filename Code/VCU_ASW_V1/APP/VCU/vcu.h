/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : vcu.h
|    Project        : VCU
|    Module         : vcu main 
|    Description    : This file contains the export variables and functions 			
|                     to initialize and Operate the vcu functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 13/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef VCU_H
#define VCU_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
 #define VCU_SAFE_MODE_TIMEOUT     (5000U)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
	VCU_INIT_STATE_E,
	VCU_START_STATE_E,
	VCU_RUN_STATE_E,
	VCU_SHUT_DOWN_STATE_E,
	VCU_SAFE_MODE_STATE_E,
	VCU_STANDBY_STATE_E,
} VCU_State_En_t;
typedef enum
{
	VCU_ON_E = 0,
	VCU_OFF_E,
} VCU_Ancillary_Supply_En_t;
typedef enum
{
	VCU_EVENT_START_E,
	VCU_KEY_SWITCH_EVENT_E = VCU_EVENT_START_E,
	VCU_KILL_SWITCH_EVENT_E,
	VCU_KICK_STAND_SWITCH_EVENT_E,
	VCU_SAFE_MODE_EVENT_E, //added lately
	VCU_VEHICLE_FALL_EVENT_E,
	VCU_VEHICLE_CHARGING_EVENT_E,
	VCU_EVENT_END_E,
} VCU_Event_En_t;
typedef enum
{
	VCU_HANDSHAKE_IDLE_E,
	VCU_HANDSHAKE_START_E,
	VCU_HANDSHAKE_RESPONSE_E,
	VCU_HANDSHAKE_COMPLETE_E,
} VCU_HandShake_En_t;
typedef enum
{
	VCU_HANDSHAKE_IDLE_EVENT_E,
	VCU_HANDSHAKE_REQUESTED_EVENT_E,
	VCU_HANDSHAKE_COMPLETED_EVENT_E,
} VCU_HandShakeEvent_En_t;

typedef struct
{

	bool Vehicle_Charging_b;
	bool Vehicle_KickStand_b;
	bool HandShakeCpmplete_b;
	bool VCU_SafeMode_Check_b;
	uint32_t       Timer_u32;
	VCU_State_En_t VCU_State_En;
	VCU_Event_En_t VCU_Event_En;
	VCU_HandShake_En_t VCU_HandShake_En;
	VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En;
} VCU_State_St_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Init
*   Description   : This function implements VCU initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void VCU_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Proc
*   Description   : This function implements VCU Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void VCU_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetState
*   Description   : This function implements VCU State setting.
*   Parameters    : VCU_State_En_t VCU_State_En
*   Return Value  : None
*******************************************************************************/
extern void VCU_SetState(VCU_State_En_t VCU_State_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetState
*   Description   : This function implements get VCU State.
*   Parameters    : None
*   Return Value  : VCU_State_En_t
*******************************************************************************/
extern VCU_State_En_t VCU_GetState(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetHandShakeEvent
*   Description   : This function implements VCU handshake event Setting.
*   Parameters    : VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
extern void VCU_SetHandShakeEvent(VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetHandShakeEvent
*   Description   : This function implements get VCU handshake event.
*   Parameters    : VCU_HandShakeEvent_En_t* VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
extern void VCU_GetHandShakeEvent(VCU_HandShakeEvent_En_t *VCU_HandShakeEvent_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Ancillary_Supply
*   Description   : This function Provides Ancillary System Supply.
*   Parameters    : VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En
*   Return Value  : None
*******************************************************************************/
extern void VCU_Ancillary_Supply(VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En);

#endif /* VCU_H */
/*---------------------- End of File -----------------------------------------*/