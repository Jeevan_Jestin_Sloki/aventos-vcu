/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : vcu.c
|    Project        : VCU
|    Module         : vcu main 
|    Description    : This file contains the variables and functions 			
|                     to initialize and Operate the vcu functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 13/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef VCU_C
#define VCU_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "vcu.h"
#include "DataBank.h"
#include "driver_modes.h"
#include "switch_signals.h"
#include "motor_control.h"
#include "Communicator.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static VCU_State_St_t VCU_State_St;
static VCU_State_En_t VCU_SetState_En;
//static VCU_Event_En_t VCU_SetEvent_En;
static VCU_HandShakeEvent_En_t VCU_SetHandShakeEvent_En;

static VCU_Ancillary_Supply_En_t VCU_Set_Ancillary_Supply_En;
uint8_t HandShackRequest_au8[0x8U] = {0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80};

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VCU_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_HandShake
*   Description   : This function implements VCU handshake event with Master 
*		    ECU.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static VCU_HandShake_En_t VCU_HandShake(void);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Init
*   Description   : This function implements VCU initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VCU_Init(void)
{

	VCU_State_St.VCU_State_En = VCU_STANDBY_STATE_E;
	VCU_SetState_En = VCU_STANDBY_STATE_E;
	VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_IDLE_E;
	VCU_State_St.VCU_HandShakeEvent_En = VCU_HANDSHAKE_IDLE_EVENT_E;
	VCU_State_St.HandShakeCpmplete_b = false;
	VCU_State_St.Vehicle_KickStand_b = false;
	VCU_State_St.Vehicle_Charging_b = false;
	SET_VCU_TIME_MS(0);
	VCU_Set_Ancillary_Supply_En = VCU_OFF_E;
	VCU_State_St.VCU_SafeMode_Check_b = false;
	VCU_SetHandShakeEvent(VCU_HANDSHAKE_IDLE_EVENT_E);
	DriverMode_Init();
	SwitchSignals_Init();
	Cluster_Init();
	BMS_Init();
	MotorControl_Init();
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Proc
*   Description   : This function implements VCU Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VCU_Proc(void)
{
	VCU_StateMachine_proc();
	DriverMode_Proc();
	SwitchSignals_Proc();
	Cluster_Proc();
	BMS_Proc();
	MotorControl_Proc();
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VCU_StateMachine_proc(void)
{

	switch (VCU_State_St.VCU_State_En)
	{
	case VCU_INIT_STATE_E:
	{
		VCU_SetState_En = VCU_INIT_STATE_E;
		VCU_Init();
		VCU_SetState_En = VCU_INIT_STATE_E;
		VCU_State_St.VCU_State_En = VCU_START_STATE_E;
		VCU_Ancillary_Supply(VCU_ON_E);
		VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_START_E;
		VCU_SetHandShakeEvent(VCU_HANDSHAKE_IDLE_EVENT_E);
		break;
	}
	case VCU_START_STATE_E:
	{
		if (TURNED_OFF_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_SHUT_DOWN_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else if ((true == Get_BatteryCharging_State()) ||
				 (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8) ||
				 (TURNED_ON_E == UserInputSig_St.Fall_SwitchState_u8) ||
				 (true == Get_SafeMode_State()))
		{
			if ((true == Get_BatteryCharging_State()) &&
				(false == VCU_State_St.Vehicle_Charging_b))
			{
				VCU_State_St.Vehicle_Charging_b = true;
				VCU_State_St.VCU_Event_En = VCU_VEHICLE_CHARGING_EVENT_E;
			}
			else
			{
				/* code */
			}

			if ((false == VCU_State_St.Vehicle_KickStand_b) &&
				(TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8))
			{
				VCU_State_St.Vehicle_KickStand_b = true;
				SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ONE);
				VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
			}
			else if ((true == VCU_State_St.Vehicle_KickStand_b) &&
					 (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8))
			{
				VCU_State_St.Vehicle_KickStand_b = false;
				SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ZERO);
				VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
			}
			else
			{
				/* code */
			}

			if (TURNED_ON_E == UserInputSig_St.Fall_SwitchState_u8)
			{
				VCU_State_St.VCU_Event_En = VCU_VEHICLE_FALL_EVENT_E;
			}
			else
			{
				/* code */
			}

			if (true == Get_SafeMode_State())
			{
				VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
				VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
			}
			else
			{
				/* code */
			}
		}
		else
		{
			if ((true == VCU_State_St.Vehicle_KickStand_b) &&
				(TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8))
			{
				VCU_State_St.Vehicle_KickStand_b = false;
				SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ZERO);
				VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
			}
			else
			{
				if ((true == VCU_State_St.HandShakeCpmplete_b) &&
					(VCU_HANDSHAKE_COMPLETE_E == VCU_HandShake()))
				{
					if (TURNED_OFF_E == UserInputSig_St.Kill_SwitchState_u8)
					{
						if ((true == GET_BMS_COMM_STATUS()) &&
							(true == GET_MC_COMM_STATUS()) &&
							(true == GET_HMI_COMM_STATUS()))
						{
							VCU_State_St.VCU_State_En = VCU_RUN_STATE_E;
							VCU_State_St.VCU_Event_En = VCU_KILL_SWITCH_EVENT_E;
						}
						else
						{
							/* code */
						}
					}
					else
					{
						/* code */
					}
				}
				else
				{
					VCU_State_St.HandShakeCpmplete_b = true;
				}
			}
			
		}
		
		VCU_SetState_En = VCU_START_STATE_E;
		break;
	}
	case VCU_RUN_STATE_E:
	{
		if (TURNED_OFF_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_SHUT_DOWN_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else if (TURNED_ON_E == UserInputSig_St.Kill_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_START_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KILL_SWITCH_EVENT_E;
		}
		else
		{
			/*need to check the communication of the BMS, MOTOR CONTROL and HMI*/
			if ((true == GET_BMS_COMM_STATUS()) &&
				(true == GET_MC_COMM_STATUS()) &&
				(true == GET_HMI_COMM_STATUS()) &&
				(false == Get_SafeMode_State()))
			{
				if ((TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8) ||
					(TURNED_ON_E == UserInputSig_St.Fall_SwitchState_u8))
				{
					if (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8)
					{
						VCU_State_St.Vehicle_KickStand_b = true;
						SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ONE);
						VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
					}
					else
					{
						/* code */
					}

					if (TURNED_ON_E == UserInputSig_St.Fall_SwitchState_u8)
					{
						VCU_State_St.VCU_Event_En = VCU_VEHICLE_FALL_EVENT_E;
					}
					else
					{
						/* code */
					}
					VCU_State_St.VCU_State_En = VCU_START_STATE_E;
				}
				else
				{
					/* code */
				}
			}
			else
			{
				if (false == VCU_State_St.VCU_SafeMode_Check_b)
				{
					VCU_State_St.VCU_SafeMode_Check_b = true;
					VCU_State_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else if (VCU_SAFE_MODE_TIMEOUT <=
						 (GET_VCU_TIME_MS() - VCU_State_St.Timer_u32))
				{
					VCU_State_St.VCU_SafeMode_Check_b = false;
					VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
					VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
				}
			}
		}

		VCU_SetState_En = VCU_RUN_STATE_E;
		break;
	}
	case VCU_SHUT_DOWN_STATE_E:
	{
		VCU_State_St.VCU_State_En = VCU_STANDBY_STATE_E;
		VCU_SetState_En = VCU_SHUT_DOWN_STATE_E;
		VCU_Ancillary_Supply(VCU_OFF_E);
		break;
	}
	case VCU_SAFE_MODE_STATE_E:
	{
		if (TURNED_OFF_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_SHUT_DOWN_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else if (false == Get_SafeMode_State())
		{
			VCU_State_St.VCU_State_En = VCU_START_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
		}
		else
		{
			VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
		}
		if (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8)
		{
			VCU_State_St.Vehicle_KickStand_b = true;
			SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ONE);
			VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
		}
		else if ((true == VCU_State_St.Vehicle_KickStand_b) &&
				 (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8))
		{
			VCU_State_St.Vehicle_KickStand_b = false;
			SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ZERO);
			VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
		}
		else
		{
			/* code */
		}
		
		VCU_SetState_En = VCU_SAFE_MODE_STATE_E;
		break;
	}
	case VCU_STANDBY_STATE_E:
	{
		if (TURNED_ON_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_INIT_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else
		{
			/* code */
		}
		VCU_SetState_En = VCU_STANDBY_STATE_E;
		/* code */
		break;
	}

	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetState
*   Description   : This function implements VCU State setting.
*   Parameters    : VCU_State_En_t VCU_State_En
*   Return Value  : None
*******************************************************************************/
void VCU_SetState(VCU_State_En_t VCU_State_En)
{
	VCU_SetState_En = VCU_State_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetState
*   Description   : This function implements get VCU State.
*   Parameters    : None
*   Return Value  : VCU_State_En_t
*******************************************************************************/
VCU_State_En_t VCU_GetState(void)
{
	return VCU_SetState_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_HandShake
*   Description   : This function implements VCU handshake event with Master 
*		    ECU.
*   Parameters    : NoneS
*   Return Value  : VCU_HandShake_En_t
*******************************************************************************/
VCU_HandShake_En_t VCU_HandShake(void)
{
	VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En = VCU_HANDSHAKE_IDLE_EVENT_E;
	VCU_HandShake_En_t Return_HandShake_En = VCU_HANDSHAKE_IDLE_E;
	VCU_GetHandShakeEvent(&VCU_HandShakeEvent_En);
	switch (VCU_State_St.VCU_HandShake_En)
	{
		case VCU_HANDSHAKE_IDLE_E:
		{
			break;
		}
		case VCU_HANDSHAKE_START_E:
		{
			if (VCU_HANDSHAKE_IDLE_EVENT_E == VCU_HandShakeEvent_En)
			{
				//R_Config_UART0_Send(&HandShackRequest_au8[0], 1U);
				VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_RESPONSE_E;
				Return_HandShake_En = VCU_HANDSHAKE_RESPONSE_E;
			}
			else
			{
				/* code */
			}

			break;
		}
		case VCU_HANDSHAKE_RESPONSE_E:
		{
			if (VCU_HANDSHAKE_REQUESTED_EVENT_E == VCU_HandShakeEvent_En)
			{
				//R_Config_UART0_Send(&HandShackRequest_au8[0], 1U);
				VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;
				Return_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;
				VCU_State_St.HandShakeCpmplete_b = true;
			}
			else
			{
				/* code */
			}
			break;
		}
		case VCU_HANDSHAKE_COMPLETE_E:
		{ /* code */
			break;
		}

		default:
		{
			break;
		}
	}
	Return_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;	/*todo-sandeep*/
	
	return Return_HandShake_En; 
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetHandShakeEvent
*   Description   : This function implements VCU handshake event Setting.
*   Parameters    : VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
void VCU_SetHandShakeEvent(VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En)
{
	VCU_SetHandShakeEvent_En = VCU_HandShakeEvent_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetHandShakeEvent
*   Description   : This function implements get VCU handshake event.
*   Parameters    : VCU_HandShakeEvent_En_t* VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
void VCU_GetHandShakeEvent(VCU_HandShakeEvent_En_t *VCU_HandShakeEvent_En)
{
	*VCU_HandShakeEvent_En = VCU_SetHandShakeEvent_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Ancillary_Supply
*   Description   : This function Provides Ancillary System Supply.
*   Parameters    : VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En
*   Return Value  : None
*******************************************************************************/
void VCU_Ancillary_Supply(VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En)
{
	if ((VCU_OFF_E == VCU_Set_Ancillary_Supply_En) && (VCU_ON_E == VCU_Ancillary_Supply_En))
	{
		PORT.P9 &= 0xFFFB;
		if (ZERO == PORT.P9 | 0x04)
		{
			VCU_Set_Ancillary_Supply_En = VCU_Ancillary_Supply_En;
		}
	}
	else if ((VCU_ON_E == VCU_Set_Ancillary_Supply_En) && (VCU_OFF_E == VCU_Ancillary_Supply_En))
	{
		PORT.P9 |= 0x04;
		if (ONE == PORT.P9 | 0x04)
		{
			VCU_Set_Ancillary_Supply_En = VCU_Ancillary_Supply_En;
		}
	}
	else
	{
		/* code */
	}
}

#endif /* VCU_C */
/*---------------------- End of File -----------------------------------------*/