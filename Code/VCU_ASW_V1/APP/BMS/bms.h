/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms.h
|    Project        : VCU
|    Module         : bms module
|    Description    : This file contains the export variables and functions
|                     to initialize and Operate the bms functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 20/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_H
#define BMS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "can_if.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define BMS_COMM_LOST_TIMEOUT (2000U)
#define BMS_COMM_RESTORE_TIME (2000U)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
    BMS_INIT_STATE_E,
    BMS_START_STATE_E,
    BMS_RUN_STATE_E,
    BMS_SHUT_DOWN_STATE_E,
    BMS_SAFE_MODE_STATE_E, // added
    BMS_STANDBY_STATE_E,
} Bms_State_En_t;
typedef enum
{
    BMS_INIT_EVENT_E,
    BMS_START_EVENT_E,
    BMS_RUN_EVENT_E,
    BMS_SHUT_DOWN_EVENT_E,
    BMS_SAFE_MODE_EVENT_E,
    BMS_STOP_EVENT_E,
} Bms_Event_En_t;
typedef struct
{
    bool Bms_Start_b;
    bool BMS_InitCommCheck_b;
    bool BMS_LateCommCheck_b;
    bool BMS_SafeMode_Module_temp_b;
    bool BMS_SafeMode_PDU_temp_b;
    bool BMS_SafeMode_BMS_temp_b;
    bool BMS_Charging_b;
    uint32_t Timer_u32;
    Bms_Event_En_t Bms_Event_En;
    Bms_State_En_t Bms_State_En;
} Bms_RunTime_St_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
extern bool CalculateRangeMileage_b;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Init
*   Description   : This function implements BNS initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Proc
*   Description   : This function implements BMS Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x2AA_RxMsgCallback(uint16_t CIL_SigName_En,
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En,
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x4AA_RxMsgCallback(uint16_t CIL_SigName_En,
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x4AB_RxMsgCallback(uint16_t CIL_SigName_En,
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x3AA_RxMsgCallback(uint16_t CIL_SigName_En,
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x2AA_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x4AA_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x4AB_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void BMS_0x3AA_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Charging_state
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
extern bool BMS_Get_Charging_state(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BMS_SOC
*   Description   : This function will Returns the Present Battery SOC.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern uint8_t Get_BMS_SOC(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Mileage_Wh_Km
*   Description   : This function returns the Estimated Mileage(Wh/Km)
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
extern uint8_t BMS_Get_Mileage_Wh_Km(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Range_Km
*   Description   : This function returns the Estimated Range(Km)
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
extern uint16_t BMS_Get_Range_Km(void);

#endif /* BMS_H */
/*---------------------- End of File -----------------------------------------*/
