/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms.c
|    Project        : VCU
|    Module         : bms module 
|    Description    : This file contains the variables and functions                    
|                     to initialize and Operate the bms functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 20/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_C
#define BMS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "bms.h"
#include "bms_can_signals.h"
#include "Communicator.h"
#include "cil_can_conf.h"
#include "RangeMileage.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/
uint32_t SOC_u32 = 0;
uint32_t SOH_u32 = 0;
uint32_t Power_u32 = 0;
uint32_t Voltage_u32 = 0;
uint32_t Temperature_u32 = 0;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static Bms_RunTime_St_t Bms_RunTime_St;
static BMS_Rx_0x2AA_t BMS_Rx_0x2AA;
static BMS_Rx_0x4AA_t BMS_Rx_0x4AA;
static BMS_Rx_0x4AB_t BMS_Rx_0x4AB;
static BMS_Rx_0x3AA_t BMS_Rx_0x3AA;

static float BMS_BatteryVoltage = 0;
static float AvailableCapacity = 0;

bool CalculateRangeMileage_b = false;
// static EstimatedRangeMileage_St_t EstimatedRangeMileage_St;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void BMS_StateMachine_Proc(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Init
*   Description   : This function implements BMS initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Init(void)
{
	Bms_RunTime_St.Bms_State_En = BMS_STANDBY_STATE_E;
	Bms_RunTime_St.Bms_Event_En = BMS_STOP_EVENT_E;
	Bms_RunTime_St.BMS_InitCommCheck_b = false;
	Bms_RunTime_St.BMS_LateCommCheck_b = false;
	Bms_RunTime_St.BMS_Charging_b = false;
	Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = false;
	Bms_RunTime_St.Timer_u32 = 0;
	Bms_RunTime_St.Bms_Start_b = false;
	SOC_u32 = ZERO;
	SOH_u32 = ZERO;
	Power_u32 = ZERO;
	Voltage_u32 = ZERO;
	Temperature_u32 = ZERO;
	CLEAR_BMS_COMM_STATUS();

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Proc
*   Description   : This function implements BMS Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Proc(void)
{
	BMS_StateMachine_Proc();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_StateMachine_Proc(void)
{
	// real_t AvailableCapacity = 0;
	// real_t BatteryVoltage = 0;
	Bms_State_En_t Bms_State_En = (Bms_State_En_t)GET_VCU_STATE();
	Bms_RunTime_St.Bms_State_En = Bms_State_En;

	switch (Bms_RunTime_St.Bms_State_En)
	{
	case BMS_INIT_STATE_E:
	{
		Bms_RunTime_St.BMS_InitCommCheck_b = true;
		Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		Bms_RunTime_St.Bms_State_En = BMS_START_STATE_E;
		Bms_RunTime_St.Bms_Start_b = true;
		Restore_Wh_Km_from_FEE();
		Compute_Wh_On_VehicleStart(AvailableCapacity, BMS_BatteryVoltage);
		//Compute_Wh_On_VehicleStart(500, 60);
		break;
	}
	case BMS_START_STATE_E:
	{

		if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
				SET_BMS_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			}
			else if (false == GET_BMS_COMM_STATUS() &&
					 (BMS_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}
		else if (true == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
			}
		}

		if((false == Bms_RunTime_St.BMS_Charging_b) && (FOUR == BMS_Rx_0x3AA.BatteryState_u8))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ONE);
			Bms_RunTime_St.BMS_Charging_b = true;
		}
		else if((true == Bms_RunTime_St.BMS_Charging_b) && (ZERO == BMS_Rx_0x3AA.BatteryState_u8))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ZERO);
			Bms_RunTime_St.BMS_Charging_b = false;
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (BMS_Rx_0x4AA.HighModuleTemperature_u8 >= 55)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_Rx_0x4AB.BMS_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (BMS_Rx_0x4AB.PDU_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}	

		break;
	}
	case BMS_RUN_STATE_E:
	{
		if (false == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			if ((false == Bms_RunTime_St.BMS_InitCommCheck_b) &&
				(false == GET_BMS_COMM_STATUS()))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}

			if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
			{
				if (true == GET_BMS_COMM_STATUS())
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
				}
				else if (false == GET_BMS_COMM_STATUS() &&
						 (BMS_COMM_LOST_TIMEOUT <=
						  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
					Bms_RunTime_St.BMS_LateCommCheck_b = true;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
					// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
					// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
					Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else if(false == GET_BMS_COMM_STATUS())
				{
					NOP();
				}
			}
			else
			{
				/* code */
			}
		}
		else if (false == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (BMS_Rx_0x4AA.HighModuleTemperature_u8 >= 55)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_Rx_0x4AB.BMS_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (BMS_Rx_0x4AB.PDU_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}	

		if(true == CalculateRangeMileage_b)
		{
			CalculateRangeMileage_b = false;

			Estimate_Mileage_Range(AvailableCapacity, BMS_BatteryVoltage);
			//Estimate_Mileage_Range(495, 58);
		}
		else
		{
			/* code */
		}
		

		break;
	}
	case BMS_SHUT_DOWN_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		Store_Wh_Km_To_FEE();
		break;
	}
	case BMS_SAFE_MODE_STATE_E:
	{
		if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
				SET_BMS_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			}
			else if (false == GET_BMS_COMM_STATUS() &&
					 (BMS_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}
		else if (true == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				if (BMS_COMM_RESTORE_TIME <=
					(GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32))
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
					Bms_RunTime_St.BMS_LateCommCheck_b = false;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
					// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
					SET_BMS_COMM_STATUS();
				}
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				//Bms_RunTime_St.BMS_LateCommCheck_b = true;
				//Bms_RunTime_St.BMS_InitCommCheck_b = true;
			}
		}
		else
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
			}
		}

		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (BMS_Rx_0x4AA.HighModuleTemperature_u8 >= 55)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true;
				NOP();
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (BMS_Rx_0x4AA.HighModuleTemperature_u8 < 50)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;
				NOP();
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_Rx_0x4AB.BMS_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;
				NOP();
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_Rx_0x4AB.BMS_Temperature_u8 < 65)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = false;
				NOP();
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (BMS_Rx_0x4AB.PDU_Temperature_u8 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;
				NOP();
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (BMS_Rx_0x4AB.PDU_Temperature_u8 < 65)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = false;
				NOP();
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
		if ((false == Bms_RunTime_St.BMS_Charging_b) && (FOUR == BMS_Rx_0x3AA.BatteryState_u8))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ONE);
			Bms_RunTime_St.BMS_Charging_b = true;
		}
		else if ((true == Bms_RunTime_St.BMS_Charging_b) && (ZERO == BMS_Rx_0x3AA.BatteryState_u8))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ZERO);
			Bms_RunTime_St.BMS_Charging_b = false;
		}
		else
		{
			/* code */
		}
		break;
	}
	case BMS_STANDBY_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		CLEAR_BMS_COMM_STATUS();
		break;
	}

	default:
	{
		break;
	}
	}

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x2AA_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_STATUS();
			Deserialize_BMS_Rx_0x2AA(&BMS_Rx_0x2AA, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AA_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_STATUS();
			Deserialize_BMS_Rx_0x4AA(&BMS_Rx_0x4AA, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AB_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_STATUS();
			Deserialize_BMS_Rx_0x4AB(&BMS_Rx_0x4AB, &Can_Applidata_St->DataBytes_au8[ZERO]);
			AvailableCapacity = (BMS_Rx_0x4AB.AvailableCapacity_u32 * BMS_RX_0X4AB_AVAILABLECAPACITY_U32FACTOR);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x3AA_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_STATUS();
			Deserialize_BMS_Rx_0x3AA(&BMS_Rx_0x3AA, &Can_Applidata_St->DataBytes_au8[ZERO]);
			
			BMS_BatteryVoltage = (BMS_Rx_0x3AA.BatteryVoltage_u16 * BMS_RX_0X3AA_BATTERYVOLTAGE_U16FACTOR);
		}
	}

}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BMS_SOC
*   Description   : This function will Returns the Present Battery SOC.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t Get_BMS_SOC(void)
{
	return BMS_Rx_0x2AA.SoC_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Charging_state
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool BMS_Get_Charging_state(void)
{
	return Bms_RunTime_St.BMS_Charging_b;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Mileage_Wh_Km
*   Description   : This function returns the Estimated Mileage(Wh/Km)
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t BMS_Get_Mileage_Wh_Km(void)
{
	return EstimatedRangeMileage_St.Esti_Mileage_Wh_Km_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Range_Km
*   Description   : This function returns the Estimated Range(Km)
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t BMS_Get_Range_Km(void)
{
	return EstimatedRangeMileage_St.Esti_Range_Km_u16;
}
#endif /* BMS_C */
/*---------------------- End of File -----------------------------------------*/
