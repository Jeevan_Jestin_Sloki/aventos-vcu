/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : motor_control.c
|    Project        : VCU
|    Module         : motor control
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the motor control functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 29/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MOTOR_CONTROL_C
#define MOTOR_CONTROL_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "motor_control.h"
#include "Communicator.h"
#include "cil_can_conf.h"
#include "temp.h"
#include "powerConsum.h"
#include "LPF.h"
#include "DataBank.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  MOTOR CONTROL RX SET GET DATA 
*  ---------------------------------------------------------------------------*/


#define Set_Batt_Volt(x) (Motor_0x200_SetRx.Battery_Voltage_u8 = x)
#define Set_Reserved_0x200_LTVS_u64(x) (Motor_0x200_SetRx.Reserved_for_LTVS_u64 = x)
#define Set_Throttle_out(x) (Motor_0x200_SetRx.Throttle_out_u8 = x)
#define Set_Throttle_ref(x) (Motor_0x200_SetRx.Throttle_Reference_u8 = x)

#define Get_Batt_Volt() (Motor_0x200_SetRx.Battery_Voltage_u8)
#define Get_Reserved_0x200_LTVS_u64() (Motor_0x200_SetRx.Reserved_for_LTVS_u64)
#define Get_Throttle_out() (Motor_0x200_SetRx.Throttle_out_u8)
#define Get_Throttle_ref() (Motor_0x200_SetRx.Throttle_Reference_u8)

#define Set_Controller_Temp(x) (Motor_0x250_SetRx.Controller_temperature_s16 = x)
#define Set_Motor_Rotation_Num(x) (Motor_0x250_SetRx.Motor_Rotation_Number_u32 = x)
#define Set_Motor_Temp(x) (Motor_0x250_SetRx.Motor_temperature_s16 = x)

#define Get_Controller_Temp(x) (Motor_0x250_SetRx.Controller_temperature_s16)
#define Get_Motor_Rotation_Num(x) (Motor_0x250_SetRx.Motor_Rotation_Number_u32)
#define Get_Motor_Temp(x) (Motor_0x250_SetRx.Motor_temperature_s16)

#define Set_Reserved_0x650_LTVS_u64(x) (Motor_0x650_SetRx.Reserved_for_LTVS_u64 = x)

#define Get_Reserved_0x650_LTVS_u64() (Motor_0x650_SetRx.Reserved_for_LTVS_u64)

#define Set_Hardware_Ver(x) (Motor_0x750_SetRx.Hardware_version_u8 = x)
#define Set_Reserved_0x750_LTVS_u64(x) (Motor_0x750_SetRx.Reserved_for_LTVS_u64 = x)
#define Set_Software_Ver(x) (Motor_0x750_SetRx.Software_version_u8 = x)

#define Get_Hardware_Ver() (Motor_0x750_SetRx.Hardware_version_u8)
#define Get_Reserved_0x750_LTVS_u64() (Motor_0x750_SetRx.Reserved_for_LTVS_u64)
#define Get_Software_Ver() (Motor_0x750_SetRx.Software_version_u8)
/******************************************************************************/

/* -----------------------------------------------------------------------------
*  MOTOR CONTROL TX SET GET DATA 
*  ---------------------------------------------------------------------------*/


#define Set_Reserved_for_0x300_LTVS_u64(x) (Vehicle_0x300_Tx.Reserved_for_LTVS_u64 = x)

#define Get_Reserved_for_0x300_LTVS_u64() (Vehicle_0x300_Tx.Reserved_for_LTVS_u64)

#define Set_Broadcast_Mode(x) (Vehicle_0x400_Tx.Broadcast_Mode_u8 = x)
#define Set_Broadcast_rate(x) (Vehicle_0x400_Tx.Broadcast_rate_u8 = x)

#define Get_Broadcast_Mode() (Vehicle_0x400_Tx.Broadcast_Mode_u8)
#define Get_Broadcast_rate() (Vehicle_0x400_Tx.Broadcast_rate_u8)


#define LPF_POS_CONST   1000
#define LPF_NEG_CONST   1000
/******************************************************************************/

// #define NEUTRAL 0U

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static MotorControl_RunTime_St_t MotorControl_RunTime_St;

static Vehicle_0x100_Tx_t Vehicle_0x100_Tx;
static Vehicle_0x300_Tx_t Vehicle_0x300_Tx;
static Vehicle_0x400_Tx_t Vehicle_0x400_Tx;
static Motor_0x150_Rx_t Motor_0x150_Rx;
static Motor_0x200_Rx_t Motor_0x200_Rx;
static Motor_0x250_Rx_t Motor_0x250_Rx;
static Motor_0x650_Rx_t Motor_0x650_Rx;
static Motor_0x750_Rx_t Motor_0x750_Rx;

static bool Send_Once_b = false;
static float MCU_BatteryCurrent = 0;
static float MCU_BatteryVoltage = 0;
static float MCU_Motor_Speed_RPM = 0;

static uint16_t Vehicle_Speed_Kmph_Cal = 0;
static uint32_t Counter = 0;
static uint16_t Speed_to_display = 0;
static uint32_t ODO_in_meters = 0;
static uint16_t Power_Consum_Bars_u16 = 0;

static uint8_t Vehicle_Throttle_u8 = 0;
static uint16_t Filter_Throttle_Out_u16 = 0;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_StateMachine_proc
*   Description   : This function implements MotorControl State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void MotorControl_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Start_Signal
*   Description   : This function implements MotorControl Start Signal 
*		    and stop operation.
*   Parameters    : MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En
*   Return Value  : None
*******************************************************************************/
static void MotorControl_Start_Signal(MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_Start_Signal
*   Description   : This function implements get Start Signal 
*		    and stop data.
*   Parameters    : None
*   Return Value  : MotorControl_Kill_Signal_En_t
*******************************************************************************/
static MotorControl_Kill_Signal_En_t MotorControl_Get_Start_Signal(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Init
*   Description   : This function implements MotorControl initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_Init(void)
{
	MotorControl_RunTime_St.MotorControl_State_En = MOTOR_CONTROL_STANDBY_STATE_E;
	MotorControl_RunTime_St.MotorControl_Event_En = MOTOR_CONTROL_STANDBY_EVENT_E;
	MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
	MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
	MotorControl_RunTime_St.MotorControl_Start_b = false;
	MotorControl_RunTime_St.Regen_state_b = false;
	Set_Broadcast_rate(ZERO);
	Send_Once_b = false;
	MCU_BatteryCurrent = 0;
	MCU_BatteryVoltage = 0;
	MCU_Motor_Speed_RPM = 0;
	MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
	CLEAR_MC_COMM_STATUS();
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Proc
*   Description   : This function implements MotorControl Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_Proc(void)
{
	Counter++;
	MotorControl_StateMachine_proc();
	if ((MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_START_STATE_E) ||
		(MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_RUN_STATE_E) ||
		(MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_SAFE_MODE_STATE_E))
	{
		if (0 == (Counter % 10))
		{
			static float PrevMotorRPM_f32 = 0;

			if(PrevMotorRPM_f32 != MCU_Motor_Speed_RPM)
			{
				Vehicle_Speed_Kmph_Cal = CalculateVehicleSpeed(MCU_Motor_Speed_RPM);
				PrevMotorRPM_f32 = MCU_Motor_Speed_RPM;
			}
			
			Speed_to_display = VehicleSpeedFilter(Vehicle_Speed_Kmph_Cal);
			Vehicle_Throttle_u8 = (uint8_t)U16_LPF_U16((uint16_t)UserInputSig_St.Throttle_u8,&Filter_Throttle_Out_u16,LPF_POS_CONST,LPF_NEG_CONST);
		}
	}
	else if (MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_SHUT_DOWN_STATE_E)
	{
		ResetTheSpeedFilterData();
	}
	else
	{
		/* code */
	}

	
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_StateMachine_proc
*   Description   : This function implements MotorControl State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_StateMachine_proc(void)
{
	MotorControl_State_En_t MotorControl_State_En = (MotorControl_State_En_t)GET_VCU_STATE();

	MotorControl_RunTime_St.MotorControl_State_En = MotorControl_State_En;
	switch (MotorControl_RunTime_St.MotorControl_State_En)
	{
	case MOTOR_CONTROL_INIT_STATE_E:
	{
		MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
		MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		MotorControl_RunTime_St.MotorControl_Start_b = true;
		Vehicle_0x100_Tx.Motor_Speed_Limit_s16 = 7000;
		// Set_Motor_Speed_Limit(7000);
		Vehicle_0x100_Tx.Max_Regeneration_u8 = 100;
		// Set_Max_Regeneration(100);
		Vehicle_0x100_Tx.Ride_mode_Request_u8 = NEUTRAL;
		Set_Broadcast_rate(50);
		Set_Broadcast_Mode(1);
		Send_Once_b = false;
		Restore_ODO_From_FEE();
		break;
	}
	case MOTOR_CONTROL_START_STATE_E:
	{
		if (true == MotorControl_RunTime_St.MotorControl_InitCommCheck_b)
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
				SET_MC_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
			}
			else if ((false == GET_MC_COMM_STATUS() &&
					 (MC_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - MotorControl_RunTime_St.Timer_u32))))
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}
		else if (true == MotorControl_RunTime_St.MotorControl_LateCommCheck_b)
		{
			MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
			}
			else
			{
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
			}
		}

		MotorControl_RunTime_St.Regen_state_b = false;
		if (ONE == Motor_0x150_Rx.Position_Sensor_Error_u8)
		{
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, THREE);
		}
		else
		{
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
		}

		/* code */
		break;
	}
	case MOTOR_CONTROL_RUN_STATE_E:
	{
		if (false == MotorControl_RunTime_St.MotorControl_LateCommCheck_b)
		{
			if ((false == MotorControl_RunTime_St.MotorControl_InitCommCheck_b) &&
				(false == GET_MC_COMM_STATUS()))
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}

			if (true == MotorControl_RunTime_St.MotorControl_InitCommCheck_b)
			{

				if (true == GET_MC_COMM_STATUS())
				{
					MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				}
				else if ((false == GET_MC_COMM_STATUS() &&
						 (MC_COMM_LOST_TIMEOUT <=
						  (GET_VCU_TIME_MS() - MotorControl_RunTime_St.Timer_u32))))
				{
					MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
					MotorControl_RunTime_St.MotorControl_LateCommCheck_b = true;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_ON_E);
					MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else
				{
					/* code */
				}
			}
			else
			{
				/* code */
			}
		}
		else if (false == MotorControl_RunTime_St.MotorControl_LateCommCheck_b)
		{
			MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			/* code */
		}

		ODO_in_meters = Estimate_ODO(Motor_0x250_Rx.Motor_Rotation_Number_u32);
		Power_Consum_Bars_u16 = CalculatePowerConsum(MCU_BatteryVoltage,MCU_BatteryCurrent,(DrivingMode_En_t)Get_DriverMode());
		if(MCU_BatteryCurrent < ZERO)
		{
			MotorControl_RunTime_St.Regen_state_b = true;
		}
		else
		{
			MotorControl_RunTime_St.Regen_state_b = false;
		}
		if (ONE == Motor_0x150_Rx.Position_Sensor_Error_u8)
		{
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, THREE);
		}
		else
		{
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
		}

		if (MOTOR_CONTROL_KILL_ON_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_OFF_E);
		}
		break;
	}
	case MOTOR_CONTROL_SHUT_DOWN_STATE_E:
	{
		if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		}
		MotorControl_RunTime_St.MotorControl_Start_b = false;
		Store_ODO_To_FEE();
		ResetTheODOEstimationData();
		break;
	}
	case MOTOR_CONTROL_SAFE_MODE_STATE_E:
	{
		if (true == MotorControl_RunTime_St.MotorControl_InitCommCheck_b)
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
				SET_MC_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
			}
			else if ((false == GET_MC_COMM_STATUS() &&
					 (MC_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - MotorControl_RunTime_St.Timer_u32))))
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}

		else if (true == MotorControl_RunTime_St.MotorControl_LateCommCheck_b)
		{
			if (true == GET_MC_COMM_STATUS())
			{
				if ((MC_COMM_RESTORE_TIME <=
					(GET_VCU_TIME_MS() - MotorControl_RunTime_St.Timer_u32)))
				{
					MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
					MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
					SET_MC_COMM_STATUS();
				}
			}
			else
			{
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
		}
		else
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
			}
			else
			{
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = true;
			}
		}
		Power_Consum_Bars_u16 = CalculatePowerConsum(MCU_BatteryVoltage,MCU_BatteryCurrent,(DrivingMode_En_t)Get_DriverMode());
		MotorControl_RunTime_St.Regen_state_b = false;
		break;
	}
	case MOTOR_CONTROL_STANDBY_STATE_E:
	{
		MotorControl_RunTime_St.MotorControl_Start_b = false;
		CLEAR_MC_COMM_STATUS();
		if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		}
		break;
	}

	default:
	{
		break;
	}
	}
	Vehicle_0x100_Tx.MotorStop_u8 = Get_Motor_Stop_Request();
	Vehicle_0x100_Tx.DrivingDirection_u8 = (uint8_t)Get_Drive_Dir();
	Vehicle_0x100_Tx.Motor_Speed_Limit_s16 = Get_Motor_Speed_Limit();
	Vehicle_0x100_Tx.Ride_mode_Request_u8 = Get_Ride_mode_Request();
	Motor_0x150_SetRx = Motor_0x150_Rx;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Speed_Display
*   Description   : This function returns the present speed to be display on  
					the cluster.
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Speed_Display(void)
{
	return Speed_to_display;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Vehicle_Speed_kmph
*   Description   : This function returns the vehicle speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Vehicle_Speed_kmph(void)
{
	return Vehicle_Speed_Kmph_Cal;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_ODO_meters
*   Description   : This function returns the vehicle ODO in meters
*   Parameters    : None
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Get_ODO_meters(void)
{
	return ODO_in_meters;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Power_consum_bar
*   Description   : This function returns the power_consumption bars speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Power_consum_bar(void)
{
	return Power_Consum_Bars_u16;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Regen_State
*   Description   : This function returns the Regenerative Breaking Status
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool Get_Regen_State(void)
{
	return MotorControl_RunTime_St.Regen_state_b;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Actual_MC_Ride_Mode
*   Description   : This function Gets Actaul Ride mode from Motor.
*   Parameters    : None, 
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t Get_Actual_MC_Ride_Mode(void)
{
	return Motor_0x150_Rx.Ride_mode_Actual_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x100_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x100_TxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		uint8_t data[EIGHT] = {ZERO};
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		Serialize_Vehicle_0x100_Tx(&Vehicle_0x100_Tx, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_MC_0x100_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x300_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x300_TxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		uint8_t data[EIGHT] = {ZERO};
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		Serialize_Vehicle_0x300_Tx(&Vehicle_0x300_Tx, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_MC_0x300_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x400_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x400_TxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if ((ZERO != Get_Broadcast_rate()) || (true == Send_Once_b))
		{
			uint8_t data[EIGHT] = {ZERO};
			uint8_t i = ZERO;
			CAN_MessageFrame_St_t Can_Applidata_St;
			Send_Once_b = false;
			Serialize_Vehicle_0x400_Tx(&Vehicle_0x400_Tx, &data[ZERO]);
			for (i = ZERO; i < EIGHT; i++)
			{
				Can_Applidata_St.DataBytes_au8[i] = data[i];
			}
			Can_Applidata_St.DataLength_u8 = EIGHT;
			Can_Applidata_St.MessageType_u8 = STD_E;
			CIL_CAN_Tx_AckMsg(CIL_MC_0x400_TX_E, Can_Applidata_St);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x150_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x150_RX_E == CIL_SigName_En)
		{
			SET_MC_COMM_STATUS();
			Deserialize_Motor_0x150_Rx(&Motor_0x150_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
			MCU_BatteryCurrent = (Motor_0x150_Rx.Battery_current_s16*MOTOR_0X150_RX_BATTERY_CURRENT_S16FACTOR);
			MCU_Motor_Speed_RPM = (Motor_0x150_Rx.Motor_Speed_s16*MOTOR_0X150_RX_MOTOR_SPEED_S16FACTOR);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x200_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x200_RX_E == CIL_SigName_En)
		{
			SET_MC_COMM_STATUS();
			Deserialize_Motor_0x200_Rx(&Motor_0x200_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
			MCU_BatteryVoltage = (Motor_0x200_Rx.Battery_Voltage_u8*MOTOR_0X200_RX_BATTERY_VOLTAGE_U8FACTOR);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x250_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x250_RX_E == CIL_SigName_En)
		{
			SET_MC_COMM_STATUS();
			Deserialize_Motor_0x250_Rx(&Motor_0x250_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x650_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x650_RX_E == CIL_SigName_En)
		{
			SET_MC_COMM_STATUS();
			Deserialize_Motor_0x650_Rx(&Motor_0x650_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x750_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x750_RX_E == CIL_SigName_En)
		{
			SET_MC_COMM_STATUS();
			Deserialize_Motor_0x750_Rx(&Motor_0x750_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x150_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x200_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x250_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x650_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x750_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Start_Signal
*   Description   : This function implements MotorControl Start Signal 
*		    		and stop operation.
*   Parameters    : MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En
*   Return Value  : None
*******************************************************************************/
void MotorControl_Start_Signal(MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En)
{
	if (MOTOR_CONTROL_KILL_ON_E == MotorControl_Kill_Signal_En)
	{
		//PORT.P9&=0xFFFE;
		if (ONE == PORT.P9 | 0x01U)
		{
			PORT.P9 &= 0xFFFE;
		}
	}
	else if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Kill_Signal_En)
	{
		if (ZERO == PORT.P9 | 0x01U)
		{
			PORT.P9 |= 0x01U;
		}
	}
	else
	{
		/* code */
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_Start_Signal
*   Description   : This function implements get Start Signal 
*		    and stop data.
*   Parameters    : None
*   Return Value  : MotorControl_Kill_Signal_En_t
*******************************************************************************/
MotorControl_Kill_Signal_En_t MotorControl_Get_Start_Signal(void)
{

	if (PORT.P9 &= 0xFFFE)
	{
		return MOTOR_CONTROL_KILL_ON_E;
	}
	else
	{
		return MOTOR_CONTROL_KILL_OFF_E;
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryVoltage
*   Description   : This function returns the Battery Volatge value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
float MotorControl_Get_BatteryVoltage(void)
{
	return MCU_BatteryVoltage;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryCurrent
*   Description   : This function returns the Battery Current value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
float MotorControl_Get_BatteryCurrent(void)
{
	return MCU_BatteryCurrent;
}

#endif /* MOTOR_CONTROL_C */
/*---------------------- End of File -----------------------------------------*/