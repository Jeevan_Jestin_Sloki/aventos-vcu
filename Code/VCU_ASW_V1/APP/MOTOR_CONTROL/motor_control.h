/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : motor_control.h
|    Project        : VCU
|    Module         : motor control
|    Description    : This file contains the export variables and functions                     
|                     to initialize and Operate the motor control functanality.      
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 29/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "can_if.h"
#include "mcu_can_signals.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
 #define MC_COMM_LOST_TIMEOUT     (5000U)
 #define MC_COMM_RESTORE_TIME     (3000U)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/

typedef enum
{
    MOTOR_CONTROL_INIT_STATE_E, //added
    MOTOR_CONTROL_START_STATE_E,
    MOTOR_CONTROL_RUN_STATE_E,
    MOTOR_CONTROL_SHUT_DOWN_STATE_E, // added
    MOTOR_CONTROL_SAFE_MODE_STATE_E, // added
    MOTOR_CONTROL_STANDBY_STATE_E,
} MotorControl_State_En_t;
typedef enum
{
    MOTOR_CONTROL_INIT_EVENT_E,
    MOTOR_CONTROL_START_EVENT_E,
    MOTOR_CONTROL_RUN_EVENT_E,
    MOTOR_CONTROL_SHUT_DOWN_EVENT_E, // added
    MOTOR_CONTROL_SAFE_MODE_EVENT_E, // added
    MOTOR_CONTROL_STANDBY_EVENT_E,
} MotorControl_Event_En_t;

typedef enum
{
    MOTOR_CONTROL_KILL_ON_E = 0,
    MOTOR_CONTROL_KILL_OFF_E,
} MotorControl_Kill_Signal_En_t;
typedef struct
{
    bool MotorControl_Start_b;
    bool MotorControl_InitCommCheck_b;
    bool MotorControl_LateCommCheck_b;
    uint32_t                Timer_u32; 
    bool                Regen_state_b;     
    MotorControl_Event_En_t MotorControl_Event_En;
    MotorControl_State_En_t MotorControl_State_En;
} MotorControl_RunTime_St_t;


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Init
*   Description   : This function implements MotorControl initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Proc
*   Description   : This function implements MotorControl Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Speed_Display
*   Description   : This function returns the present speed to be display on  
					the cluster.
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
extern uint16_t Get_Speed_Display(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Vehicle_Speed_kmph
*   Description   : This function returns the vehicle speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
extern uint16_t Get_Vehicle_Speed_kmph(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_ODO_meters
*   Description   : This function returns the vehicle ODO in meters
*   Parameters    : None
*   Return Value  : uint32_t
*******************************************************************************/
extern uint32_t Get_ODO_meters(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Power_consum_bar
*   Description   : This function returns the power_consumption bars speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
extern uint16_t Get_Power_consum_bar(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Regen_State
*   Description   : This function returns the Regenerative Breaking Status
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
extern bool Get_Regen_State(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Actual_MC_Ride_Mode
*   Description   : This function Gets Actaul Ride mode from Motor.
*   Parameters    : None, 
*   Return Value  : uint8_t
*******************************************************************************/
extern uint8_t Get_Actual_MC_Ride_Mode(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x100_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x100_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x300_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x300_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x400_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x400_TxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x150_RxMsgCallback(uint16_t CIL_SigName_En, 
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x200_RxMsgCallback(uint16_t CIL_SigName_En, 
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x250_RxMsgCallback(uint16_t CIL_SigName_En, 
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x650_RxMsgCallback(uint16_t CIL_SigName_En, 
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, 
*                   CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x750_RxMsgCallback(uint16_t CIL_SigName_En, 
                                    CAN_MessageFrame_St_t *Can_Applidata_St);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x150_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x200_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x250_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x650_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void MotorControl_0x750_TimeOut_RxMsgCallback(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryVoltage
*   Description   : This function returns the Battery Volatge value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
extern float MotorControl_Get_BatteryVoltage(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryCurrent
*   Description   : This function returns the Battery Current value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
extern float MotorControl_Get_BatteryCurrent(void);

#endif /* MOTOR_CONTROL_H */
/*---------------------- End of File -----------------------------------------*/