/******************************************************************************
 *    FILENAME    : cil_can_conf.h
 *    DESCRIPTION : File contains CIL_CAN_Conf.c related definitions and declarations.
 ******************************************************************************
 * Revision history
 *  
 * Ver   Author       Date               Description
 * 1     Sushil      27/10/2018		     Initial version
 ******************************************************************************
*/ 
#ifndef _CIL_CAN_CONF_H_
#define _CIL_CAN_CONF_H_
/*
 ****************************************************************************************
 *    Includes
 ****************************************************************************************
*/
#include "can_if.h"
//#include <stdint.h>
//#include <stdlib.h>
#include "diag_sys_conf.h"
#include "vcu_conf.h"

/*
 ****************************************************************************************
 *    Defines
 ****************************************************************************************
*/  
//#define NULL 0
#define CIL_CAN_GetID(sig)          (CIL_CAN_Conf_aSt[sig].HAL_CAN_MsgConf_St.ID_u32)

//#define NUMBYTES_SID 		        1      // Number of bytes that can hold a valid SID according to ISO protocol
//#define NUMBYTES_PID 		        1      // Number of bytes that can hold a valid PID according to ISO protocol
//#define NUMBYTES_DID 		        2      // Number of bytes that can hold a valid DID according to UDS protocol
//#define NUMBYTES_RESPCODE 	        1      // Number of bytes to hold positive or neg response code.
//
//#define ISO15765_CONF_NUMDATABYTES 	512    // Remaining data reserved for sending data and for OBD change its value to 253//Todo Sandeep K Y
//
//// This includes 1 byte for SID, 1 byte for PID and the remaining for data
//#define MAX_LENGTH_DIAGBUFFER       ((NUMBYTES_SID) + (NUMBYTES_PID) + (ISO15765_CONF_NUMDATABYTES))
//// Number of bytes available for the service interpreter is the max size - the size of
//// response code (which is 1 byte) and the SID sent in the response
//#define MAX_LENGTH_DIAGSERVICE      (MAX_LENGTH_DIAGBUFFER - NUMBYTES_RESPCODE - NUMBYTES_SID)

/*
 ****************************************************************************************
 *    ENUM Definition 
 ****************************************************************************************
*/
/*
    Enum defined to sort out the type callback recieved from cil or network layer to upper layer.
*/
typedef enum
{
    CIL_NEW_REQ_Rxed_E = 1,
    CIL_Rx_ERROR_E = 2,
    CIL_TX_CONFRM_E = 3,
    CIL_TX_ERROR_E = 4,
    CIL_FF_RXED_E = 5,
}nl2tp_CbkType_En_t;


typedef enum
{
    CIL_CAN_START_E = 0,
	CIL_RX_UDS_START = CIL_CAN_START_E,
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
    CIL_CANTP_REQ_IVN_RX_E  = CIL_RX_UDS_START,
    CIL_CANTP_REQ_TESTER_RX_E,
    CIL_CANTP_REQ_FUNC_RX_E,
	CIL_RX_UDS_END,
#else
	CIL_RX_UDS_END = CIL_CAN_START_E,
#endif
	CIL_RX_OBD_START = CIL_RX_UDS_END,
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
	CIL_CANTP_REQ_OBD_TESTER_RX_E = CIL_RX_OBD_START,
	CIL_RX_OBD_END,
#else
	CIL_RX_OBD_END = CIL_RX_OBD_START,
#endif
	CIL_RX_J1939_START = CIL_RX_OBD_END,
//    CIL_EEPROM_FAULT_FUNC_RX_E,
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
    CIL_J1939_TPCM_BAM_RX_E = CIL_RX_J1939_START,
	CIL_J1939_TPCM_RX_E,
    CIL_J1939_TPDT_BAM_RX_E,
    CIL_J1939_TPDT_RX_E,    
    CIL_J1939_DM22_RX_E,    
    CIL_J1939_81_NMAC_RX_E, 
    CIL_J1939_REQ_DA_BAM_RX_E,
    CIL_J1939_REQ_DA_RX_E,  
	CIL_RX_J1939_END,
#else
	CIL_RX_J1939_END = CIL_RX_J1939_START,
#endif
#if(TRUE == DIAG_TEST_FM_EEPROM_DEMO)
	CIL_EEPROM_FAULT_FUNC_RX_E = CIL_RX_J1939_END,
	CIL_EEPROM_FAULT_FUNC_RX_END,
#else
	CIL_EEPROM_FAULT_FUNC_RX_END = CIL_RX_J1939_END,
#endif
// #if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
// 	CIL_RX_FM_TEST_DEMO_E = CIL_EEPROM_FAULT_FUNC_RX_END,
// 	CIL_RX_MC_START,
// #else
//     CIL_RX_MC_START = CIL_EEPROM_FAULT_FUNC_RX_END,
// 	// CIL_TX_UDS_START = CIL_EEPROM_FAULT_FUNC_RX_END,
// #endif
#if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
	CIL_RX_FM_TEST_DEMO_E = CIL_EEPROM_FAULT_FUNC_RX_END,
	CIL_RX_BMS_START,
#else
    CIL_RX_BMS_START = CIL_EEPROM_FAULT_FUNC_RX_END,
	// CIL_TX_UDS_START = CIL_EEPROM_FAULT_FUNC_RX_END,
#endif
#if(TRUE == VCU_CONF_BMS_SUPPORTED)
    CIL_BMS_0x4AA_RX_E = CIL_RX_BMS_START,
    CIL_BMS_0x4AB_RX_E,
    CIL_BMS_0x3AA_RX_E,
    CIL_BMS_0x2AA_RX_E, 
    CIL_RX_BMS_END,
    CIL_RX_MC_START = CIL_RX_BMS_END,
#else
    CIL_RX_MC_START = CIL_RX_BMS_START,
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    CIL_MC_0x150_RX_E = CIL_RX_MC_START,
    CIL_MC_0x200_RX_E,
    CIL_MC_0x250_RX_E,
    CIL_MC_0x650_RX_E,
    CIL_MC_0x750_RX_E,  
    CIL_RX_MC_END,
    CIL_TX_UDS_START = CIL_RX_MC_END,
#else
    CIL_TX_UDS_START = CIL_RX_MC_START,
    // CIL_MC_0x750_RX_E = CIL_TX_UDS_START,
#endif
    CIL_DCAN_TOTAL_RX_E = CIL_TX_UDS_START,
#if(TRUE == DIAG_CONF_UDS_SUPPORTED)
    CIL_CANTP_RESP_TESTER_TX_E = CIL_DCAN_TOTAL_RX_E,
    CIL_CANTP_RESP_IVN_TX_E,
	CIL_TX_UDS_END,
#else
	CIL_TX_UDS_END = CIL_TX_UDS_START,
#endif
	CIL_TX_OBD_START = CIL_TX_UDS_END,
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED)
	CIL_CANTP_RESP_OBD_TESTER_TX_E = CIL_TX_OBD_START,
	CIL_TX_OBD_END,
#else
	CIL_TX_OBD_END = CIL_TX_OBD_START,
#endif
	CIL_TX_J1939_START = CIL_TX_OBD_END,
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
    CIL_J1939_TPCM_TX_E = CIL_TX_J1939_START,
    CIL_j1939_tpdt_TX_E, 
    CIL_J1939_ACK_TX_E,  
    CIL_J1939_REQ_TX_E, 
    CIL_J1939_71_TEST1_TX_E,
    CIL_J1939_71_TEST2_TX_E,
    CIL_J1939_71_TEST3_TX_E,
	CIL_TX_J1939_END,
#else
	CIL_TX_J1939_END = CIL_TX_J1939_START,
#endif
#if(TRUE == DIAG_TEST_FM_EEPROM_DEMO)
	CIL_EEPROM_FAULT_FUNC_TX_E = CIL_TX_J1939_END,
    CIL_TEST3_TX_E,
#else
    CIL_TEST3_TX_E = CIL_TX_J1939_END,
#endif
#if(TRUE == DIAG_TEST_FM_DEMO)
	CIL_FM_TEST1_TX_E,
	CIL_FM_TEST2_TX_E,
#else
	CIL_FM_TEST2_TX_E = CIL_TEST3_TX_E,
#endif
#if(TRUE == VCU_CONF_HMI_SUPPORTED)
    CIL_HMI_EVE_1_TX_E,
    CIL_I_HMI_EVE_1_TX_E,
    CIL_HMI_PER_1_TX_E,  
    CIL_I_HMI_PER_1_TX_E,
    CIL_HMI_PER_2_TX_E,  
    CIL_I_HMI_PER_2_TX_E,
    
#else
   CIL_I_HMI_PER_2_TX_E = CIL_FM_TEST2_TX_E,
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    CIL_MC_0x100_TX_E,
    CIL_MC_0x300_TX_E,
    CIL_MC_0x400_TX_E,  
#else
    CIL_MC_0x400_TX_E = CIL_I_HMI_PER_2_TX_E,
#endif
    CIL_DCAN_TX_E = CIL_MC_0x400_TX_E,
    CIL_DCAN_END_E
}CIL_SigName_En_t;

/*
 ****************************************************************************************
 *    Structure Definition 
 ****************************************************************************************
*/


/*
 ****************************************************************************************
 *    al Variables
 ****************************************************************************************
*/ 
extern const CIL_CAN_Conf_St_t CIL_CAN_Conf_aSt[CIL_DCAN_END_E];

/*
 ****************************************************************************************
 *    Function Prototypes
 ****************************************************************************************
*/

#endif

/* *****************************************************************************
 End of File
 */

