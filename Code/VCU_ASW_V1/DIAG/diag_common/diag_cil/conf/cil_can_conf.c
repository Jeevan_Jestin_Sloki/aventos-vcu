/******************************************************************************
 *    FILENAME    : cil_can_conf.c
 *    DESCRIPTION : File contains the common declarations related to CIL layers.
 ******************************************************************************
 * Revision history
 *  
 * Ver   Author       Date               Description
 * 1     Sushil      27/10/2018		     Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                       */
//#include "can_if.h"
#include "cil_can_conf.h"
#include "diag_appl_test.h"
#include "vcu_conf.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */

/*
 * @summary:- can structure message buffer  
 */
CanSchedMsg_St_t CanSB_St [CIL_DCAN_TOTAL_RX_E] = {0};


const CIL_CAN_Conf_St_t CIL_CAN_Conf_aSt[CIL_DCAN_END_E] =
{
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
    { {0x6F0,  0x7FF, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_IVN_RX_E      , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_IVN_RX_E].msg  }  ,//@
//    { {0x7E0,  0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_TESTER_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[1].msg  }  ,//@
    { {0x7F0,  0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_TESTER_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_TESTER_RX_E].msg  }  ,//@
    { {0x7DF,  0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_FUNC_RX_E].msg  }  ,//@
#endif
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)

	{ {0x18DB33F0, 0x1FFFFFFC, CAN1_E,  0x10,    EXT_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_OBD_TESTER_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_OBD_TESTER_RX_E].msg  }  ,//@
#endif
//    { {0x7DF, 0x7DF,   0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[2].msg  }  ,//@
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
	{ { 0x10ECFF00   ,  0x10FFFF00,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPCM_BAM_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_TPCM_BAM_RX_E].msg         }  ,/*@ */
	{ { 0x10EC0000   ,  0x10FF0000, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPCM_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_TPCM_RX_E].msg         }  ,/*@ */
	{ { 0x10EBFF00   ,	0x10FFFF00, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPDT_BAM_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_TPDT_BAM_RX_E].msg         }  ,/*@ */
	{ { 0x10EB0000   ,	0x10FF0000, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPDT_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_TPDT_RX_E].msg         }  ,/*@ */
	{ { 0x18C300F0   ,	0x1FFF0000, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_DM22_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_DM22_RX_E].msg         }  ,/*@ */
	{ { 0x10EEFF00   ,	0x10FFFF00, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_81_NMAC_RX_E    , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_81_NMAC_RX_E].msg         }  ,/*@ */
	{ { 0x10EAFF00   ,	0x10FFFF00, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_REQ_DA_BAM_RX_E , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_REQ_DA_BAM_RX_E].msg        }  ,/*@ */
	{/*0x18EA0000*/ { 0x10EA0000   ,	0x10FF0000, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_REQ_DA_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_J1939_REQ_DA_RX_E].msg        }  ,/*@ */
#endif
#if(TRUE == DIAG_TEST_FM_EEPROM_DEMO)
    { {0x7EF, 0x7EF,  CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_EEPROM_FAULT_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanTest_St[0].msg  }  ,//@
#endif
#if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
    { {0x710, 0x710,  CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_RX_FM_TEST_DEMO_E     , CanSched_RxCallBackInt    , &CanSB_St[12].msg  }  ,//@
#endif
#if(TRUE == VCU_CONF_BMS_SUPPORTED)
    { {0x4AA, 0x7FF,  CAN3_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_BMS_0x4AA_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_BMS_0x4AA_RX_E].msg  }  ,//@
    { {0x4AB, 0x7FF,  CAN3_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_BMS_0x4AB_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_BMS_0x4AB_RX_E].msg  }  ,//@
    { {0x3AA, 0x7FF,  CAN3_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_BMS_0x3AA_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_BMS_0x3AA_RX_E].msg  }  ,//@
    { {0x2AA, 0x7FF,  CAN3_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_BMS_0x2AA_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_BMS_0x2AA_RX_E].msg  }  ,//@
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    { {0x150, 0x7FF,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_MC_0x150_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_MC_0x150_RX_E].msg  }  ,//@
    { {0x200, 0x7FF,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_MC_0x200_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_MC_0x200_RX_E].msg  }  ,//@
    { {0x250, 0x7FF,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_MC_0x250_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_MC_0x250_RX_E].msg  }  ,//@
    { {0x650, 0x7FF,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_MC_0x650_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_MC_0x650_RX_E].msg  }  ,//@
    { {0x750, 0x7FF,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_MC_0x750_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[CIL_MC_0x750_RX_E].msg  }  ,//@
#endif
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
//    { {0x150,  0U, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_TESTER_TX_E  , NULL                  	, NULL  }  ,//@
	{ {0x7F1,  0U, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_TESTER_TX_E  , NULL                  	, NULL  }  ,//@
    { {0x6F1,  0U, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_IVN_TX_E     , NULL                  	, NULL  }  ,//@
#endif
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
	{ {0x18DAF110, 0U, CAN1_E,  0x10,    EXT_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_OBD_TESTER_TX_E     , NULL                  	, NULL  }  ,//@
#endif
//    { {0x6A3,  0U,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_TEST3_TX_E     	   , NULL                  	, NULL  }  ,//@
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
    { { 0x18EC0100   ,	0U, CAN2_E,  0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_TPCM_TX_E       , NULL        , NULL        }  ,/*@ */
    { { 0x18EB0100   , 0U, CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_j1939_tpdt_TX_E       , NULL        , NULL        }  ,/*@ */
    { { 0x18E8FF00   ,	0U,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_ACK_TX_E        , NULL        , NULL        }  ,/*@ */
    { { 0x18EAFF00   ,	0U,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_REQ_TX_E        , NULL        , NULL        }  ,/*@ */
    { { 0x18AA0000   ,	0U,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST1_TX_E        , NULL        , NULL        }  ,/*@ */
    { { 0x18AB0000   ,	0U,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST2_TX_E        , NULL        , NULL        }  ,/*@ */
    { { 0x18AC0000   ,	0U,	CAN2_E, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST3_TX_E        , NULL        , NULL        }  ,/*@ */
#endif
#if(TRUE == DIAG_TEST_FM_EEPROM_DEMO)
    { {0x512, 0U,  CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_EEPROM_FAULT_FUNC_TX_E     , NULL    , NULL  }  ,//@
#endif
    { {0x6A3, 	0U, CAN1_E,   0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_TEST3_TX_E     	   , NULL                  	, NULL  }  ,//@
#if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
    { {0x701, 	0U, CAN1_E,    0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_FM_TEST1_TX_E     	   , NULL                  	, NULL  }  ,//@
    { {0x702, 	0U, CAN1_E,    0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_FM_TEST2_TX_E     	   , NULL                  	, NULL  }  ,//@
#endif
#if(TRUE == VCU_CONF_HMI_SUPPORTED)
    { { 0x700,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_HMI_EVE_1_TX_E,     NULL, NULL  },
    { { 0x701,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_I_HMI_EVE_1_TX_E,   NULL, NULL  },
    { { 0x702,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_HMI_PER_1_TX_E,     NULL, NULL  },
    { { 0x703,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_I_HMI_PER_1_TX_E,   NULL, NULL  },
    { { 0x704,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_HMI_PER_2_TX_E,     NULL, NULL  },
    { { 0x705,0U, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_I_HMI_PER_2_TX_E,   NULL, NULL  },
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    { {0x100, 0U,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_MC_0x100_TX_E     , NULL    , NULL  }  ,//@
    { {0x300, 0U,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_MC_0x300_TX_E     , NULL    , NULL  }  ,//@
    { {0x400, 0U,  CAN2_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_MC_0x400_TX_E     , NULL    , NULL  }  ,//@
#endif
};



/* *****************************************************************************
 End of File
 */

