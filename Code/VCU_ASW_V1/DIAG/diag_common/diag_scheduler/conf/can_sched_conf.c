/******************************************************************************
 *    FILENAME    : can_sched.c
 *    DESCRIPTION : description of a can scheduler
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 



/* Section: Included Files                                                   */
#include "can_sched.h"
#include "can_sched_conf.h"
#include "cil_can_conf.h"
#include "math_util.h"
#include "diag_appl_test.h"
#include "vcu_conf.h"
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
	#include "j1939.h"
#endif
#if(TRUE == DIAG_CONF_UDS_SUPPORTED)

#endif
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED)

#endif
#if(TRUE == VCU_CONF_HMI_SUPPORTED)
    #include "cluster.h"  
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    #include "motor_control.h" 
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    #include "bms.h" 
#endif
#include "DataBank.h"
//#include "DataAquire.h"
/**************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */

/*
 * @summary:- can structure message buffer  
 */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)

uint32_t CAN_CONFIG_SCHED_ACTIVE_RX_u32;
uint32_t CAN_CONFIG_SCHED_ACTIVE_TX_u32;

#endif


/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

#if(TRUE == DIAG_CONF_J1939_SUPPORTED)

BOOL IsRXCanMessageActive(UINT32 BitPosCANMessage)
{
    BOOL ret_b = (BOOL)FALSE;
    
    if(BitPosCANMessage == 0xFFFFU)
    {
        ret_b = (BOOL)TRUE;        
    }
    else 
    {            
        ret_b = GETBIT(CAN_CONFIG_SCHED_ACTIVE_RX_u32, BitPosCANMessage);
    }
    
    return ret_b;
}

BOOL IsTXCanMessageActive(UINT32 BitPosCANMessage)
{
    BOOL ret_b = (BOOL)FALSE;
    
    if(BitPosCANMessage == 0xFFFFU)
    {
        ret_b = (BOOL)TRUE;        
    }
    else 
    {    
        ret_b = (BOOL)GETBIT(CAN_CONFIG_SCHED_ACTIVE_TX_u32, BitPosCANMessage);
    }
    
    return ret_b;
}
#endif
/*  A brief description of a section can be given directly below the section
    banner.
 */

// *****************************************************************************


//count of receive interrupt callbacks"
//static uint16_t CAN_IntCbCnt_u16 = 0;    

// Counter for Time out timer
//static uint32_t CAN_MsgRxBoxTimeOutCnt [CIL_DCAN_TOTAL_RX_E] = { 0 };



/*
 * @summary:-    array of can Application structures    
 */
const CANSCHED_RX_Conf_St_t   CANSCHED_RX_Conf_aSt[CIL_DCAN_TOTAL_RX_E]=
{
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
	{CIL_CANTP_REQ_IVN_RX_E,	        PERIODICITY_MS(5),	  CANSched_RxMsgCallback,	NULL},  //
	{CIL_CANTP_REQ_TESTER_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
	{CIL_CANTP_REQ_FUNC_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
#endif
#if(TRUE == DIAG_CONF_OBD2_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
	{CIL_CANTP_REQ_OBD_TESTER_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
#endif
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
    /*1 */    {CIL_J1939_REQ_DA_RX_E,     PERIODICITY_MS(20U),     J1939_reqPgnClbk ,     J1939_rqPgnClbk_timeout},
    /*1 */    {CIL_J1939_REQ_DA_BAM_RX_E,     PERIODICITY_MS(20U),     J1939_reqPgnClbk ,     J1939_rqPgnClbk_timeout},
    
    
    /*2 */    {CIL_J1939_TPCM_RX_E,       PERIODICITY_MS(20U),     J1939_tpcmClbk,        NULL},
    /*3 */    {CIL_J1939_TPCM_BAM_RX_E,   PERIODICITY_MS(20U),     J1939_tpcmClbk,        NULL},
    /*4 */    {CIL_J1939_TPDT_RX_E,       PERIODICITY_MS(20U),     J1939_tpdtClbk,        NULL},
    /*5 */    {CIL_J1939_DM22_RX_E,       PERIODICITY_MS(20U),     J1939_dm22Clbk,        NULL},
    /*6 */    {CIL_J1939_TPDT_BAM_RX_E,   PERIODICITY_MS(20U),     J1939_tpdtClbk,        NULL},
    /*7 */    {CIL_J1939_81_NMAC_RX_E,     NO_TIMEOUT,     J1939_81_RXNMAC_Callback,     NULL},

#endif
#if(TRUE == DIAG_TEST_FM_EEPROM_DEMO)
	{CIL_EEPROM_FAULT_FUNC_RX_E,   	    PERIODICITY_MS(5),	  NULL, 	NULL},  //
#endif
#if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
	{CIL_RX_FM_TEST_DEMO_E,	        PERIODICITY_MS(5),	  diag_appl_test_fm_drv_Cycle,	NULL},  //
#endif
#if(TRUE == VCU_CONF_BMS_SUPPORTED) 
								/*CBF*/ /*TimeOut Function*/
    {CIL_BMS_0x4AA_RX_E,	        PERIODICITY_MS(200),	  /*NULL*/BMS_0x4AA_RxMsgCallback,	/*NULL*/BMS_0x4AA_TimeOut_RxMsgCallback},  // /*todo:dileepabs - add function name to NULL*/
    {CIL_BMS_0x4AB_RX_E,	        PERIODICITY_MS(200),	  /*NULL*/BMS_0x4AB_RxMsgCallback,	/*NULL*/BMS_0x4AB_TimeOut_RxMsgCallback},  //
    {CIL_BMS_0x3AA_RX_E,	        PERIODICITY_MS(200),	  /*NULL*/BMS_0x3AA_RxMsgCallback,	/*NULL*/BMS_0x3AA_TimeOut_RxMsgCallback},  //
    {CIL_BMS_0x2AA_RX_E,	        PERIODICITY_MS(200),	  /*NULL*/BMS_0x2AA_RxMsgCallback,	/*NULL*/BMS_0x2AA_TimeOut_RxMsgCallback},  //
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED) 
    {CIL_MC_0x150_RX_E,	        PERIODICITY_MS(100),	  /*NULL*/MotorControl_0x150_RxMsgCallback,	/*NULL*/MotorControl_0x150_TimeOut_RxMsgCallback},  //	/*todo:dileepabs - add function name to NULL*/
    {CIL_MC_0x200_RX_E,	        PERIODICITY_MS(100),	  /*NULL*/MotorControl_0x200_RxMsgCallback,	NULL/*MotorControl_0x200_TimeOut_RxMsgCallback*/},  //
    {CIL_MC_0x250_RX_E,	        PERIODICITY_MS(100),	  /*NULL*/MotorControl_0x250_RxMsgCallback,	NULL/*MotorControl_0x250_TimeOut_RxMsgCallback*/},  //
    {CIL_MC_0x650_RX_E,	        PERIODICITY_MS(100),	  /*NULL*/MotorControl_0x650_RxMsgCallback,	NULL/*MotorControl_0x650_TimeOut_RxMsgCallback*/},  //
    {CIL_MC_0x750_RX_E,	        PERIODICITY_MS(100),	  /*NULL*/MotorControl_0x750_RxMsgCallback,	NULL/*MotorControl_0x750_TimeOut_RxMsgCallback*/},  //
#endif
};


const CANSCHED_TX_Conf_St_t   CANSCHED_TX_Conf_aSt[CAN_SCHED_CONF_TOTAL_TX_MSG]=
{
    // CIL Sig name,			Cycle Time					Offset (Timeslice)			Call Back Function     
    { CIL_TEST3_TX_E,			PERIODICITY_MS(500), 		PERIODICITY_MS(10),   		CANSched_Debug_TxMsgCallback}, // todo harsh
#if(TRUE == DIAG_CONF_J1939_SUPPORTED)
	{CIL_J1939_71_TEST1_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U),	  J1939_71_TX_TEST1},
    {CIL_J1939_71_TEST2_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  J1939_71_TX_TEST2},
    {CIL_J1939_71_TEST3_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  J1939_71_TX_TEST3},
    {CIL_j1939_tpdt_TX_E,         PERIODICITY_MS(60U),     PERIODICITY_MS(25U),       J1939_tp_txDtManage},
#endif
#if(TRUE == DIAG_TEST_FM_DEMO && TRUE == DIAG_CONF_FM_SUPPORTED)
    {CIL_FM_TEST1_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  diag_fm_tx_fault1},
    {CIL_FM_TEST1_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  diag_fm_tx_fault2},
#endif
#if(TRUE == VCU_CONF_HMI_SUPPORTED)
    {CIL_HMI_PER_1_TX_E,     PERIODICITY_MS(100U),      PERIODICITY_MS(25U), 	  CANSched_HMI_PER_1_TxMsgCallback}, /*todo:dileepabs - add function name to NULL*/
    {CIL_I_HMI_PER_1_TX_E,   PERIODICITY_MS(100U),      PERIODICITY_MS(50U), 	  CANSched_I_HMI_PER_1_TxMsgCallback},
    {CIL_HMI_PER_2_TX_E,     PERIODICITY_MS(100U),      PERIODICITY_MS(75U), 	  CANSched_HMI_PER_2_TxMsgCallback},
    {CIL_I_HMI_PER_2_TX_E,   PERIODICITY_MS(100U),      PERIODICITY_MS(100U), 	  CANSched_I_HMI_PER_2_TxMsgCallback},
#endif
#if(TRUE == VCU_CONF_MC_SUPPORTED)
    {CIL_MC_0x100_TX_E,     PERIODICITY_MS(100U),      PERIODICITY_MS(25U), 	  MotorControl_0x100_TxMsgCallback},
    {CIL_MC_0x300_TX_E,   PERIODICITY_MS(55U),      PERIODICITY_MS(50U), 	  MotorControl_0x300_TxMsgCallback}, /*todo:dileepabs - add function name to NULL*/
    {CIL_MC_0x400_TX_E,     PERIODICITY_MS(50U),      PERIODICITY_MS(10U), 	  MotorControl_0x400_TxMsgCallback},
#endif
};




/* *****************************************************************************
 End of File
 */
