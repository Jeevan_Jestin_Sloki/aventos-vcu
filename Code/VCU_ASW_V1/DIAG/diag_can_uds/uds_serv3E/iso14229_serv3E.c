/******************************************************************************
 *    FILENAME    : uds_serv3E.c
 *    DESCRIPTION : Service description for UDS service - Tester present.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                                    */
#include <stdlib.h>
#include "uds_conf.h"
#include "iso14229_serv3E.h"

/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
/*
 @summary : this is for disable timer 
 */

/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
 
 

/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
 
 /**
*  FUNCTION NAME : iso14229_serv3E
*  FILENAME      : iso14229_serv3E.c
*  @param        : UDS_Serv_St_t* DataBuff_pu8 - pointer to service distributor table.
*  @brief        : This function will process the service_3E requests
*  @return       : Type of response.                     
*/
UDS_Serv_resptype_En_t iso14229_serv3E (UDS_Serv_St_t* UDS_Serv_pSt){
	
	UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
	uint8_t	subFunction_u8 = 0;

	
	if(ZERO_FUNCTION_LENGTH != UDS_Serv_pSt->RxLen_u16)
	{
                UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
		UDS_Serv_pSt->TxBuff_pu8[1] = SID_TESTERPRESENT;
		UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
		UDS_Serv_pSt->TxLen_u16 = 3;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
	else
	{
		subFunction_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
        switch(subFunction_u8)
		{
            case ZERO_SUBFUN_POSRESP:
			{
                                UDS_Serv_pSt->TxLen_u16 = 1;
                                UDS_Serv_pSt->TxBuff_pu8[1] = ZERO_SUBFUN_POSRESP;
				Serv_resptype_En = UDS_SERV_RESP_POS_E;
				break;
            }
            case ZERO_SUBFUN_SUPPPOSRESP:
			{
				Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
				break;
            }
            default:
			{
				UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
				UDS_Serv_pSt->TxBuff_pu8[1] = SID_TESTERPRESENT;
				UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
				UDS_Serv_pSt->TxLen_u16 = 3;
				Serv_resptype_En = UDS_SERV_RESP_NEG_E;
				break;
            }
        }             
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : UDS_Serv3E_Timeout
*  FILENAME      : UDS_Serv3E.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv3E_Timeout(void){

	return;
}

// *****************************************************************************

/* *****************************************************************************
 End of File
 */
