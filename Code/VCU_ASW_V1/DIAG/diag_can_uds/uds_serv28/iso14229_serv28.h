/******************************************************************************
 *    FILENAME    : uds_serv28.h
 *    DESCRIPTION : Header file for UDS service - Communication Control.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       21/01/2019		   Initial version
 ******************************************************************************
*/ 

#ifndef _UDS_SERV28_H_
#define	_UDS_SERV28_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.*/
#include "uds_conf.h"
#include "comm_cntrl_adapt.h"
  
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* @summary: minimum message length */
#define SERV_28_MIN_LEN                                     0x03u
/* @summary: maximum message length */
#define SERV_28_MAX_LEN                                     0x05u
  
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Data Types                                                        */
/* ************************************************************************** */
/* ************************************************************************** */
  
/* @Summary : - sub-functions which is going to be supported in this service */
typedef enum
{
    ENABLE_RX_AND_TX_SUB_ID_E = 0x00u,  
    ENABLE_RX_AND_DISABLE_TX_SUB_ID_E,
    DISABLE_RX_AND_ENABLE_TX_SUB_ID_E,
    DISABLE_RX_AND_TX_SUB_ID_E,
    ENABLE_RX_AND_DISABLE_TX_WITH_ENHANCED_ADDR_INFO_SUB_ID_E,
    ENABLE_RX_AND_TX_WITH_ENHANCED_ADDR_INFO_SUB_ID_E,
}COMMUNICATION_SUBID_En_t ;


/* @Summary : - this enum for communication type */
typedef enum
{
    NORMAL_COMM_MSG_E = 0x01u,
    NETWORK_MANAGEMENT_COMM_MSG_E,
    NETWORK_MANAGEMENT_AND_NORMAL_COMM_MSG_E,
    TOTAL_COMM_TYPE_E,
    
}COMMUNICATION_TYPE_En_t ;

typedef void (*COMM_MSG_Fptr_t)(bool , bool );

typedef struct
{
    COMMUNICATION_TYPE_En_t COMMUNICATION_TYPE_En;
    COMM_MSG_Fptr_t COMM_MSG_Fptr;
}Comm_Config_St_t;
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : CommunicationMessage_Fn
*  FILENAME      : uds_serv28.c
*  @param1       : uint8_t communicationType_u8 - communication type parameter.
*  @param2       : bool Rx_Status - control type RX status.
*  @param3       : bool Tx_Status - control type TX status.
*  @brief        : This function will process the communication type.
*  @return       : None.
**/
extern void CommunicationMessage_Fn(uint8_t , bool , bool );

/** 
*  FUNCTION NAME : iso14229_serv28
*  FILENAME      : iso14229_serv28.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_28 requests
*  @return       : Type of response.
**/
extern UDS_Serv_resptype_En_t iso14229_serv28(UDS_Serv_St_t* );

/**
*  FUNCTION NAME : UDS_Serv28_Timeout
*  FILENAME      : uds_serv28.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv28_Timeout(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ISO14229_SER3D_H */
