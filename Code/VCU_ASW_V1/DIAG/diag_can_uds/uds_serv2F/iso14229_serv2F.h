
/*********************************************************************************
 *    FILENAME    : uds_serv2F.h                                                 *          
 *    DESCRIPTION : Header file for UDS service - InputOutputControlByIdentifier.*
 *********************************************************************************
 * Revision history                                                              *               
 *                                                                               *
 * Ver Author       Date               Description                               *
 * 1   Sloki     1/10/2019		   Initial version                           *
 *********************************************************************************
*/  


#ifndef _UDS_SERV2F_H_
#define _UDS_SERV2F_H_


/*
 ****************************************************************************************
 *    Includes                                                                          *
 ****************************************************************************************
*/


#include "uds_conf.h"
#include "uds_DID.h"
#include "uds_stimulus.h"

/*
 ****************************************************************************************
 *    Defines                                                                           *
 ****************************************************************************************
*/  

#define MESSAGE_LEN                    0x03 //minimum message length
#define RETURN_CONTROL_TO_ECU          0x00 //returnControlToECU
#define RESET_TO_DEFAULT               0x01 //resetToDefault
#define FREEZE_CURRNT_STATE            0x02 //freezeCurrentState
#define SHORT_TERM_ADJUSTMENT          0x03 //shortTermAdjustment
#define SHORT_TERM_ADJUSTMENT_LEN      0x08
#define MAX_DEFAULT_VALUE_LENGTH       0x05
#define IO_BIT_POSITION                2u

 /*
 **************************************************************************************************
 *    Union
 **************************************************************************************************
 */   


 /**
*  FUNCTION NAME : iso14229_serv2F
*  FILENAME      : iso14229_serv2F.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service table.
*  @brief        : This function will process the service_2F requests
*  @return       : Type of response.                     
*/   
extern UDS_Serv_resptype_En_t iso14229_serv2F (UDS_Serv_St_t*UDS_Serv_pSt);
 
 
/**
*  FUNCTION NAME : UDS_Serv2F_Timeout
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv2F_Timeout(void);
 
 /**
*  FUNCTION NAME : UDS_Serv2F_Init
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function to reset mode to false
*  @return       : none.                     
*/
extern void UDS_Serv2F_Init(void);

 
  /**
*  FUNCTION NAME : Temp_Proc_10ms
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function to call every 10 ms
*  @return       : none.                     
*/
 
extern void Temp_Proc_10ms();
 
 
extern bool EOL_Cmd_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);
 
#endif
/* *****************************************************************************
 End of File
 */

