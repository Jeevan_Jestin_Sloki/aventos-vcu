/******************************************************************************
 *    FILENAME    : uds_serv2C.c
 *    DESCRIPTION : Service description for UDS service - Dynamically Defined By Data Identifier(0x2C).
 ******************************************************************************
 * Revision history
 *  
 * Ver Author          Date                     Description
 * 1   Sloki     06/05/2019		   Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                                    */
 
#include "iso14229_serv2C.h"

/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
/*
 @summary : this is for disable timer 
 */



/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

uint32_t DDID_count_u32 = 0u;



/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */
 
bool DDID_Range_Check_Fn(uint16_t DDID_Val_u16)
{
  if(((DDID_Val_u16 >= 0xF200) && (DDID_Val_u16 <= 0xF27F)) ||
     ((DDID_Val_u16 >= 0xF300) && (DDID_Val_u16 <= 0xF3FF)))
    return TRUE;
  else
    return FALSE;
}


bool Source_DID_Check(uint8_t *Rx_Buffer, uint8_t numbytes_u16)
{
  uint8_t i = 0;
  uint8_t j = 0;
  uint16_t ReqDDID_u16 = 0;
  bool Status_ret = TRUE;
  for(i=FOUR; i<numbytes_u16; i+=4)
  {
     ReqDDID_u16 = (Rx_Buffer[i]<<8) | Rx_Buffer[i+1];
     for(j=0; j<DID_TOTAL_E; j++)
     {
       if(ReqDDID_u16 == DID_Config_aSt[j].DID_Num)
       {
         break;
       }
     }
     if(j == DID_TOTAL_E)
     {
       Status_ret = FALSE;
       break;
     }
  }
  return Status_ret;
}

/**
*  FUNCTION NAME : iso14229_serv2C
*  FILENAME      : iso14229_serv2C.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributor table.
*  @brief        : This function will process the service_2C requests
*  @return       : Type of response.                     
*/
UDS_Serv_resptype_En_t iso14229_serv2C(UDS_Serv_St_t* UDS_Serv_pSt)
{
// uint32_t Serv2C_Index_u32 = 0u;                                    /* Variable mainly used for identify the start byte of the Nested DID's - 'DDID_Comm_Fn' in "uds_DID.c" */
 uint32_t Total_SDID_Req_u32 = 0u; 
 uint16_t Req_SDID_List[MAX_SDID_REQ] = {0};                       /* Array for DID storage from Request*/
// uint8_t Position_Start_au8[MAX_SDID_REQ] = {0u};                             /* Buffer used for hold the 'positionInSourceDataRecord' parameters when requested by the Client */
 uint8_t Memory_Size_au8[MAX_SDID_REQ] = {0u};                                /* Buffer used for hold the 'memorySize' parameters when requested by the Client */
// bool SDID_Found_b = FALSE;                                         /* Variable used as a Flag for DID's Compare */
// uint32_t DDID_Index_u32 = 0u;
// uint32_t Total_Req_Source_DID_Buff_Size_u32 = 0u;
 uint32_t i = 0u;                                                    /*Variables i and j used for looping operation(LOOP Count)*/
 uint32_t j = 0u;
 uint32_t DDID_Table_Check_u32 = 0u;
 uint8_t Sub_Function_u8 = UDS_Serv_pSt->RxBuff_pu8[1];                            
 uint16_t ReqDDID_u16 =0u;                                         /* Used for 2 Bytes DID NUMBER Storage for DID Comparison*/
 uint16_t numbytes_u16 = UDS_Serv_pSt->RxLen_u16;              /* Variable used for hold the size requested by the Client */
 UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;        /* Enum variable is used return a corresponding response*/ 
 bool Supp_Pos_Resp_b = FALSE;                                     /* Variable used as a flag for 'suppressPosRspMsgIndicationBit' when requested by the Client */
 bool Sfns_b = FALSE;                                              /* Variable used as a flag for 'sub-functionNotSupported(SFNS)' when requested by the Client */
 bool Request_OutOf_Range_b = FALSE;
    
   ReqDDID_u16 =  (UDS_Serv_pSt->RxBuff_pu8[2]<<8) | UDS_Serv_pSt->RxBuff_pu8[3];          /* Hold the 2 Byte DDID requested by the Client */
  Sub_Function_u8 = Sub_Function_u8 & 0x7F;
  
 if(Sub_Function_u8  & 0x80)              /* Check whether 'suppressPosRspMsgIndicationBit' TRUE/FALSE */
 {
   Supp_Pos_Resp_b = TRUE;                   
 }

 if((SERV2C_MIN_LEN > numbytes_u16 ) || ((Sub_Function_u8 == 0x01)&& (((numbytes_u16-4)%4) != 0x00)) || ((Sub_Function_u8 == 0x03) && numbytes_u16 != 4))/* Check whether 'incorrectMessageLengthOrInvalidFormat(0x13)' Occurs/Not */
 {
   UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
   UDS_Serv_pSt->TxBuff_pu8[1] = SID_DDID;
   UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;   /*NRC-0x13*/
   UDS_Serv_pSt->TxLen_u16 = 3u;
   Serv_resptype_En = UDS_SERV_RESP_NEG_E;
 }
 
  /*else if(FALSE == Check_SecurityClearance()) 
  {
    UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
    UDS_Serv_pSt->TxBuff_pu8[1] = SID_RDBDID;
    UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;   //NRC-0x33
    UDS_Serv_pSt->TxLen_u16 = 3u;
    Serv_resptype_En = SERV_RESP_NEG_E;
  }	*/

 else if((FALSE == Source_DID_Check(UDS_Serv_pSt->RxBuff_pu8, numbytes_u16) || (FALSE == DDID_Range_Check_Fn(ReqDDID_u16))
          || (DDID_count_u32 > TOTAL_DDID)))
 {
    UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
    UDS_Serv_pSt->TxBuff_pu8[1] = SID_DDID;
    UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;            /* NRC-0x31 */
    UDS_Serv_pSt->TxLen_u16 = 3u;
    Serv_resptype_En = UDS_SERV_RESP_NEG_E;
 }
 

 else
 {     
  switch(Sub_Function_u8)
  {  
    /* defineByIdentifier */
    case DEFINE_BY_IDENTIFIER:
         for(i=FOUR,j=0u; i<UDS_Serv_pSt->RxLen_u16; i+=4,j++)
         {
           /* Loop for Storing all SubDID's requested by the Client */
           Req_SDID_List[j] = (UDS_Serv_pSt->RxBuff_pu8[i]<<8) | (UDS_Serv_pSt->RxBuff_pu8[i+1]);   
           Total_SDID_Req_u32++;
         }//TODO seven
         for(i=0,j=SIX; i<Total_SDID_Req_u32; i++,j+=4)
         {
           /* Loop for Storing 'memorySize' parameters respectively when requested by the Client */
           Memory_Size_au8[i] = UDS_Serv_pSt->RxBuff_pu8[j+1];
         }
         for(DDID_Table_Check_u32=0; DDID_Table_Check_u32<DDID_count_u32; DDID_Table_Check_u32++)
         {
           if((ReqDDID_u16 == DDID_ast[DDID_Table_Check_u32].DDID_Num_u16))
           {
             DDID_ast[DDID_Table_Check_u32].DDID_DID_Count_u16 = Total_SDID_Req_u32;
             for(i=0; i<Total_SDID_Req_u32; i++)
             {
               DDID_ast[DDID_Table_Check_u32].Req_DID_Table[i] = Req_SDID_List[i];
               DDID_ast[DDID_Table_Check_u32].Req_DIDSize[i] =   Memory_Size_au8[i];
             }
             break;
           }
         }
         if(DDID_Table_Check_u32 == DDID_count_u32)
         {      
            DDID_ast[DDID_count_u32].DDID_Num_u16 = ReqDDID_u16;
            DDID_ast[DDID_count_u32].DDID_DID_Count_u16 = Total_SDID_Req_u32;
            for(i=0; i<Total_SDID_Req_u32; i++)
            {
              DDID_ast[DDID_count_u32].Req_DID_Table[i] = Req_SDID_List[i];
              DDID_ast[DDID_count_u32].Req_DIDSize[i] = Memory_Size_au8[i];
            }
            DDID_count_u32++;
         }
         break;
       
       
           
    /* defineByMemoryAddress */                      
    case DEFINE_BY_MEMORY_ADDRESS:
            Sfns_b = TRUE;
            break;
                       
       
            
   /* clearDynamicallyDefinedDataIdentifier */                
   case CLEAR_DDID:
         if(TRUE == DDID_Range_Check_Fn(ReqDDID_u16))
         {
          for(i=0; i<DDID_count_u32; i++)
          {
            if(ReqDDID_u16 == DDID_ast[i].DDID_Num_u16)     /* Compare the requested DDID with the DDID's available in structure */
            {
              Request_OutOf_Range_b = FALSE;
              if(i == DDID_count_u32){
                
                for(j=0; j<DDID_ast[i].DDID_DID_Count_u16; j++)
                {
                  DDID_ast[i].Req_DID_Table[j] = 0;
                  DDID_ast[i].Req_DIDSize[j] = 0;
                } 
                DDID_ast[i].DDID_Num_u16 = 0;
                DDID_ast[i].DDID_DID_Count_u16 = 0;
              }
              else{
              for(;i < DDID_count_u32-1 ; i++)
              {
                DDID_ast[i].DDID_Num_u16 = DDID_ast[i+1].DDID_Num_u16;
                DDID_ast[i].DDID_DID_Count_u16 = DDID_ast[i+1].DDID_DID_Count_u16;
                for(j=0; j<DDID_ast[i].DDID_DID_Count_u16; j++)
                {
                  DDID_ast[i].Req_DID_Table[j] = DDID_ast[i+1].Req_DID_Table[j];
                  DDID_ast[i].Req_DIDSize[j] = DDID_ast[i+1].Req_DIDSize[j];
                }
              }
              }
              DDID_count_u32--;
              break;
            }
            else
            {
              Request_OutOf_Range_b = TRUE;
            }
          }
         }
         else
         {
           Request_OutOf_Range_b = TRUE;
         }
         break;
              
         
   /* SubFunction Not Supported */              
   default :
       Supp_Pos_Resp_b = FALSE;
       Sfns_b = TRUE;
      break;        
  }
    
      if(TRUE == Sfns_b)
      {
        Supp_Pos_Resp_b = FALSE;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_DDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED; /*NRC- 0x12*/
        UDS_Serv_pSt->TxLen_u16 = 3u;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
      }
      
      else if(TRUE == Request_OutOf_Range_b)
      {
        Supp_Pos_Resp_b = FALSE;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_DDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;            /* NRC-0x31 */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
      }
         
      else if(Supp_Pos_Resp_b == TRUE)
      {
        Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
      }
        
      else
      {
        UDS_Serv_pSt->TxLen_u16 = 3u;                            /*Total Number of Response Bytes*/
        UDS_Serv_pSt->TxBuff_pu8[1] = UDS_Serv_pSt->RxBuff_pu8[1]; 
        UDS_Serv_pSt->TxBuff_pu8[2] = (ReqDDID_u16 & 0xFF00)>>8;
        UDS_Serv_pSt->TxBuff_pu8[3] =  ReqDDID_u16 & 0x00FF;
//        Serv2C_Index_u32=0u;
        Serv_resptype_En = UDS_SERV_RESP_POS_E;
      }
    }
    return Serv_resptype_En;
}
    

/**
*  FUNCTION NAME : UDS_Serv2C_Timeout
*  FILENAME      : uds_serv2C.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv2C_Timeout(void){

	return;

}    
        
       
/* *****************************************************************************
 End of File
 */
