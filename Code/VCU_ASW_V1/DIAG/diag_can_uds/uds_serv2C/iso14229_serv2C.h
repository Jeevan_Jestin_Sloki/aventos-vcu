/******************************************************************************
 *    FILENAME    : uds_serv2C.h
 *    DESCRIPTION : Header file for UDS service - Dynamically Defined  By Data Identifier(0x2C).
 ******************************************************************************
 * Revision history
 *  
 * Ver Author          Date                     Description
 * 1   Sloki     08/02/2019		   Initial version
 ******************************************************************************
*/  

#ifndef _UDS_SERV2C_H_
#define	_UDS_SERV2C_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "uds_conf.h"
#include "iso14229_serv27.h"
#include "uds_DID.h"
    
/*
 **************************************************************************************************
 *    Defines
 **************************************************************************************************
 ************************************************************************** */

#define MAX_SDID_REQ                     20u
#define MAX_SDID_MEM_ADR                 0x20u
#define SERV2C_SEESION_CHECK_BIT_POS     3u
#define ZERO                             0u
#define SERV2C_MIN_LEN                   4u
#define DEFINE_BY_IDENTIFIER             0x01u
#define DEFINE_BY_MEMORY_ADDRESS         0x02u
#define CLEAR_DDID                       0x03u
#define FOUR                             4u
#define SIX                              6u
#define MAX_REQ_SOURCE_DIDS     

/*
 **************************************************************************************************
 *    Export Variables
 **************************************************************************************************
 */
extern uint32_t DDID_count_u32;
// uint32_t DDID_Index_u32 ;
//    uint32_t Req_Mem_Adr[MAX_SDID_MEM_ADR];
// uint8_t *Mem_Ptr_pu32;
//    uint32_t Count_u32;
//    uint32_t Serv2C_Index_u32;
   
//     uint32_t Memory_Size_u32;                                      /* Variable used to hold 'LengthFormatIdentifier' requested by the Client */
                                       
//     uint32_t Mem_Adr_Count_u32;                                  /* Variable used for Count the Memory Addresses requested by the Client */
//     uint32_t Size_Cmp_u32;                                       /* Variable used for check the SubDID Buffer size with the Nested DID Size */

//     uint16_t Req_SDID_List[MAX_SDID_REQ];                       /* Array for DID storage from Request*/
//     bool DID_Found_b;                                         /* Variable used as a Flag for DID's Compare */
//     bool RTRN_b;                                              /* Holds the return value from the Corresponding Function Pointer*/
//     bool Mem_Valid_b;                                         /* Variable used Flag when atleat one Client memory request is valid */
//     bool DDID_Found_b;
//     bool Addrs_Len_b;
//     uint8_t Position_Start_au8[MAX_SDID_REQ];                             /* Buffer used for hold the 'positionInSourceDataRecord' parameters when requested by the Client */
//     uint8_t Memory_Size_au8[MAX_SDID_REQ];                                /* Buffer used for hold the 'memorySize' parameters when requested by the Client */
//     uint32_t Total_SDID_Req_u32;
//    uint32_t Total_Req_Source_DID_Buff_Size_u32;
/**
*  FUNCTION NAME : iso14229_serv2C
*  FILENAME      : iso14229_serv2C.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributor table.
*  @brief        : This function will process the service_22 requests
*  @return       : Type of response.                     
*/   
extern UDS_Serv_resptype_En_t iso14229_serv2C(UDS_Serv_St_t* UDS_Serv_pSt);
 
 /**
*  FUNCTION NAME : UDS_Serv2C_Timeout
*  FILENAME      : uds_serv2C.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv2C_Timeout(void);
 
#ifdef	__cplusplus
}
#endif
 
 
#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
