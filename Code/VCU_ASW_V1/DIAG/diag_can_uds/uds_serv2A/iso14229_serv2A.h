/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Sloki Auto Technologies

  @File Name
    uds_serv2A.h

  @Summary
    This File contains macros, typedefs,  definitions and 
    user defined datatypes required by iso14229_serv2A.c

  @Author
    Sloki C Mathew
**/
/* ************************************************************************** */

#ifndef UDS_SERV2A_H_    /* Guard against multiple inclusion */
#define UDS_SERV2A_H_


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv27.h"
#include "uds_conf.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
 "C" {
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* @Summary: length size */
#define SERV_2A_MIN_LEN                                       0x02u
#define SERV_2A_MAX_LEN                                       0x06u

/* @Summary: Read / Write Bit Position */
#define SERV_2A_BIT_POS                                       0x00u


/* @Summary: Periodic Scheduler Poll Rate */
#define PS_POLL_RATE                                          25u

 
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Data Types                                                        */
/* ************************************************************************** */
/* ************************************************************************** */

/* @Summary: Storing Data for different PDIDs */
 //extern uint8_t PDID_F2A1_Comm_au8[];
 //extern uint8_t PDID_F2A2_Comm_au8[];
 //extern uint8_t PDID_F2A3_Comm_au8[];

/* @Summary: the periodic rate for each periodic data response message */
 extern uint32_t	 PeriodicRate_u32;

/* @summary : this variable is for index */
 extern uint32_t  Index_Val_u32;


/* @Summary: PDIDs which is going to be supported in this service */
typedef enum
{
    PDID_F2A1_E = 0x00u,  
    PDID_F2A2_E,
    PDID_F2A3_E,
    TOTAL_PDID_E,
}PDID_List_En_t ;

/* @Summary: transmissionMode Parameter */
typedef enum
{
    SEND_AT_SLOW_RATE_E = 0x01u,  
    SEND_AT_MEDIUM_RATE_E,
    SEND_AT_FAST_RATE_E,
    STOP_SENDING_E,
}TransmissionMode_En_t ;


/* @Summary: PDID Config Structure */
typedef struct
{
    PDID_List_En_t          PDID_List_En; /* PDID Name */
    uint32_t                PDID_u16;  /* PDID */
    uint8_t                 *PDID_Comm_pu8; /* PDID Data */

    uint8_t                 PDID_size; /* PDID Size */
    uint8_t                 Permission_u8[TOTAL_SESS_E]; /* PDID Read / Write Session Check */
    uint32_t	            PeriodicRate_u32; /* the periodic rate for each PDID */
    bool                    PDID_Status_b; /* the periodic status for each PDID */
}PDID_Config_St_t;
extern PDID_Config_St_t PDID_Config_aSt[TOTAL_PDID_E];
extern uint8_t PDID_Counter_u8;
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/**
*  FUNCTION NAME : PDID_Session_Check
*  FILENAME      : uds_serv2A.c
*  @param1       : uint16_t PDID_u16 - Indicates the PDID.
*  @param2       : uint8_t bitPosition_u8 - Indicates the bit position.
*  @brief        : function to Session_Check is valid or not
*  @return       : none.                     
**/
//extern bool PDID_Session_Check(uint16_t PDID_u16, uint8_t bitPosition_u8);


/** 
*  FUNCTION NAME : iso14229_serv2A
*  FILENAME      : iso14229_serv2A.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer
*                  table.
*  @brief        : This function will process the service_2a requests
*  @return       : Type of response.
**/
extern UDS_Serv_resptype_En_t iso14229_serv2A(UDS_Serv_St_t*  UDS_Serv_pSt);

/**
*  FUNCTION NAME : Periodic_Data_Transmit_Fn
*  FILENAME      : uds_serv2A.c
*  @param        : None.
*  @brief        : This function will process the different PDIDs
*  @return       : None.
**/
extern void Periodic_Data_Transmit_proc();
// void PDID_F2A3_timeout();

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _ISO14229_SERV2A_H */

/* *****************************************************************************
 End of File
*/
