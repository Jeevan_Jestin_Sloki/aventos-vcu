/******************************************************************************
 *    FILENAME    : uds_serv10.c
 *    DESCRIPTION : Header file for UDS service - Diagnostic Session Control.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki        28/08/2018		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv10.h"
#include "iso14229_serv2E.h"
#include "uds_DID.h"
#include "comm_cntrl_adapt.h"
#include "iso14229_serv31.h"
//#include "eeprom.h"
#include "fee_conf.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  @summary :- timer variable */
uint32_t Session_capture_time_u32 = 0;


/* Address of  the Flash from where the application starts executing */
/* Rule: Set BOOTLOADER_FLASH_BASE_ADDRESS to _RESET_ADDR value of application linker script*/
#define APP_START_ADDRESS			0x0000E000 /*This is application  memory address*/
#define APP_SIZE					0x0000B000
#define APP_END_ADDRESS     		(APP_START_ADDRESS + (APP_SIZE - 1))
#define BOOT_START_ADDRESS			0x00000000 /*This application boot memory*/
#define CRC_RESET_VALUE             0xFFFFFFFF
//#define DRIVE_VALIDATION_START_ADDRESS    APP_END_ADDRESS
#define DRIVE_VALIDATION_START_ADDRESS    (APP_START_ADDRESS - 0x1000)
//#define DRIVE_VALIDATION_END_ADDRESS      (DRIVE_CRC_END_ADDRESS)
#define DRIVE_VALIDATION_END_ADDRESS      (APP_START_ADDRESS - 0x01)


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

static void UDS_Default_Diag_Init();
static void UDS_Programming_Session_Init();
static void UDS_Extended_Diag_Init();
static void UDS_Engineering_Diag_Init();
//static void UDS_Safety_System_Diag_Init();

/**
*  @Function: void UDS_Default_Diag_Init()
*  @Summary: This Function is used to initialize default diagnostic session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
static void UDS_Default_Diag_Init()
{
    current_active_session_u8 = DEFAULT_SESSION_SUB_ID_E;
    //TODO: other init operations need to be done here reset the security flag (graphine req) 
	NWMngmnt_NormalCommMsg_Adapt(TRUE, TRUE);		/* Rx Enable, Tx Enable */
    UDS_serv27_reset();
	return;
}

/**
*  @Function: void UDS_Programming_Session_Init()
*  @Summary: This Function is used to initialize Programming session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
static void UDS_Programming_Session_Init()
{
    current_active_session_u8 = PROGRAMMING_SESSION_SUB_ID_E;
    //TODO: other init operations need to be done here
	NWMngmnt_NormalCommMsg_Adapt(TRUE, TRUE);		/* Rx Enable, Tx Enable */
    UDS_serv27_reset();
	return;
}

/**
*  @Function: void UDS_Extended_Diag_Init()
*  @Summary: This Function is used to initialize extended diagnostic session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
static void UDS_Extended_Diag_Init()
{
    current_active_session_u8 = EXTENDED_DIAG_SESSION_SUB_ID_E;
    //TODO: other init operations need to be done here
    UDS_serv27_reset();
	ClearRoutineResult();
	return;
}

/**
*  @Function: void UDS_Safety_System_Diag_Init()
*  @Summary: This Function is used to initialize safety system diagnostic session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
static void  UDS_Engineering_Diag_Init()
{
  current_active_session_u8 = ENGINEERING_DIAG_SESSION_SUB_ID_E;
  //TODO: other init operations need to be done here
  return ;
}


/**
*  @Function: void UDS_Safety_System_Diag_Init()
*  @Summary: This Function is used to initialize safety system diagnostic session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
static void  UDS_EOL_Init()
{
  current_active_session_u8 = SUPPLIER_EOL_SESSION_SUB_ID_E;
  NWMngmnt_NormalCommMsg_Adapt(TRUE, TRUE);		/* Rx Enable, Tx Enable */
  u1_Start_EOL();
  //TODO: other init operations need to be done here
  return ;
}




/**
*  @Function: void UDS_Safety_System_Diag_Init()
*  @Summary: This Function is used to initialize safety system diagnostic session, all flags
            are reset and all variables are re-initialized
*  @Params: None
*  @Return: None
**/
//void UDS_Safety_System_Diag_Init()
//{
//    current_active_session_u8 = SAFETY_SYSTEM_DIAG_SESSION_SUB_ID_E;
//    //TODO: other init operations need to be done here
//    return;
//}


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/**
*  FUNCTION NAME : iso14229_serv10
*  FILENAME      : uds_serv10.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_10 requests
*  @return       : Type of response.                  
*/
UDS_Serv_resptype_En_t iso14229_serv10(UDS_Serv_St_t*  UDS_Serv_pSt)
{
    
    uint8_t DiagSessionType_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
//    uint8_t SuppressResponse_u8 = UDS_Serv_pSt->RxBuff_pu8[1] & 0x80;
    bool NoResponseFlag_b = FALSE;
    UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
    
    if((UDS_Serv_pSt->RxBuff_pu8[1] == 0x81) || (UDS_Serv_pSt->RxBuff_pu8[1] == 0x82) || (UDS_Serv_pSt->RxBuff_pu8[1] == 0x83))
    {
        NoResponseFlag_b 	= TRUE;
		DiagSessionType_u8 	= (UDS_Serv_pSt->RxBuff_pu8[1] & 0x7F);  
		
    }
	if(SERV_10_MAX_LEN != UDS_Serv_pSt->RxLen_u16)
    {
        /* check whether length should be valid or not */
        UDS_Serv_pSt->TxLen_u16 	= 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_SESSIONCONTROL;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        switch(DiagSessionType_u8)
        { 
            case DEFAULT_SESSION_SUB_ID_E: // default session
            {
                UDS_Default_Diag_Init();
                UDS_Serv_pSt->TxLen_u16  	= 5u;
                UDS_Serv_pSt->TxBuff_pu8[1] = DiagSessionType_u8;
                UDS_Serv_pSt->TxBuff_pu8[2] = (uint8_t)DEFAULT_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[3] = (uint8_t)DEFAULT_P2_CAN_MAX;
                UDS_Serv_pSt->TxBuff_pu8[4] = (uint8_t)ENHANCED_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[5] = (uint8_t)ENHANCED_P2_CAN_MAX;
				Serv_resptype_En = UDS_SERV_RESP_POS_E;
                break;
            }
            case PROGRAMMING_SESSION_SUB_ID_E: // programming session 
            {
				if(UDS_GetCurrentSession() == EXTENDED_DIAG_SESSION_SUB_ID_E) 
				{
					//uint8_t data[10] = {0};
					UDS_Programming_Session_Init();
					if(NoResponseFlag_b == FALSE)
					{
		                UDS_Serv_pSt->TxLen_u16  	= 5u;
		                UDS_Serv_pSt->TxBuff_pu8[1] = DiagSessionType_u8;
		                UDS_Serv_pSt->TxBuff_pu8[2] = (uint8_t)DEFAULT_P2_CAN_MAX_HB;
		                UDS_Serv_pSt->TxBuff_pu8[3] = (uint8_t)DEFAULT_P2_CAN_MAX;
		                UDS_Serv_pSt->TxBuff_pu8[4] = (uint8_t)ENHANCED_P2_CAN_MAX_HB;
		                UDS_Serv_pSt->TxBuff_pu8[5] = (uint8_t)ENHANCED_P2_CAN_MAX;
		                Serv_resptype_En = UDS_SERV_RESP_POS_E;
#if(TRUE == DIAG_BOOT_LOADER_SUPPORTED)
		                Flash_Pattern_Write(DRIVE_VALIDATION_START_ADDRESS, BOOT_PATTERN);
#endif
//		                status_t status;
//		    		    INT_SYS_DisableIRQGlobal();
//		    		    uint8_t data_buff_au8[8] = {0x55,0xAA,0x55,0xAA,0x55,0xAA,0x55,0xAA};
//		    		    status = FLASH_DRV_EraseSector(&flashSSDConfig, DRIVE_VALIDATION_START_ADDRESS, 0x1000);
//		    			DEV_ASSERT(STATUS_SUCCESS == status);
//
//		    			status = FLASH_DRV_Program(&flashSSDConfig, DRIVE_VALIDATION_START_ADDRESS, 8, data_buff_au8);
//		    			DEV_ASSERT(STATUS_SUCCESS == status);
//		    		    INT_SYS_EnableIRQGlobal();
//		        		OSIF_TimeDelay(10);


//		        		while ((FTFC-> FSTAT & FTFC_FSTAT_CCIF_MASK) == 0){};
//		        		SystemSoftwareReset();
						/* Program session entery reponse pending data                   
							Acknowledgement for this will be transmitted from Primary
							Bootloader                                                   */
						//Can_u1_Transmit(2, 0, 0x708,0,(U1*)&data[0], 8);
//						Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
					}
	    

					/* After calling this function microcontroller will get reset and 
						enters into bootloader                                      */
					//todo  harsh EnterBootMode();
				}
				else{
					UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
					UDS_Serv_pSt->TxBuff_pu8[1] = SID_SESSIONCONTROL;
					UDS_Serv_pSt->TxBuff_pu8[2] = SERVNOSUPP_IN_ACTIVE_SESS ;
					UDS_Serv_pSt->TxLen_u16     = 3;
					Serv_resptype_En = UDS_SERV_RESP_NEG_E;
				}
                break;
            }
            case EXTENDED_DIAG_SESSION_SUB_ID_E: // extended session 
            {
                UDS_Extended_Diag_Init();
                UDS_Serv_pSt->TxLen_u16  	= 5u;
                UDS_Serv_pSt->TxBuff_pu8[1] = DiagSessionType_u8;
                UDS_Serv_pSt->TxBuff_pu8[2] = (uint8_t)DEFAULT_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[3] = (uint8_t)DEFAULT_P2_CAN_MAX;
                UDS_Serv_pSt->TxBuff_pu8[4] = (uint8_t)ENHANCED_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[5] = (uint8_t)ENHANCED_P2_CAN_MAX;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                break;                
            }
            case ENGINEERING_DIAG_SESSION_SUB_ID_E: // ENGINEERING DIAG session 
            {
                UDS_Engineering_Diag_Init();
                UDS_Serv_pSt->TxLen_u16  = 1u;
                UDS_Serv_pSt->TxBuff_pu8[1] = DiagSessionType_u8;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                break;                
            }
			case SUPPLIER_EOL_SESSION_SUB_ID_E: // EOL DIAG session 
            {
                UDS_EOL_Init();
                UDS_Serv_pSt->TxLen_u16  	= 5u;
                UDS_Serv_pSt->TxBuff_pu8[1] = DiagSessionType_u8;
                UDS_Serv_pSt->TxBuff_pu8[2] = (uint8_t)DEFAULT_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[3] = (uint8_t)DEFAULT_P2_CAN_MAX;
                UDS_Serv_pSt->TxBuff_pu8[4] = (uint8_t)ENHANCED_P2_CAN_MAX_HB;
                UDS_Serv_pSt->TxBuff_pu8[5] = (uint8_t)ENHANCED_P2_CAN_MAX;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                break;                
            }
            default: // sun function not supported 
            {
                UDS_Serv_pSt->TxLen_u16 = 3u;
                UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
                UDS_Serv_pSt->TxBuff_pu8[1] = SID_SESSIONCONTROL;
                UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
                Serv_resptype_En = UDS_SERV_RESP_NEG_E;
               // NoResponseFlag_b = FALSE;
                break;
            }            
        }
        if(NoResponseFlag_b == TRUE)
        {
            Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
        }
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : UDS_Serv10_Timeout
*  FILENAME      : uds_serv10.c
*  @param        : none
*  @brief        : This function will call default session because of timeout
*  @return       : Type of response.                  
*/

void UDS_Serv10_Timeout(void)
{    
    return;
}

uint8_t UDS_GetCurrentSession(void)
{
	return (uint8_t)current_active_session_u8;
}
/******************************************************************************
 End of File
 ******************************************************************************/
